#!/bin/bash 

build_mode="Release"
#// use debug mode
#build_mode="Debug"
print_stacktrace="FALSE"

#// use boost::stacktrace to print stacktrace on errors
#print_stacktrace="TRUE"
#// set compiler
export CXX=/usr/bin/g++-9
#// remember current path
current_path=${pwd}

#// generate build path
if [[ "$(hostname)" -eq "heacBoehm" ]]; then
  server_path="hb1"
else
  server_path="arm"
fi

build_mode_path=""
if [[ "$build_mode" == "Release" ]]; then
  build_mode_path="release/bin"
fi
if [[ "$build_mode" == "Debug" ]]; then
  build_mode_path="debug/bin"
fi

build_path=build/${server_path}/${build_mode_path}
mkdir -p ${build_path}
cd ${build_path}

echo "Build in ${build_path}"
#// run cmake
CMAKE_OPTIONS="-D BUILD_ALL=TRUE -D NO_SELF_MANAGING=TRUE -D ENABLE_MONITORING=TRUE -D CAVX512=TRUE -D CAVXTWO=TRUE -D QMMIB=TRUE"
cmake -D CMAKE_BUILD_TYPE=${build_mode} -D INCLUDE_BOOST_STACKTRACE=${print_stacktrace} -D CMAKE_CXX_COMPILER=g++-9 ${CMAKE_OPTIONS} ../../../..

#// build
echo -e "\n\n>>> Build on $(hostname) <<<"
make -j 16

#// go back to previous directory
cd ${current_path}
