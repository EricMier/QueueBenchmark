

class Config:
    elementCases = []
    groupCountCases = []
    partitionCases = []
    partitionSizeCases = []
    repeatCount = 10
    elementByteSize = 8

    def __init__(self):
        KB = 1024
        MB = 1024 * KB
        GB = 1024 * MB
        sizeOfInt = 8

        self.elementCases.append(      2 * MB / sizeOfInt)  # < L1 Cache (2 MiB HB)
        self.elementCases.append(2 *   2 * MB / sizeOfInt)  # < 2 * L1 Cache (4 MiB HB)
        self.elementCases.append(      8 * MB / sizeOfInt)  # < L1 Cache (8 MiB Arm)
        self.elementCases.append(2 *   8 * MB / sizeOfInt)  # < 2 * L1 Cache (16 MiB Arm)
        self.elementCases.append(     64 * MB / sizeOfInt)  # < L2 Cache (64 MiB HB & Arm)
        self.elementCases.append(2 *  64 * MB / sizeOfInt)  # < 2 * L2 Cache (128 MiB HB & Arm)
        self.elementCases.append(     88 * MB / sizeOfInt)  # < L3 Cache (88 MiB HB)
        self.elementCases.append(2 *  88 * MB / sizeOfInt)  # < 2 * L3 Cache (172 MiB HB)
        self.elementCases.append(    256 * MB / sizeOfInt)  # < L3 Cache (256 MiB Arm)
        self.elementCases.append(2 * 256 * MB / sizeOfInt)  # < 2 * L3 Cache (512 MiB Arm)
        self.elementCases.append(4 *  88 * MB / sizeOfInt)  # 4 x L3 (4 x 88 MiB = 352 MiB HB)
        self.elementCases.append(8 *  88 * MB / sizeOfInt)  # 2 * 4 x L3 (2 * 4 x 88 MiB = 704 MiB HB)

        self.elementCases.append(     4 * 256 * MB / sizeOfInt)  # 4 x L3 (4 x 256 MiB = 1024 MiB Arm)
        self.elementCases.append( 2 * 4 * 256 * MB / sizeOfInt)  # 2 * 4 x L3 (2 * 4 x 256 MiB = 2048 MiB Arm)
        self.elementCases.append( 3 * 4 * 256 * MB / sizeOfInt)  # 3 GiB
        self.elementCases.append( 4 * 4 * 256 * MB / sizeOfInt)  # 4 GiB
        self.elementCases.append( 6 * 4 * 256 * MB / sizeOfInt)  # 6 GiB
        self.elementCases.append( 8 * 4 * 256 * MB / sizeOfInt)  # 8 GiB
        self.elementCases.append(12 * 4 * 256 * MB / sizeOfInt)  # 12 GiB
        self.elementCases.append(16 * 4 * 256 * MB / sizeOfInt)  # 16 GiB
        self.elementCases.sort()

        # self.groupCountCases = [8, 128, 1024, 2096, 4096, 8192]
        self.groupCountCases = [4096]
        # partitionCases = [120, 240, 360, 480, 600, 720, 840, 960, 1080]
        self.partitionCases = [120, 600, 1200, 2400, 4800]
        # self.partitionCases = [4800]
        self.repeatCount = 10

        self.partitionSizeCases.append( 64 * KB)  # < L1 Cache of HB
        self.partitionSizeCases.append(128 * KB)  # < L1 Cache of HB
        self.partitionSizeCases.append(256 * KB)  # < L1 Cache of HB
        self.partitionSizeCases.append(512 * KB)  # < L1 Cache of HB
        self.partitionSizeCases.append(  1 * MB)  # < L1 Cache of HB
        self.partitionSizeCases.append(  2 * MB)  # L1 Cache of HB
        self.partitionSizeCases.append(  4 * MB)  # L1 Cache of HB
        self.partitionSizeCases.append(  5 * MB)  # L1 Cache of HB
        self.partitionSizeCases.append(  6 * MB)  # L1 Cache of HB
        self.partitionSizeCases.append(  8 * MB)  # L1 Cache of Arm
        # self.partitionSizeCases.append(32 * MB)  # < L2 Cache of HB & Arm
        # self.partitionSizeCases.append(64 * MB)  # L2 Cache of HB & Arm
        # self.partitionSizeCases.append(88 * MB)  # L3 Cache of HB
        # self.partitionSizeCases.append(88 * 2 * MB)  # > L3 Cache of HB
        # self.partitionSizeCases.append(256 * MB)  # L3 Cache of Arm
        # self.partitionSizeCases.append(512 * MB)  # > L3 Cache of Arm

