import socket
import os
from datetime import datetime
import subprocess
import sys
from config import Config

# // read information from underlying system
host = socket.gethostname()
cpuConfig = open("cpuConfig.tmp", "w")
subprocess.run(["cpufreq-info", "-p"], stdout=cpuConfig)
cpuConfig.close()

benchmark_version = 0
if os.path.exists("benchmark_version"):
    benchmark_version_f = open("benchmark_version", "r")
    benchmark_version = int(benchmark_version_f.read())
    benchmark_version_f.close()
benchmark_version_f = open("benchmark_version", "w")
benchmark_version_f.write(str(benchmark_version+1))
benchmark_version_f.close()


# // generate required paths
root = ".."
executable_name = "AggregationBenchmark"
# executable_name = "FilterBenchmark"
benchmark_name = "benchmark_" + str(benchmark_version)
print("This is MorphStore Prototype " + benchmark_name + " / " + executable_name + " on " + host)

host_dir = ""
if host == "haecBoehm":
    host_dir = "hb1"
else:
    host_dir = "arm"

build_dir = root + "/build/" + host_dir + "/release/bin"
executable = build_dir + "/" + executable_name

# // build project
retVal = os.system("cd .. && bash build.sh")
if retVal != 0:
    print("Could not build project. Abort.")
    exit(1)

print("\n")

if os.path.exists(executable):
    print("Executable " + executable + " found")
else:
    print("Executable (" + executable + ") not found. Abort.")
    exit(1)

# // write additional notes to this benchmark
notes = ""


config: Config = Config()

dirName = "output/" + host_dir + "/" + executable_name + "/" + benchmark_name
if not os.path.exists(dirName):
    os.makedirs(dirName)

# // write info file
out = open(dirName + "/benchmark_info.txt", "w")
error = open(dirName + "/errors.txt", "w")
out.write("Benchmark name:\n" + benchmark_name + "\n")


today = datetime.now()
out.write("Date:\n" + today.strftime("%d.%m.%Y %H:%M:%S") + "\n")
out.write("Host:\n" + host + "\n")
out.write("Binary:\n" + executable + "\n")
out.write("Element count:\n" + ''.join(str(e) + "; " for e in config.elementCases) + "\n")
out.write("Partition count:\n" + ''.join(str(e) + "; " for e in config.partitionCases) + "\n")
out.write("Partition sizes:\n" + ''.join(str(e) + "; " for e in config.partitionSizeCases) + "\n")
out.write("Group count:\n" + ''.join(str(e) + "; " for e in config.groupCountCases) + "\n")
out.write("Repeat each run n times:\n" + str(config.repeatCount) + "\n")

cpuConfig = open("cpuConfig.tmp", "r")
out.write("CpuFreqConfig:\n" + cpuConfig.readline().replace("\n", "") + "\n")
cpuConfig.close()

out.write("Notes:\n" + notes + "\n")

out.close()


def readValue(file):
    f = open(file, "r")
    res = f.read()
    f.close()
    return res


def doBenchmark(outFile, elements, partitions, groups):
    cmd = executable + " -e " + str(elements) + " -f " + outFile + " -p " + str(partitions) + " -g " + str(groups)
    print(cmd)
    sys.stdout.flush()
    status = os.WEXITSTATUS(os.system(cmd))
    if status > 0:
        error.write("Error(" + str(status) + ") occcured for" + cmd + "\n")


def doBenchmark2(outFile, elements, partitionSize, groups):
    cmd = executable + " -e " + str(elements) + " -f " + outFile + " -s " + str(partitionSize) + " -g " + str(groups)
    print(cmd)
    sys.stdout.flush()
    status = os.WEXITSTATUS(os.system(cmd))
    if status > 0:
        error.write("Error(" + str(status) + ") occcured for" + cmd + "\n")


# ## do benchmark
run = 1



if executable_name == "AggregationBenchmark":
    maxRun = len(config.partitionCases) * len(config.groupCountCases) * len(config.elementCases) * config.repeatCount
    for partitions in config.partitionCases:
        for groups in config.groupCountCases:
            benchmark_results_out = dirName + "/" + str(groups) + "groups_" + str(partitions) + "partitions.csv"
            tmp_out_file = "output.txt"
            print(benchmark_results_out)
            sys.stdout.flush()
            benchmark_results_f = open(benchmark_results_out, "w")
            for elements in config.elementCases:
                benchmark_results_f.write(str(elements))
                for x in range(config.repeatCount):
                    print("This is Run " + str(run) + "/" + str(maxRun))
                    sys.stdout.flush()
                    run += 1
                    doBenchmark(tmp_out_file, elements, partitions, groups)
                    benchmark_results_f.write(";" + readValue(tmp_out_file))
                    benchmark_results_f.flush()
                benchmark_results_f.write("\n")
            benchmark_results_f.close()


# // for partitions with fixed size
if executable_name == "FilterBenchmark":
    maxRun = len(config.partitionSizeCases) * len(config.groupCountCases) * len(config.elementCases) * config.repeatCount
    for partitionSize in config.partitionSizeCases:
        for groups in config.groupCountCases:
            filePath = dirName + "/" + str(groups) + "groups_" + str(partitionSize) + "KiBpartitions.csv"
            print(filePath)
            sys.stdout.flush()
            outBench = open(filePath, "w")
            for elements in config.elementCases:
                outBench.write(str(elements))
                for x in range(config.repeatCount):
                    print("<<<This is Run " + str(run) + "/" + str(maxRun) + ">>>")
                    sys.stdout.flush()
                    run += 1
                    outBench.write(";")
                    outBench.close()
                    doBenchmark2(filePath, elements, partitionSize / config.elementByteSize, groups)
                    outBench = open(filePath, "a")
                outBench.write("\n")
            outBench.close()

error.close()

