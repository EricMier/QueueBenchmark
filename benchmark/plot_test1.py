import matplotlib.pyplot as plt
import numpy as np
from linePlot import LinePlot
from datetime import datetime
from shutil import copyfile
import os
import math
from config import Config





def addLine(file, label, marker=".", upperBound=1000000000, lowerBound=0):
    xValues = []
    yValues = []

    for line_ in file:
        values = line_.replace("\n", "").split(";")
        xValue = float(values[0]) * 8 / 1024 / 1024
        # print("values: ", values)
        if xValue < lowerBound:
            continue
        if xValue > upperBound:
            continue

        data = values[1:]
        #// truncate best and worst value
        data.sort()
        if len(data) < config.repeatCount:
            print("Warning: fewer data points as expected:")
            print(line_)
        else:
            # Skip best and worst data point
            data = data[1:-1]
        if len(data) == 0:
            continue
        avg = 0
        count = 0
        for d in data:
            if d == "":
                continue
            avg += float(d)
            count += 1
        if count == 0:
            continue
        avg = avg / count
        xValues.append(xValue)
        yValues.append(avg / 1000 / 1000)

    line.addLine(xValues, yValues, label=label, marker=marker, linewidth=1)
    # print("xValues", xValues)


def bytesToHumanReadable(value):
    if value < 1024:
        return str(int(value)) + "B"
    value /= 1024
    if value < 1024:
        return str(int(value)) + "KiB"
    value /= 1024
    if value < 1024:
        return str(int(value)) + "MiB"
    value /= 1024
    if value < 1024:
        return str(int(value)) + "GiB"
    value /= 1024
    if value < 1024:
        return str(int(value)) + "TiB"
    value /= 1024
    return str(int(value)) + "PiB"



config: Config = Config()

# bench_name = "benchmark12_scalar"
# bench_name = "benchmark12_2"
# bench_name = "benchmark7_2"
bench_name = "benchmark13"
# binary_name = "AggregationBenchmark"
binary_name = "FilterBenchmark"
# case_name = "120partitions"
# case_name = "240partitions"
# case_name = "360partitions"
# servers = ["arm", "hb1"]
# servers = ["arm"]
servers = ["hb1"]

lowerBound = 0
# lowerBound = 1024
today = datetime.now()

# if not os.path.exists("plot/" + bench_name):
#     os.mkdir("plot/" + bench_name)
if not os.path.exists("plot/" + binary_name + "/" + bench_name):
    os.makedirs("plot/" + binary_name + "/" + bench_name)

# print([f / 1024 / 1024 for f in [int(tick * config.elementByteSize) for tick in config.elementCases]])
# exit()

# for groups in config.groupCountCases:
#     line: LinePlot = LinePlot()
#     # line.setTitle("Comparision of runtime for increasing data size\n" + bench_name + "_" + case_name)
#     line.setXLabel("Data size in MiB")
#     line.setYLabel("Runtime in ms")
#     # line.setXTicks(xTicks)
#     line.setXTicks([int(tick * config.elementByteSize / 1024 / 1024) for tick in config.elementCases])
#
#     line.setTitle("Comparision of runtime for increasing data size\n" + bench_name + "_" + str(groups) + "_groups")
#     for server in servers:
#         for partitions in config.partitionCases:
#             filePath = "output/" + server + "/" + bench_name + "/" + str(groups) + "groups_" + str(partitions) + "partitions.csv"
#             data = open(filePath)
#             if server == "hb1":
#                 addLine(data, server + " " + str(partitions) + " partitions", marker="s")
#             else:
#                 addLine(data, server + " " + str(partitions) + " partitions", marker="^")
#
#     line.plot("plot/" + bench_name + "/" + str(groups) + "_groups_" + today.strftime("%d_%m_%Y__%H_%M_%S"))
#     line.show()


for groups in config.groupCountCases:
    line: LinePlot = LinePlot()
    # line.setTitle("Comparision of runtime for increasing data size\n" + bench_name + "_" + case_name)
    line.setXLabel("Data size in MiB")
    line.setYLabel("Runtime in ms")
    # line.setXTicks(xTicks)
    line.setXTicks([int(math.ceil(tick * config.elementByteSize / 1024 / 1024)) for tick in config.elementCases])

    line.setTitle("Comparision of runtime for increasing data size\n" + binary_name + "/" + bench_name + "_" + str(groups) + "_groups")
    for server in servers:
        for partitions in config.partitionCases:
            filePath = "output/" + server + "/" + binary_name + "/" + bench_name + "/" + str(groups) + "groups_" + str(partitions) + "partitions.csv"
            print("Process " + filePath)
            data = open(filePath)
            marker = "^"
            if server == "hb1":
                marker = "s"
            addLine(data, server + " " + str(partitions) + " partitions", marker=marker, lowerBound=lowerBound)
    line.rotateXLabels(45)
    line.plot("plot/" + binary_name + "/" + bench_name + "/" + str(groups) + "_groups_" + today.strftime("%d_%m_%Y__%H_%M_%S") + ".pdf")
    line.show()


# // for fixes partition size // #
# for groups in config.groupCountCases:
#     line: LinePlot = LinePlot()
#     # line.setTitle("Comparision of runtime for increasing data size\n" + bench_name + "_" + case_name)
#     line.setXLabel("Data size in MiB")
#     line.setYLabel("Runtime in ms")
#     # line.setXTicks(xTicks)
#     line.setXTicks([int(tick * config.elementByteSize / 1024 / 1024) for tick in config.elementCases])
#
#     line.setTitle("Comparision of runtime for increasing data size\n" + bench_name + "_" + str(groups) + "_groups")
#     for server in servers:
#         for partitions in config.partitionSizeCases:
#             filePath = "output/" + server + "/" + binary_name + "/" + bench_name + "/" + str(groups) + "groups_" + str(partitions) + "KiBpartitions.csv"
#             data = open(filePath)
#             marker = "^"
#             if server == "hb1":
#                 marker = "s"
#             addLine(data, server + " " + bytesToHumanReadable(partitions * 8) + " partitions", marker=marker, lowerBound=lowerBound)
#
#     line.rotateXLabels(45)
#     line.plot("plot/" + bench_name + "/" + str(groups) + "_groups_" + today.strftime("%d_%m_%Y__%H_%M_%S") + ".pdf")
#     line.show()






