import string

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from typing import NamedTuple, List

from matplotlib.ticker import MultipleLocator


class LinePlot:

    # scaleFactor=10
    scaleFactor=3
    defaultFigSize = (10 * scaleFactor, 7 * scaleFactor)
    # fig
    # ax
    title: string = ""
    xlabel: string = "x axis"
    ylabel: string = "y axis"
    legend_loc = "best"
    minx = 1000000000000.0
    maxx = 0.0

    def __init__(self):
        self.fig, self.ax = plt.subplots(ncols=1, figsize=self.defaultFigSize)
        self.ax.set_xlabel("x axis")
        self.ax.set_ylabel("y axis")

        # self.ax.grid(linestyle="--", linewidth=0.3*self.scaleFactor, color='.25', zorder=-10, which="both")
        self.ax.grid(linestyle="--", linewidth=0.5*self.scaleFactor, color='.25', zorder=-10, which="both")
        # self.ax.grid(True, color="#808080", linestyle="dotted")

        plt.xticks(fontsize=11*self.scaleFactor)
        plt.yticks(fontsize=11*self.scaleFactor)
        # plt.yscale("log")
        plt.xscale("log")
        self.ax.xaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
        self.ax.yaxis.set_major_formatter(matplotlib.ticker.ScalarFormatter())
        # self.ax.xaxis.set_major_locator(MultipleLocator(14*2))
        # self.ax.xaxis.set_minor_locator(MultipleLocator(14))
        # self.ax.legend(loc="best")

    def setXTicks(self, ticks):
        self.ax.xaxis.set_ticks(ticks)

    def setYTicks(self, ticks):
        plt.yticks(ticks, fontsize=11*self.scaleFactor)

    def setTitle(self, title: string):
        self.ax.set_title(title, fontsize=18.0*self.scaleFactor, fontdict={'verticalalignment': 'top'})

    def setXLabel(self, label: string):
        self.ax.set_xlabel(label, fontsize=16*self.scaleFactor)

    def setYLabel(self, label: string):
        self.ax.set_ylabel(label, fontsize=16*self.scaleFactor)

    def addLine(self, xValues: List[float], yValues: List[float], label: string = "no label", marker="^", linewidth=0, color=None):
        if(min(xValues) < self.minx):
            self.minx = min(xValues)
        if(max(xValues) > self.maxx):
            self.maxx = max(xValues)
        self.ax.plot(
            np.array(xValues),
            np.array(yValues),
            label=label,
            marker=marker,
            color=color,
            linewidth=linewidth*self.scaleFactor,
            markersize=6*self.scaleFactor
        )

    def addStack(self, xValues: List[float], *args, labels=None, colors=None):
        if(min(xValues) < self.minx):
            self.minx = min(xValues)
        if(max(xValues) > self.maxx):
            self.maxx = max(xValues)
        self.ax.stackplot(
            np.array(xValues),
            args,
            labels=labels,
            colors=colors
        )

    def fin(self):
        self.ax.legend(loc=self.legend_loc, fontsize=11*self.scaleFactor, markerscale=2)
        # print("min: " + str(self.minx) + " max: " + str(self.maxx))
        self.ax.set_xlim(left=max(self.minx * 0.8, 0), right=self.maxx * 1.1)
        # self.ax.set_ylim(bottom=0)
        self.fig.tight_layout()


    def plot(self, file: string):
        self.fin()
        plt.savefig(file)

    def show(self):
        self.fin()
        plt.show()

    def close(self):
        plt.close()

    def rotateXLabels(self, degree):
        plt.xticks(rotation=degree)

    def rotateYLabels(self, degree):
        plt.yticks(rotation=degree)


