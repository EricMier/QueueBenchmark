=== Generieren der Daten ===
1. Generator in externes Verzeichnis herunterladen: git clone https://github.com/lemire/StarSchemaBenchmark.git
2. Ins gelonte Verzeichnis wechseln: cd StarSchemaBenchmark
3. Das Projekt bauen: make
4. Tabelle Lineorder generieren: ./dbgen -s 1 -T l
   Parameter -s erhöhen, um generierte Daten zu vervielfachen
   Mehr Infos unter: https://github.com/lemire/StarSchemaBenchmark
5. lineorder.tbl nach <project>/resources/ssb verschieben
6. in <project>/preprocessing wechseln
7. TableEncoding.py ausführen, LineOrder wird Integer-kodiert
   Resultat befindet sich in <project>/resources/ssb/encoded/lineorder.csv, wo sie vom Prototypen gelsen wird
8. Done.



=== Bauen ===
Zum Bauen das build.sh Skript im Projektwurzelverzeichnis ausführen. Bei Bedarf Debug Mode und Stacktracing aktivieren.



=== Ausführen ===
Zum Ausführen des Benchmarks für 1 bis 112 Partitionen, das run.sh Skript verwenden.
In dem Skript wird ein Output Pfad gebaut, in jenem die Messwerte gespeichert werden.
Falls die existierenden Messwerte nicht überschrieben werden sollen, diesen Pfad ändern!



=== Misc ===
Im folgenden die Stellschrauben und wo sie zu finden sind:

Maximale Anzahl an Nodes:
MAX_NODES_USED -> <project>/src/util/defines.h:26
Default: 1000000

Anzahl an Workern pro Node:
WORKER_CNT_PER_NODE -> <project>/src/util/defines.h:17
Default: Anz Threads per Node - 1 (wegen Coordinator)

Anzahl Tupel, die in Lineorder Table gelesen werden:
lineorderPP.tableSize -> <project>/src/engine/PrototypeEngine:209

File aus welchem Lineorder Table gelesen wird:
reader. ... .fromFile("resources/ssb/encoded/lineorder.tbl") -> <project>/src/engine/PrototypeEngine:249
