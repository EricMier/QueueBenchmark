
uniqueIndex = int(0)
dictionary = {}
outseperator = ";"


def compareAndAdd(column, dictionary_l):
    global uniqueIndex, dictionary
    if column in dictionary_l:
        column = dictionary_l[column]
    else:
        dictionary_l[column] = uniqueIndex
        column = uniqueIndex
        uniqueIndex += 1
    return column


def processCutomer(source, output):
    global uniqueIndex, dictionary, outseperator
    customerDict = {
        "C_NAME": {},           # 1
        "C_ADDRESS": {},        # 2
        "C_CITY": {},           # 3
        "C_NATION": {},         # 4
        "C_REGION": {},         # 5
        "C_PHONE": {},          # 6
        "C_MKTSEGEMENT": {}     # 7
    }
    with open(source) as file, open(output, 'w') as out:
        line = file.readline()
        while line:
            columns = line.strip("\n").split("|")
            line = file.readline()

            columns[1] = compareAndAdd(columns[1], customerDict["C_NAME"])
            columns[2] = compareAndAdd(columns[2], customerDict["C_ADDRESS"])
            columns[3] = compareAndAdd(columns[3], customerDict["C_CITY"])
            columns[4] = compareAndAdd(columns[4], customerDict["C_NATION"])
            columns[5] = compareAndAdd(columns[5], customerDict["C_REGION"])
            columns[6] = compareAndAdd(columns[6], customerDict["C_PHONE"])
            columns[7] = compareAndAdd(columns[7], customerDict["C_MKTSEGEMENT"])

            for col in columns:
                out.write(str(col) + outseperator)
            out.write("\n")

    dictionary["CUSTOMER"] = customerDict


def processSupplier(source, output):
    global uniqueIndex, dictionary, outseperator
    customerDict = {
        "S_NAME": {},           # 1
        "S_ADDRESS": {},        # 2
        "S_CITY": {},           # 3
        "S_NATION": {},         # 4
        "S_REGION": {},         # 5
        "S_PHONE": {}           # 6
    }
    with open(source) as file, open(output, 'w') as out:
        line = file.readline()
        while line:
            columns = line.strip("\n").split("|")
            line = file.readline()

            columns[1] = compareAndAdd(columns[1], customerDict["S_NAME"])
            columns[2] = compareAndAdd(columns[2], customerDict["S_ADDRESS"])
            columns[3] = compareAndAdd(columns[3], customerDict["S_CITY"])
            columns[4] = compareAndAdd(columns[4], customerDict["S_NATION"])
            columns[5] = compareAndAdd(columns[5], customerDict["S_REGION"])
            columns[6] = compareAndAdd(columns[6], customerDict["S_PHONE"])

            for col in columns:
                out.write(str(col) + outseperator)
            out.write("\n")

    dictionary["CUSTOMER"] = customerDict


def processPart(source, output):
    global uniqueIndex, dictionary, outseperator
    customerDict = {
        "P_NAME": {},           # 1
        "P_MFGR": {},           # 2
        "P_CATEGORY": {},       # 3
        "P_BRAND1": {},         # 4
        "P_COLOR": {},          # 5
        "P_TYPE": {},           # 6
        "P_CONTAINER": {}       # 8
    }
    with open(source) as file, open(output, 'w') as out:
        line = file.readline()
        while line:
            columns = line.strip("\n").split("|")
            line = file.readline()

            columns[1] = compareAndAdd(columns[1], customerDict["P_NAME"])
            columns[2] = compareAndAdd(columns[2], customerDict["P_MFGR"])
            columns[3] = compareAndAdd(columns[3], customerDict["P_CATEGORY"])
            columns[4] = compareAndAdd(columns[4], customerDict["P_BRAND1"])
            columns[5] = compareAndAdd(columns[5], customerDict["P_COLOR"])
            columns[6] = compareAndAdd(columns[6], customerDict["P_TYPE"])
            columns[8] = compareAndAdd(columns[8], customerDict["P_CONTAINER"])

            for col in columns:
                out.write(str(col) + outseperator)
            out.write("\n")

    dictionary["CUSTOMER"] = customerDict



def processDate(source, output):
    global uniqueIndex, dictionary, outseperator
    customerDict = {
        "D_DATE": {},           # 1
        "D_DAYOFWEEK": {},      # 2
        "D_MONTH": {},          # 3
        "D_YEARMONTH": {},      # 6
        "D_SELLINGSEASON": {},  # 12
    }
    with open(source) as file, open(output, 'w') as out:
        line = file.readline()
        while line:
            columns = line.strip("\n").split("|")
            line = file.readline()

            columns[1]  = compareAndAdd(columns[1],  customerDict["D_DATE"])
            columns[2]  = compareAndAdd(columns[2],  customerDict["D_DAYOFWEEK"])
            columns[3]  = compareAndAdd(columns[3],  customerDict["D_MONTH"])
            columns[6]  = compareAndAdd(columns[6],  customerDict["D_YEARMONTH"])
            columns[12] = compareAndAdd(columns[12], customerDict["D_SELLINGSEASON"])

            for col in columns:
                out.write(str(col) + outseperator)
            out.write("\n")

    dictionary["CUSTOMER"] = customerDict


def processLineOrder(source, output):
    global uniqueIndex, dictionary, outseperator
    customerDict = {
        "LO_ORDERPRIORITY": {},           # 6
        "LO_SHIPPRIORITY": {},      # 7
        "LO_SHIPMODE": {},          # 16
    }
    with open(source) as file, open(output, 'w') as out:
        line = file.readline()
        while line:
            columns = line.strip("\n").split("|")
            line = file.readline()

            columns[6]  = compareAndAdd(columns[6],  customerDict["LO_ORDERPRIORITY"])
            columns[7]  = compareAndAdd(columns[7],  customerDict["LO_SHIPPRIORITY"])
            columns[16] = compareAndAdd(columns[16], customerDict["LO_SHIPMODE"])

            for col in columns:
                out.write(str(col) + outseperator)  
            out.write("\n")

    dictionary["CUSTOMER"] = customerDict


# processCutomer("../resources/ssb/customer.tbl", "../resources/ssb/encoded/customer.tbl")
# processSupplier("../resources/ssb/supplier.tbl", "../resources/ssb/encoded/supplier.tbl")
# processPart("../resources/ssb/part.tbl", "../resources/ssb/encoded/part.tbl")
# processDate("../resources/ssb/date.tbl", "../resources/ssb/encoded/date.tbl")
# processLineOrder("../resources/ssb/lineorder.tbl", "../resources/ssb/encoded/lineorder.tbl")
processLineOrder("../resources/ssb/lineorder_scale_10.tbl", "../resources/ssb/encoded/lineorder_scale_10.tbl")
