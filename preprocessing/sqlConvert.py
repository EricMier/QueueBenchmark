import mysql.connector


def processLineOrder(source, output):
    tupleCnt = 0
    part = 1
    with open(source) as file, open(output + ".part1.sql", 'w') as out:
        line = file.readline()
        out.write("INSERT INTO lineorder VALUES\n")
        lineSep = ""
        while line:
            columns = line.strip("\n").split(";")
            line = file.readline()

            out.write(lineSep + "(")
            lineSep = ","
            sep = ""
            for col in columns:
                out.write(sep + str(col))
                sep = ","
            out.write(")\n")
            tupleCnt += 1
            if(tupleCnt >= 600000):
                tupleCnt = 0
                part += 1
                out.write(";")
                out.close()
                out = open(output + ".part" + str(part) + ".sql", 'w')
                out.write("INSERT INTO lineorder VALUES\n")

        out.write(";")
        out.close()


def importLineOrder(source):
    mydb = mysql.connector.connect(
      host="localhost",
      user="root",
      passwd="",
      database="ssb"
    )

    mycursor = mydb.cursor()

    sql = "INSERT INTO lineorder2 (orderkey, partkey, suppkey, linenumber, quantity, extendedprice, discount, tax, returnflag, linestatus, shipdate, commitdate, receiptdate, shipinstruct, shipmode, comment, dump) VALUES\n"
    exe = sql
    # val = ("John", "Highway 21")
    tupleCnt = 0
    lineCnt = 0
    with open(source) as file:
        line = file.readline()
        sep = ""
        while line:
            columns = line.strip("\n").split(";")[:17]
            line = file.readline()
            tupleCnt += 1
            lineCnt += 1
            exe += sep + "("
            colSep = ""
            for col in columns:
                exe += colSep + col
                colSep = ", "
            exe += ")"
            sep =",\n"
            if tupleCnt >= 5000:
                # print(exe)
                mycursor.execute(exe)
                mydb.commit()
                exe = sql
                tupleCnt = 0
                sep = ""

        mycursor.execute(exe)
        mydb.commit()

        file.close()
    print("line count : " + str(lineCnt))
    # mydb.commit()

# processCutomer("../resources/ssb/customer.tbl", "../resources/ssb/encoded/customer.tbl")
# processSupplier("../resources/ssb/supplier.tbl", "../resources/ssb/encoded/supplier.tbl")
# processPart("../resources/ssb/part.tbl", "../resources/ssb/encoded/part.tbl")
# processDate("../resources/ssb/date.tbl", "../resources/ssb/encoded/date.tbl")
# processLineOrder("../resources/ssb/lineorder.tbl", "../resources/ssb/encoded/lineorder.tbl")


# processLineOrder("../resources/ssb/encoded/lineorder.csv", "../resources/ssb/encoded/lineorder")


importLineOrder("../resources/ssb/encoded/lineorder.csv")
