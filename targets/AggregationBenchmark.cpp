/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/
 
#include <logging>
#include <partitioning/TablePartitioningPolicy.h>
//#include <stdlibs>
//#include <interfaces>
//#include <../abbreviate/interfaces>
//#include <storage/Database.h>
#include <iostream>
#include <storage>
#include "memory/NumaAllocator.h"
#include <threading>
#include <util/NumaColumnGenerator.h>
#include <monitoring/Monitor.h>

/// include morphstore operators
#include <vector/vector_extension_structs.h>
#include <operators>

morphstore::MemoryManager * morphstore::MemoryManager::instance = nullptr;
std::mutex ThreadManager::printingLock;

/// default values, overwritten by command line parameters
//#define TABLE_SIZE (88UL * 1024 * 1024 / sizeof(uint64_t) * 4)
#define TABLE_SIZE (256 * 101)
#define PART_COUNT (0)
#define PART_SIZE (0)

#define GROUP_COUNT 4

/// Worker count per node (socket)
//uint64_t Worker::workerCnt = Platform::numCpusPerNode() - 1;
uint64_t Worker::workerCnt = (60 / 4);
//uint64_t Worker::workerCnt = 1;

//void setupDatabase(size_t tableSize, size_t partitionCount, size_t groupCount){
void setupDatabase(TablePartitioningPolicy * randomTable, uint64_t groupCount){
    using namespace morphstore;
    const uint64_t generationThreads = 128;
    Database::createDatabase();
    /// unsigned integer distribution for data generation (min to max value)
    std::uniform_int_distribution<uint64_t> distrColumn1(0, groupCount-1);
    std::uniform_int_distribution<uint64_t> distrColumn2(1, 10000);


    
    Table * rTable = Database::createNewTable("RandomTable", randomTable);

    ColumnIdentifier column1_id = ColumnIdentifier::createNew();
    ColumnIdentifier column2_id = ColumnIdentifier::createNew();
    rTable->registerColumn(GlobalColumnMetainformation("column1", BaseType::uint64, column1_id));
    rTable->registerColumn(GlobalColumnMetainformation("column2", BaseType::uint64, column2_id));
    
    Monitor * mon = Monitor::subRunMonitor;
    mon->start(MonitoringType::DataGeneration);
    /// generate column distributed on numa nodes
    std::vector<NumaColumnGenerator::columnPair_t>* column1
      = NumaColumnGenerator::generate_with_distribution_multithreaded(randomTable, distrColumn1, generationThreads,42);

    std::vector<NumaColumnGenerator::columnPair_t>* column2
      = NumaColumnGenerator::generate_with_distribution_multithreaded(randomTable, distrColumn2, generationThreads, 42);
    mon->end(MonitoringType::DataGeneration);
    
    info("Data generation done in " << mon->getDuration<std::chrono::milliseconds>(MonitoringType::DataGeneration) << " ms.");
    
    /// add generated columns to table
    for(uint64_t i = 0; i < randomTable->partitionCount; ++i){
        if( get<0>(column1->at(i)) != get<0>(column2->at(i)) ){
            severe("Nodes where columns reside do not match!");
        }
        NodeIdentifier nodeID = get<0>(column1->at(i));
        NumaColumnGenerator::column_t * col1 = get<1>(column1->at(i));
        NumaColumnGenerator::column_t * col2 = get<1>(column2->at(i));
        /// create Partition on correct Node
        Partition * partition = rTable->createNewPartition(nodeID);
        partition->addColumn(column1_id, col1);
        partition->addColumn(column2_id, col2);
    }
    
}

void scanForAllocationErrors(){
    info("Scan for Allocation Errors started.")
    ThreadManager::setup();
    /// set Numa Memory Allocator
    morphstore::MemoryManager::setMemoryAllocator(new NumaMemoryAllocator());
//    using namespace morphstore;
    TablePartitioningPolicy * testTable = new TablePartitioningPolicy();
    testTable->addNodes(Platform::getNodeIds());
    testTable->partitionDispatchStrategy = PartitionDispatchStrategy::FillNode;
    testTable->tableSize = 1UL * 1024 * 1024 * 1024 / 8;
//    testTable->tableSize = 22;
    /// one for each node (for now)
    testTable->partitionCount = 1000;
    
    std::uniform_int_distribution<uint64_t> distrColumn1(0, 9);
    std::vector<morphstore::NumaColumnGenerator::columnPair_t>* column1
      = morphstore::NumaColumnGenerator::generate_with_distribution_multithreaded(testTable, distrColumn1, 120, 42);
    
    info("Generation done.")
    
    std::vector<std::thread*> threadDump;
    
    for(auto& [nodeID_, column_] : *column1){
        NodeIdentifier nodeID = nodeID_;
        morphstore::column<morphstore::uncompr_f>* column = column_;
        info("Column on node " << nodeID);
//        print_columns<5>(column);
        uint64_t * data = column->get_data();
        threadDump.push_back(new std::thread(
          [nodeID, column, data] () {
            for(uint64_t i = 0; i < column->get_count_values(); ++i){
                if(Platform::getNodeOfPointer(data + i) != nodeID){
//                    std::lock_guard<std::mutex> lg(ThreadManager::printingLock);
                    error("Value not correctly allocated. Allocated on node " << Platform::getNodeOfPointer(data + i)
                    << " designated for node " << nodeID);
                }
            }
          }
          ));
    }
    for(auto& t : threadDump){
        t->join();
    }
    
    
    info("Scan for Allocation Errors ended.")
}
//uint64_t Worker::workerCnt = 2;

int main(int argc, char **argv){
//    scanForAllocationErrors();
//    return 0;

    /// init Monitors
    Monitor::resetSubRunMonitor();
    Monitor::resetGlobalMonitor();
    
    /// set thread name for logging
    ThreadManager::threadName = "MAIN";
    using namespace morphstore;
    using namespace std;
    
    /// set Numa Memory Allocator
    morphstore::MemoryManager::setMemoryAllocator(new NumaMemoryAllocator());
    
    info("Running on " << Platform::getHostname());
    uint64_t tableSize = TABLE_SIZE, partitionCount = PART_COUNT, groupCount = GROUP_COUNT, partitionSize = PART_SIZE;
    std::string filePath = "output/tmp.txt";
    
    ::testAndCreateDirectory("output");
 
	//// === parse runtime parameters === ////
	std::vector<std::string> args(argv + 1, argv + argc);
	for (auto it = args.begin(); it != args.end(); it++) {
		if (*it == "-p" && (it + 1) != args.end()) {
			partitionCount = (uint64_t) std::stoll(*(it + 1));
			it++;
			continue;
		}
		if (*it == "-e" && (it + 1) != args.end()) {
			tableSize = (uint64_t) std::stoll(*(it + 1));
			it++;
			continue;
		}
		if (*it == "-s" && (it + 1) != args.end()) {
			partitionSize = (uint64_t) std::stoll(*(it + 1));
			it++;
			continue;
		}
		if (*it == "-f" && (it + 1) != args.end()) {
			filePath = *(it + 1);
			it++;
			continue;
		}
		if (*it == "-g" && (it + 1) != args.end()) {
			groupCount = (uint64_t) std::stoll(*(it + 1));
			it++;
			continue;
		}
		error("Unkown command line argument: " << *it);
	}
    
    /// setup Platform and initial allocation directives
    ThreadManager::setup();
    
    TablePartitioningPolicy * randomTablePP = new TablePartitioningPolicy();
    randomTablePP->addNodes(Platform::getNodeIds());
    randomTablePP->partitionDispatchStrategy = PartitionDispatchStrategy::FillNode;
    randomTablePP->tableSize = tableSize;
    /// one for each node (for now)
    randomTablePP->partitionCount = partitionCount;
    randomTablePP->partitionSize = partitionSize;
    randomTablePP->fillupUnknownParameter();
    
    info("Generate " << dotNumber(randomTablePP->tableSize) << " values ("
      << dotNumber(randomTablePP->tableSize * 8 / 1024 / 1024)
      << " MiB) on " << randomTablePP->partitionCount << " partitions");
    
//    setupDatabase(tableSize, partitionCount, groupCount);
    setupDatabase(randomTablePP, groupCount);
    
    
    Table * randomTable = Database::getLocalDatabase()->getTable("RandomTable");
//    randomTable->printTables();
    
    std::mutex * printingLock = &ThreadManager::printingLock;
    
    using namespace morphstore;
    using namespace vectorlib;
    
    /// vector extension
    using scalar_ve = scalar<v64<uint64_t>>;
    #ifdef NEON
    using ve128 = neon<v128<uint64_t>>;
    info("Using Neon instructions")
    #endif
    #ifdef SSE
    using ve128 = sse<v128<uint64_t>>;
    info("Using SSE instructions")
    #endif
//    using ve128 = scalar_ve;
    /// group by operator
    OperatorWrapper randomTable_opGr(OperatorType::Group_Unary);
    randomTable_opGr.targetTable = randomTable->getId();
    randomTable_opGr.in_ColumnIdentifier[0] = randomTable->getColumnID("column1");
    randomTable_opGr.execFunction = new std::function<void(OperatorWrapper*)>(
        [] (OperatorWrapper * op) {
            const std::tuple< const column<uncompr_f>*, const column<uncompr_f>*> result =
            morphstore::group_first<ve128, uncompr_f, uncompr_f, uncompr_f>(op->in[0]);
            
            op->out[0] = get<0>(result);
            op->out[1] = get<1>(result);
            
//            std::lock_guard<std::mutex> lg(ThreadManager::printingLock);
//            info("Print intermediate of group_first:");
//            print_columns<5>(op->in[0],op->out[0], op->out[1]);
        }
    );
    
    /// aggregation operator
    OperatorWrapper randomTable_opAggr(OperatorType::Agg_Sum_Grouped);
    randomTable_opAggr.targetTable = randomTable->getId();
    randomTable_opAggr.in_ColumnIdentifier[0] = randomTable_opGr.out_ColumnMeta[0]->cid;
    randomTable_opAggr.in_ColumnIdentifier[1] = randomTable->getColumnID("column2");
    randomTable_opAggr.in_ColumnIdentifier[2] = randomTable_opGr.out_ColumnMeta[1]->cid;
    randomTable_opAggr.execFunction = new std::function<void(OperatorWrapper*)>(
        [] (OperatorWrapper * op) {
//            debug("Run Aggregation");
            /// scalar, because there is no vectorized implementation of grouped aggregation sum
            const column<uncompr_f>* result =
            morphstore::agg_sum_grouped<scalar_ve, uncompr_f, uncompr_f, uncompr_f>(op->in[0], op->in[1], op->in[2]->get_count_values());
            
            op->out[0] = result;
            
//            std::lock_guard<std::mutex> lg(ThreadManager::printingLock);
//            info("Print intermediate of group_first:");
//            print_columns<5>(op->in[0],op->in[1], op->out[0]);
        }
    );
    OperatorPrerequisite randomTable_opAggr_req;
    randomTable_opAggr_req += randomTable_opAggr.in_ColumnIdentifier[0];
    randomTable_opAggr_req += randomTable_opAggr.in_ColumnIdentifier[2];
    
    /// project operator
    OperatorWrapper randomTable_opProj(OperatorType::Projection);
    randomTable_opProj.targetTable = randomTable->getId();
    randomTable_opProj.in_ColumnIdentifier[0] = randomTable->getColumnID("column1");
    randomTable_opProj.in_ColumnIdentifier[1] = randomTable_opGr.out_ColumnMeta[1]->cid;
    randomTable_opProj.execFunction = new std::function<void(OperatorWrapper*)>(
        [] (OperatorWrapper * op) {
//            debug("Run Projection");
            const column<uncompr_f>* result =
            morphstore::project<ve128, uncompr_f, uncompr_f, uncompr_f>(op->in[0], op->in[1]);
            
            op->out[0] = result;
            
//            std::lock_guard<std::mutex> lg(ThreadManager::printingLock);
//            info("Print intermediate of project:");
//            print_columns<5>(op->in[0],op->in[1], op->out[0]);
        }
    );
    OperatorPrerequisite randomTable_opProj_req;
    randomTable_opProj_req += randomTable_opProj.in_ColumnIdentifier[1];
    
    /// aggregation merge operator
    OperatorWrapper randomTable_opAggMerge(OperatorType::Agg_Sum_Merge);
    randomTable_opAggMerge.targetTable = randomTable->getId();
    randomTable_opAggMerge.in_ColumnIdentifier[0] = randomTable_opProj.out_ColumnMeta[0]->cid;
    randomTable_opAggMerge.in_ColumnIdentifier[1] = randomTable_opAggr.out_ColumnMeta[0]->cid;
    randomTable_opAggMerge.execFunction = new std::function<void(OperatorWrapper*)>(
        [] (OperatorWrapper * op) {
//            debug("Run Aggregation Merge");
            std::tuple<
              const column<uncompr_f>*,
              const column<uncompr_f>*
            > result =
            morphstore::agg_sum_merge<scalar_ve, uncompr_f, uncompr_f, uncompr_f, uncompr_f>(
              (const column<uncompr_f>**) op->in[0],
              (const column<uncompr_f>**) op->in[1],
              op->val[0]);
            
            op->out[0] = get<0>(result);
            op->out[1] = get<1>(result);
            
//            std::lock_guard<std::mutex> lg(ThreadManager::printingLock);
//            info("Print result of agg sum merge:");
//            print_columns<5>(op->out[0], op->out[1]);
        }
    );
    OperatorPrerequisite randomTable_opAggMerge_req;
    randomTable_opAggMerge_req += randomTable_opAggMerge.in_ColumnIdentifier[0];
    randomTable_opAggMerge_req += randomTable_opAggMerge.in_ColumnIdentifier[1];
    
    
    
    /// setup master coordinator and runtime (worker, coordinator)
    MasterCoordinator master;
    master.setupRuntime();
    Monitor * mon = Monitor::subRunMonitor;
    
    /// dispatch operators to all slave coordinators
    for (auto & slave : master.slaves) {
//		debug("Next slave dispatch")
        slave->distributeOperator(randomTable_opGr);
        slave->enqueueSucceedingOperator(randomTable_opAggr, OperatorScope::Partition, randomTable_opAggr_req);
        slave->enqueueSucceedingOperator(randomTable_opProj, OperatorScope::Partition, randomTable_opProj_req);
//        slave->enqueueSucceedingOperator(l_opProj, OperatorScope::Partition, l_opProj_req);
//        slave->printLinkedOperators();
    }
    master.slaves[0]->enqueueGlobalOperator(randomTable_opAggMerge, OperatorScope::Global, randomTable_opAggMerge_req);
    
    info("Start query execution");
    /// start processing
    master.start();
    
    info("Runtime for " << dotNumber(tableSize) << " values / " << dotNumber(tableSize * sizeof(uint64_t)) << " Bytes: " << dotNumber( mon->getDuration(MonitoringType::GlobalRuntime).count() ) << "ns");
    
    std::ofstream out;
    
    out.open(filePath, ios::app);
    out << mon->getDuration(MonitoringType::GlobalRuntime).count();
    out.close();
    
}



