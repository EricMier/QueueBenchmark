/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Worker.h"

#include <storage>
#include <threading>

#include <monitoring/Monitor_old.h>
#include <monitoring/Monitor.h>
#include "test/Benchmark.h"


thread_local Worker* Worker::currentWorker = nullptr;
unsigned Worker::nextId = 0;

mutex Worker::waitMutex;
condition_variable Worker::waitCondVar;
volatile bool Worker::waitCond = false;

Worker::Worker(Coordinator* coordinator, CPUInfo& cpuInfo) : cpuInfo(cpuInfo), coordinator(coordinator), id(WorkerIdentifier::createNew()) {
//    id = nextId++;
    jobCnt = &coordinator->jobCnt;
	#ifdef USE_LIBNUMA
//    buffer = (unsigned long*) numa_alloc_onnode(Benchmark::getMinBufferSize(), cpuInfo.socketId);
    #else
//    buffer = (unsigned long*) malloc(Benchmark::getMinBufferSize());
	#endif
}

Worker::Worker(const Worker& orig) : cpuInfo(orig.cpuInfo), coordinator(orig.coordinator) {
    id = orig.id;
    jobCnt = orig.jobCnt;
}

Worker::~Worker() = default;

void * Worker::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(Worker));
}

void Worker::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Worker));
}

/**
 * @brief Starts the worker thread.
 *
 * Creates a new thread, stores it in this instance and pins the created thread to the specified core.
 */
void Worker::init(){
    delete thread;
//	thread = new std::thread(&Worker::run_old, this);
	thread = new std::thread(&Worker::run, this);
//	ThreadManager::pinWorker(this);
}

/**
 * @brief This method is executed by the workers thread.
 *
 * The run method implements the behaviour of a worker.
 * It is passed to the thread in the init method.
 */
//void Worker::run_old(){
//	//// set memory allocator to this node
//	auto fence = ThreadManager::directAllocationToNode(&cpuInfo.nodeId);
//
//	//// wait till thread variable is set
//	while (thread == nullptr) {
//		asm volatile ("" : : : "memory");
//	}
//
//	stringstream ss;
////	ss << "Worker #" << id << " running on core " << cpuInfo.physicalId << " reporting for duty!" << std::endl;
////	cout << ss.str();
////	ss.str("");
//
//	//// set thread local variables
//	Worker::currentWorker = this;
//	Coordinator::currentCoordinator = coordinator;
//	Monitor_old::resetThreadMonitor();
//
//	partitionOffset = 0;
//
////	jobCnt = &Coordinator::currentCoordinator->jobCnt;
//	jobCnt = Coordinator::currentCoordinator->jobCntV->at(cpuInfo.nodeId.getPhysicalNode());
//
//
//
//	//// retrieve pointer to partition list
//	partitions = Database::getPartitionList(cpuInfo.nodeId.getPhysicalNode());
//	if(partitions == nullptr) {
//		std::cerr << "Worker #" << id << " found no PartitionList. Terminating." << std::endl;
//		return;
//	}
//
//	//// wait until coordinator sends start signal
//	{
//		unique_lock<mutex> lock(Worker::waitMutex);
//		Worker::waitCondVar.wait(lock, [] { return Worker::waitCond; });
//	}
//
//	//// start monitoring
//	Monitor_old::threadMonitor->start(MONITOR_GLOBAL_RUNTIME);
//
//
//
////	while(true){
////		database = Database::getDatabase(cpuInfo->NodeID);
////		if(database != nullptr)
////			break;
////	}
//
//	while(*jobCnt > 0){
//		if(findJob()){
////			ss << "Worker #" << id << " (" << cpuInfo.to_string() << ") found a job!" << std::endl;
////			cout << ss.str();
////			ss.str("");
//			Monitor_old::threadMonitor->resume(MONITOR_EXECUTE_JOB);
//			//// do found job
//
//			currentJob.getOperator()->run(currentPartition);
//
//			unsigned jobsDone = 1;
//			//// unqueue jobs until queue is empty
//			while(jobsDone < WORK_PACKAGE_SIZE && currentPartition->queue_old.try_dequeue(currentJob)){
//				currentJob.getOperator()->run(currentPartition);
//				jobsDone++;
//			}
//			Monitor_old::threadMonitor->end(MONITOR_EXECUTE_JOB);
//			//// unlock the partition
//			currentPartition->unlock();
////            ss << "Worker #" << id << " has done " << jobsDone << " jobs.\n";
////            std::cout << ss.str();
////            ss.str("");
//		}
//	}
//
//	Monitor_old::threadMonitor->end(MONITOR_GLOBAL_RUNTIME);
//	Monitor_old::threadMonitor->transmit(Monitor_old::globalMonitor);
////	cout << to_string(result);
////	ss.str("");
////	ss << "Worker #" << id << " had to wait " << waitCnt << "ms in total." << std::endl;
////	ss << "Worker #" << id << " (" << cpuInfo.to_string() <<  ") is terminating. ("
////		<< chrono::duration_cast<chrono::milliseconds>(Monitor::threadMonitor->getDuration(MONITOR_GLOBAL_RUNTIME)).count()
////		<< "ms)" <<std::endl;
////	std::cout << ss.str();
//}

void Worker::run(){
	ThreadManager::threadName = str_rfill("W" + str_lfill(id.to_string(),3,"."), 4, " ");
	//// set default memory allocator to this node
	ThreadManager::bindToNode(&cpuInfo.nodeId);
//	NodeAllocationFence fence(cpuInfo.nodeId);
	
	//// wait till thread variable is set
	while (thread == nullptr) {
		asm volatile ("" : : : "memory");
	}
	/// pin this worker thread to its configured CPU
	ThreadManager::pinWorker(this);
	Monitor::resetThreadMonitor();
	
	//// set thread local variables
	Worker::currentWorker = this;
	Coordinator::currentCoordinator = coordinator;
//	Monitor_old::resetThreadMonitor();
	
//	if(Worker::waitCond){
//		error("Wait condition true, before worker is ready");
//	}
	//// wait until coordinator sends start signal
	{
//		debug("aquire lock");
		unique_lock<mutex> lock(Worker::waitMutex);
//		debug("lock aquired");
		++MasterCoordinator::WorkerReady;
		Worker::waitCondVar.wait(lock, [] { return Worker::waitCond; });
//		debug("wait condition fullfilled");
	}
	
	#ifdef PRINT_THREAD_MESSAGES
	info("is starting on CPU " << cpuInfo.logicalId << ".")
    #endif
	database = Database::getLocalDatabase();
	
	loop();
	#ifdef PRINT_THREAD_MESSAGES
	info("is terminating on CPU " << cpuInfo.logicalId << ".");
    #endif
}

void Worker::loop() {
	Monitor::threadMonitor->start(MonitoringType::wDequeuingJob);
	Job * jobPtr = nullptr;
	while(!terminate) {
		/// iterate over tables
		unordered_map<TableIdentifier, Table *, IdentifierHash> const * tables = database->getTables();
		for (auto & [tid, table] : *tables) {
			if(terminate) break;
			/// iterate over partitions
			unordered_map<PartitionIndex, Partition *, IdentifierHash> const * partitions = table->getPartitions();
			for (auto & [pid, partition] : *partitions) {
				if(terminate) break;
				if(!partition->tryLock())
					continue;
				/// look for jobs
				while (partition->try_dequeue(jobPtr)) {
					Monitor::threadMonitor->end(MonitoringType::wDequeuingJob);
					Monitor::threadMonitor->resume(MonitoringType::wExecuteJob);
					/// operator setup
//					for (uint32_t i = 0U; i < jobPtr->op->inSize; ++i) {
//						/// test if input is already set, if not, get columns from partition
//						if (!jobPtr->op->in[i]) {
//							jobPtr->op->in[i] = partition->getColumn(jobPtr->op->in_ColumnIdentifier[i]);
//						}
//					}
					
					/// operator execution
					jobPtr->op->run(partition);
					
					/// report results to coordinator
					coordinator->reportFinishedJob(jobPtr);
				}
				partition->unlock();
				Monitor::threadMonitor->end(MonitoringType::wExecuteJob);
				Monitor::threadMonitor->resume(MonitoringType::wDequeuingJob);
			} /// for partitions
			processGlobalQueue();
		} /// for tables
//		debugh("All partitions visited.");
	} /// while(!terminate)
	
//	if(Monitor::threadMonitor->getDuration(MonitoringType::wExecuteJob).count() > 0) {
//		Monitor::threadMonitor->transmit(Monitor::subRunMonitor, MonitoringType::wDequeuingJob);
//		Monitor::threadMonitor->transmit(Monitor::subRunMonitor, MonitoringType::wExecuteJob);
//	}
//	if(Monitor::threadMonitor->getDuration(MonitoringType::wExecuteGlobalJob).count() > 0) {
//		Monitor::threadMonitor->transmit(Monitor::subRunMonitor, MonitoringType::wExecuteGlobalJob);
//	}

}

/**
 * @brief Waits for the worker thread to finish.
 */
void Worker::join() {
    thread->join();
    delete thread;
}

/**
 * @brief Returns a pointer to the workers thread.
 * @return pointer to the workers thread
 */
std::thread* Worker::getThread(){
	return this->thread;
}

/**
 * @brief Returns this workers id.
 * @return worker id
 */
WorkerIdentifier Worker::getId() {
	return id;
}

/**
 * @brief Returns a pointer to this workers CPUInfo.
 * @return pointer to CPUInfo
 */
const CPUInfo *Worker::getCpuInfo() {
    return &cpuInfo;
}

/**
 * @brief Returns a pointer to the currently executed job.
 * @return pointer to Job
 */
//Job_old *Worker::getCurrentJob() {
//    return &currentJob;
//}

//// === private ===
/**
 * @brief This method implements the search for a new job.
 *
 * This method searches the database hierarchy for jobs to execute.
 * @return Whether a job is found or not
 */
bool Worker::findJob() {
//	Monitor_old::threadMonitor->resume(MONITOR_FIND_JOB);
//	auto begin = partitions->begin() + partitionOffset;
//	for(; begin != partitions->end(); ++begin){
//	    Partition* partition = *begin;
//        if(partition->tryLock()){
//            if(partition->queue_old.try_dequeue(currentJob)){
//                currentPartition = partition;
//                partitionOffset++;
//                Monitor_old::threadMonitor->end(MONITOR_FIND_JOB);
//                return true;
//            }
//            partition->unlock();
//        }
//        partitionOffset++;
//	}
//	partitionOffset = 0;
//
//	Monitor_old::threadMonitor->end(MONITOR_FIND_JOB);
//	return false;
}

void Worker::processGlobalQueue() {
	Job * jobPtr;
	while(coordinator->globalOperatorQueue.try_dequeue(jobPtr)){
		Monitor::threadMonitor->end(MonitoringType::wDequeuingJob);
		Monitor::threadMonitor->resume(MonitoringType::wExecuteGlobalJob);
		
		/// operator setup
//		for (uint32_t i = 0U; i < job.op.inSize; ++i) {
			/// test if input is already set, if not, get columns from partition
//			if (!job.op.in[i]) {
//				job.op.in[i] = partition->getColumn(job.op.in_ColumnIdentifier[i]);
//			}
//		}
		/// operator execution
		jobPtr->op->run(nullptr);
		/// report to coordinator
		/// todo
		
		
		/// todo === separate handling of global operator results
		coordinator->reportFinishedJob(jobPtr);
		
		Monitor::threadMonitor->end(MonitoringType::wExecuteGlobalJob);
		Monitor::threadMonitor->resume(MonitoringType::wDequeuingJob);
	}
}




