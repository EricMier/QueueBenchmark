/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class Coordinator;

#ifndef QUEUEBENCHMARK_COORDINATOR_H
#define QUEUEBENCHMARK_COORDINATOR_H



#include "platform/CPUInfo.h"
#include "platform/NodeIdentifier.h"
#include "concurrentqueue/concurrentqueue.h"
#include "task/OperatorDependency.h"
#include "task/Job.h"
#include <task>

#include <core/storage/column.h>



class Coordinator {
	friend class ThreadManager;
	friend class MasterCoordinator;
	using column_t = const morphstore::column<morphstore::uncompr_f>;
public:
    // Constructor
    Coordinator();
    Coordinator(NodeIdentifier & nodeId);

    // Copy constructor
    Coordinator(const Coordinator &other);

    // Move constructor
    Coordinator(Coordinator &&other) noexcept;

    // Destructor
    ~Coordinator();

    // Assignment operator
    Coordinator &operator=(const Coordinator &rhs) = delete;

    // Move assignment operator
    Coordinator &operator=(Coordinator &&rhs) noexcept = delete;
    
	void* operator new(size_t size);
	void operator delete(void* p);

    void setupCpuOrderForAllNodes();
    void setupWorkerForAllNodes();
    void initWorkerThreads();
    void waitForWorkerThreads();
    int getJobCnt();
    
    static thread_local Coordinator * currentCoordinator;
    
    //// new
    void linkCoordinator(Coordinator * coordinator);
    void linkCoordinatorToAllSiblings(Coordinator * coordinator);
    
    // todo: override for master coordinator
    void distributeOperator(OperatorWrapper & op);
    void distributeOperator(OperatorWrapper & op, PartitionIndex pid);
    
    void enqueueSucceedingOperator(OperatorWrapper & op, OperatorScope opScope, OperatorPrerequisite & opPrerequisite, Coordinator * targetCoordinator = nullptr);
    void enqueueGlobalOperator(OperatorWrapper & op, OperatorScope opScope, OperatorPrerequisite & opPrerequisite);
    
    void reportFinishedJob(Job & job);
    void reportFinishedJob(Job * job);
    void reportGlobalResult(ColumnIdentifier cid, PartitionIndex pid, column_t * column);
    
    void processIncomingJobsBuffer();
    void processIncomingGlobalResultsBuffer();
    
    void processResultColumn(ColumnIdentifier cid, PartitionIndex pid, column_t * column);
    void processGlobalResultColumn(ColumnIdentifier cid, PartitionIndex pid, column_t * column);
    
    void purgeOperatorDependency(identifier_map<ColumnIdentifier, std::vector<OperatorDependency*>> * list, OperatorDependency * dependency);
    
    void setupCpuOrder();
    void setupWorker();
    
    CoordinatorIdentifier getId();
    
    
    void terminateWorkers();
    void print();
    
    void initThread();
    void run();
    void join();
    
    static void createCoordinators();
    
    void printLinkedOperators();


    /**
     * @brief used to keep track of unfinished jobs
     */
    std::atomic<uint64_t> jobCnt;
    std::vector<std::atomic<uint64_t>*>* jobCntV;
    
    moodycamel::ConcurrentQueue<Job*> nodeOperatorQueue;

    moodycamel::ConcurrentQueue<Job*> globalOperatorQueue;
//    moodycamel::ConcurrentQueue<Job> globalOperatorQueue2;
    
	
protected:
	bool terminateCondition();
	
	NodeIdentifier nodeId;
	CoordinatorIdentifier id;
	Database * databaseNodeLocal = nullptr;
    std::vector<CPUInfo> cpuInfoL;
    std::vector<CPUInfo*> cpuOrderL;
    std::vector<Worker*> workerL;
    std::map<NodeIdentifier, Coordinator*> siblings;
    
	//// Pointer to the coordinators thread
	std::thread * thread = nullptr;
	CPUInfo cpuInfo;
	//// Mutex for boolean waitCond
    inline static std::mutex waitMutex;
    //// Condition variable to block/unblock worker threads
	inline static std::condition_variable waitCondVar;
	//// Condition to block/unblock worker threads
	inline static volatile bool waitCond = false;
    
    /// todo: work-in-progress-jobs in liste speichern
//    map<OperatorIdentifier, vector<PartitionIndex>> jobsInProgress;
    /// todo: erledigte jobs in liste speichern
//    map<OperatorIdentifier, map<PartitionIndex, Operator>> jobsFinished;

    /// todo: con-queue fuer erledigte jobs (Schnittstelle fuer Worker)
    moodycamel::ConcurrentQueue<Job*> incomingJobsBuffer;
//    moodycamel::ConcurrentQueue<Job> incomingJobsBuffer2;
    moodycamel::ConcurrentQueue<std::tuple<ColumnIdentifier, PartitionIndex, column_t*>> incomingGlobalResultsBuffer;
    
    /// local dependency graph : maps ColumnIdentifier to a list of operators, which are awaiting this column
    identifier_map<ColumnIdentifier, std::vector<OperatorDependency*>> linkedOperators;
    /// global dependency graph : maps ColumnIdentifier to a list of global operators, which are awaiting this column
    identifier_map<ColumnIdentifier, std::vector<OperatorDependency*>> globalLinkedOperators;
    
    std::vector<column_t*> generatedColumns;

};


//class MasterCoordinator : public Coordinator {
//  public:
//	~MasterCoordinator();
//	void distributeOperator(Operator & op);
//	void addSlave(Coordinator slave);
//	void setupRuntime();
//	void start();
//	vector<Coordinator*> slaves;
//
//	inline static atomic<uint64_t> CoordinatorReady = 0UL;
//	inline static atomic<uint64_t> WorkerReady = 0UL;
//  private:
//};


#endif //QUEUEBENCHMARK_COORDINATOR_H
