/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "MasterCoordinator.h"
#include <threading>

#include <storage/Database.h>
#include <monitoring/Monitor.h>

#include "platform/Platform.h"
#include "engine/PrototypeEngine.h"

MasterCoordinator::~MasterCoordinator() {
	for(auto& slave : slaves){
		delete slave;
	}
}

void MasterCoordinator::setupRuntime() {
	/// create as many slave coordinators as nodes on this platform
	for(auto& node : *Platform::getNodeIds()){
		NodeAllocationFence fence(*node);
		auto coord = new Coordinator(*node);
		if(!slaves.empty()){
			slaves[0]->linkCoordinatorToAllSiblings(coord);
		}
		slaves.push_back(coord);
		#ifdef ONLY_ONE_COORDINATOR
			break;
		#endif
	}
	
	for(auto coord : slaves){
		/// assign cpu informations of corresponding node ...
		coord->setupCpuOrder();
		/// ... to use as link between cpus and workers
		coord->setupWorker();
		/// start worker threads for each node
//		coord->initWorkerThreads();
		/// start coordinator thread on each node
//		coord->initThread();
	}
}



void MasterCoordinator::start() {
	uint64_t workerCnt = 0UL;
	for(auto coord : slaves){
		workerCnt += coord->workerL.size();
		/// start worker threads for each node
		coord->initWorkerThreads();
		/// start coordinator thread on each node
		coord->initThread();
	}
//	ThreadManager::waitS(2);
	Monitor * mon = Monitor::subRunMonitor;
//	uint64_t cycle = 0UL;
	while(MasterCoordinator::CoordinatorReady < slaves.size() || MasterCoordinator::WorkerReady < workerCnt){
		ThreadManager::wait(1);
//		++cycle;
	}
//	info("Cycles:" << cycle);
	
	mon->start(MonitoringType::GlobalRuntime);
	ThreadManager::unblockWorkerThreads();
	ThreadManager::unblockCoordinatorThreads();
	
	for(auto coord : slaves){
//		coord->waitForWorkerThreads();
		coord->join();
	}
//	debugh("Stoped all Coordinaotrs");
	mon->end(MonitoringType::GlobalRuntime);
	MasterCoordinator::CoordinatorReady = 0UL;
	MasterCoordinator::WorkerReady = 0UL;
}

