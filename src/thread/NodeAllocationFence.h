/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class NodeAllocationFence;

#ifndef QUEUEBENCHMARK_NODEALLOCATIONFENCE_H
#define QUEUEBENCHMARK_NODEALLOCATIONFENCE_H

//// if defined, the name of the caler is added to the Constructor call
#define ADD_CALLER_TO_ALLOCATION

#ifdef ADD_CALLER_TO_ALLOCATION
  #define addCaller , __FUNCTION__
#else
  #define addCaller
#endif



//#include "includesForHeader.h"
#include "platform/NodeIdentifier.h"
#include "platform/Platform.h"
#include "thread/ThreadManager.h"


#include <logging>
#include <libnuma>


/**
 * @brief This fence sets a new memory allocation policy and restores the previous if destroyed.
 */
class [[nodiscard]] NodeAllocationFence {
  public:
	NodeAllocationFence() = default;
	explicit NodeAllocationFence(NodeIdentifier& node);
	NodeAllocationFence(NodeIdentifier& node, std::string caller);
	~NodeAllocationFence();
	
	
//	NodeAllocationFence(const NodeAllocationFence & other) = delete;
//	NodeAllocationFence & operator=(const NodeAllocationFence & other) = delete;
	NodeAllocationFence(NodeAllocationFence && other) = default;
	
	
	NodeAllocationFence & operator=(NodeAllocationFence && rhs) noexcept;
	
	std::string getFenceID();
	unsigned long fenceID = (unsigned long) -1;
	
  private:
		NodeIdentifier currentNodeId;
		NodeIdentifier prevNodeId;
		
		bool activeFence = false;
		std::string caller;
		static uint64_t counter;
	
	static void setAllocationPolicy(const NodeIdentifier& node);
};


#endif //QUEUEBENCHMARK_NODEALLOCATIONFENCE_H
