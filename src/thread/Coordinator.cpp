/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized Column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include <monitoring/Monitor_old.h>
#include <monitoring/Monitor.h>
/// /thread/
#include <threading>

#include <platform>
#include <utils>
#include <storage>
#include "task/Job.h"
#include "engine/PrototypeEngine.h"
#include "Coordinator.h"


thread_local Coordinator * Coordinator::currentCoordinator = nullptr;

/**
 * @brief Constructor of Coordinator
 */
Coordinator::Coordinator() : nodeId(NodeIdentifier::createInvalidNodeIdentifier()) {
	Coordinator::currentCoordinator = this;
	jobCntV = new vector<atomic<uint64_t>*>();
	for(auto& nodeID : *Platform::getNodeIds()){
		auto fence = NodeAllocationFence(*nodeID);
		jobCntV->push_back(new atomic<uint64_t>(0));
		
	}
}
Coordinator::Coordinator(NodeIdentifier & nodeId) : nodeId(nodeId), id(CoordinatorIdentifier::createNew()){
	databaseNodeLocal = nodeId.getNode()->getDatabase();
	jobCnt = 0UL;
}

/**
 * @brief Copy constructor of Coordinator
 * @param other Instance of Coordinator to copy from
 */
Coordinator::Coordinator(const Coordinator & other) : nodeId(NodeIdentifier::createInvalidNodeIdentifier()) {
	nodeId = other.nodeId;
	// @todo Implement Coordinator copy constructor
}


/**
 * @brief Move constructor of Coordinator
 * @param other RValue of type Coordinator to steal from
 */
Coordinator::Coordinator(Coordinator && other) noexcept : nodeId(NodeIdentifier::createInvalidNodeIdentifier()){
		nodeId = other.nodeId;
	// @todo Implement Coordinator move constructor
}


/**
 * @brief Destructor of Coordinator
 */
Coordinator::~Coordinator() {
//	debug("Destroy generated Columns");
//	for(auto& column : generatedColumns){
//		__destruct(Column, Column);
//	}
	for(auto& worker : workerL){
		delete worker;
	}
}

void * Coordinator::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(Coordinator));
}

void Coordinator::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Coordinator));
}

/**
 * @brief Initializes a list, in which order cpus are equipped with a worker.
 */
void Coordinator::setupCpuOrderForAllNodes() {
	//// all threads
	cpuOrderL.insert(cpuOrderL.end(),
	        Platform::getAllThreads()->begin(),
	        Platform::getAllThreads()->end());
	//// MainThreads first, HyperThreads second
//	cpuOrderL.insert(cpuOrderL.end(),
//	        Platform::getMainThreads()->begin(),
//	        Platform::getMainThreads()->end());
//    cpuOrderL.insert(cpuOrderL.end(),
//            Platform::getHyperThreads()->begin(),
//            Platform::getHyperThreads()->end());
}

/**
 * @brief Creates the worker instances.
 *
 * This method creates the worker instances, but does not start the worker threads.
 * To start the worker threads use 'initWorkerThreads'
 */
void Coordinator::setupWorkerForAllNodes() {
	if (!workerL.empty())
		throw std::runtime_error("Worker already initialized.");


	if (WORKER_CNT_PER_NODE * Platform::numNodes() > cpuOrderL.size())
		throw std::runtime_error("CPU order list is to short. "
		                         "Size: " + std::to_string(cpuOrderL.size()) + " " +
		                         "Needed: " + std::to_string(WORKER_CNT_PER_NODE * Platform::numNodes()));
	//// create Worker classes
	for (int i = 0; i < WORKER_CNT_PER_NODE * Platform::numNodes(); i++) {
		auto fence = NodeAllocationFence(cpuOrderL.at(i)->nodeId);
		workerL.push_back(new Worker(this, *cpuOrderL.at(i)));
	}
}

/**
 * @brief Starts the worker threads.
 *
 * Assuming the worker instances are allready created, this method starts the worker threads.
 */
void Coordinator::initWorkerThreads() {
//	Worker::waitCond = false;
	for (Worker * wo : workerL) {
		wo->init();
	}
}


/**
 * @brief The current thread waits for all worker threads to finish.
 */
void Coordinator::waitForWorkerThreads() {
	for (Worker * wo : workerL) {
		wo->join();
	}
	Worker::waitCond = false;
}

int Coordinator::getJobCnt() {
	int res = 0;
	for(auto const& j : *jobCntV){
		res += *j;
	}
	return res;
}

/**
 * Creates one Coordinator instance on every available node
 */
void Coordinator::createCoordinators() {
	Coordinator * prev = nullptr;
	for( auto node : *Platform::getNodeIds() ){
		NodeAllocationFence fence(*node);
		Coordinator * co = new Coordinator(*node);
		if(prev)
			prev->linkCoordinatorToAllSiblings(co);
		prev = co;
		node->getNode()->linkCoordinator(co);
	}
}

/**
 * Takes another coordinator instance by pointer and links it with all instances known by this coordinator (itself included).
 * @param coordinator
 */
void Coordinator::linkCoordinatorToAllSiblings(Coordinator * coordinator) {
	debug1("Link new Coordinator[" << coordinator->nodeId.getUNI() << "] to all siblings of Coordinator[" << nodeId.getUNI() << "]");
	for( auto s : siblings ){
		s.second->linkCoordinator(coordinator);
	}
	linkCoordinator(coordinator);
}

/**
 * Takes another coordinator instance and links it with itself.
 * @param coordinator
 */
void Coordinator::linkCoordinator(Coordinator * coordinator) {
	debug2("Link new Coordinator[" << coordinator->nodeId.getUNI() << "] to Coordinator[" << nodeId.getUNI() << "]");
	siblings[coordinator->nodeId] = coordinator;
	coordinator->siblings[nodeId] = this;
}

/**
 * Takes an operator and distributes it over all partitions of the targeted table, which reside on the same node
 * as the instance of this coordinator.
 * @threadsafe
 * @param op Operator to distribute
 */
void Coordinator::distributeOperator(OperatorWrapper & op) {
	NodeAllocationFence fence(nodeId);
	if(!op.isValid()){
		error("Passed operator is invalid");
		return;
	}
	if(!databaseNodeLocal->_hasTable(op.targetTable)){
		debugwarn("Table[" << op.targetTable << "] has no instance on this node " << nodeId);
		return;
	}
	Table * targetTable = databaseNodeLocal->getTable(op.targetTable);
	
	/// create new entry for wip jobs
//	jobsInProgress.insert({op.id, vector<PartitionIndex>()});
	
	/// iterate over all partitions of this table and add a operator job
	for(const pair<const PartitionIndex, Partition*> & partition : *targetTable->getPartitions()){
//		jobsInProgress[op.id].push_back( partition.first );
		#ifdef ENQUEUE_JOB_POINTER
		Job * const job = new Job(new OperatorWrapper(op), partition.first);
		partition.second->enqueue(job);
		#else
		partition.second->enqueue(Job(op, partition.first));
		#endif
		
		++jobCnt;
	}
}

/**
 * Takes an operator and enqueues it in the target partition, given through specified PartitionIndex
 * @param op Operator to execute
 * @param pid PartitionIndex of target partition
 */
void Coordinator::distributeOperator(OperatorWrapper & op, PartitionIndex pid) {
	NodeAllocationFence fence(nodeId);
//	debugh("Send operator " << op.id << " to partition " << pid);
	Partition * targetPartition = databaseNodeLocal->getTable(op.targetTable)->getPartition(pid);
//	debug("Create new Job with operator: " << op);
	targetPartition->enqueue( new Job(new OperatorWrapper(op), pid) );
	
	++jobCnt;
}

void Coordinator::print() {
	info("Print currently work in progress jobs");
//	for(auto & op : jobsInProgress){
//		info("Operator[" << op.first << "]");
//		for(auto & pid : op.second){
//			info(" Partition[" << pid << "]");
//		}
//	}
}

/**
 * Interface mehtod for workers to report finished jobs to this coordinator.
 * @threadsafe
 * @param job
 */
void Coordinator::reportFinishedJob(Job & job) {
    severe("Not implemented!");
//	incomingJobsBuffer2.enqueue(std::move(job));
}

void Coordinator::reportFinishedJob(Job * job) {
//    debug("Report job " << job->op->id << ":" << job->partId);
	incomingJobsBuffer.enqueue(job);
}

void Coordinator::reportGlobalResult(ColumnIdentifier cid, PartitionIndex pid, column_t * Column) {
	NodeAllocationFence fence(nodeId);
//	debugh("Report global result / [CID:" << cid << "] [PID:" << pid << "] [Column:" << Column << "] to coordinator " << id);
	incomingGlobalResultsBuffer.enqueue(tuple<ColumnIdentifier, PartitionIndex, column_t*>(cid, pid, Column));
}



void Coordinator::setupCpuOrder() {
	cpuInfo = **Platform::getAllThreadsOfSocket(nodeId.getPhysicalNode())->begin();
	//// all threads
	cpuOrderL.insert(cpuOrderL.end(),
	        ++Platform::getAllThreadsOfSocket(nodeId.getPhysicalNode())->begin(),
	        Platform::getAllThreadsOfSocket(nodeId.getPhysicalNode())->end());
}

void Coordinator::setupWorker() {
	if (!workerL.empty())
		throw std::runtime_error("Worker already initialized.");


	if ((Worker::workerCnt) > cpuOrderL.size())
		throw std::runtime_error("CPU order list is to short. "
		                         "Size: " + std::to_string(cpuOrderL.size()) + " " +
		                         "Needed: " + std::to_string(Worker::workerCnt));
	//// create Worker classes
	for (int i = 0; i < Worker::workerCnt; i++) {
//		info(cpuOrderL.at(i)->physicalId);
		auto fence = ThreadManager::directAllocationToNode(&cpuOrderL.at(i)->nodeId);
		workerL.push_back(new Worker(this, *cpuOrderL.at(i)));
	}
}



void Coordinator::enqueueSucceedingOperator(
  OperatorWrapper & op, OperatorScope opScope,
  OperatorPrerequisite & opPrerequisite, Coordinator * targetCoordinator
	) {
	NodeAllocationFence fence(nodeId);
	OperatorDependency * dep = new OperatorDependency(new OperatorWrapper(op), opScope, new OperatorPrerequisite(opPrerequisite), targetCoordinator);
	
	/// add all partitions of target table to dependency queue
	Table * targetTable = databaseNodeLocal->getTable(op.targetTable);
	for(auto& partition : *targetTable->getPartitions()){
		dep->addPartition(partition.first);
	}
	
	/// if there are no target partitions, skip this table
	if(dep->isFulfilled()) {
		return;
	}
	
	/// link dependency with all source Columns of this operator
	for(auto& req : opPrerequisite.requisites){
		if(!req.first.isValid()){
			severe("Required ColumnIdentifier is invalid! Affected operator: [ID:" << op.id << "/Type:" << op.type << "]");
		}
		if(opScope == OperatorScope::Global){
//			debugh("enqueue global operator " << op.id << " to Column " << req.first);
		}
		linkedOperators[req.first].push_back( dep );
	}
}



void Coordinator::enqueueGlobalOperator(
  OperatorWrapper & op, OperatorScope opScope,
  OperatorPrerequisite & opPrerequisite
	) {
	NodeAllocationFence fence(nodeId);
	OperatorDependency * dep = new OperatorDependency(new OperatorWrapper(op), opScope, new OperatorPrerequisite(opPrerequisite), this);
	
	/// add all partitions of node local table
	Table * targetTable = databaseNodeLocal->getTable(op.targetTable);
	for(auto& partition : *targetTable->getPartitions()){
		dep->addPartition(partition.first);
	}
	/// add all partitions of remote tables
	for(const auto& table : *targetTable->getSiblings()){
		for(auto& partition : *table.second->getPartitions()){
			dep->addPartition(partition.first);
		}
	}
	
	/// link dependency with all source Columns of this operator
	for(auto& req : opPrerequisite.requisites){
		globalLinkedOperators[req.first].push_back( dep );
	}
	
	/// dispatch operator to all nodes
	enqueueSucceedingOperator(op, opScope, opPrerequisite, this);
	for(auto& sib : siblings){
		sib.second->enqueueSucceedingOperator(op, opScope, opPrerequisite, this);
	}
}





void Coordinator::purgeOperatorDependency(identifier_map<ColumnIdentifier, vector<OperatorDependency*>> * list, OperatorDependency * dependency) {
	/// iterate every source Column searching this dependency
	for(auto linkIt = list->begin(); linkIt != list->end();){
//	for(pair<ColumnIdentifier, vector<OperatorDependency*>>& link : waitingOperators){
		vector<OperatorDependency*> * dependencies = &(*linkIt).second;
		/// check all dependencies for this Column
		for(auto dependencyIt = dependencies->begin(); dependencyIt != dependencies->end();){
			if(*dependencyIt == dependency){
				dependencyIt = dependencies->erase(dependencyIt);
				break;
			} else {
				++dependencyIt;
			}
		}
		if(dependencies->empty()){
			/// delete this link
			linkIt = list->erase(linkIt);
		} else {
			++linkIt;
		}
	}
	/// finally delete dependency object
	delete dependency;
}



CoordinatorIdentifier Coordinator::getId() {
	return id;
}



void Coordinator::initThread() {
//    delete thread;
    Coordinator::waitCond = false;
	thread = new std::thread(&Coordinator::run, this);
}



void Coordinator::join() {
	thread->join();
//	delete thread;
}



void Coordinator::terminateWorkers() {
//	debug("Terminate workers");
	for(auto& worker : workerL){
		worker->terminate = true;
	}
}



bool Coordinator::terminateCondition() {
//	debug("Check terminate Condintion / jobCnt=" << jobCnt << " linkedOperators.size=" << linkedOperators.size() );
//	for(auto& link : linkedOperators){
//		debug("Size of dependency vector= " << link.second.size());
//	}
	bool res = (jobCnt == 0) && linkedOperators.empty() && globalLinkedOperators.empty();
	return res;
}



void Coordinator::printLinkedOperators() {
//	debug("Print linked operators:");
	for(auto& link : linkedOperators){
		ColumnIdentifier cid = link.first;
		vector<OperatorDependency*> * dependencies = &link.second;
		for(auto& dep : *dependencies){
			auto * partitions = &dep->requiredPartitions;
			for(auto& part : *partitions){
				PartitionIndex pid = part.first;
				OperatorPrerequisite * requisite = part.second;
				stringstream ss;
				string delimiter = "";
				for(auto& req : requisite->requisites){
					ColumnIdentifier req_cid = req.first;
					column_t * Column = req.second;
					ss << delimiter << req_cid << ":" << Column;
					delimiter = ", ";
				}
				
//				debug("[CID:" << cid << "]->[DEP:" << dep << ":" << dep->op.type << "]->[PART:" << pid << "]->[REQ:" << requisite << "] = [" << ss.str() << "]");
			}
		}
	}
}



void Coordinator::run() {
	ThreadManager::threadName = str_rfill("C" + str_lfill(id.to_string(),3,"."), 4, " ");
	debugh("TestThread");
	//// set memory allocator to this node
	ThreadManager::bindToNode(&cpuInfo.nodeId);
	
	//// wait till thread variable is set
	while (thread == nullptr) {
		asm volatile ("" : : : "memory");
	}
	/// pin this worker thread to its configured CPU
	ThreadManager::pinThread(thread, &cpuInfo);
	
	//// set thread local variables
	Coordinator::currentCoordinator = this;
//	Monitor_old::resetThreadMonitor();
	Monitor::resetThreadMonitor();
	
	//// wait until coordinator sends start signal
	{
		
		unique_lock<mutex> lock(Coordinator::waitMutex);
		++MasterCoordinator::CoordinatorReady;
		Coordinator::waitCondVar.wait(lock, [] { return Coordinator::waitCond; });
	}
	#ifdef PRINT_THREAD_MESSAGES
	info( BLUE <<"is starting on CPU " << cpuInfo.logicalId << ".");
	#endif
//	debugh("Coordinator starts with " << jobCnt << " jobs.");
	/// run while there are waiting operators left
//	while(!linkedOperators.empty()){
	while(!terminateCondition()){
		processIncomingJobsBuffer();
		processIncomingGlobalResultsBuffer();
//		debug("Loop finished")
//		sleep(3);

//		if(terminateCondition()) { break; }
//		Job jobReceiver;
//		while(incomingJobsBuffer.try_dequeue(jobReceiver)){
//			for(uint64_t outPos = 0; outPos < jobReceiver.op.outSize; ++outPos){
//				processResultColumn(
//						jobReceiver.op.out_ColumnMeta[outPos]->cid,
//						jobReceiver.partId,
//						jobReceiver.op.out[outPos]
//				);
//			}
//		}
//		ThreadManager::wait(1000 * 1000 * 1000);
	}
	
	debugh("terminate")
	
	terminateWorkers();
	waitForWorkerThreads();
	if(Monitor::threadMonitor->getDuration(MonitoringType::cProcessBuffer).count() > 0) {
		Monitor::threadMonitor->transmit(Monitor::subRunMonitor, MonitoringType::cProcessBuffer);
		Monitor::threadMonitor->transmit(Monitor::subRunMonitor, MonitoringType::cProcessGlobalBuffer);
	}
	if(Monitor::threadMonitor->getDuration(MonitoringType::Test).count() > 0) {
		Monitor::threadMonitor->transmit(Monitor::subRunMonitor, MonitoringType::Test);
		Monitor::threadMonitor->transmit(Monitor::subRunMonitor, MonitoringType::Test2);
	}
	#ifdef PRINT_THREAD_MESSAGES
	info( BLUE <<"is terminating on CPU " << cpuInfo.logicalId << ".");
	#endif
}



/**
 * This method transfers finished jobs received by workers from the unsorted buffer
 * into a sorted map.
 * @threadsafe
 */
void Coordinator::processIncomingJobsBuffer() {
	Job * jobPtr = nullptr;
	Monitor::threadMonitor->resume(MonitoringType::cProcessBuffer);
	while(incomingJobsBuffer.try_dequeue(jobPtr)){
//		debug("Dequeued job " << jobPtr->op->id << ":" << jobPtr->partId);
//		debug("Dequeued a job from incoming buffer: " << jobReceiver << " outSize:" << jobReceiver->op.outSize);
//		map<OperatorIdentifier, map<PartitionIndex, Operator>>::iterator jobList = jobsFinished.find(jobReceiver.op.id);
//		if(jobList == jobsFinished.end()){
//			jobList = jobsFinished.insert({jobReceiver.op.id, map<PartitionIndex, Operator>()}).first;
//		}
//		jobList->second.insert({jobReceiver.partId, jobReceiver.op});
		
		/// process result Columns of dequeued operator / pass result Columns to succeeding operators
		const PartitionIndex pid = jobPtr->partId;
//		debug("Process job of partition " << pid);
		for (int j = 0; j < jobPtr->op->outSize; ++j) {
			const ColumnIdentifier cid = jobPtr->op->out_ColumnMeta[j]->cid;
			column_t * const Column      = jobPtr->op->out[j];
			
//			debugh("================================================");
//			printLinkedOperators();
			processResultColumn(cid, pid, Column);
//			printLinkedOperators();
//			debugh("================================================");
		}
		--jobCnt;
//		debug("Check terminate Condintion / jobCnt=" << jobCnt << " linkedOperators.size=" << linkedOperators.size() );
		delete jobPtr;
		
	}
	Monitor::threadMonitor->end(MonitoringType::cProcessBuffer);
}



void Coordinator::processResultColumn(ColumnIdentifier cid, PartitionIndex pid, column_t * Column) {
//	Monitor::threadMonitor->resume(MonitoringType::Test);
//	debug("Process result colum / [CID:" << cid << "] [PID:" << pid << "] [Column:" << Column << "]");
	generatedColumns.push_back(Column);
	if(linkedOperators.find(cid) == linkedOperators.end()){
	    debugwarn("Skip column " << cid << " of part " << pid << " at " << Column);
		return;
	}
//	Monitor::threadMonitor->resume(MonitoringType::Test);
	vector<OperatorDependency*> * dependencies = &linkedOperators[cid];
	vector<OperatorDependency*> garbageCollection;
	
//	debug("Process column " << cid << " of part " << pid << " at " << Column);
	
	for(auto dependencyIt = dependencies->begin(); dependencyIt != dependencies->end();){
//	    debug("Dependency")
		OperatorDependency * dep = *dependencyIt;
		/// add Column to prerequisite of this operator to specified partition
		OperatorPrerequisite * prerequisite = dep->requiredPartitions[pid];
		if(!prerequisite){
			severe("prerequisite is nullptr");
		}
		(*prerequisite)[cid] = Column;
		
		/// check if next operator is node local
		if(dep->opScope == OperatorScope::Node){
			/// check if all prerequisites are fulfilled
			if(dep->isFulfilled()){
				OperatorWrapper * op = dep->op;
				/// fill up incoming Column pointer
				uint64_t size = dep->requiredPartitions.size();
				for(uint64_t inIndex = 0; inIndex < op->inSize; ++inIndex){
					if(dep->opPrerequisite->requisites.find(op->in_ColumnIdentifier[inIndex]) == dep->opPrerequisite->requisites.end())
						continue;
					column_t ** inColumns = new column_t*[size];
					uint64_t partPos = 0UL;
					for(auto& part : dep->requiredPartitions){
						inColumns[partPos] = part.second->requisites[op->in_ColumnIdentifier[inIndex]];
						column_t * col = part.second->requisites[op->in_ColumnIdentifier[inIndex]];
						++partPos;
					}
					op->in[inIndex] = (column_t*) inColumns;
				}
				
				/// AGG_SUM_MERGE specific
				/// todo: generalize necessary?
				op->val[0] = static_cast<int64_t>(size);
				/// enqueue operator
				
	//			debugh("enqueue global operator");
				globalOperatorQueue.enqueue(new Job(new OperatorWrapper(*op), PartitionIndex(0)));
				
				++jobCnt;
				garbageCollection.push_back(dep);
				
			}
		}
		
		
		/// check if all prerequisites are apply, if true: enqueue operator to specified partition
		if(prerequisite->isFullfilled()){
			OperatorWrapper * op = dep->op;
			/// set collected Column pointer into operator before dispatching
			for(uint64_t inIndex = 0; inIndex < op->inSize; ++inIndex){
				auto found = prerequisite->requisites.find(op->in_ColumnIdentifier[inIndex]);
				if(found != prerequisite->requisites.end()) {
					op->in[inIndex] = prerequisite->requisites[op->in_ColumnIdentifier[inIndex]];
				}
			}
			
//			debug("Dependency fullfilled / scope: " << dep->opScope);
			/// scope of an operator defines where to send it to
			switch(dep->opScope){
				/// send operator to a target partition
				case OperatorScope::Partition:
					distributeOperator(*op, pid);
					break;
					/// send operator to a node local queue
				case OperatorScope::Node:
					severe("Node scope not implemented yet.")
					break;
					/// send operator to a global queue on a predefined coordinator
				case OperatorScope::Global:
					for(auto& req : prerequisite->requisites){
						ColumnIdentifier p_cid = req.first;
						column_t * p_Column = req.second;
						dep->targetCoordinator->reportGlobalResult(p_cid, pid, p_Column);
					}
//					severe("Global scope not implemented yet.")
					break;
			}
			/// delete specified partition from target queue
			dep->requiredPartitions.erase(pid);
			/// check if dependency queue is depleted, if true: delete it
			if(dep->requiredPartitions.empty()) {
				garbageCollection.push_back(dep);
				/// delete from current Column
//				dependencyIt = dependencies->erase(dependencyIt);
				/// purge remaining links to this dependency from other Columns
//				purgeOperatorDependency(*dependencyIt);
				/// if this Column id is not required anymore, delete it as well
//				if(dependencies->empty()){
//					linkedOperators.erase(cid);
//					return;
//				}
			}
		}
		++dependencyIt;
	}
//	Monitor::threadMonitor->end(MonitoringType::Test);
	for(auto& dependency : garbageCollection){
		purgeOperatorDependency(&linkedOperators, dependency);
	}
//	Monitor::threadMonitor->end(MonitoringType::Test);
//    debug("Go out of processResultColumn();")
}



void Coordinator::processIncomingGlobalResultsBuffer() {
	tuple<ColumnIdentifier, PartitionIndex, column_t*> result;
//	debug("process incoming global results buffer")
	Monitor::threadMonitor->resume(MonitoringType::cProcessGlobalBuffer);
	while(incomingGlobalResultsBuffer.try_dequeue(result)){
		ColumnIdentifier cid = get<0>(result);
		PartitionIndex   pid = get<1>(result);
		column_t *      Column = get<2>(result);
//		debug("Process global result " << cid << " " << pid)
//		generatedColumns.push_back(Column);
		processGlobalResultColumn(cid, pid, Column);
	}
	Monitor::threadMonitor->end(MonitoringType::cProcessGlobalBuffer);
}



void Coordinator::processGlobalResultColumn(ColumnIdentifier cid, PartitionIndex pid, Coordinator::column_t * Column) {
//	debugh("Proces global Result/ [CID:" << cid << "] [PID:" << pid << "] [Column:" << Column << "]");
	if(globalLinkedOperators.find(cid) == globalLinkedOperators.end()){
		warn("Column with ID " << cid << " partition " << pid << " not found in pending queue.");
		return;
	}
	
	vector<OperatorDependency*> * dependencies = &globalLinkedOperators[cid];
	vector<OperatorDependency*> garbageCollection;
	for(auto dependencyIt = dependencies->begin(); dependencyIt != dependencies->end();++dependencyIt){
		OperatorDependency * dep = *dependencyIt;
		/// add Column to prerequisite of this operator to specified partition
		OperatorPrerequisite * prerequisite = dep->requiredPartitions[pid];
		if(!prerequisite){
			severe("prerequisite is nullptr");
		}
		(*prerequisite)[cid] = Column;
		
		
//		debug("Requirements:");
//		for(auto& part : dep->requiredPartitions){
//			stringstream ss;
//			for(auto& col : part.second.requisites)
//				ss << col.first << " ";
//			debug("  Part:" << part.first << " is fullfilled?: " << part.second.isFullfilled() << " CIDs: " << ss.str());
//		}


		if(dep->isFulfilled()){
			OperatorWrapper * op = dep->op;
			/// todo dispatch global operator
			/// fill up incoming Column pointer
			uint64_t size = dep->requiredPartitions.size();
			for(uint64_t i = 0; i < op->inSize; ++i){
				if(dep->opPrerequisite->requisites.find(op->in_ColumnIdentifier[i]) == dep->opPrerequisite->requisites.end())
					continue;
				column_t ** inColumns = new column_t*[size];
				uint64_t partPos = 0UL;
				for(auto& part : dep->requiredPartitions){
					inColumns[partPos] = part.second->requisites[op->in_ColumnIdentifier[i]];
					column_t * col = part.second->requisites[op->in_ColumnIdentifier[i]];
//					debug(GREEN << "Append Column [ID:" << col->getMeta()->cid <<" / Part:" << part.first << "  / Addr:" << col << "]  expected cid: " << op->in_ColumnIdentifier[i]);
					++partPos;
				}
				op->in[i] = (column_t*) inColumns;
			}
			
			/// AGG_SUM_MERGE specific
			/// todo: generalize necessary?
			op->val[0] = static_cast<int64_t>(size);
			/// enqueue operator
			
//			debugh("enqueue global operator");
			#ifdef ENQUEUE_JOB_POINTER
			globalOperatorQueue.enqueue(new Job(new OperatorWrapper(*op), PartitionIndex(0)));
		    #else
			globalOperatorQueue2.enqueue(Job(*op, PartitionIndex(0)));
			#endif
			
			++jobCnt;
			garbageCollection.push_back(dep);
		}
	}
	for(auto& dependency : garbageCollection){
		purgeOperatorDependency(&globalLinkedOperators, dependency);
	}
}



