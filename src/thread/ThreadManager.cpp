/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "ThreadManager.h"

#include "Worker.h"
#include "Coordinator.h"
#include <platform/Platform.h>
#include "NodeAllocationFence.h"

//// statics
thread_local NodeIdentifier ThreadManager::currentExecutionNode = NodeIdentifier::createInvalidNodeIdentifier();
thread_local NodeIdentifier ThreadManager::currentAllocationNode = NodeIdentifier::createInvalidNodeIdentifier();

thread_local std::map<uint64_t, size_t> ThreadManager::memoryMappings = std::map<uint64_t, size_t>();

thread_local std::string ThreadManager::threadName = "    ";
/**
 * @brief Pins a worker instance to its specified cpu.
 *
 * The worker instance hold a CPUInfo object, which contains the physical & logical cpu id,
 * it is assigned to. This information is used to pin the worker thread to the correct cpu.
 * @param worker Pointer to the worker instance.
 */
void ThreadManager::pinWorker(Worker * worker) {
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(worker->getCpuInfo()->logicalId, &cpuset);
	int rc = pthread_setaffinity_np(worker->getThread()->native_handle(), sizeof(cpu_set_t), &cpuset);
	if (rc != 0) {
		delete worker->getThread();
		severe("Error calling pthread_setaffinity_np: " << rc);
	}
}


void ThreadManager::pinThread(std::thread * thread, CPUInfo * cpuInfo) {
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(cpuInfo->logicalId, &cpuset);
	int rc = pthread_setaffinity_np(thread->native_handle(), sizeof(cpu_set_t), &cpuset);
	if (rc != 0) {
		delete thread;
		severe("Error calling pthread_setaffinity_np: " << rc);
	}
}



void ThreadManager::bindToNode(NodeIdentifier* node) {
#   ifdef USE_LIBNUMA
	if(Platform::isNuma()) {
		alloc("Switch thread to node " << node->getUNI())
		numa::nodemask_t nodemask;
		nodemask_zero(&nodemask);
		nodemask_set_compat(&nodemask, node->getPhysicalNode());
		numa_bind_compat(&nodemask);
	}
#   endif
	currentExecutionNode = *node;
	currentAllocationNode = *node;
}

/**
 * @brief Unblocks all waiting worker threads.
 */
void ThreadManager::unblockWorkerThreads() {
	{
		std::lock_guard<std::mutex> lock(Worker::waitMutex);
		Worker::waitCond = true;
	}
	Worker::waitCondVar.notify_all();
}

void ThreadManager::unblockCoordinatorThreads() {
	{
		std::lock_guard<std::mutex> lock(Coordinator::waitMutex);
		Coordinator::waitCond = true;
	}
	Coordinator::waitCondVar.notify_all();
}

[[nodiscard]]
NodeAllocationFence ThreadManager::directAllocationToNode(Node * node) {
	return ThreadManager::directAllocationToNode(node->getIdentifier());
}
[[nodiscard]]
NodeAllocationFence ThreadManager::directAllocationToNode(NodeIdentifier * nodeID) {
//	return ThreadManager::switchNode(nodeID->getPhysicalNode());
	return NodeAllocationFence(*nodeID);
}

std::string ThreadManager::checkMemoryConfiguration() {
	#ifdef USE_LIBNUMA
	numa::nodemask_t nodemask = numa::numa_get_membind_compat();
	std::stringstream s;
	s << "[";
	for(auto node : *Platform::getSockets())
		if (nodemask_isset_compat(&nodemask, node->getPhysicalSocket()))
			s << node->getPhysicalSocket();
		else
			s << " ";
	s << "]";
	return s.str();
	#else
	return "[0123]";
	#endif
}

void ThreadManager::waitMS(unsigned long waitTime) {
	std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));
}

void ThreadManager::waitS(unsigned long waitTime) {
	std::this_thread::sleep_for(std::chrono::seconds(waitTime));
}

void ThreadManager::waitUS(unsigned long waitTime) {
	std::this_thread::sleep_for(std::chrono::microseconds(waitTime));
}

void ThreadManager::wait(unsigned long waitTime) {
	std::this_thread::sleep_for(std::chrono::nanoseconds(waitTime));
}

bool ThreadManager::isAllocator(NodeIdentifier * node) {
	return currentAllocationNode == *node;
}

void ThreadManager::printAllocationPolicy() {
  #ifdef USE_LIBNUMA
	alloc("[ThreadManger] Current : " << ThreadManager::currentAllocationNode << " / NUMA nodemask : " << bitset<8>(numa_get_membind_compat().n[0]))
  #endif
}


/**
 * This method sets the current allocation and execution pointers to node 0 (zero). By default, this is the starting node.
 * All numa aware memory allocations are directed to this node, if not modified (e.g. by a NodeAllocationFence).
 */
void ThreadManager::setup() {
    Platform::setup();
    ThreadManager::currentAllocationNode = *((*Platform::getNodeIds())[0]);
    ThreadManager::currentExecutionNode  = *((*Platform::getNodeIds())[0]);
}


#ifdef USE_LIBNUMA
std::ostream& operator<<(std::ostream & os, const numa::bitmask mask) {
	os << "<bitmask>(size:" << mask.size << ",maskp:" << std::bitset<64>(mask.maskp[0]) << ") size of maskp:" << sizeof(mask.maskp) / sizeof(mask.maskp[0]);
	return os;
}

std::ostream & operator<<(std::ostream & os, const numa::nodemask_t mask) {
	unsigned long size = NUMA_NUM_NODES/(sizeof(unsigned long)*8);
	os << "<nodemask_t>(size:" << size;
	for(unsigned long i = 0; i < size; i++){
		os << "," << std::bitset<64>(mask.n[i]);
	}
	os << ")";
	return os;
}

//void* operator new (size_t size){
//	void* ptr = numa_alloc_onnode(size, ThreadManager::currentExecutionNode.getPhysicalNode());
//	ThreadManager::memoryMappings[(uint64_t) ptr] = size;
//	return ptr;
//}
//
//void operator delete(void * ptr) {
//	numa_free(ptr, ThreadManager::memoryMappings[(uint64_t) ptr]);
//}

#endif

