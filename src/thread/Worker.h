/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class Worker;

#ifndef WORKER_H
#define WORKER_H


#include "platform/CPUInfo.h"
#include "task/Job.h"
#include "Coordinator.h"

class Database;

class Worker {
	friend class Coordinator;
  public:
	Worker(Coordinator * coordinator, CPUInfo & cpuInfo);
	Worker(const Worker & orig);
	virtual ~Worker();
	
	void* operator new(size_t size);
	void operator delete(void* p);
	
	void init();
//	void run_old();
	void run();
	void join();
	void processGlobalQueue();
	
	std::thread * getThread();
	WorkerIdentifier getId();
	const CPUInfo * getCpuInfo();
//	Job_old * getCurrentJob();
    static uint64_t workerCnt;
	
  private:
	bool findJob();
	void loop();
  
  
  public:
    //// Pointer to the worker instance of this thread
	static thread_local Worker * currentWorker;

	//// Mutex for boolean waitCond
    static std::mutex waitMutex;
    //// Condition variable to block/unblock worker threads
	static std::condition_variable waitCondVar;
	//// Condition to block/unblock worker threads
	static volatile bool waitCond;
	
	unsigned waitCnt = 0;
	//// shadow pointer to the job counter of this workers coordinator
    std::atomic<uint64_t>* jobCnt;
    unsigned long result = 0UL;
    unsigned long* buffer;
    bool terminate = false;
  
  private:
    //// Incrementing counter to assign an ID to new worker instances
	static unsigned nextId;

    //// Pointer to this workers coordinator
	Coordinator * coordinator;
	//// Pointer to the workers thread
	std::thread * thread = nullptr;
	//// Workers id
	WorkerIdentifier id;
	//// Information about the core this worker is assigned to
	CPUInfo cpuInfo;

	//// Pointer to the partition list of local node
//	std::vector<Partition *> * partitions = nullptr;
	//// Pointer to the database of local node
	Database* database = nullptr;

	//// Pointer to the partition the worker is currently working on
	Partition * currentPartition = nullptr;
	//// Stores the currently executed job
//	Job_old currentJob;


    //// Offset in partition list to store last position
    unsigned partitionOffset = 0;
    
    
};

#endif /* WORKER_H */

