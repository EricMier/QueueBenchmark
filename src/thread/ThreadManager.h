/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class ThreadManager;

#ifndef QUEUEBENCHMARK_THREADMANAGER_H
#define QUEUEBENCHMARK_THREADMANAGER_H

//#include "includesForHeader.h"
#include <platform/NodeIdentifier.h>
#include <stdlibs>
#include <forward>
#include <libnuma>
  #include "Worker.h"
  #include "NodeAllocationFence.h"
  #include "platform/Node.h"

class ThreadManager {
public:
    // Constructor
    ThreadManager() = default;

    // Destructor
    virtual ~ThreadManager() = default;
    
    static void pinWorker(Worker * worker);
    static void pinThread(std::thread * thread, CPUInfo * cpuInfo);
//	static NodeAllocationFence directAllocationToNode(int node);
	static NodeAllocationFence directAllocationToNode(Node* node);
	static NodeAllocationFence directAllocationToNode(NodeIdentifier* nodeID);
//	static void bindToNode(int node);
	static void bindToNode(NodeIdentifier* node);
	static void unblockWorkerThreads();
	static void unblockCoordinatorThreads();
	static std::string checkMemoryConfiguration();

	static void waitS(unsigned long waitTime);
	static void waitMS(unsigned long waitTime);
	static void waitUS(unsigned long waitTime);
	static void wait(unsigned long waitTime);
	
	static bool isAllocator(NodeIdentifier * node);
	
	static void printAllocationPolicy();
	
	static thread_local NodeIdentifier currentExecutionNode;
	static thread_local NodeIdentifier currentAllocationNode;
	static thread_local std::string threadName;
	static thread_local std::map<uint64_t , size_t> memoryMappings;
	
	static std::mutex printingLock;
	
	static void setup();
private:

};

#ifdef USE_LIBNUMA
std::ostream & operator<<(std::ostream & os, const numa::bitmask mask);
std::ostream & operator<<(std::ostream & os, const numa::nodemask_t mask);
#endif

//void* operator new (size_t size);
//void operator delete(void* ptr);

#endif //QUEUEBENCHMARK_THREADMANAGER_H
