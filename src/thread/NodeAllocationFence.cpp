/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

#include "NodeAllocationFence.h"
#include <utility>
#include <utils>

//// statics
unsigned long NodeAllocationFence::counter = 0UL;

//NodeAllocationFence::NodeAllocationFence() {
//    #ifdef USE_LIBNUMA
//      #ifdef CONTROL_NUMA_POLICY
//        if(Platform::isNuma()) {
//            prevNodeId = ThreadManager::currentAllocationNode;
//        }
//      #endif
//    #endif
//}


NodeAllocationFence::NodeAllocationFence(NodeIdentifier& node) : NodeAllocationFence(node, "") {
    //
}

NodeAllocationFence::NodeAllocationFence(NodeIdentifier& node, std::string caller)  : caller(std::move(caller))  {
    fenceID = counter++;
    #ifdef USE_LIBNUMA
      #ifdef CONTROL_NUMA_POLICY
        if(Platform::isNuma()) {
            if(node != ThreadManager::currentAllocationNode) {
                alloc("Allocation change [" << ThreadManager::currentAllocationNode.getUNIs() << "->"
                      << node.getUNIs() << "] by NodeAllocationFence create.   [fence:"
                      << str_lfill(getFenceID(), 4, "0") << "]"
                      << (caller.empty() ? "" : " [caller:" + caller + "]")
                );
                currentNodeId = node;
                prevNodeId = ThreadManager::currentAllocationNode;
                ThreadManager::currentAllocationNode = node;
                setAllocationPolicy(node);
                activeFence = true;
            }
//				//// store old node mask
//				prevNodemask = numa_get_membind_compat();
//				//// create new node mask
//				nodemask_t nodemask;
//				nodemask_zero(&nodemask);
//				nodemask_set_compat(&nodemask, node.getPhysicalNode());
//				//// set memory policy
//				numa_set_membind_compat(&nodemask);
        
        }
      #else
        debug("NodeAllocationFence to node " << node << " without effect.")
      #endif
    #else
        debug("NodeAllocationFence to node " << node << " without effect.")
    #endif
}

/**
 * @brief Destructor of the memory allocation fence. Restores previous policy.
 */
NodeAllocationFence::~NodeAllocationFence() {
    #ifdef USE_LIBNUMA
      #ifdef CONTROL_NUMA_POLICY
    if (Platform::isNuma()) {
        //// restore pre-fence node memory allocation policy
        if (activeFence) {
            alloc("Allocation change [" << ThreadManager::currentAllocationNode.getUNIs() << "->"
                                        << prevNodeId.getUNIs() << "] by NodeAllocationFence destroy.  [fence:"
                                        << str_lfill(getFenceID(), 4, "0") << "]"
                                        << (caller.empty() ? "" : " [caller:" + caller + "]"));
            setAllocationPolicy(prevNodeId);
            ThreadManager::currentAllocationNode = prevNodeId;
        }
//				numa_set_membind_compat(&prevNodemask);
//				ThreadManager::currentAllocationNode = prevNodeId;
    }
      #endif
    #endif
}

/**
 * Assign operator to override current NodeAllocationFence.
 * Usage:
 * NodeAllocationFence fence;
 * fence = NodeAllocationFence(...);
 * @param rhs
 * @return
 */
NodeAllocationFence & NodeAllocationFence::operator =(NodeAllocationFence && rhs) noexcept {
    //// if rhs fence is no active fence, do nothing
    if(!rhs.activeFence){
        return *this;
    }
    //// deactivate rhs fence to disable its destructor
    rhs.activeFence = false;
    
    //// if same, do nothing
    if(currentNodeId == rhs.currentNodeId) {
        debug2("New fence directs allocations to same node. Ignore. [rhs:fence:" << rhs.getFenceID() << "]");
        return *this;
    }
    //// when overriding inactive fences
    activeFence = true;
    //// else copy new node id
    alloc("Allocation change [" << rhs.prevNodeId.getUNIs() << "->"
          << rhs.currentNodeId.getUNIs() << "] by NodeAllocationFence override. [fence:"
          << str_lfill(getFenceID(), 4, "0") << "<-"
          << rhs.getFenceID() << "]"
          << (caller.empty() ? "" : "[caller:" + caller + "]")
    );
    currentNodeId = rhs.currentNodeId;
    fenceID = rhs.fenceID;
    caller = rhs.caller;
    //// lhs remains an active fence and keps its previous node id
    return *this;
}

std::string NodeAllocationFence::getFenceID() {
	return (fenceID == ((unsigned long) -1)) ? RED + "invalid" + GRAY : std::to_string(fenceID);
}

void NodeAllocationFence::setAllocationPolicy(const NodeIdentifier& node) {
    if(node.isValid()) {
        #ifdef USE_LIBNUMA
        #ifdef CONTROL_NUMA_POLICY
        //// create new node mask
        numa::nodemask_t nodemask;
        nodemask_zero(&nodemask);
        nodemask_set_compat(&nodemask, node.getPhysicalNode());
        //// set memory policy
        numa_set_membind_compat(&nodemask);
        #endif
        #endif
    } else {
        warn("Try to set allocator to an invalid node.");
    }
}
