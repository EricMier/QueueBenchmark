/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Partition.h
 * Author: EricMier
 *
 * Created on 1. August 2019, 14:52
 */


#include <core/memory/DefaultAllocator.h>
///// forward declaration
class Partition;

#ifndef PARTITION_H
#define PARTITION_H

#include "util/Identifier.h"
#include "task/Job.h"
#include "concurrentqueue/concurrentqueue.h"
#include "Column.h"

#include <forward>

#include <core/storage/column.h>
#include <core/memory/DefaultAllocator.h>




//    //// todo: delete this
//    struct PartitionMetadata {
//        int partitionIndex;
//        /// todo: durch TableIdentifier ersetzen
//        std::string table;
//
//
//        PartitionMetadata(int partitionIndex, std::string table) : partitionIndex(partitionIndex), table(table) {};
//
//        //// hotfix
//        PartitionMetadata() {};
//    };
/**
 * Some Desc
 * @tparam TAllocator
 */
    class Partition {
      public:
//        Partition(unsigned long wait, PartitionMetadata & pMeta);
//        Partition(unsigned long wait, PartitionMetadata && pMeta);
        
//        Partition(
//          std::vector<unsigned long> * data,
//          unsigned long dataOffset,
//          unsigned long dataSize,
//          PartitionMetadata & pMeta
//        );
//        Partition(
//          std::vector<unsigned long> * data,
//          unsigned long dataOffset,
//          unsigned long dataSize,
//          PartitionMetadata && pMeta
//        );
        
        Partition(const Partition & orig) = delete;
        ~Partition();
        
//        std::vector<unsigned long> * getData();
//        unsigned long getOffset();
//        unsigned long getSize();
        
//        std::vector<unsigned long>::iterator begin();
//        std::vector<unsigned long>::iterator end();
        
        
        void print(std::vector<ColumnIdentifier> * cids);
//        PartitionMetadata * getMeta();
        
        bool tryLock();
        void unlock();
        
        /// vars
//        unsigned long waitSum = 0;
//        moodycamel::ConcurrentQueue<Job_old> queue_old;
      
      private:
//        PartitionMetadata partitionMetadata;
        
//        std::vector<unsigned long> * data = nullptr;
//        unsigned long dataOffset = 0;
//        unsigned long dataSize = 0;
        
        
//        #ifdef MICROBENCHMARK
//        unsigned long wait;
//        #else
//        std::map<std::string, Column> columns;
//        #endif // ifdef MICROBENCHMARK
        std::atomic_flag lockFlag = ATOMIC_FLAG_INIT;
        
        
        //// new
        
        /// @todo later??
//        using column_t = std::variant<morphstore::column<morphstore::uncompr_f> *>;
        using column_t = morphstore::column<morphstore::uncompr_f>;
      
      public:
        Partition(PartitionIndex partitionIndex);
        Partition(PartitionIndex partitionIndex, unsigned long size, unsigned long tolerance);
        void addColumn(column_t * column);
        
        PartitionIndex getPartitionIndex();
//        void insert(GenericRow * row, std::vector<ColumnIdentifier> * cids);
        
        bool isFull();
        bool empty();
        unsigned long freeRowsLeft();
        
        void addColumn(ColumnIdentifier cid, column_t * column);
        column_t * getColumn(std::string name);
        column_t * getColumn(ColumnIdentifier cid);
        identifier_map<ColumnIdentifier, column_t *> * getColumns();
        
//        void enqueue(Job & job);
//        void enqueue(Job && job);
//        bool try_dequeue(Job & jobReceiver);
        void enqueue(Job * job);
        bool try_dequeue(Job *& jobReceiver);
        
        void* operator new(size_t size);
        void operator delete(void* p);
      
      
      private:
        identifier_map<ColumnIdentifier, column_t *> columns;
        PartitionIndex partitionIndex = PartitionIndex::createInvalid();
        
        unsigned long currentSize = 0UL;
        unsigned long maxSize = 0UL;
        unsigned long minSize = 0UL;
        unsigned long desiredSize = 0UL;
        
        moodycamel::ConcurrentQueue<Job *> queue;
//        moodycamel::ConcurrentQueue<Job> queue2;
        
    };

#endif /* PARTITION_H */

