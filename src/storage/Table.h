/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Table.h
 * Author: EricMier
 *
 * Created on 2. August 2019, 16:27
 */

class Table;
//struct TableEntry;

#ifndef MORPHSTORE_PROTOTYPE_STORAGE_TABLE_H
#define MORPHSTORE_PROTOTYPE_STORAGE_TABLE_H

#include <partitioning/TablePartitioningPolicy.h>
#include "abbreviate/forward"
#include "utils"
#include "GlobalColumnMetainformation.h"
#include "Partition.h"
#include "partitioning/TableInsertHelper.h"
#include "GenericRow.h"
  #include <core/memory/DefaultAllocator.h>


class TablePartitioningPolicy;
class PartitionIndexProducer;

//struct TableEntry {
//	std::string name;
//	Table* ptr;
//
//	TableEntry(std::string& name, Table* ptr): name(name), ptr(ptr) {}
//	TableEntry(std::string&& name, Table* ptr): name(name), ptr(ptr) {}
//};


//struct TableMetainformation {
//	string tablename;
//	vector<string> columns;
//	NodeIdentifier nodeID = NodeIdentifier::createInvalidNodeIdentifier();
//	vector<TableMetainformation> siblings;
//	Table* linkedTable;
//};

class Table {
	friend class Test;
	friend class PrototypeEngine;
	using column_t = morphstore::column<morphstore::uncompr_f>;
  public:
	Table(std::string& name, TableIdentifier & tableIdentifier, TablePartitioningPolicy * partitioningPolicy, NodeIdentifier & nodeIdentifier,
	      PartitionIndexProducer * partitionIndex, TableInsertHelper * insertHelper);
	Table(std::string&& name, TableIdentifier & tableIdentifier, TablePartitioningPolicy * partitioningPolicy, NodeIdentifier & nodeIdentifier,
	      PartitionIndexProducer * partitionIndex, TableInsertHelper * insertHelper);
	Table(const Table& orig) = delete;
	~Table();
	
	const identifier_map<PartitionIndex, Partition*> * getPartitions() const;
	const std::string & getName();
	
	Partition* operator[](PartitionIndex partitionIndex);
	
	
	void linkDatabase(Database* database);
	void linkTableToAllSiblings(Table* table);
	const NodeIdentifier* getNodeIdentifier();
	
//	void addColumn(ColumnMetainformation column);
	void addColumn(std::string name, BaseType type);
	void addPartitions(unsigned long amount);
	Partition * getPartition(PartitionIndex partitionIndex);
	Partition * findPartition(PartitionIndex partitionIndex);
	
	void insert(GenericRow * row, std::vector<ColumnIdentifier> * cids);
	
	void printPartition(PartitionIndex partitionIndex, std::vector<ColumnIdentifier> * cids);
	void printTables();
	ColumnIdentifier getColumnID(std::string columnName);
	GlobalColumnMetainformation * getColumnInfo(ColumnIdentifier & cid);
	TableIdentifier & getId();
	std::map<NodeIdentifier, Table*> const * getSiblings();
	
	
	void* operator new(size_t size);
	void operator delete(void* p);
	
	Partition* createNewPartition(NodeIdentifier nodeID);
	void registerColumn(GlobalColumnMetainformation information);
	Table * getTableOnNode(NodeIdentifier nodeID);
	
	void print();
  private:
	void linkTable(Table* table);
	void appendColumn(GlobalColumnMetainformation column);
	void addExistingColumnsToPartition(Partition * partition);
	
	std::string name;
	TableIdentifier tableIdentifier;
	identifier_map<PartitionIndex, Partition*> partitions;
//	vector<unsigned long> partitionIds;
//	TableMetainformation metaInformation;
	
	TablePartitioningPolicy* partitioningPolicy;
	Database* linkedDatabase = nullptr;
	NodeIdentifier nodeIdentifier;
	std::map<NodeIdentifier, Table*> siblings = {};
	
	std::map<std::string, ColumnIdentifier> columnNames;
	std::map<ColumnIdentifier, GlobalColumnMetainformation> columns;
	
	PartitionIndexProducer * partitionIndexProducer = nullptr;
//	TableInsertHelper * insertHelper;
	Partition* currentPartition;
};

#endif /* MORPHSTORE_PROTOTYPE_STORAGE_TABLE_H */

