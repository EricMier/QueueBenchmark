/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Partition.cpp
 * Author: EricMier
 * 
 * Created on 1. August 2019, 14:52
 */


#include "Partition.h"
#include <storage>
#include "task/Job.h"

    
//    Partition::Partition(unsigned long wait, PartitionMetadata & pMeta) : wait(wait), partitionMetadata(pMeta) {}
//
//    Partition::Partition(unsigned long wait, PartitionMetadata && pMeta) : wait(wait), partitionMetadata(pMeta) {
//    }
//
//
//    Partition::Partition(
//      vector<unsigned long> * data, unsigned long dataOffset, unsigned long dataSize,
//      PartitionMetadata & pMeta
//    ) : data(data), dataOffset(dataOffset), dataSize(dataSize),
//        partitionMetadata(pMeta) {}
//
//    Partition::Partition(
//      vector<unsigned long> * data, unsigned long dataOffset, unsigned long dataSize,
//      PartitionMetadata && pMeta
//    ) : data(data), dataOffset(dataOffset), dataSize(dataSize),
//        partitionMetadata(pMeta) {}
    
    
    Partition::~Partition() {
        //// @todo: fix this
//        #ifdef USE_PHYSICAL_PARTITIONING
//        delete data;
//        #endif
//	debugh("Delete Partition");
//        for (auto &[cid, column] : columns) {
////		column->~Column();
//            __destruct(column, Column);
//        }
    }

    
    
    void Partition::print(vector<ColumnIdentifier> * cids) {
        /*
//	info("Print Partition #" << partitionIndex)
        unsigned colLength = 15;
        column_t * cols[cids->size()];
        for (uint64_t j = 0; j < cids->size(); ++ j) {
            cols[j] = columns[cids->at(j)];
        }
        info("Print Partition #" << partitionIndex << "\n" << printColumnsAsTable(cols, cids->size()));
        return;

//	vector<Column*> cols;
//	for(auto c : columns){
//		cols.push_back(c.second);
//	}
        
        for (auto cid : *cids) {
            cout << "|" << str_lfill(" " + columns[cid]->getMeta()->name + " =", colLength, "=");
        }
        cout << "|" << endl;

//	unsigned i = 0;
//	for(auto cid : *cids){
//		Table* table = Database::getLocalDatabase()->getTable("Customer");
//		string tname = table->getColumnInfo(cid)->name;
//		cout << i++ << ": " << cid << " / " << columns[cid]->getMeta()
//		<< " // orignial from table : " << tname << "[" << table->getColumnInfo(cid) << "]" <<endl;
//	}
        
        for (unsigned long i = 0; i < currentSize; i ++) {
            cout << "|";
            for (auto cid : *cids) {
                column_t * column = columns[cid];
                stringstream ss;
                switch (column->getMeta()->type) {
                    case BaseType::uint64:
                        ss << ((uint64_t *) column->getData())[i];
                        break;
                    case BaseType::uint32:
                        break;
                    case BaseType::int64:
                        break;
                    case BaseType::int32:
                        break;
                    case BaseType::string:
                        break;
                    case BaseType::float32:
                        break;
                    case BaseType::double64:
                        break;
                    case BaseType::undefined:
                        break;
                }
                cout << str_lfill(ss.str(), colLength) << "|";
            }
            cout << endl;
        }
        cout << "current size: " << currentSize << endl;
        */
    }
    
//    PartitionMetadata * Partition::getMeta() {
//        return &partitionMetadata;
//    }
    
    
    bool Partition::tryLock() {
        return ! lockFlag.test_and_set();
    }
    
    
    void Partition::unlock() {
        lockFlag.clear();
    }
    
//    vector<unsigned long> * Partition::getData() {
//        return data;
//    }

//    unsigned long Partition::getOffset() {
//        return dataOffset;
//    }

//    unsigned long Partition::getSize() {
//        return dataSize;
//    }

//    vector<unsigned long>::iterator Partition::begin() {
//        return data->begin() + dataOffset;
//    }

//    vector<unsigned long>::iterator Partition::end() {
//        return data->begin() + dataOffset + dataSize;
//    }

//// new
Partition::Partition(PartitionIndex partitionIndex) : partitionIndex(partitionIndex), desiredSize(0), minSize(0), maxSize(0){

}

Partition::Partition(PartitionIndex partitionIndex, unsigned long size, unsigned long tolerance)
      : partitionIndex(partitionIndex) {
        desiredSize = size;
        minSize = size - tolerance;
        maxSize = size + tolerance;
    }
    
    
    void Partition::addColumn(column_t * column) {
        severe("Not implemented");
//        columns[column->getMeta()->cid] = column;
    }
    
    
    PartitionIndex Partition::getPartitionIndex() {
        return partitionIndex;
    }
    
    
//    void Partition::insert(GenericRow * row, vector<ColumnIdentifier> * cids) {
//        trace("[Partition::insert] " << *row)
//        debug2("[Partition::insert] " << *row)
//        if (row->columnCount() != columns.size()) severe(
//          "Inserted tuple row count does not match Partition row count. Incoming row count: "
//            << row->columnCount() << " vs partition row count: " << columns.size());
//
//        for (unsigned long col = 0; col < row->columnCount(); col ++) {
//            Column * column = columns.at(cids->at(col));
//            column->addValue(row->get<string>(col));
//        }
//        currentSize ++;
//    }
    
    
    bool Partition::isFull() {
        return currentSize >= maxSize;
    }
    
    
    bool Partition::empty() {
        return currentSize == 0UL;
    }
    
    
    unsigned long Partition::freeRowsLeft() {
        return maxSize - currentSize;
    }
    
    
    identifier_map<ColumnIdentifier, Partition::column_t *> * Partition::getColumns() {
        return &columns;
    }
    
//    void Partition::enqueue(Job & job) {
//        queue2.enqueue(job);
//    }

//    void Partition::enqueue(Job && job) {
//        queue2.enqueue(job);
//    }

//    bool Partition::try_dequeue(Job & jobReceiver) {
//        return queue2.try_dequeue(jobReceiver);
//    }
    
    
    void Partition::enqueue(Job * job) {
        queue.enqueue(job);
    }
    
    
    bool Partition::try_dequeue(Job *& jobReceiver) {
        return queue.try_dequeue(jobReceiver);
    }

void Partition::addColumn(ColumnIdentifier cid, Partition::column_t * column) {
    columns[cid] = column;
    currentSize = column->get_count_values();
    minSize = currentSize;
    maxSize = currentSize;
    desiredSize = currentSize;
}

Partition::column_t * Partition::getColumn(ColumnIdentifier cid) {
        auto it = columns.find(cid);
        if (it == columns.end()) {
            error("Column[" << cid << "] not found.");
            return nullptr;
        }
        return it->second;
    }

void * Partition::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(Partition));
}

void Partition::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Partition));
}
