/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class GenericRow;

#ifndef QUEUEBENCHMARK_GENERICROW_H
#define QUEUEBENCHMARK_GENERICROW_H

#include <stdlibs>

class GenericRow {
	std::vector<std::string> columns;
	
  public:
	void append(const std::string & column) {
		columns.push_back(column);
	}
	
	unsigned long columnCount() {
		return columns.size();
	}
	
	template<typename T> T get(unsigned long colIdx);
	
	const std::vector<std::string>& getColumns() const;
		

};

std::ostream& operator<<(std::ostream& os, const GenericRow& gr);

#endif //QUEUEBENCHMARK_GENERICROW_H
