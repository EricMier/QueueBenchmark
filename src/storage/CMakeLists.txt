foreach(target IN ITEMS ${TARGETS})
    target_sources(${target}
        PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/Database.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Database.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Partition.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Partition.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Table.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Table.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/GenericRow.h
        ${CMAKE_CURRENT_SOURCE_DIR}/GenericRow.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Column.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Column.cpp
)
endforeach(target)
