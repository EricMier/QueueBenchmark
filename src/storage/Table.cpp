/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Table.cpp
 * Author: EricMier
 * 
 * Created on 2. August 2019, 16:27
 */

#include <storage>
#include <utils>
#include <threading>

#include "partitioning/TableInsertHelper.h"
#include "Table.h"




/// statics
//uint64_t TableIdentifier::nextId = 0;
//mutex TableIdentifier::idLock;



Table::Table(std::string& name,
			 TableIdentifier & tableIdentifier,
			 TablePartitioningPolicy * partitioningPolicy,
			 NodeIdentifier & nodeIdentifier,
             PartitionIndexProducer * partitionIndex,
             TableInsertHelper * insertHelper)
             : name(name),
               tableIdentifier(tableIdentifier),
               partitioningPolicy(partitioningPolicy),
               nodeIdentifier(nodeIdentifier),
               partitionIndexProducer(partitionIndex)
{
	debug1("Create new Table (name: " << name << ")")
}



Table::Table(std::string&& name, TableIdentifier & tableIdentifier, TablePartitioningPolicy * partitioningPolicy, NodeIdentifier & nodeIdentifier,
             PartitionIndexProducer * partitionIndex, TableInsertHelper * insertHelper)
  : Table(name, tableIdentifier, partitioningPolicy, nodeIdentifier, partitionIndex, insertHelper) {
}


Table::~Table() {
//	debugh("Delete Table");
	for(auto & [pid, partition] : partitions){
//		partition->~Partition();
		__destruct(partition, Partition);
	}
	/// todo : delete objects behind pointers (belong to all table objects)
	/// todo : unlink from siblings
}



const identifier_map<PartitionIndex, Partition*> * Table::getPartitions() const {
	return &partitions;
}



Partition* Table::operator[](PartitionIndex partitionIndex){
	return partitions[partitionIndex];
}



const string & Table::getName() {
	return name;
}




//
//void Table::linkDatabase(Database * database) {
//	linkedDatabase = database;
//}


void Table::linkDatabase(Database * database) {
	linkedDatabase = database;
}



void Table::linkTableToAllSiblings(Table * table) {
//	const NodeIdentifier * oid = table->getNodeIdentifier();
//	info("Link Table on Node " << oid->getPhysicalNode() << " to Node " << nodeIdentifier.getPhysicalNode())
	//// link to siblings
//	map<NodeIdentifier, Table*>::iterator it = siblings.begin();
//	for(;it != siblings.end();it++){
//		it->second->linkTable(table);
////		it->second->siblings[*oid] = table;
////		table->siblings[it->first] = it->second;
//	}


	for( auto s : siblings )
		s.second->linkTable(table);
	//// link to self
	linkTable(table);
//	table->partitionIndex = partitionIndex;
//	table->insertHelper = insertHelper;
}


const NodeIdentifier * Table::getNodeIdentifier() {
	return &nodeIdentifier;
}


/**
 * Links this table with another one
 * @param table Table to link
 */

void Table::linkTable(Table * table) {
	debug2("Link Tables on Nodes " << nodeIdentifier.getPhysicalNode() << " - " << table->nodeIdentifier.getPhysicalNode())
	siblings[table->nodeIdentifier] = table;
	table->siblings[nodeIdentifier] = this;
}


void Table::addColumn(string name, BaseType type) {
	debug2("[Table::addColumn] Add Column \"" << name << "\"[" << type << "] to Table " << this->name << "[" << nodeIdentifier << "]")
	if(columnNames.find(name) != columnNames.end()){
		severe("Column \"" << name << "\" already exists.")
	}
	//// todo : each table -> own ColumnMetainformation
	GlobalColumnMetainformation column(name, type, ColumnIdentifier::createNew());
	appendColumn(column);
	for(auto s : siblings)
		s.second->appendColumn(column);
}


void Table::appendColumn(GlobalColumnMetainformation column) {
    severe("Not implemented");
    /*
	NodeAllocationFence fence(nodeIdentifier addCaller);
	debug2("[Table::appendColumn] Append column \"" << column.name << "\"[" << column.type << "] to Table " << name << "[" << nodeIdentifier << "]")
	columns[column.cid] = column;
	columnNames[column.name] = column.cid;
	
	for( identifier_map<const PartitionIndex, Partition*>::iterator part = partitions.begin(); part != partitions.end(); part++ ){
		part->second->addColumn(
			new column_t(&columns[column.cid],
			partitioningPolicy->partitionSize + partitioningPolicy->partitionSizeTolerance)
		);
	}
	*/
}


void Table::addPartitions(unsigned long amount) {
	NodeAllocationFence fence;
	debug("Add partitions: " << amount);
	for(unsigned long i = 0UL; i < amount; i++){
		NodeIdentifier node = partitioningPolicy->nextDispatchNode();
		fence = NodeAllocationFence(node addCaller);
		PartitionIndex partIndex = partitionIndexProducer->nextID();
		Partition * part = new Partition(partIndex, partitioningPolicy->partitionSize,
		                                 partitioningPolicy->partitionSizeTolerance);
		if(node == nodeIdentifier){
			partitions.insert(pair<PartitionIndex,Partition*>(partIndex,part));
			addExistingColumnsToPartition(part);
		} else {
			siblings[node]->partitions.insert(pair<PartitionIndex,Partition*>(partIndex,part));
			siblings[node]->addExistingColumnsToPartition(part);
		}
	}
}


void Table::addExistingColumnsToPartition(Partition * partition) {
    severe("Not implemented!");
//	for( auto it = columns.begin(); it != columns.end(); it++){
//		debug2("[Table::addExistingColumnsToPartition] Add Column " << it->first << " to partition " << partition->getPartitionIndex())
//		GlobalColumnMetainformation * cmi = &it->second;
//		partition->addColumn(new(__alloc(Column)) Column(cmi, partitioningPolicy->partitionSize + partitioningPolicy->partitionSizeTolerance));
//	}
}

/**
 * @brief For given PartitionIndex, returns a pointer to the associated partition, if located on the local table instance.
 * @param partitionIndex
 * @return Partition*
 */

Partition * Table::getPartition(PartitionIndex partitionIndex) {
	auto find = partitions.find(partitionIndex);
	if(find != partitions.end()){
		return find->second;
	}
	return nullptr;
//	...
//	auto it = find(partitionIds.begin(), partitionIds.end(), partitionIndex);
//	if(it != partitionIds.end()){
//		unsigned long index = static_cast<unsigned long>(distance(partitionIds.begin(), it));
//		return partitions.at(index);
//	}
//	return nullptr;
}


void Table::insert(GenericRow * row, vector<ColumnIdentifier> * cids) {
//	debug2("[Table::insert](" << *row << ")")
//	//// lookup partition to insert
//	if(insertHelper->currentPartition == nullptr){
//		insertHelper->currentPartition = findPartition(insertHelper->currentPartitionIndex);
//		if(insertHelper->currentPartition == nullptr) {
//			severe("Could not find partition with index " << insertHelper->currentPartitionIndex);
//		}
//	}
//	while(insertHelper->currentPartition->isFull()){
//		(*insertHelper)++;
//
//		if(insertHelper->currentPartitionIndex > partitionIndexProducer->highestId()){
//			error("TODO: Implement adding Partitions, if last Partition is full."
//					      << endl << insertHelper->currentPartitionIndex
//					      << endl << partitionIndexProducer->highestId());
//		}
//
//		insertHelper->currentPartition = findPartition(insertHelper->currentPartitionIndex);
//		if(insertHelper->currentPartition == nullptr)
//			severe("Could not find partition with index " << insertHelper->currentPartitionIndex);
//	}
//
//	insertHelper->currentPartition->insert(row, cids);
}

/**
 * @brief Tries to find the partition associated to given PartitionIndex. (global)
 * @param partitionIndex
 * @return Partition*
 */

Partition * Table::findPartition(PartitionIndex partitionIndex) {
	Partition * res = nullptr;
	NodeIdentifier * node = partitioningPolicy->predictLocationOfPartition(partitionIndex);
	if(*node == nodeIdentifier) {
		res = getPartition(partitionIndex);
	} else {
		res = siblings[*node]->getPartition(partitionIndex);
	}
	if(res != nullptr)
		return res;
	/// Backup: search all nodes
	if(*node != nodeIdentifier){
		res = getPartition(partitionIndex);
		if(res != nullptr)
			return res;
	}
	for( auto s : siblings ){
		if(*node != s.second->nodeIdentifier){
			res = s.second->getPartition(partitionIndex);
			if(res != nullptr)
				return res;
		}
	}
	lowwarn("Partition with partition index " << partitionIndex << " not found.")
	return nullptr;
}


void Table::printPartition(PartitionIndex partitionIndex, vector<ColumnIdentifier> * cids) {
	Partition * part = findPartition(partitionIndex);
	if(!part){
		error("Partition with Index " << partitionIndex << " not found!")
		return;
	}
	
	part->print(cids);
	
}


void Table::printTables() {
	print();
	for(auto t : siblings)
		t.second->print();
}


void Table::print() {
	info("Print Table " << name << " on Node " << nodeIdentifier << " (check: " << Platform::getNodeOfPointer(this) << ")");
	cout << "Partitions in this Table: ";
	for(auto partition = partitions.begin(); partition != partitions.end(); ++partition){
		cout << partition->first << ", ";
	}
	cout << endl;
}


ColumnIdentifier Table::getColumnID(string columnName) {
	if(columnNames.find(columnName) != columnNames.end())
		return columnNames[columnName];
	severe("Column name \"" + columnName + "\" not found.");
	return ColumnIdentifier::createInvalid();
}


GlobalColumnMetainformation * Table::getColumnInfo(ColumnIdentifier & cid) {
	return &columns[cid];
}


TableIdentifier & Table::getId() {
	return tableIdentifier;
}


map<NodeIdentifier, Table *> const * Table::getSiblings() {
	return &siblings;
}


void* Table::operator new(size_t size){
    return morphstore::MemoryManager::staticAllocate(sizeof(Table));
}

void Table::operator delete(void * p){
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Table));
}

Partition * Table::createNewPartition(NodeIdentifier nodeID) {
    Table * numaTable = getTableOnNode(nodeID);
    
    auto fence = NodeAllocationFence(nodeID);
    auto partID = partitionIndexProducer->nextID();
    auto result = new Partition(partID);
    
    /// add partition to correct table
    numaTable->partitions[partID] = result;
    
    return result;
}

void Table::registerColumn(GlobalColumnMetainformation information) {
    columnNames[information.name] = information.cid;
    columns[information.cid] = information;
    for(auto& sibling : siblings){
        Table * s = sibling.second;
        s->columnNames[information.name] = information.cid;
        s->columns[information.cid] = information;
    }
}

Table * Table::getTableOnNode(NodeIdentifier nodeID) {
    if(nodeID == nodeIdentifier)
        return this;
    if(!siblings.contains(nodeID)){
        severe("Table does not exist on node " << nodeID);
    }
    return siblings[nodeID];
}

