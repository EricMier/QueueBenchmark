/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

struct GlobalColumnMetainformation;

#ifndef QUEUEBENCHMARK_SRC_STORAGE_GLOBALCOLUMNMETAINFORMATION_H
#define QUEUEBENCHMARK_SRC_STORAGE_GLOBALCOLUMNMETAINFORMATION_H

#include <string>
#include <utils>
#include <util/BaseType.h>

struct GlobalColumnMetainformation {
	std::string name;
	BaseType type;
	ColumnIdentifier cid;
	/// todo
	uint64_t entries = 0;
	
	GlobalColumnMetainformation(std::string name, BaseType type, ColumnIdentifier cid) : name(name), type(type), cid(cid) {};
	GlobalColumnMetainformation(){
		name = "";
		type = BaseType::undefined;
	}
	void* operator new(size_t size){
        return morphstore::MemoryManager::staticAllocate(sizeof(GlobalColumnMetainformation));
	}
	void operator delete(void* p){
        morphstore::MemoryManager::staticDeallocate(p, sizeof(GlobalColumnMetainformation));
	}
};

#endif //QUEUEBENCHMARK_SRC_STORAGE_GLOBALCOLUMNMETAINFORMATION_H
