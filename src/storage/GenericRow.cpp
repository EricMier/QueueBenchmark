/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/
 

#include "GenericRow.h"

/**
 * @brief Converts value at index "colIdx" to type T.
 *
 * This function is used, to get specific data formats from the row at given index.
 * Warning! This function is not fail save. If the value at given position is not suitable to convert from string
 * to specified type T, this function throws an exception.
 * @tparam T
 * @param colIdx
 * @return
 */
template<typename T>
T GenericRow::get(unsigned long colIdx) {
	return T(columns.at(colIdx));
}

const std::vector<std::string> & GenericRow::getColumns() const {
	return columns;
}

template<>
int GenericRow::get<int>(unsigned long colIdx) {
	return stoi(columns.at(colIdx));
}

template<>
std::string GenericRow::get<std::string>(unsigned long colIdx) {
	return columns.at(colIdx);
}

template<>
unsigned long GenericRow::get<unsigned long>(unsigned long colIdx) {
	return stoul(columns.at(colIdx));
}

template<>
float GenericRow::get<float>(unsigned long colIdx) {
	return stof(columns.at(colIdx));
}

template<>
double GenericRow::get<double>(unsigned long colIdx) {
	return stod(columns.at(colIdx));
}

std::ostream& operator<<(std::ostream& os, const GenericRow& gr){
	std::string delimiter = "";
	os << "GenericRow[";
	for(const std::string& str : gr.getColumns()) {
		os << delimiter << str;
		delimiter = ",";
	}
	os << "]";
	return os;
}
