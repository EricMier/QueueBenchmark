/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class Database;

#ifndef DATABASE_H
#define DATABASE_H

#include "Table.h"
#include <partitioning/TablePartitioningPolicy.h>
#include "platform/NodeIdentifier.h"

using namespace std;

class Database {
  public:
	explicit Database(NodeIdentifier & nodeID);
	Database(const Database & orig) = default;
	virtual ~Database();
	
	
	NodeIdentifier getNodeID();

	/// benchmark legacy / todo: purge
//	static void init();
//	static Database * createNewDatabase(int socket);
//	static Database * getDatabase(int socket);
	
//	static std::map<int, Database *> databases;
	static void createPartitionList(int socket, std::vector<Partition *> * partitions);
	static std::vector<Partition *> * getPartitionList(int socket);
	static std::map<int, std::vector<Partition *> *> partitions;
	
	static void createDataVector(int socket);
	static void createDataVector(int socket, size_t tuples);
	static vector<unsigned long>* getData(int socket);
	static map<int, vector<unsigned long> *> data;
	
	static void printData();
	static void printPartitions();
	
	/// new prototype functionality
	
	/// stores node identifier to database object mapping
	static map<NodeIdentifier, Database *> databases;
	
	static void createDatabase();
	static void destroyDatabase();
	static Database* getLocalDatabase();
	
	static Table* createNewTable(string name, TablePartitioningPolicy * partitioningPolicy);
	static bool hasTable(const string & name);
	static bool hasTable(const string && name);
	static bool hasTable(const TableIdentifier & tid);
	bool _hasTable(const string & name);
	bool _hasTable(const TableIdentifier & tid);

	
	void linkDatabase(Database* database);
	void linkDatabaseToAllSiblings(Database* database);
	const NodeIdentifier* getNodeIdentifier();
	
	Table * getTable(const string & name);
	Table * getTable(const TableIdentifier & tid);
	TableIdentifier getTableId(const string & name);
	identifier_map<TableIdentifier, Table *> const * getTables() const;
	
	void* operator new(size_t size);
	void operator delete(void* p);
  
  private:
	/// stores name to identifier mapping
	unordered_map<string, TableIdentifier> tableNames;
	/// stores identifier to table object mapping
	identifier_map<TableIdentifier, Table *> tables;
	/// the node identifier to node where this database instance resides
	NodeIdentifier nodeID;
	/// node identifier to database object mapping of database instances on other nodes
	map<NodeIdentifier, Database*> siblings;
	
	
	void addTable(Table * table, TableIdentifier tid);
	
};

#endif /* DATABASE_H */

