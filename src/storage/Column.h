/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class Column;

#ifndef QUEUEBENCHMARK_COLUMN_H
#define QUEUEBENCHMARK_COLUMN_H

#include "GlobalColumnMetainformation.h"

#include <logging>
#include <utils>




struct column_meta_data {
   size_t m_CountLogicalValues;
   size_t m_SizeUsedByte;
   size_t m_SizeComprByte;
   // TODO make this const again
   size_t /*const*/ m_SizeAllocByte;

   column_meta_data(
      size_t p_CountLogicalValues,
      size_t p_SizeUsedByte,
      size_t p_SizeComprByte,
      size_t p_SizeAllocByte
   ) :
      m_CountLogicalValues{ p_CountLogicalValues },
      m_SizeUsedByte{ p_SizeUsedByte },
      m_SizeComprByte{ p_SizeComprByte },
      m_SizeAllocByte{ p_SizeAllocByte }{
      trace(
         "Column Meta Data - ctor( |Logical Values| =" << p_CountLogicalValues <<
         ", |Data total column| =" << p_SizeUsedByte << "Byte" <<
         ", |Data compressed part| =" << p_SizeComprByte << "Byte" <<
         ", Allocated Size = " << p_SizeAllocByte << " Bytes ).");
   }
   
   column_meta_data( column_meta_data const & ) = delete;
   column_meta_data( column_meta_data && ) = default;
   column_meta_data & operator=( column_meta_data const & ) = delete;
   column_meta_data & operator=( column_meta_data && that) {
       m_CountLogicalValues = that.m_CountLogicalValues;
       m_SizeUsedByte = that.m_SizeUsedByte;
       m_SizeComprByte = that.m_SizeComprByte;
       m_SizeAllocByte = that.m_SizeAllocByte;
       return *this;
   }
};


class Column {
  public:
	/// Constructor
	Column(GlobalColumnMetainformation * metaInformation, unsigned long elements);
	//// Copy constructor
	Column(const Column & other) = delete;
	//// Move constructor
	Column(Column && other) noexcept = delete;
	// Destructor
	~Column();
	
	//// Assignment operator
	Column & operator=(const Column & rhs) = default;
	//// Move assignment operator
	Column & operator=(Column && rhs) noexcept = default;
	
	const std::string & getName();
	//// todo : template?
	void* getData() const;
	morphstore::voidptr_t get_data() const;
	size_t get_size_used_byte() const;
	GlobalColumnMetainformation * getMeta() const;
	void addValue(std::string value);
	uint64_t getElementCount();
	
	template<typename type>
	void addValue_(type value);
	
	template<typename type>
	type getRow(uint64_t index);
	
	size_t get_count_values() const;
	
//	typedef T type;
  private:
	//// Points to object which resides in node-local table instance
	GlobalColumnMetainformation * metaInformation = nullptr;
	void* data = nullptr;
	//// size in bytes of data array
	size_t size = 0UL;
	unsigned long elementCount = 0UL;
	unsigned long maxElementCount = 0UL;
	
	column_meta_data mdata;
	
	void allocateDataArray(unsigned long elements);
	
  public:
	inline void set_meta_data(size_t p_CountValues, size_t p_SizeUsedByte, size_t p_SizeComprByte)  {
	 mdata.m_CountLogicalValues = p_CountValues;
	 mdata.m_SizeUsedByte = p_SizeUsedByte;
	 mdata.m_SizeComprByte = p_SizeComprByte;
	 elementCount = p_CountValues;
	}
	inline void set_meta_data( size_t p_CountValues, size_t p_SizeUsedByte )  {
	  set_meta_data(p_CountValues, p_SizeUsedByte, 0);
	}
};

//// stream out operator
std::ostream & operator<<(std::ostream & os, const Column & obj);


static std::string printColumnsAsTable(Column ** columns, uint64_t count){
	uint64_t columnWidth[count];
	uint64_t minWidth = 20;
	uint64_t largestColumn = 0;
	std::stringstream output;

	output << "|";

	for(uint64_t c = 0; c < count; ++c){
		std::string name = "= [" + columns[c]->getMeta()->cid.to_string() + "]" + columns[c]->getName() + " =";
		columnWidth[c] = (name.length() > minWidth) ? name.length() : minWidth;
		output << str_mfill(name, columnWidth[c], "=") << "|";
		largestColumn = (largestColumn < columns[c]->getElementCount()) ? columns[c]->getElementCount() : largestColumn;
	}
	output << "\n";

	for(uint64_t line = 0; line < largestColumn; ++line){
		output << "|";
		for(uint64_t c = 0; c < count; ++c){
			std::string out = (columns[c]->getElementCount() > line)
					? std::to_string(((uint64_t*)columns[c]->getData())[line])
					: "";
			output << str_lfill(out, columnWidth[c]) << "|";
		}
		output << "\n";
	}
	return output.str();
};

#endif //QUEUEBENCHMARK_COLUMN_H
