/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Database.h"
#include <storage>
#include <logging>
#include <threading>
#include "platform/Platform.h"
#include "partitioning/TableInsertHelper.h"

using namespace std;

//// statics
//std::map<int, Database*> Database::databases;
std::map<NodeIdentifier, Database*> Database::databases;
std::map<int, std::vector<Partition*>*> Database::partitions;
map<int, vector<unsigned long>*> Database::data;




Database::Database(NodeIdentifier & nodeID) : nodeID(nodeID){
//	cout << "Created new Database on Node " << nodeID.getPhysicalNode() << " UNI: "
//		<< nodeID.getUNI() << endl;
	nodeID.getNode()->linkDatabase(this);
}



Database::~Database() {
//	debugh("Delete Database");
	for(auto & [tid, table] : tables){
//		table->~Table();
		__destruct(table, Table);
	}
	/// todo : unlink from siblings
}




NodeIdentifier Database::getNodeID() {
	return nodeID;
}


void Database::createPartitionList(int socket, std::vector<Partition*>* partitions) {
	auto partIt = Database::partitions.find(socket);
	if(partIt != Database::partitions.end()){
		// delete old database object
		delete partIt->second;
		partIt->second = partitions;
	} else {
		Database::partitions.insert({socket, partitions});
	}
}


std::vector<Partition*>* Database::getPartitionList(int socket) {
	auto partIt = Database::partitions.find(socket);
	if(partIt != Database::partitions.end()){
		return partIt->second;
	}
	return nullptr;
}


vector<unsigned long> * Database::getData(int socket) {
//	cout << "Enter Database::getData on socket " << socket << endl;
	auto dataIt = Database::data.find(socket);
	if(dataIt != Database::data.end()){
		return dataIt->second;
	}
	cout << "Database::getData returns nullptr" << endl;
	return nullptr;
}


void Database::createDataVector(int socket, size_t tuples) {
//	cout << "CreateDataVector " << socket << endl;
	auto dataIt = Database::data.find(socket);
	if(dataIt != Database::data.end()){
		// delete old database object
		delete dataIt->second;
		dataIt->second = new vector<unsigned long>(tuples);
	} else {
		Database::data.insert({socket, new vector<unsigned long>(tuples)});
	}
}


void Database::createDataVector(int socket) {
	Database::createDataVector(socket, 0);
}


void Database::printData() {
	for(int socket = 0; socket < Platform::numNodes(); socket++){
//	for(int socket = 0; socket < 1; socket++){
		cout << "===== Socket " << socket << " =====" << endl;
		unsigned line = 0;
//		if(Database::getData(socket) == nullptr)
//			cout << "its null" << endl;
		for(auto const& v : *Database::getData(socket)){
			cout << str_lfill(to_string(line), 4, "0") << ": " << v << endl;
			line++;
		}
	}
}


void Database::printPartitions() {
	for(int socket = 0; socket < Platform::numNodes(); socket++){
		cout << "===== Socket " << socket << " =====" << endl;
		for(auto const& partition : *Database::getPartitionList(socket)){
//			partition->print();
		}
	}
}


/**
 * Creates a global database with instances on each available node
 */

void Database::createDatabase() {
	//// check if database already exists
	if(!databases.empty()){
		severe("Database already created. Abort.");
	}
	
//	vector<NodeIdentifier*>::iterator nodeids = Platform::getNodeIds()->begin();
//	NodeIdentifier* nodeid = *nodeids++;
//	auto fence = ThreadManager::switchNode(nodeid);
//	Database* mainDB = new Database(*nodeid);
	
//	databases.insert(, Database(node->get));
//	for(; nodeids != Platform::getNodeIds()->end();nodeids++){
//		auto allocFence = ThreadManager::switchNode(*nodeids);
//		Database* db = new Database(*(*nodeids));
//		mainDB->linkDatabase(db);
//	}
	
	Database * prev = nullptr;
	for( auto node : *Platform::getNodeIds() ){
		auto fence = ThreadManager::directAllocationToNode(node);
		Database * db = new Database(*node);
		if(prev != nullptr)
			prev->linkDatabaseToAllSiblings(db);
		prev = db;
		Database::databases[*node] = db;
	}
}


void Database::destroyDatabase() {
	for(auto& [nid, database] : databases){
//		database->~Database();
		__destruct(database, Database);
	}
	databases.clear();
}

/**
 * @brief Links a newly created Database-Object to all existing Database-Objects.
 *
 * Not threadsafe.
 * @param database
 */

void Database::linkDatabase(Database * database) {
	debug2("Link new Database[" << database->nodeID.getUNI() << "] to Database[" << nodeID.getUNI() << "]")
	siblings[database->nodeID] = database;
	database->siblings[nodeID] = this;
}


void Database::linkDatabaseToAllSiblings(Database * database) {
	debug1("Link new Database[" << database->nodeID.getUNI() << "] to all siblings of Database[" << nodeID.getUNI() << "]")
	//// link to siblings
	for( auto s : siblings ){
		s.second->linkDatabase(database);
	}
	//// link to self
	linkDatabase(database);
}


const NodeIdentifier * Database::getNodeIdentifier() {
	return &nodeID;
}


/**
 * @brief Checks if a Table with given name exists. (local)
 * @param name Name of the Table
 * @return Existence of Table
 */

bool Database::_hasTable(const string & name) {
	debug1("Check database[" << nodeID.getPhysicalNode() << "] for table \"" << name << "\"")
	return tableNames.find(name) != tableNames.end();
}


/**
 * @brief Checks if a Table with given id exists. (local)
 * @param tid ID of the Table
 * @return Existence of Table
 */

bool Database::_hasTable(const TableIdentifier & tid) {
	debug1("Check database[" << nodeID.getPhysicalNode() << "] for table id \"" << tid << "\"")
	return tables.find(tid) != tables.end();
}


/**
 * @brief Checks if a Table with given name exists. (global)
 *
 * This function checks all available database objects for existence of a Table with given name.
 * @param name Name of the Table
 * @return Existence of Table
 */

bool Database::hasTable(const string & name) {
	debug1("Check database for table \"" << name << "\"")
	for( auto db : databases )
		if(db.second->_hasTable(name))
			return true;
	return false;
}

bool Database::hasTable(const string && name) {
	return hasTable(name);
}


/**
 * @brief Checks if a Table with given id exists. (global)
 *
 * This function checks all available database objects for existence of a Table with given id.
 * @param tid ID of the Table
 * @return Existence of Table
 */

bool Database::hasTable(const TableIdentifier & tid) {
	debug("Check database for table id \"" << tid << "\"")
	for( auto db : databases )
		if(db.second->_hasTable(tid))
			return true;
	return false;
}



void Database::addTable(Table * table, TableIdentifier tid) {
	if(_hasTable(table->getName())) {
		severe("Table " << table->getName() << " already exists.");
	}
	tableNames.insert(pair<string, TableIdentifier>(table->getName(), tid));
	tables.insert(pair<TableIdentifier, Table*>(tid, table));
	table->linkDatabase(this);
}


Table * Database::createNewTable(string name, TablePartitioningPolicy * partitioningPolicy) {
	//// check if there are nodes specified, where the new table shall be dispatched to
	if(partitioningPolicy->nodes.empty()) {
		severe("Partitioning policy does not at least specify one node where to locate this table.")
	}
	//// check if specified nodes are valid
	for(auto node : partitioningPolicy->nodes)
		if(!node->isValid()) {
			severe("Node " << node << " is not valid. Abort.")
		}
	//// check if table name already exist
	if(Database::hasTable(name)){
		severe("Table \"" << name << "\" already exists. Abort.")
	}
	
	if(partitioningPolicy->definedParameterCount() < 2){
		severe("Insufficient partitioning parameters defined for table " << name)
	}
	partitioningPolicy->fillupUnknownParameter();
	
	auto partitionIndex = new(__alloc(PartitionIndexProducer)) PartitionIndexProducer();
	auto insertHelper = new(__alloc(TableInsertHelper)) TableInsertHelper();
	Table* prev = nullptr;
	TableIdentifier tid = TableIdentifier::createNew();
	//// create table for each node specified
	for(auto node : partitioningPolicy->nodes){
		NodeAllocationFence fence(*node addCaller);
		Database* db = node->getNode()->getDatabase();
		if(db == nullptr){
			severe("No Database available. " << "Abort.")
		}
		Table* table = new Table(name, tid, partitioningPolicy, *node, partitionIndex, insertHelper);
		if(prev != nullptr)
			prev->linkTableToAllSiblings(table);
		db->addTable(table, tid);
		prev = table;
	}
//	prev->addPartitions(partitioningPolicy->partitionCount);
	return getLocalDatabase()->getTable(name);
}

/**
 * Returns the database instance of the node this thread is executed on.
 * @return Pointer to database instance
 */
Database * Database::getLocalDatabase() {
	#ifdef DEBUG
	if(!ThreadManager::currentExecutionNode.isValid()){
		severe("While querying local database: current execution node is not valid.");
	}
	#endif
	
	return ThreadManager::currentExecutionNode.getNode()->getDatabase();
}


Table * Database::getTable(const string & name) {
	if(_hasTable(name))
		return tables[tableNames[name]];
	error("Table \"" << name << "\" not found.")
	return nullptr;
}


Table * Database::getTable(const TableIdentifier & tid) {
	if(_hasTable(tid))
		return tables[tid];
	error("Table id \"" << tid << "\" not found.")
	return nullptr;
}


/**
 * Returns the table id of table, if existend, with given name
 * @param name Name of table
 * @return TableIdentifier of table
 */

TableIdentifier Database::getTableId(const string & name) {
	if(_hasTable(name))
		return tableNames[name];
	warn("Table \"" << name << "\" does not exist.");
	return TableIdentifier::createInvalid();
}


void* Database::operator new(size_t size){
    return morphstore::MemoryManager::staticAllocate(sizeof(Database));
}

void Database::operator delete(void * p){
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Database));
}


identifier_map<TableIdentifier, Table *> const * Database::getTables() const {
	return &tables;
}


