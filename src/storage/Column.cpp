/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Column.h"

//uint64_t ColumnIdentifier::nextId = {0};
//mutex ColumnIdentifier::idLock;

/**
 * @brief Constructor of Column
 */
Column::Column(GlobalColumnMetainformation * metaInformation, unsigned long elements)
  : metaInformation(metaInformation), maxElementCount(elements), mdata(0,0,0, elements * sizeof(uint64_t)) {
//	debugh("New Column : " << metaInformation->name << " [" << metaInformation << "/" << this->metaInformation << "] id: " << metaInformation->cid)
	allocateDataArray(elements);
	elementCount = 0UL;
}

/**
 * @brief Copy constructor of Column
 * @param other Instance of Column to copy from
 */
//Column::Column(const Column & other){
//	// @todo Implement Column copy constructor
//	error("Column copy constructor")
//}


/**
 * @brief Move constructor of Column
 * @param other RValue of type Column to steal from
 */
//Column::Column(Column && other) noexcept {
//	// @todo Implement Column move constructor
//	error("Column move constructor")
//}


/**
 * @brief Destructor of Column
 */
Column::~Column() {
//	debugh("Delete Column");
	delete (uint64_t*) data;
}

const std::string & Column::getName() {
	return metaInformation->name;
}

void Column::allocateDataArray(unsigned long elements) {
	char** tmp;
	switch (metaInformation->type){
		case BaseType::uint64:
			data = new uint64_t[elements];
			size = sizeof(uint64_t) * elements;
			break;
		case BaseType::uint32:
			data = new uint32_t[elements];
			size = sizeof(uint32_t) * elements;
			break;
		case BaseType::int64:
			data = new int64_t[elements];
			size = sizeof(int64_t) * elements;
			break;
		case BaseType::int32:
			data = new int32_t[elements];
			size = sizeof(int32_t) * elements;
			break;
		case BaseType::string:
			tmp = new char*[elements];
			for(int i = 0; i < elements; i++){
				tmp[i] = new char[256];
			}
			data = tmp;
			size = sizeof(char) * elements * 256 + sizeof(char*) * elements;
			break;
		case BaseType::float32:
			data = new float_t[elements];
			size = sizeof(float_t) * elements;
			break;
		case BaseType::double64:
			data = new double_t[elements];
			size = sizeof(double_t) * elements;
			break;
		case BaseType::undefined:
			severe("Type of Column undefined.");
	}
	debug2("[Column::allocateDataArray] Allocataed Column with " << elements << " elements (size: " << size << "B)")
}

void * Column::getData() const {
	return data;
}
morphstore::voidptr_t Column::get_data() const {
	return morphstore::voidptr_t(data);
}

GlobalColumnMetainformation * Column::getMeta() const {
	return metaInformation;
}

void Column::
addValue(std::string value) {
	debug2("[Column::addValue](" << value << ")");
	char tmp[256];
	if(elementCount >= maxElementCount)
		severe("Column is at max capacity.");
	switch (metaInformation->type){
		case BaseType::uint64:
			((uint64_t *) data)[elementCount] = stoul(value);
			mdata.m_SizeUsedByte += sizeof(uint64_t);
			break;
		case BaseType::uint32:
			((uint32_t *) data)[elementCount] = static_cast<uint32_t>(stoul(value));
			mdata.m_SizeUsedByte += sizeof(uint32_t);
			break;
		case BaseType::int64:
			((int64_t *) data)[elementCount] = stol(value);
			mdata.m_SizeUsedByte += sizeof(int64_t);
			break;
		case BaseType::int32:
			((int32_t *) data)[elementCount] = stoi(value);
			mdata.m_SizeUsedByte += sizeof(int32_t);
			break;
		default:
			severe("Add " << metaInformation->type << " not implemented yet");
//		case BaseType::string:
////			((char **) data)[elementCount] = const_cast<char *>(value.c_str());
////			tmp = value.c_str();
//			break;
//		case BaseType::float32:
//			//// todo ................
//			break;
//		case BaseType::double64:
//			break;
//		case BaseType::undefined:
//			break;
	}
	++elementCount;
	++mdata.m_CountLogicalValues;
}

template<typename type>
void Column::addValue_(type value) {
	((type *) data)[elementCount] = value;
	elementCount++;
}

template void Column::addValue_<uint64_t>(uint64_t value);

template<typename type>
type Column::getRow(uint64_t index) {
	return ((type*) data)[index];
}

size_t Column::get_count_values() const {
	return elementCount;
}

size_t Column::get_size_used_byte() const {
	return elementCount * sizeof(uint64_t);
}

uint64_t Column::getElementCount() {
	return elementCount;
}


template uint64_t Column::getRow(uint64_t index);


std::ostream & operator<<(std::ostream & os, const Column & obj) {
	//os << "<Column>[" << "attrib1:" << obj.___ << ", attrib2:" << obj.___ << "]";
	os << "[TODO: Implement outstream operator for <Column>]";
	return os;
}
