/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Logger.h"
#include "loggingMacros.h"
#include "stringManipulation.h"

#include <stdlibs>
#include <utils>
//#include <threading>


///statics
thread_local Logger * Logger::instance;

/**
 * @brief Constructor of Logger
 */
Logger::Logger() {
	// @todo Implement Logger constructor
}

/**
 * @brief Copy constructor of Logger
 * @param other Instance of Logger to copy from
 */
Logger::Logger(const Logger & other) {
	// @todo Implement Logger copy constructor
}


/**
 * @brief Move constructor of Logger
 * @param other RValue of type Logger to steal from
 */
Logger::Logger(Logger && other) noexcept {
	// @todo Implement Logger move constructor
}


/**
 * @brief Destructor of Logger
 */
Logger::~Logger() {
	// @todo Implement Logger destructor
}


/**
 * @brief Assignment operator of Logger
 * @param rhs Instance of Logger to copy from
 * @return Reference of this Logger-object
 */
Logger & Logger::operator=(const Logger & rhs) {
	// @todo Implement assignment operator
	throw std::runtime_error("Logger::AssignmentOperator not implemented!");
	return *this;
}

/**
 * @brief Move assignment operator of Logger
 * @param rhs RValue of type Logger to steal from
 * @return Reference of this Logger-object
 */
Logger & Logger::operator=(Logger && rhs) noexcept {
	// @todo Implement move assignment operator
	throw std::runtime_error("Logger::MoveAssignmentOperator not implemented!");
	return *this;
}

//Logger & Logger::operator<<( manip1 & in ) {
//	if (in == (std::basic_ostream<char, std::char_traits<char> >& (*)(std::basic_ostream<char, std::char_traits<char> >&)) std::endl) {
//		flush();
//		return *this;
//	}
//	(*(std::ostringstream*)this) << in;
//	return *this;
//}

Logger & Logger::getInstance() {
	if(!instance)
		instance = new Logger();
	return *instance;
}

void Logger::flush() {
	std::stringstream message;
	std::string text = str();
	std::string head = buildHead();
	message << head;
	
	uint64_t length = text.length();
	
	/// add line breaks
	while(text.length() > LINE_LENGTH){
	    message << text.substr(0, LINE_LENGTH) << "\n" << std::setfill(' ') << std::setw((int) head.length()) << " ";
	    text = text.substr(LINE_LENGTH, text.length());
	}
	message << text;
	if(!location.empty()){
		message << " " << DARK_GRAY << str_lfill("_ [ " + location + " ]", LINE_LENGTH - text.length() + invisibleCharacters - 1, "_");
	}
	message << "\n" << RESET;
	
	std::cout << message.str() << std::flush;
	str("");
	location = "";
	invisibleCharacters = 0UL;
	currentLevel = LogLevel::info;
}

Logger & Logger::operator<<(LogColor color) {
	std::string tmp = decodeLogColor(color);
	invisibleCharacters += tmp.length();
	*this << tmp;
	return *this;
}

std::string Logger::decodeLogColor(LogColor color) {
	switch(color) {
		
		case LogColor::nocolor:
			return RESET;
			break;
		case LogColor::black:
			break;
		case LogColor::red:
			return RED;
			break;
		case LogColor::green:
			return GREEN;
			break;
		case LogColor::yellow:
			return YELLOW;
			break;
		case LogColor::blue:
			return BLUE;
			break;
		case LogColor::magenta:
			break;
		case LogColor::cyan:
			return CYAN;
			break;
		case LogColor::light_gray:
			return GRAY;
			break;
		case LogColor::dark_gray:
			return DARK_GRAY;
			break;
		case LogColor::light_red:
			break;
		case LogColor::light_green:
			break;
		case LogColor::light_yellow:
			break;
		case LogColor::light_blue:
			break;
		case LogColor::light_magenta:
			break;
		case LogColor::light_cyan:
			break;
		case LogColor::white:
			return WHITE;
			break;
	}
	return "";
}

std::string  Logger::buildHead() {
	std::stringstream head;
	std::string lvl, pre_color, post_color;
	
	switch(currentLevel){
		case LogLevel::severe:
			lvl = "SEVERE ";
			pre_color = DARK_RED;
			post_color = DARK_RED;
			break;
		case LogLevel::error:
			lvl = " ERROR ";
			pre_color = RED;
			post_color = RESET;
			break;
		case LogLevel::warning:
			lvl = "WARNING";
			pre_color = YELLOW;
			post_color = RESET;
			break;
		case LogLevel::info:
			lvl = " INFO  ";
			pre_color = GREEN;
			post_color = RESET;
			break;
		case LogLevel::debugh:
			lvl = " DBG H ";
			pre_color = PURPLE;
			post_color = RED;
			break;
		case LogLevel::low_warning:
			lvl = "WARNING";
			pre_color = YELLOW;
			post_color = RESET;
			break;
		case LogLevel::debug:
			lvl = " DBG 0 ";
			pre_color = CYAN;
			post_color = RESET;
			break;
		case LogLevel::debugstack:
			lvl = " DBG ST";
			pre_color = PURPLE;
			post_color = RED;
			break;
		case LogLevel::debugwarn:
			lvl = "DBGWARN";
			pre_color = YELLOW;
			post_color = RESET;
			break;
		case LogLevel::debug1:
			lvl = " DBG 1 ";
			pre_color = GRAY;
			post_color = GRAY;
			break;
		case LogLevel::debug2:
			lvl = " DBG 2 ";
			pre_color = GRAY;
			post_color = GRAY;
			break;
		case LogLevel::alloc:
			lvl = " ALLOC ";
			pre_color = CYAN;
			post_color = GRAY;
			break;
		case LogLevel::nohead:
			return "";
			break;
	}
//	std::string threadId = "";
//	std::thread::id;
//	#ifdef DEBUG
//	threadId =
//    #endif
	head << "" << pre_color << "[" << RESET << std::setfill(' ') << std::setw(4) << ThreadManager::threadName
	  << pre_color << "|" << RESET << "N" << ThreadManager::currentExecutionNode.getPhysicalNode() << pre_color << "][" << lvl << "] " << post_color;
	return head.str();
}

Logger & Logger::operator<<(LogLevel level) {
	currentLevel = level;
	return *this;
}

void Logger::addLocation(std::string file, unsigned line) {
	this->location = file + ":" + std::to_string(line);
}


std::ostream & operator <<(std::ostream & os, std::chrono::nanoseconds ns) {
    os << ns.count();
    return os;
}

std::ostream & operator <<(std::ostream & os, std::chrono::microseconds us) {
    os << us.count();
    return os;
}

std::ostream & operator <<(std::ostream & os, std::chrono::milliseconds ms) {
    os << ms.count();
    return os;
}

std::ostream & operator <<(std::ostream & os, std::chrono::seconds s) {
    os << s.count();
    return os;
}

std::ostream & operator <<(std::ostream & os, std::chrono::minutes m) {
    os << m.count();
    return os;
}
