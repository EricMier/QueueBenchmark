/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_LOGGER_H
#define QUEUEBENCHMARK_LOGGER_H

#include <stdlibs>

enum class LogLevel {
	severe,         /// fatal errors
	error,          /// errors
	warning,        /// warnings
	info,           /// default messages
	debugh,         /// debug messages with high priority (for temporary debugging)
	low_warning,    /// optional low level warnings
	debug,          /// debug messages
	debugstack,     /// debug messages with stacktrace attached
	debugwarn,      /// warnings for debug mode only
	debug1,         /// lower level debug messages
	debug2,         /// lowerer level debug messages
	alloc,          /// allocation-specific messages
	nohead          /// to skip header generation (like default cout << ... )
};

// log message colorization
enum class LogColor {
	nocolor,
	black,
	red,
	green,
	yellow,
	blue,
	magenta,
	cyan,
	light_gray,
	dark_gray,
	light_red,
	light_green,
	light_yellow,
	light_blue,
	light_magenta,
	light_cyan,
	white
};


//typedef std::ostream& (*manip1)( std::ostream& );

class Logger : public std::ostringstream {
  public:
	//// Constructor
	Logger();
	//// Copy constructor
	Logger(const Logger & other);
	//// Move constructor
	Logger(Logger && other) noexcept;
	// Destructor
	virtual ~Logger();
	
	//// Assignment operator
	Logger & operator=(const Logger & rhs);
	//// Move assignment operator
	Logger & operator=(Logger && rhs) noexcept;
	
	static Logger& getInstance();
	
	void flush();
	void addLocation(std::string file, unsigned line);
	
//	Logger & operator << ( manip1 & in );
	Logger & operator << (LogColor color);
	Logger & operator << (LogLevel level);
//	Logger & operator << (std::chrono::nanoseconds);
//	Logger & operator << (std::chrono::milliseconds);
	
  private:
	static thread_local Logger * instance;
	static std::string decodeLogColor(LogColor color);
	
	std::string buildHead();
	uint64_t invisibleCharacters = 0UL;
	std::string location;
	
	LogLevel currentLevel = LogLevel::info;
};

std::ostream& operator<<(std::ostream& os, std::chrono::nanoseconds);
std::ostream& operator<<(std::ostream& os, std::chrono::microseconds);
std::ostream& operator<<(std::ostream& os, std::chrono::milliseconds);
std::ostream& operator<<(std::ostream& os, std::chrono::seconds);
std::ostream& operator<<(std::ostream& os, std::chrono::minutes);

#endif //QUEUEBENCHMARK_LOGGER_H
