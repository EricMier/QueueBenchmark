/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_QOFL_H
#define QUEUEBENCHMARK_QOFL_H

#include <numaif.h>
#include <execinfo.h>

#include "stringManipulation.h"


/**
 * @brief Checks if a given directory exists and creates it, if not.
 * @param path to create
 * @return
 */
static bool testAndCreateDirectory(const std::string path){
	// Check if output directory exists, if not, create it
	struct stat buffer;
	
	auto pathParts = splitString(path, "/");
	std::string currentPath = "";
	for(auto & pp : *pathParts){
		currentPath += pp;
		if ((stat (currentPath.c_str(), &buffer) != 0) && (mkdir(currentPath.c_str(), 0777) == -1)) {
			std::cerr << "Could not create directory \"" << currentPath << "\"\nError: " << strerror(errno) << std::endl;
			return false;
		}
		currentPath += "/";
	}
	return true;
}


// This function returns the NUMA node that a pointer address resides on.
static int get_node(void *p)
{
	#ifndef USE_LIBNUMA
	return -1;
	#endif
	
	int status[1] = {7};
	void *pa;
	unsigned long a;
	
	// round p down to the nearest page boundary
	a  = (unsigned long) p;
	a  = a - (a % ((unsigned long) getpagesize()));
	pa = (void *) a;
	
	if (move_pages(0,1,&pa,NULL,status,0) != 0) {
		fprintf(stderr,"Problem in %s line %d calling move_pages()\n",__FILE__,__LINE__);
		abort();
	}
	
	return(status[0]);
}




//struct voidptr_helper {
//  void const * const m_Ptr;
//  constexpr voidptr_helper(void const * const p_Ptr): m_Ptr{p_Ptr}{}
//  template<typename T>
//  operator T const * () const {
//     trace("[ VoidPtr Helper ] Implicit cast [ const void * const ] --> [ const " << typeid(T).name() << " * ]");
//     return reinterpret_cast<T const * const >( m_Ptr );
//  }
//  template<typename T>
//  operator T * () const {
//     trace("[ VoidPtr Helper ] Implicit cast [ const void * const ] --> [ " << typeid(T).name() << " * ]");
//     return reinterpret_cast<T * >( const_cast< void * >( m_Ptr ) );
//  }
//};





#endif //QUEUEBENCHMARK_QOFL_H
