/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_LOGGINGMACROS_H
#define QUEUEBENCHMARK_LOGGINGMACROS_H

#include <stdlibs>
#include <sys/syscall.h>

#define LINE_LENGTH 180

#define COLOR(X) std::string("\e[38;5;" + std::to_string(X) + "m")
//#define COLOR(X) ""
#define GREEN COLOR(40)
#define CYAN COLOR(50)
#define RED COLOR(196)
#define DARK_RED COLOR(124)
#define DARK_GRAY COLOR(240)
#define GRAY COLOR(244)
#define YELLOW COLOR(226)
#define BLUE COLOR(27)
#define PURPLE COLOR(99)
#define WHITE COLOR(255)
#define RESET std::string("\e[39m")
//#define RESET ""

//// for manually switch on debug levels
#ifndef ALLOC_MESSAGES
//#   define ALLOC_MESSAGES
#endif
#ifndef DEBUG
//#   define DEBUG
#endif
#ifndef DEBUG1
//#   define DEBUG1
#endif
#ifndef DEBUG2
//#   define DEBUG2
#endif
#ifndef TRACE
//#   define TRACE
#endif


/// ========== Macros to create a stream like notation out of a parameter list, i.e. "paramA, paramB, paramC, ..." becomes "<< paramA << paramB << paramC << ..."

#define CREATE_STREAM_001(elem, ...) << elem
#define CREATE_STREAM_002(elem, ...) << elem CREATE_STREAM_001(__VA_ARGS__)
#define CREATE_STREAM_003(elem, ...) << elem CREATE_STREAM_002(__VA_ARGS__)
#define CREATE_STREAM_004(elem, ...) << elem CREATE_STREAM_003(__VA_ARGS__)
#define CREATE_STREAM_005(elem, ...) << elem CREATE_STREAM_004(__VA_ARGS__)
#define CREATE_STREAM_006(elem, ...) << elem CREATE_STREAM_005(__VA_ARGS__)
#define CREATE_STREAM_007(elem, ...) << elem CREATE_STREAM_006(__VA_ARGS__)
#define CREATE_STREAM_008(elem, ...) << elem CREATE_STREAM_007(__VA_ARGS__)
#define CREATE_STREAM_009(elem, ...) << elem CREATE_STREAM_008(__VA_ARGS__)
#define CREATE_STREAM_010(elem, ...) << elem CREATE_STREAM_009(__VA_ARGS__)
#define CREATE_STREAM_011(elem, ...) << elem CREATE_STREAM_010(__VA_ARGS__)
#define CREATE_STREAM_012(elem, ...) << elem CREATE_STREAM_011(__VA_ARGS__)
#define CREATE_STREAM_013(elem, ...) << elem CREATE_STREAM_012(__VA_ARGS__)
#define CREATE_STREAM_014(elem, ...) << elem CREATE_STREAM_013(__VA_ARGS__)
#define CREATE_STREAM_015(elem, ...) << elem CREATE_STREAM_014(__VA_ARGS__)
#define CREATE_STREAM_016(elem, ...) << elem CREATE_STREAM_015(__VA_ARGS__)
#define CREATE_STREAM_017(elem, ...) << elem CREATE_STREAM_016(__VA_ARGS__)
#define CREATE_STREAM_018(elem, ...) << elem CREATE_STREAM_017(__VA_ARGS__)
#define CREATE_STREAM_019(elem, ...) << elem CREATE_STREAM_018(__VA_ARGS__)
#define CREATE_STREAM_020(elem, ...) << elem CREATE_STREAM_019(__VA_ARGS__)
#define CREATE_STREAM_021(elem, ...) << elem CREATE_STREAM_020(__VA_ARGS__)
#define CREATE_STREAM_022(elem, ...) << elem CREATE_STREAM_021(__VA_ARGS__)
#define CREATE_STREAM_023(elem, ...) << elem CREATE_STREAM_022(__VA_ARGS__)
#define CREATE_STREAM_024(elem, ...) << elem CREATE_STREAM_023(__VA_ARGS__)
#define CREATE_STREAM_025(elem, ...) << elem CREATE_STREAM_024(__VA_ARGS__)
#define CREATE_STREAM_026(elem, ...) << elem CREATE_STREAM_025(__VA_ARGS__)
#define CREATE_STREAM_027(elem, ...) << elem CREATE_STREAM_026(__VA_ARGS__)
#define CREATE_STREAM_028(elem, ...) << elem CREATE_STREAM_027(__VA_ARGS__)
#define CREATE_STREAM_029(elem, ...) << elem CREATE_STREAM_028(__VA_ARGS__)
#define CREATE_STREAM_030(elem, ...) << elem CREATE_STREAM_029(__VA_ARGS__)
#define CREATE_STREAM_031(elem, ...) << elem CREATE_STREAM_030(__VA_ARGS__)
#define CREATE_STREAM_032(elem, ...) << elem CREATE_STREAM_031(__VA_ARGS__)
#define CREATE_STREAM_033(elem, ...) << elem CREATE_STREAM_032(__VA_ARGS__)
#define CREATE_STREAM_034(elem, ...) << elem CREATE_STREAM_033(__VA_ARGS__)
#define CREATE_STREAM_035(elem, ...) << elem CREATE_STREAM_034(__VA_ARGS__)
#define CREATE_STREAM_036(elem, ...) << elem CREATE_STREAM_035(__VA_ARGS__)
#define CREATE_STREAM_037(elem, ...) << elem CREATE_STREAM_036(__VA_ARGS__)
#define CREATE_STREAM_038(elem, ...) << elem CREATE_STREAM_037(__VA_ARGS__)
#define CREATE_STREAM_039(elem, ...) << elem CREATE_STREAM_038(__VA_ARGS__)
#define CREATE_STREAM_040(elem, ...) << elem CREATE_STREAM_039(__VA_ARGS__)
#define CREATE_STREAM_041(elem, ...) << elem CREATE_STREAM_040(__VA_ARGS__)
#define CREATE_STREAM_042(elem, ...) << elem CREATE_STREAM_041(__VA_ARGS__)
#define CREATE_STREAM_043(elem, ...) << elem CREATE_STREAM_042(__VA_ARGS__)
#define CREATE_STREAM_044(elem, ...) << elem CREATE_STREAM_043(__VA_ARGS__)
#define CREATE_STREAM_045(elem, ...) << elem CREATE_STREAM_044(__VA_ARGS__)
#define CREATE_STREAM_046(elem, ...) << elem CREATE_STREAM_045(__VA_ARGS__)
#define CREATE_STREAM_047(elem, ...) << elem CREATE_STREAM_046(__VA_ARGS__)
#define CREATE_STREAM_048(elem, ...) << elem CREATE_STREAM_047(__VA_ARGS__)
#define CREATE_STREAM_049(elem, ...) << elem CREATE_STREAM_048(__VA_ARGS__)
#define CREATE_STREAM_050(elem, ...) << elem CREATE_STREAM_049(__VA_ARGS__)

#define CREATE_STREAM_SEQUENCE() \
        CREATE_STREAM_050, \
        CREATE_STREAM_049, \
        CREATE_STREAM_048, \
        CREATE_STREAM_047, \
        CREATE_STREAM_046, \
        CREATE_STREAM_045, \
        CREATE_STREAM_044, \
        CREATE_STREAM_043, \
        CREATE_STREAM_042, \
        CREATE_STREAM_041, \
        CREATE_STREAM_040, \
        CREATE_STREAM_039, \
        CREATE_STREAM_038, \
        CREATE_STREAM_037, \
        CREATE_STREAM_036, \
        CREATE_STREAM_035, \
        CREATE_STREAM_034, \
        CREATE_STREAM_033, \
        CREATE_STREAM_032, \
        CREATE_STREAM_031, \
        CREATE_STREAM_030, \
        CREATE_STREAM_029, \
        CREATE_STREAM_028, \
        CREATE_STREAM_027, \
        CREATE_STREAM_026, \
        CREATE_STREAM_025, \
        CREATE_STREAM_024, \
        CREATE_STREAM_023, \
        CREATE_STREAM_022, \
        CREATE_STREAM_021, \
        CREATE_STREAM_020, \
        CREATE_STREAM_019, \
        CREATE_STREAM_018, \
        CREATE_STREAM_017, \
        CREATE_STREAM_016, \
        CREATE_STREAM_015, \
        CREATE_STREAM_014, \
        CREATE_STREAM_013, \
        CREATE_STREAM_012, \
        CREATE_STREAM_011, \
        CREATE_STREAM_010, \
        CREATE_STREAM_009, \
        CREATE_STREAM_008, \
        CREATE_STREAM_007, \
        CREATE_STREAM_006, \
        CREATE_STREAM_005, \
        CREATE_STREAM_004, \
        CREATE_STREAM_003, \
        CREATE_STREAM_002, \
        CREATE_STREAM_001

#define GET_CREATE_STREAM( \
        _001,                \
        _002,                \
        _003,                \
        _004,                \
        _005,                \
        _006,                \
        _007,                \
        _008,                \
        _009,                \
        _010,                \
        _011,                \
        _012,                \
        _013,                \
        _014,                \
        _015,                \
        _016,                \
        _017,                \
        _018,                \
        _019,                \
        _020,                \
        _021,                \
        _022,                \
        _023,                \
        _024,                \
        _025,                \
        _026,                \
        _027,                \
        _028,                \
        _029,                \
        _030,                \
        _031,                \
        _032,                \
        _033,                \
        _034,                \
        _035,                \
        _036,                \
        _037,                \
        _038,                \
        _039,                \
        _040,                \
        _041,                \
        _042,                \
        _043,                \
        _044,                \
        _045,                \
        _046,                \
        _047,                \
        _048,                \
        _049,                \
        _050,                \
        NAME, ...) NAME

//#define CREATE_STREAM(...) GET_CREATE_STREAM(__VA_ARGS__, CREATE_STREAM_009, CREATE_STREAM_008, CREATE_STREAM_007, \
//CREATE_STREAM_006, CREATE_STREAM_005, CREATE_STREAM_004, CREATE_STREAM_003, CREATE_STREAM_002, CREATE_STREAM_001)(__VA_ARGS__)
#define CREATE_STREAM_(...) GET_CREATE_STREAM(__VA_ARGS__)
#define CREATE_STREAM(...) __VA_OPT__( CREATE_STREAM_(__VA_ARGS__, CREATE_STREAM_SEQUENCE())(__VA_ARGS__) )








#undef ADD_LOCATION
#undef ADD_STACKTRACE

#define LOGGER Logger::getInstance()
#define FLUSH Logger::getInstance().flush()
#define LINEEND Logger::getInstance() << "\n";
#define NOHEAD Logger::getInstance() << LogLevel::nohead;
#define ADD_LOCATION Logger::getInstance().addLocation( __FILE__, __LINE__ )
#ifdef INCLUDE_STACKTRACE
  #define ADD_STACKTRACE Logger::getInstance() << LogLevel::nohead << boost::stacktrace::stacktrace(); FLUSH;
#else
  #define ADD_STACKTRACE
#endif


/// undef macros from morphstore
#undef severe
#undef error
#undef warn
#undef info
#undef trace
#undef debug

#define severe(...)   {LOGGER << LogLevel::severe      << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH; ADD_STACKTRACE; throw std::runtime_error("see log message");}
#define error(...)    {LOGGER << LogLevel::error       << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH; ADD_STACKTRACE; }
#define warn(...)     {LOGGER << LogLevel::warning     << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION;               ; FLUSH;}
#define warnStack(...){LOGGER << LogLevel::warning     << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH; ADD_STACKTRACE;}
#define lowwarn(...)  {LOGGER << LogLevel::low_warning << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION;               ; FLUSH;}
#define info(...)     {LOGGER << "" __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); FLUSH;}

#define trace(...)


#define debugh(...)
#define debug(...)
#define debugstack(...)
#define debugwarn(...)
#define debug1(...)
#define debug2(...)

#ifdef DEBUG
	#undef info
	#undef debugh
	#undef debug
	#undef debugstack
	#undef debugwarn
    #define info(...)        {LOGGER << "" __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH;}
	#define debugh(...)      {LOGGER << LogLevel::debugh     << "" << std::this_thread::get_id() << " / " << syscall(__NR_gettid) << " " __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH;}
	#define debug(...)       {LOGGER << LogLevel::debug      << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH;}
	#define debugstack(...)  {LOGGER << LogLevel::debugstack << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH; ADD_STACKTRACE; FLUSH;}
	#define debugwarn(...)   {LOGGER << LogLevel::debugwarn  << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH;}
	  #ifdef DEBUG1
		#undef debug1
		#define debug1(...)  LOGGER << LogLevel::debug << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH;
	  #endif
	  #ifdef DEBUG2
		#undef debug2
		#define debug2(...)  LOGGER << LogLevel::debug << ""  __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); ADD_LOCATION; FLUSH;
	  #endif
	  
#endif


#ifdef ALLOC_MESSAGES
	#define alloc(...)   {LOGGER << LogLevel::alloc       << "" << __VA_OPT__(CREATE_STREAM(__VA_ARGS__)); FLUSH;}
#else
  #define alloc(STREAM)
#endif

#endif //QUEUEBENCHMARK_LOGGINGMACROS_H
