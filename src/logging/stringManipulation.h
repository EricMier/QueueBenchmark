/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_STRINGMANIPULATION_H
#define QUEUEBENCHMARK_STRINGMANIPULATION_H

#include <stdlibs>



static std::string str_lfill(std::string& str, uint64_t length, const char fillChar[1]){
	if(str.length() > length)
		return str;
	uint64_t fillup = length - str.length();
	std::stringstream s;
	for (unsigned i = 0; i < fillup; ++i) {
		s << fillChar;
	}
	s << str;
	return s.str();
}

static std::string str_lfill(std::string&& str, uint64_t length, const char fillChar[1]){
	return str_lfill(str, length, fillChar);
}

static std::string str_lfill(std::string & str, uint64_t length) {
	return str_lfill(str, length, " ");
}

static std::string str_lfill(std::string && str, uint64_t length) {
	return str_lfill(str, length, " ");
}



static std::string str_rfill(std::string& str, uint64_t length, const char fillChar[1]){
	if(str.length() > length)
		return str;
	uint64_t fillup = length - str.length();
	std::stringstream s;
	s << str;
	for (unsigned i = 0; i < fillup; ++i) {
		s << fillChar;
	}
	return s.str();
}

static std::string str_rfill(std::string&& str, uint64_t length, const char fillChar[1]) {
	return str_rfill(str, length, fillChar);
}

static std::string str_rfill(std::string& str, uint64_t length) {
	return str_rfill(str, length, " ");
}

static std::string str_rfill(std::string&& str, uint64_t length) {
	return str_rfill(str, length, " ");
}



static std::string str_mfill(std::string& str, uint64_t length, const char fillChar[1]){
	if(str.length() > length)
		return str;
	uint64_t fillup = (length - str.length()) / 2;
	std::string s = str_lfill(str, length - fillup, fillChar);
	return str_rfill(s, length, fillChar);
}

static std::string str_mfill(std::string&& str, uint64_t length, const char fillChar[1]) {
	return str_mfill(str, length, fillChar);
}

static std::string str_mfill(std::string& str, uint64_t length){
	return str_mfill(str, length, " ");
}

static std::string str_mfill(std::string&& str, uint64_t length){
	return str_mfill(str, length, " ");
}

/**
 * @brief Splits a string by given delimiter.
 * @param input string to split
 * @param delimiter
 * @return vector of substrings
 */
static std::vector<std::string>* splitString(std::string input, const std::string& delimiter){
	std::vector<std::string>* split = new std::vector<std::string>();
	size_t pos = input.find(delimiter);
	while(pos != std::string::npos){
		split->push_back(input.substr(0, pos));
		input.erase(0, pos + delimiter.length());
		pos = input.find(delimiter);
	}
	split->push_back(input);
	return split;
}

static std::string dotNumber(const std::string & input){
	std::string out;
	int64_t pos = input.length() - 1;
	uint8_t interval = 0;
	for(;pos >= 0; --pos){
		out = input.substr(pos,1) + out;
		++interval;
		if(interval == 3 && pos != 0){
			interval = 0;
			out = "," + out;
		}
	}
	return out;
}



// trim from start (in place)
inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}



#endif //QUEUEBENCHMARK_STRINGMANIPULATION_H
