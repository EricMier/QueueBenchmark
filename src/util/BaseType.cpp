/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/
 
#include "BaseType.h"

std::ostream& operator<<(std::ostream& os, const BaseType & type){
	switch(type){
		case BaseType::uint32:
			os << "uint32";
			break;
		case BaseType::uint64:
			os << "uint64";
			break;
		case BaseType::int64:
			os << "int64";
			break;
		case BaseType::int32:
			os << "int32";
			break;
		case BaseType::string:
			os << "string";
			break;
		case BaseType::float32:
			os << "float32";
			break;
		case BaseType::double64:
			os << "double64";
			break;
		case BaseType::undefined:
			os << "undefined";
			break;
	}
	return os;
}



