/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_CSVREADER_H
#define QUEUEBENCHMARK_CSVREADER_H

#include <stdlibs>
#include <forward>
#include <utils>

class CSVReader {
  public:
	// Constructor
	CSVReader();
	CSVReader(char delimiter);
	// Copy constructor
	CSVReader(const CSVReader & other);
	// Move constructor
	CSVReader(CSVReader && other) noexcept;
	// Destructor
	virtual ~CSVReader();
	
	// Assignment operator
	virtual CSVReader & operator=(const CSVReader & rhs) = delete;
	// Move assignment operator
	virtual CSVReader & operator=(CSVReader && rhs) noexcept = delete;
	
	CSVReader& setColumnNames(std::vector<std::string> * columnNames);
	CSVReader& setColumnTypes(std::vector<BaseType> * columnTypes);
	CSVReader& fromFile(std::string && pathToFile);
	CSVReader& fromFile(std::string &  pathToFile);
	CSVReader& toTable(Table *  table);
	CSVReader& setLimit(uint64_t limit);
	CSVReader& go();
	CSVReader& reset();
	
  private:
	char delimiter;
	std::string pathToFile;
	std::vector<std::string> * columnNames;
	std::vector<BaseType> * columnTypes;
	Table * table;
	uint64_t limit = (uint64_t) -1;
};


#endif //QUEUEBENCHMARK_CSVREADER_H
