/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_INCLUDELIBS_H
#define QUEUEBENCHMARK_INCLUDELIBS_H
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>

#include <unistd.h>
//#ifdef USE_LIBNUMA
//  #include <numa.h>
//#endif
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <memory>

#include <cmath>
#include <random>
#include <vector>
#include <map>
#include <functional>
#include <cstring>
#include <algorithm>
#include <iterator>
#include <math.h>


#include <bits/stdc++.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>

#ifdef INCLUDE_STACKTRACE
  #include <boost/stacktrace.hpp>
#endif

#if defined( AVX512 )
  #include <immintrin.h>
#endif



#endif //QUEUEBENCHMARK_INCLUDELIBS_H
