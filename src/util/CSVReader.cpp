/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "CSVReader.h"
#include <stdexcept>
#include <storage>
#include <logging>



/**
 * @brief Constructor of CSVReader
 */
CSVReader::CSVReader() : CSVReader(';') {
}
CSVReader::CSVReader(char delimiter) {
	this->delimiter = delimiter;
}

/**
 * @brief Copy constructor of CSVReader
 * @param other Instance of CSVReader to copy from
 */
CSVReader::CSVReader(const CSVReader & other) {
	// @todo Implement CSVReader copy constructor
}


/**
 * @brief Move constructor of CSVReader
 * @param other RValue of type CSVReader to steal from
 */
CSVReader::CSVReader(CSVReader && other) noexcept {
	// @todo Implement CSVReader move constructor
}


/**
 * @brief Destructor of CSVReader
 */
CSVReader::~CSVReader() {
	// @todo Implement CSVReader destructor
}


///**
// * @brief Assignment operator of CSVReader
// * @param rhs Instance of CSVReader to copy from
// * @return Reference of this CSVReader-object
// */
//CSVReader & CSVReader::operator=(const CSVReader & rhs) {
//	// @todo Implement assignment operator
//	throw std::runtime_error("CSVReader::AssignmentOperator not implemented!");
//	return *this;
//}
//
///**
// * @brief Move assignment operator of CSVReader
// * @param rhs RValue of type CSVReader to steal from
// * @return Reference of this CSVReader-object
// */
//CSVReader & CSVReader::operator=(CSVReader && rhs) noexcept {
//	// @todo Implement move assignment operator
//	throw std::runtime_error("CSVReader::MoveAssignmentOperator not implemented!");
//	return *this;
//}

CSVReader & CSVReader::fromFile(std::string && pathToFile) {
	this->pathToFile = std::move(pathToFile);
	return *this;
}

CSVReader & CSVReader::fromFile(std::string & pathToFile) {
	this->pathToFile = pathToFile;
	return *this;
}

CSVReader & CSVReader::toTable(Table * table) {
	this->table = table;
	return *this;
}

CSVReader & CSVReader::setColumnNames(std::vector<std::string> * columnNames) {
	this->columnNames = columnNames;
	return *this;
}

CSVReader & CSVReader::setColumnTypes(std::vector<BaseType> * columnTypes) {
	this->columnTypes = columnTypes;
	return *this;
}

CSVReader & CSVReader::go() {
	if(pathToFile.empty()){
		severe("No path to parse specified. Use CSVReader::fromFile(<path>).")
	}
	if(!columnNames || columnNames->empty()){
		severe("No column names specified. Use CSVReader::setColumnNames(<vector<string>*>).")
	}
	if(!columnTypes || columnTypes->empty()){
		severe("No column types specified. Use CSVReader::setColumnTypes(<vector<BaseType>*>).")
	}
	if(!table){
		severe("No Table to save to specified. Use CSVReader::toTable(<Table*>).")
	}
	
	std::vector<ColumnIdentifier> columnIds;
	for(auto n : *columnNames){
		ColumnIdentifier c = table->getColumnID(n);
		if(!c.isValid()) {
			severe("Column \"" + n + "\" does not exists in table \"" + table->getName() + "\"");
		}
		columnIds.push_back(c);
	}
	
	std::fstream csvIn;
	csvIn.open(pathToFile, std::ios::in);
	if(!csvIn.is_open()){
		severe("File " << pathToFile << " not found.")
	}
	std::string line, value, temp;
	uint64_t counter = 0UL;
	while(getline(csvIn, line)){
		if(counter++ >= limit) break;
		GenericRow row;
		//// read line
//		getline(csvIn, line);
//		cout << "line: " << line << endl;
		trim(line);
		std::stringstream lineStream(line);
		//// parse line
		while(getline(lineStream, value, delimiter) && row.getColumns().size() < columnNames->size()){
			row.append(value);
		}
//		cout << row << endl;
		table->insert(&row, &columnIds);
	}
	
	reset();
	return *this;
}

CSVReader & CSVReader::reset() {
	pathToFile = "";
	columnNames = nullptr;
	columnTypes = nullptr;
	table = nullptr;
	limit = (uint64_t) -1;
	return *this;
}

CSVReader & CSVReader::setLimit(uint64_t limit) {
	this->limit = limit;
	return *this;
}
