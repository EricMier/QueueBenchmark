/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class Identifier;
class ColumnIdentifier;
class ColumnIdentifier;
class OperatorIdentifier;
class PartitionIndex;
class TableIdentifier;

#ifndef QUEUEBENCHMARK_IDENTIFIER_H
#define QUEUEBENCHMARK_IDENTIFIER_H

#include <stdlibs>

class Identifier {
	friend class IdentifierHash;
	friend class IdentifierEqual;
//	friend struct std::hash<ColumnIdentifier>;
  public:
	/// Constructor
	Identifier();
	explicit Identifier(uint64_t id);
	/// Copy constructor
	Identifier(const Identifier & other);
	/// Move constructor
	Identifier(Identifier && other) noexcept;
	// Destructor
	~Identifier() = default;
	
	/// Assignment operator
	Identifier & operator=(const Identifier & rhs);
	/// Move assignment operator
	Identifier & operator=(Identifier && rhs) noexcept;
	
	void* operator new(size_t size);
	void operator delete(void* p);
	
	bool operator == (const Identifier & rhs) const {
		return id == rhs.id;
	};
	bool operator != (const Identifier & rhs) const {
		return id != rhs.id;
	};
	bool operator >= (const Identifier & rhs) const {
		return id >= rhs.id;
	};
	bool operator <= (const Identifier & rhs) const {
		return id >= rhs.id;
	};
	bool operator >  (const Identifier & rhs) const {
		return id > rhs.id;
	};
	bool operator <  (const Identifier & rhs) const {
		return id < rhs.id;
	};
//	size_t operator()(const Identifier & other){
//		return std::hash<uint64_t>()(id);
//	}
//	bool operator()(const Identifier & id1, const Identifier & id2){
//		return id1.id == id2.id;
//	}
	
	uint64_t getID() const{
		return id;
	}
	
	[[nodiscard]]
	std::string to_string() const {
		return std::to_string(id);
	}
	
	bool isValid() const {
		return id != (uint32_t) -1;
	}
	
	void reset() {
		id = Identifier::invalid;
	}
	
	static Identifier createInvalid(){
		return Identifier();
	}
	static Identifier createNew(){
		std::lock_guard<std::mutex> lock(idLock_);
		return Identifier(nextId_++);
	}
	
	Identifier & operator++() {
		id++;
		return *this;
	}
	
	const Identifier operator++(int) {
		const Identifier tmp = *this;
		++*this;
		return tmp;
	}
	static std::string type() {
		return "Identifier";
	}
	
	static void resetAllIds();
  
    inline static uint64_t invalid = (uint64_t) -1;
  protected:
	uint64_t id;
	inline static uint64_t nextId_ = 0UL;
	inline static std::mutex idLock_;
	
	
};



//// stream out operator
std::ostream & operator<<(std::ostream & os, const Identifier & id);



class PartitionIndex : public Identifier {
	friend class Identifier;
	friend class PartitionIndexProducer;
  public:
	PartitionIndex() : Identifier() {};
	PartitionIndex(uint64_t id) : Identifier(id) {};
	static PartitionIndex createInvalid(){
		return PartitionIndex();
	}
	static PartitionIndex createNew() = delete;
	static std::string type() {
		return "PartitionIndex";
	}
  private:
};


// todo assign to table for reuse
class PartitionIndexProducer {
  private:
	unsigned long index = 0UL;
	std::mutex lock;
  public:
	PartitionIndex nextID();
	PartitionIndex highestId();
};



class ColumnIdentifier : public Identifier {
	friend class Identifier;
  public:
	ColumnIdentifier() : Identifier() {};
	static ColumnIdentifier createInvalid(){
		return ColumnIdentifier();
	}
	static ColumnIdentifier createNew(){
		std::lock_guard<std::mutex> lock(idLock);
		return ColumnIdentifier(nextId++);
	}
	static std::string type() {
		return "ColumnIdentifier";
	}
  private:
	ColumnIdentifier(uint64_t id) : Identifier(id) {};
	inline static uint64_t nextId = 0UL;
	inline static std::mutex idLock;
};


//namespace std {
//	template<>
//	struct hash<ColumnIdentifier> {
//		size_t operator()(const ColumnIdentifier& cid) const {
//			return hash<uint64_t>()(cid.id);
//		}
//	};
//}


class TableIdentifier : public Identifier {
	friend class Identifier;
  public:
	TableIdentifier() : Identifier() {};
	static TableIdentifier createInvalid(){
		return TableIdentifier();
	}
	static TableIdentifier createNew(){
		std::lock_guard<std::mutex> lock(idLock);
		return TableIdentifier(nextId++);
	}
	static std::string type() {
		return "TableIdentifier";
	}
  private:
	explicit TableIdentifier(uint64_t id) : Identifier(id) {};
	inline static uint64_t nextId = 0UL;
	inline static std::mutex idLock;
};



class OperatorIdentifier : public Identifier {
	friend class Identifier;
  public:
	OperatorIdentifier() : Identifier() {};
	static OperatorIdentifier createInvalid(){
		return OperatorIdentifier();
	}
	static OperatorIdentifier createNew(){
		std::lock_guard<std::mutex> lock(idLock);
		return OperatorIdentifier(nextId++);
	}
	static std::string type() {
		return "OperatorIdentifier";
	}
  private:
	explicit OperatorIdentifier(uint64_t id) : Identifier(id) {};
	inline static uint64_t nextId = 0UL;
	inline static std::mutex idLock;
};



class WorkerIdentifier : public Identifier {
	friend class Identifier;
  public:
	WorkerIdentifier() : Identifier() {};
	static WorkerIdentifier createInvalid(){
		return WorkerIdentifier();
	}
	static WorkerIdentifier createNew(){
		std::lock_guard<std::mutex> lock(idLock);
		return WorkerIdentifier(nextId++);
	}
	static std::string type(){
		return "WorkerIdentifier";
	}
  private:
	explicit WorkerIdentifier(uint64_t id) : Identifier(id) {};
	inline static uint64_t nextId = 0UL;
	inline static std::mutex idLock;
};



class CoordinatorIdentifier : public Identifier {
	friend class Identifier;
  public:
	CoordinatorIdentifier() : Identifier() {};
	static CoordinatorIdentifier createInvalid(){
		return CoordinatorIdentifier();
	}
	static CoordinatorIdentifier createNew(){
		std::lock_guard<std::mutex> lock(idLock);
		return CoordinatorIdentifier(nextId++);
	}
	static std::string type(){
		return "CoordinatorIdentifier";
	}
  
  protected:
	explicit CoordinatorIdentifier(uint64_t id) : Identifier(id) {};
	inline static uint64_t nextId = 0UL;
	inline static std::mutex idLock;
};


inline
void Identifier::resetAllIds(){
	Identifier::nextId_ = 0UL;
	ColumnIdentifier::nextId = 0UL;
	TableIdentifier::nextId = 0UL;
	OperatorIdentifier::nextId = 0UL;
	WorkerIdentifier::nextId = 0UL;
	CoordinatorIdentifier::nextId = 0UL;
}


class IdentifierHash {
  public:
	size_t operator()(const Identifier & hashable){
		return std::hash<uint64_t>()(hashable.id);
	}
	size_t operator()(const ColumnIdentifier & hashable) const {
		return std::hash<uint64_t>()(hashable.id);
	}
	size_t operator()(const PartitionIndex & hashable) const {
		return std::hash<uint64_t>()(hashable.id);
	}
	size_t operator()(const TableIdentifier & hashable) const {
		return std::hash<uint64_t>()(hashable.id);
	}
	size_t operator()(const WorkerIdentifier & hashable) const {
		return std::hash<uint64_t>()(hashable.id);
	}
	size_t operator()(const CoordinatorIdentifier & hashable) const {
		return std::hash<uint64_t>()(hashable.id);
	}
	size_t operator()(const OperatorIdentifier & hashable) const {
		return std::hash<uint64_t>()(hashable.id);
	}
};


class IdentifierEqual {
  public:
	bool operator()(const Identifier & id1, const Identifier & id2){
		return id1.id == id2.id;
	}
	bool operator()(const ColumnIdentifier & id1, const ColumnIdentifier & id2){
		return id1.id == id2.id;
	}
	bool operator()(const PartitionIndex & id1, const PartitionIndex & id2){
		return id1.id == id2.id;
	}
	bool operator()(const TableIdentifier & id1, const TableIdentifier & id2){
		return id1.id == id2.id;
	}
	bool operator()(const WorkerIdentifier & id1, const WorkerIdentifier & id2){
		return id1.id == id2.id;
	}
	bool operator()(const CoordinatorIdentifier & id1, const CoordinatorIdentifier & id2){
		return id1.id == id2.id;
	}
	bool operator()(const OperatorIdentifier & id1, const OperatorIdentifier & id2){
		return id1.id == id2.id;
	}
};


template<typename Key, typename Value>
using identifier_map = std::unordered_map<Key, Value, IdentifierHash>;


#endif //QUEUEBENCHMARK_IDENTIFIER_H
