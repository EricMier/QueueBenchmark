/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_BENCHMARKRESULT_OLD_H
#define QUEUEBENCHMARK_BENCHMARKRESULT_OLD_H

using namespace std;
#include "platform/Platform.h"

struct BenchmarkResult_old {
	std::string label;
	unsigned operatorCnt;
	double optimalValue;
	double measuredValue;
	
	BenchmarkResult_old(std::string label, unsigned operatorCnt, double optimalValue, double measuredValue)
			: label(label), operatorCnt(operatorCnt), optimalValue(optimalValue), measuredValue(measuredValue) {}
};

template<typename ResultT>
struct MeasurementTuple {
	ResultT value;
	ResultT optimum;
	MeasurementTuple(ResultT& value,  ResultT& optimum)  : value(value), optimum(optimum) {}
	MeasurementTuple(ResultT&& value, ResultT&& optimum) : value(value), optimum(optimum) {}
	MeasurementTuple(ResultT& value)  : value(value), optimum(0) {}
	MeasurementTuple(ResultT&& value) : value(value), optimum(0) {}
	string to_csv() const {
		stringstream s;
		s << value;
		if(optimum != 0)
			s << "," << optimum;
		return s.str();
	}
};

template<typename ResultT>
struct Measurement {
	vector<MeasurementTuple<ResultT>> measurments;
	void addMeasurement(ResultT& value, ResultT& optimum){
		measurments.emplace_back(value, optimum);
	}
	void addMeasurement(ResultT&& value, ResultT&& optimum){
		measurments.emplace_back(value, optimum);
	}
	void addMeasurement(ResultT& value){
		measurments.emplace_back(value);
	}
	void addMeasurement(ResultT&& value){
		measurments.emplace_back(value);
	}
	string to_csv() const {
		stringstream s;
		string delimiter = "";
		for(MeasurementTuple<ResultT> const& m : measurments){
			s << delimiter << m.to_csv();
			delimiter = ";";
		}
		return s.str();
	}
};

template<typename SeriesT, typename ResultT>
struct BenchmarkSeries {
	string testSeriesLabel = "default series label";
	SeriesT testSeriesValue;
	vector<Measurement<ResultT>> testResults;
	Measurement<ResultT>* currentMeasurement;
	
	BenchmarkSeries(string && testSeriesLabel, SeriesT testSeriesValue)
	: testSeriesLabel(testSeriesLabel), testSeriesValue(testSeriesValue) {}
	
	BenchmarkSeries(string & testSeriesLabel, SeriesT testSeriesValue)
	: testSeriesLabel(testSeriesLabel), testSeriesValue(testSeriesValue) {}
	
	void newMeasurement(){
		#if __cplusplus > 201402L
		currentMeasurement = &testResults.emplace_back();
		#else
		testResults.emplace_back();
//		currentMeasurement = &(*testResults.end()) - 1;
		currentMeasurement = &testResults.at(testResults.size() - 1);
		#endif
	}
	
	void addMeasurement(ResultT& value, ResultT& optimum){
		currentMeasurement->addMeasurement(value, optimum);
	}
	void addMeasurement(ResultT&& value, ResultT&& optimum){
		currentMeasurement->addMeasurement(value, optimum);
	}
	void addMeasurement(ResultT& value){
		currentMeasurement->addMeasurement(value);
	}
	void addMeasurement(ResultT&& value){
		currentMeasurement->addMeasurement(value);
	}
 
	string to_csv() const {
		stringstream s;
		s << testSeriesLabel << "###" << testSeriesValue << "$";
		string delimiter;
		for(Measurement<ResultT> const& res : testResults){
			s << delimiter << res.to_csv();
			delimiter = "###";
		}
		return s.str();
	}
};

template<typename ValueT, typename SeriesT, typename ResultT>
struct BenchmarkResult {
	string testCase = "testcase_not_set";
	string subTestCase = "subtestcase_not_set";
	string suffix = "";
	string prefix = "";
	string version = "version_not_set";
	//// label of x axis
	string testValuesLabel = "default x axis label";
	//// test values / x values
	vector<ValueT> * testValues;
	//// array of test scenarios and their results
	vector<BenchmarkSeries<SeriesT, ResultT>> * testSeries;
	
	vector<string> * additionalInformation;
	map<string, map<string, string>> settings;
	
	//// Constructor
	BenchmarkResult() {
		testValues = new vector<ValueT>();
		testSeries = new vector<BenchmarkSeries<SeriesT, ResultT>>();
		additionalInformation = new vector<string>();
	}
	
	//// Copy constructor
	BenchmarkResult(BenchmarkResult & orig) {
		testCase = orig.testCase;
		subTestCase = orig.subTestCase;
		version = orig.version;
		testValuesLabel = orig.testValuesLabel;
		
		testValues = new vector<ValueT>(orig.testValues->begin(), orig.testValues->end());
		testSeries = new vector<BenchmarkSeries<SeriesT, ResultT>>
						(orig.testSeries->begin(), orig.testSeries->end());
		additionalInformation = new vector<string>(orig.additionalInformation->begin(),
		                                           orig.additionalInformation->end());
	}
	
	/**
	 * Adds the values to the benchmark, which shall be benchmarked for each series
	 * @param values std::vector of values
	 */
	void addTestValues(vector<ValueT> && values) {
		testValues->insert(testValues->begin(), values.begin(), values.end());
	}
	
	
	/**
	 * Adds the values to the benchmark, which shall be benchmarked for each series
	 * @param values std::vector of values
	 */
	void addTestValues(vector<ValueT> & values) {
		testValues->insert(testValues->begin(), values.begin(), values.end());
	}
	
	/**
	 * Add a value to the benchmark, which shall be benchmarked for each series
	 * @param value The Value
	 */
	void addTestValue(ValueT& value){
		testValues->push_back(value);
	}
	
	/**
	 * Add a value to the benchmark, which shall be benchmarked for each series
	 * @param value The Value
	 */
	void addTestValue(ValueT&& value){
		testValues->push_back(value);
	}
	
	/**
	 * Adds the values and labels of the test series that shall be benchmarked
	 * @param values std::vector of values
	 * @param labels std::vector of labels
	 */
	void addTestSeries(vector<SeriesT> & values, vector<string> & labels) {
		if (values.size() != labels.size()) {
			cout << "Size of series values and its labels dont match." << endl;
			return;
		}
		for (int i = 0; i < values.size(); ++i) {
			testSeries->emplace_back(labels.at(i), values.at(i));
		}
	}
	
	/**
	 * Adds the values and labels of the test series that shall be benchmarked
	 * @param values std::vector of values
	 * @param labels std::vector of labels
	 */
	void addTestSeries(vector<SeriesT> && values, vector<string> && labels) {
		if (values.size() != labels.size()) {
			cout << "Size of series values and its labels dont match." << endl;
			return;
		}
		for (int i = 0; i < values.size(); ++i) {
			testSeries->emplace_back(labels.at(i), values.at(i));
//			testSeries->push_back(BenchmarkSeries(labels.at(i), series.at(i)));
		}
	}
	
	void addTestSeries(SeriesT& value, string& label){
		testSeries->emplace_back(label, value);
	}
	void addTestSeries(SeriesT&& value, string&& label){
		testSeries->emplace_back(label, value);
	}
	
	void addComment(string & info) {
		additionalInformation->push_back(info);
	}
	
	void addComment(string && info) {
		additionalInformation->push_back(info);
	}
	
	void addSetting(string category, string setting, string value){
		settings[category][setting] = value;
	}
	
	/**
	 * Builds and returns the benchmark results as a csv string
	 * @return
	 */
	string to_csv() const {
		stringstream s;
		for(BenchmarkSeries<SeriesT, ResultT> const& series : *testSeries){
			s << series.to_csv() << "\n";
		}
		return s.str();
	}
	
	/**
	 * Builds and returns the benchmark settings as an ini string
	 * @return
	 */
	string to_ini() {
		stringstream xvalues;
		string delimiter = "";
		for(auto const& v : *testValues){
			xvalues << delimiter << v;
			delimiter = ",";
		}
//		settings["Benchmark"]["XValues"] = xvalues.str();
		addSetting("Plot", "XValues", xvalues.str());
		stringstream s;
		for(auto const& [cat, set] : settings){
			s << "[" << cat << "]\n";
			for(auto const& [key, value] : set){
				s << key << "=" << value << "\n";
			}
		}
		return s.str();
	}
	
	/**
	 * Builds and returns addtitional informations as a string
	 * @return
	 */
	string to_txt() const {
		stringstream s;
		for(auto const& info : *additionalInformation){
			s << info << "\n";
		}
		return s.str();
	}
	
	/**
	 * Builds and returns the subdirectory the results of this benchmark shall be stored
	 * @return
	 */
	string path(){
		return "output/" + Platform::getHostname() + "/" + testCase + "/" + subTestCase + "/" + version;
	}
};

#endif //QUEUEBENCHMARK_BENCHMARKRESULT_OLD_H
