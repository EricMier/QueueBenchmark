/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Identifier.h"
#include <logging>
#include <core/memory/MemoryManager.h>


Identifier::Identifier() {
	id = Identifier::invalid;
}

/**
 * @brief Constructor of Identifier
 */
Identifier::Identifier(uint64_t id) : id(id) {
}

/**
 * @brief Copy constructor of Identifier
 * @param other Instance of Identifier to copy from
 */
Identifier::Identifier(const Identifier & other) {
	id = other.id;
}


/**
 * @brief Move constructor of Identifier
 * @param other RValue of type Identifier to steal from
 */
Identifier::Identifier(Identifier && other) noexcept {
	id = other.id;
}



/**
 * @brief Assignment operator of Identifier
 * @param rhs Instance of Identifier to copy from
 * @return Reference of this Identifier-object
 */
Identifier & Identifier::operator=(const Identifier & rhs) {
	#ifdef DEBUG
	if(!rhs.isValid()){
		warn("Assigning from invalid Identifier! lhs: " << type() << "/" << id << " //  rhs: " << rhs.type() << "/" << rhs.id)
	}
	#endif
	id = rhs.id;
	return *this;
}

/**
 * @brief Move assignment operator of Identifier
 * @param rhs RValue of type Identifier to steal from
 * @return Reference of this Identifier-object
 */
Identifier & Identifier::operator=(Identifier && rhs) noexcept {
	/// todo: call operator(&...) ?
	#ifdef DEBUG
	if(!rhs.isValid()){
		warn("Assigning from invalid Identifier!")
	}
	#endif
	id = rhs.id;
	rhs.id = Identifier::invalid;
	return *this;
}

void * Identifier::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(Identifier));
}

void Identifier::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Identifier));
}


std::ostream & operator<<(std::ostream & os, const Identifier & id){
	os << id.to_string();
	return os;
}


/**
 * @brief Provides the next unique PartitionIndex.
 * @return
 */
PartitionIndex PartitionIndexProducer::nextID() {
	std::lock_guard<std::mutex> lck(lock);
	return PartitionIndex(index++);
}

/**
 * @brief Returns the highest PartitionIndex of an existing Partition.
 * @return
 */
PartitionIndex PartitionIndexProducer::highestId() {
	return PartitionIndex(index-1);
}
