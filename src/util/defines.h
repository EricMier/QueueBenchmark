/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   defines.h
 * Author: EricMier
 *
 * Created on 8. August 2019, 14:04
 */
#ifndef DEFINES_H
#define DEFINES_H

#include <libnuma>
#include <thread/ThreadManager.h>

/// HB max 128 / 4 = 32 // HB2 max 112 / 4 = 28 // -1 for coordinators
#define WORKER_CNT_PER_NODE (Platform::numCpus() / Platform::numNodes() - 1)
/// use all cores
//#define WORKER_CNT_PER_NODE 28 - 1
/// use only main threads
//#define WORKER_CNT_PER_NODE 14 - 1
/// use only one worker
//#define WORKER_CNT_PER_NODE 7

/// here you can limit the nodes/sockets which are used
#define MAX_NODES_USED 1000000


/// testing with only one coordinator on node 0
//#define ONLY_ONE_COORDINATOR

/// print out the results of the operators (warning: may be a lot of output)
//#define PRINT_OPERATOR_RESULTS
/// print out when a coordinator/worker threads starts or ends its duty
//#define PRINT_THREAD_MESSAGES

/// if defined, jobs are enqueued as pointer references, otherwise job objects are queued
#define ENQUEUE_JOB_POINTER






//#define CONTROL_NUMA_POLICY
#define REPEAT_BENCHMARK 1
#define WORKLOAD_RANDOM_MEMORY_ACCESS

//// if defined, data is copied into partitions, otherwise its a pointer to a global data vector
#define USE_PHYSICAL_PARTITIONING


#ifdef USE_LIBNUMA
  /// allocates an new object using numa_alloc_onnode on the currently set allocation node of this thread
  #define __alloc(Type) numa::numa_alloc_onnode(sizeof(Type), ThreadManager::currentAllocationNode.getPhysicalNode())
  /// allocates an new array using numa_alloc_onnode on the currently set allocation node of this thread
  #define __allocArray(Type, Elements) numa::numa_alloc_onnode(sizeof(Type) * (Elements), ThreadManager::currentAllocationNode.getPhysicalNode())
  
  /// allocates a defined size
  #define __allocSize(Size) numa::numa_alloc_onnode(Size, ThreadManager::currentAllocationNode.getPhysicalNode())
  #define __deallocSize(Ptr, Size) numa::numa_free((Ptr), Size)
  
  /// shortcut for using __alloc
  #define __new(Type) new(numa::numa_alloc_onnode(sizeof(Type), ThreadManager::currentAllocationNode.getPhysicalNode())) Type
  /// shortcut for using __allocArray
  #define __newArray(Type, Elements) new(numa::numa_alloc_onnode(sizeof(Type) * (Elements), ThreadManager::currentAllocationNode.getPhysicalNode())) Type[Elements]

/// deallocates an object which is allocated with numa_alloc_onnode
  #define __delete(ObjPtr) numa::numa_free((ObjPtr) , sizeof(ObjPtr));
  /// deallocates an array which is allocated with numa_alloc_onnode
  #define __deleteArray(ObjPtr, Elements) numa::numa_free((ObjPtr), sizeof(ObjPtr[0]) * (Elements));
  /// destroys object which is allocated with numa_alloc_onnode and deallocates its memory
  #define __destruct(ObjPtr, Class) (ObjPtr)->~Class(); numa::numa_free((ObjPtr) , sizeof(ObjPtr));
  /// calls the destructor of each object in the array, then frees the memory
  #define __destructArray(ObjPtr, Class, Elements) { for(uint64_t i = 0; i < Elements; ++i){ (ObjPtr)[i].~Class(); }; numa::numa_free((ObjPtr), sizeof(ObjPtr[0]) * (Elements));}
  
  #define __move_pages(pid, count, page_address, nodes, status_ptr, flags) numa::numa_move_pages(pid, count, page_address, nodes, status_ptr, flags)
#else
  /// fallback macros, if libnuma is not available
  #define __alloc(Type) malloc(sizeof(Type))
  #define __allocArray(Type, Elements) malloc(sizeof(Type) * Elements)
  #define __allocSize(Size) malloc(Size)
  #define __deallocSize(Ptr, Size) free(Ptr)
  
  #define __new(Type) new(malloc(sizeof(Type))) Type
  #define __newArray(Type, Elements) new(malloc(sizeof(Type) * Elements)) Type[Elements]

  #define __delete(ObjPtr) free(ObjPtr);
//  #define __deleteArray(ObjPtr, Elements) free(ObjPtr);
  #define __deleteArray(ObjPtr, Elements) delete ObjPtr;
  #define __destruct(ObjPtr, Class) (ObjPtr)->~Class(); free(ObjPtr);
  #define __destructArray(ObjPtr, Class, Elements) { for(uint64_t i = 0; i < Elements; ++i){ (ObjPtr)[i].~Class(); }; free(ObjPtr);}
  
  #define __move_pages(pid, count, page_address, nodes, status_ptr, flags) (0)
#endif




/// legacy macros

#define MONITOR_VALS 3
#define MONITOR_GLOBAL_RUNTIME 0
#define MONITOR_FIND_JOB 1
#define MONITOR_EXECUTE_JOB 2


#define WORK_PACKAGE_SIZE 10
// how many partitions should be generated
#define PARTITION_CNT 28
// how many columns has the table
#define COLUMN_CNT 10
// how long an operator has to work/wait per tuple in ns (min/max)
#define SIMULATED_WAIT_MIN 25
#define SIMULATED_WAIT_MAX 50

//#define JOBS 10 * 1000
#define TUPLE_CNT 1000000

#define MICROBENCHMARK


// how many values per column
#define VALUES_PER_COLUMN (TUPLE_CNT / PARTITION_CNT + 1)




#endif /* DEFINES_H */

