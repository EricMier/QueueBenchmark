/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Randomizer.cpp
 * Author: EricMier
 * 
 * Created on 2. August 2019, 15:10
 */

#include "Randomizer.h"
#include "platform/Platform.h"

Randomizer::Randomizer() : Randomizer(0) {
//	std::random_device dev;
//	std::mt19937 rng_(dev);
//	rng = rng_;
//	dist = std::uniform_int_distribution<std::mt19937::result_type>(0, pow((float) 2, sizeof(int) * 8 - 1));
//	dist = std::uniform_int_distribution<std::mt19937::result_type>();
//	rng.seed(0);
}

Randomizer::Randomizer(unsigned long seed) {
	dist = std::uniform_int_distribution<std::mt19937::result_type>();
	rng.seed(seed);
}

Randomizer::Randomizer(unsigned long min, unsigned long max) : Randomizer(min, max, 0) {}

Randomizer::Randomizer(unsigned long min, unsigned long max, unsigned long seed): min(min), max(max), seed(seed) {
	dist = std::uniform_int_distribution<std::mt19937::result_type>(min, max);
	rng.seed(seed);
}


Randomizer::Randomizer(const Randomizer& orig) {
}

Randomizer::~Randomizer() {
}




//void Randomizer::fillTableRandom(Table* table, size_t partitions, size_t columns, size_t size) {
//	for(size_t i = 0; i < partitions; i++){
////		std::cout << "Create partition #" << i << std::endl;
//		std::map<std::string, Column> p_columns;
//
//		fillPartitionRandom(&p_columns, columns, size);
//
//		Partition* p = new Partition(p_columns, PartitionMetadata((int) i, table->getName()));
//
////		p.print();
//
//		table->appendPartition(p);
//	}
//}



//void Randomizer::fillPartitionRandom(std::map<std::string, Column>* partition, size_t columns, size_t size){
//	for(size_t i = 0; i < columns; i++){
//		partition->insert({"Col" + std::to_string(i), Column()});
//	}
//	fillPartitionRandom(partition, size);
//}
//
//void Randomizer::fillPartitionRandom(std::map<std::string, Column>* partition, size_t size){
//	for(auto& column : *partition){
//		fillColumnRandom(&column.second, size);
//	}
//}

//void Randomizer::fillColumnRandom(Column* column, size_t size){
//	if(column->data)
//		delete column->data;
//	column->data = new std::vector<int>();
//
//	for(size_t i = 0; i < size; i++){
////		column->data[i] = dist(rng);
////		column->data[i] = 1;
//		column->data->push_back(dist(rng));
//	}
//}

/// @todo delete
[[deprecated]]
void Randomizer::fillPartitionList(std::vector<Partition*>* partitionList, size_t partitions, unsigned long wait) {
//	for(size_t i = 0; i < partitions; i++){
//		if(wait == 0)
//			wait = dist(rng);
//		partitionList->push_back(new Partition(wait ? wait : dist(rng), PartitionMetadata(i,"undefined table")));
//	}
}

unsigned long Randomizer::random() {
    return dist(rng);
}

void Randomizer::generateData(std::vector<unsigned long> * data, size_t size) {
	std::vector<std::vector<unsigned long>*> parts;
	std::vector<Randomizer*> generators;
	std::vector<std::thread*> threads;
//	unsigned long * newData = static_cast<unsigned long *>(malloc(size * sizeof(unsigned long)));
	unsigned long * newData = data->data();
	int threadCnt = Platform::numCpus() / Platform::numNodes();
	
	size_t valuesLeft = size;
	size_t valuesPerThread;
	if(size <= threadCnt)
		valuesPerThread = size;
	else
		valuesPerThread = size / threadCnt + 1;
	
	std::cout << "Start data generation\n";
	//// start generator threads
	for(int core = 0; core < threadCnt; core++){
		generators.push_back(new Randomizer(min,max, seed + core));
		parts.push_back(new std::vector<unsigned long>());
		size_t s = (valuesPerThread >= valuesLeft ? valuesLeft : valuesPerThread);
//		threads.push_back(new thread(&Randomizer::generateDataPart, generators.at(core), parts.at(core), s));
		threads.push_back(new std::thread(&Randomizer::generateDataPart, generators.at(core), &newData[core * valuesPerThread], s));
//		threads.push_back(new thread(&Randomizer::generateDataPart, generators.at(core), &(&data[0])[core * valuesPerThread], s));
		valuesLeft -= s;
		if(valuesLeft == 0)
			break;
	}
	
	
	//// wait for threads
	for(auto t : threads){
		t->join();
	}
	
	//// concatenate parts
//	for(int i = 0; i < Platform::numCpus(); i++){
//		if(parts[i].size() == 0)
//			break;
//		data->insert(data->end(), parts[i].begin(), parts[i].end());
//	}

//	cout << "concatenate parts" << endl;
//	for(auto p : parts){
//		data->insert(data->end(), std::make_move_iterator(p->begin()), std::make_move_iterator(p->end()));
//	}
//	data->insert(data->end(), std::begin(newData), std::end(newData));
//	cout << "Copy data vector" << endl;
//	std::copy(newData, newData + sizeof(newData) / sizeof(newData[0]), data->begin());
//	std::copy(newData, newData + size, data->begin());
}
//void Randomizer::generateDataPart(vector<unsigned long> * data, size_t size) {
//	for (size_t i = 0; i < size; ++i) {
//		data->push_back(random());
//	}
//}
void Randomizer::generateDataPart(unsigned long * data, size_t size) {
	for (size_t i = 0; i < size; ++i) {
		data[i] = random();
	}
}

/// @todo delete
[[deprecated]]
void Randomizer::partitionData(std::vector<unsigned long> * data, size_t parts, std::vector<Partition *> * partitionList) {
//	unsigned long entriesLeft = data->size();
//	unsigned long partitionSize = floor((double) data->size() / (double) parts);
//	unsigned long offset = 0;
//	unsigned long idx = 0;
//	unsigned long size = 0;
//	mutex partitionListMutex;
//	//// clear old partition list
//	for(Partition* part : *partitionList)
//		delete part;
//	partitionList->clear();
//
//	#ifdef USE_PHYSICAL_PARTITIONING
//	//// physical partitioning
////	vector<unsigned long>* newData;
//	vector<thread> threads;
//	while(entriesLeft){
////		newData = nullptr;
//		size = (entriesLeft >= partitionSize ? partitionSize : entriesLeft);
//		threads.emplace_back([&, offset, size, idx](){
//			auto newData = new vector<unsigned long>(data->begin() + offset, data->begin() + offset + size);
////			cout << "New data elements:" << newData->at(19) << " tltSize: " << newData->size() << endl;
//			auto newPart = new Partition(newData, 0, size, PartitionMetadata(idx, "undefined table"));
//			lock_guard<mutex> lockGuard(partitionListMutex);
//			partitionList->push_back(newPart);
//		});
//		offset += size;
//		entriesLeft -= size;
//		idx++;
//	}
////	cout << "Debug in Randomizer::partitionData" << endl;
//	for(auto& t : threads){
//		t.join();
//	}
//	#else
//	//// logical partitioning
//	while(entriesLeft){
//		size = (entriesLeft >= partitionSize ? partitionSize : entriesLeft);
//		partitionList->push_back(new Partition(data, offset, size, PartitionMetadata(idx, "undefinde table")));
//		entriesLeft -= size;
//		offset += size;
//		idx++;
//	}
//	#endif
//	cout << "Created " + to_string(idx) + " partitions with size of " + to_string(partitionSize) + "\n";
}


