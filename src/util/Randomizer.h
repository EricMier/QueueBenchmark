/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Randomizer.h
 * Author: EricMier
 *
 * Created on 2. August 2019, 15:10
 */

class Randomizer;

#ifndef RANDOMIZER_H
#define RANDOMIZER_H

#include "storage/Partition.h"
#include "storage/Table.h"

class Randomizer {
  public:
	Randomizer();
	Randomizer(unsigned long seed);
	Randomizer(unsigned long min, unsigned long max);
	Randomizer(unsigned long min, unsigned long max, unsigned long seed);
	Randomizer(const Randomizer & orig);
	virtual ~Randomizer();
	
//	void fillOperators(std::vector<Operator_old> * operators, size_t operatorCnt);
	
//	void fillTableRandom(Table * table, size_t partitions, size_t columns, size_t size);
//	void fillPartitionRandom(std::map<std::string, Column> * partition, size_t size);
//	void fillPartitionRandom(std::map<std::string, Column> * partition, size_t columns, size_t size);
//	void fillColumnRandom(Column * column, size_t size);
	
    /// @todo delete
    [[deprecated]]
	void fillPartitionList(std::vector<Partition *> * partitionList, size_t partitions, unsigned long wait = 0);
	
	void generateData(std::vector<unsigned long>* data, size_t size);
//	void generateDataPart(vector<unsigned long>* data, size_t size);
	void generateDataPart(unsigned long* data, size_t size);
    /// @todo delete
    [[deprecated]]
	void partitionData(std::vector<unsigned long>* data, size_t parts, std::vector<Partition*>* partitionList);
	
	unsigned long random();
  
  private:
	std::uniform_int_distribution<std::mt19937::result_type> dist;
	std::mt19937 rng;
	unsigned long min;
	unsigned long max;
	unsigned long seed;
	
	
};

#endif /* RANDOMIZER_H */

