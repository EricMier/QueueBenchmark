/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   forwards.h
 * Author: EricMier
 *
 * Created on 7. August 2019, 14:24
 */

#ifndef FORWARDS_H
#define FORWARDS_H


//// container
class Database;
class Table;
class Partition;
class Column;
class GenericRow;
//// engine
class PrototypeEngine;
//// monitoring
class Monitor_old;
//// partitioning
class TablePartitionPolicy;
class TableInsertHelper;
//// platform
struct CPUInfo;
class Node;
class NodeIdentifier;
class Platform;
//// task
class Job_old;
class Operator_old;
//// thread
class Coordinator;
class NodeAllocationFence;
class ThreadManager;
class Worker;
//// util
class Randomizer;
class Identifier;
class ColumnIdentifier;
class OperatorIdentifier;
class PartitionIndex;
class TableIdentifier;
class OperatorDependency;



#endif /* FORWARDS_H */

