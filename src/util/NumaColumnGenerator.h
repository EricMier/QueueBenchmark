/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_SRC_UTIL_NUMACOLUMNGENERATOR_H
#define QUEUEBENCHMARK_SRC_UTIL_NUMACOLUMNGENERATOR_H

#include <storage>
#include <threading>
#include <memory.h>

namespace morphstore {
    
    class NumaColumnGenerator {
      public:
        using column_t = morphstore::column<morphstore::uncompr_f>;
        using columnPair_t = std::tuple<NodeIdentifier, column_t*>;
        
        //// Constructor
        NumaColumnGenerator() = delete;
        //// Copy constructor
        NumaColumnGenerator(const NumaColumnGenerator & other) = delete;
        //// Move constructor
        NumaColumnGenerator(NumaColumnGenerator && other) noexcept = delete;
        // Destructor
        virtual ~NumaColumnGenerator() = delete;
        
        //// Assignment operator
        NumaColumnGenerator & operator =(const NumaColumnGenerator & rhs) = delete;
        //// Move assignment operator
        NumaColumnGenerator & operator =(NumaColumnGenerator && rhs) noexcept = delete;
        
        template< template< typename > class TDistribution >
        static PartitionedColumn<NumaPartitioner, uncompr_f, uint64_t>*
        generate_with_distribution(
          std::vector<NodeIdentifier*>* nodes,  size_t countValues,
          TDistribution<uint64_t> distr,        size_t seed = 0
        ){
            size_t valuesPerNode = countValues / nodes->size();
            size_t remainder     = countValues % nodes->size();
            if(seed == 0)
                seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            std::default_random_engine generator(seed);
            
            auto result = new PartitionedColumn<NumaPartitioner, uncompr_f, uint64_t>();
            
            for(auto*& node : *nodes){
                /// direct allocations to certain node
                auto fence = NodeAllocationFence(*node);
                
                size_t elements = valuesPerNode + ((remainder > 0) ? 1 : 0);
                if(remainder > 0) --remainder;
                /// Generate new column on certain node
                auto col = new column<uncompr_f>(sizeof(uint64_t) * elements);
                uint64_t* colData = col->get_data();
                
                /// Generate data
                for(size_t i = 0; i < elements; ++i){
                    colData[i] = distr(generator);
                }
                col->set_meta_data(elements, sizeof(uint64_t) * elements);
                
                /// add column to PartitionedColumn output
                result->addPartition(col);
            }
            
            return result;
        };
        
        
        template< template< typename > class TDistribution >
        static
        std::vector<columnPair_t> *
        generate_with_distribution(
          TablePartitioningPolicy * tpp,
          TDistribution<uint64_t> distr,            size_t seed = 0
          ){
            return generate_with_distribution(&tpp->nodes, tpp->partitionDispatchStrategy, tpp->partitionCount, tpp->tableSize, distr, seed);
        }
        
        template< template< typename > class TDistribution >
        static
        std::vector<columnPair_t> *
        generate_with_distribution(
          std::vector<NodeIdentifier*>* nodes,      PartitionDispatchStrategy pds,
          size_t partitions,                        size_t countValues,
          TDistribution<uint64_t> distr,            size_t seed = 0
          ){
            size_t partitionsPerNode   = partitions / nodes->size();
            size_t remainingPartitions = partitions % nodes->size();
            
            size_t valuesPerPartition = countValues / partitions;
            size_t remainingValues    = countValues % partitions;
            
            if(seed == 0)
                seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            std::default_random_engine generator(seed);
            
            auto result = new std::vector<columnPair_t>();
            
            auto currentNode = nodes->begin();
            /// only used for ::FillNode
            size_t nodesVisited = 0;      // number of nodes already filled
            size_t iterateCondition = 0;  // placeholder condition variable for node iteration
            
            for(size_t partition = 0; partition < partitions; ++partition){
                NodeIdentifier nodeID = **currentNode;
                auto fence = NodeAllocationFence(nodeID);
                
                size_t valuesToGenerate = valuesPerPartition + ((remainingValues > 0) ? 1 : 0);
                if(remainingValues > 0) --remainingValues;
                
                /// Generate new column on certain node
                auto col = new column<uncompr_f>(sizeof(uint64_t) * valuesToGenerate);
                uint64_t* colData = col->get_data();
                
                /// Generate data
                for(size_t i = 0; i < valuesToGenerate; ++i){
                    colData[i] = distr(generator);
                }
                col->set_meta_data(valuesToGenerate, sizeof(uint64_t) * valuesToGenerate);

//                debug("Generated " << valuesToGenerate << " values on node " << nodeID);
                result->push_back(columnPair_t{nodeID, col});
                
                /// Increment node pointer depending on dispatching strategy
                switch(pds){
                    case PartitionDispatchStrategy::FillNode:
                        /// switch to next node after partitionsPerNode (+1)
                        /**
                         * This is equivalent to:
                         *
                         * // Base threshold depending on node-iterations
                         * cond = (nodeCount+1) * partitionsPerNode;
                         *
                         * // Add additional partitions to threshold, which are remaining, depending on node-iterations
                         * if( remainingPartitions > nodeCount){
                         *   // Add amount of already done node-iterations +1
                         *   cond += nodeCount+1;
                         * } else {
                         *   // Add amount of remaining partitions to threshold (they are already added to previous nodes)
                         *   cond += remainingPartitions;
                         * }
                         */
                        iterateCondition = ((nodesVisited + 1) * partitionsPerNode + ((remainingPartitions > nodesVisited) ? (nodesVisited + 1) : remainingPartitions ));
//                        info("Partition: " << partition << "  Condition: " << cond);
                        if((partition+1) >= iterateCondition){
                            ++currentNode;
                            ++nodesVisited;
                        }
                        break;
                    case PartitionDispatchStrategy::RoundRobin:
                        /// switch to next node after each partition
                        ++currentNode;
                        if(currentNode == nodes->end()){
                            currentNode = nodes->begin();
                        }
                        break;
                } // switch(pds)
            } // for(partitions)
            return result;
        }
        
        template< template< typename > class TDistribution >
        static
        std::vector<columnPair_t> *
        generate_with_distribution_multithreaded(
          TablePartitioningPolicy * tpp,  TDistribution<uint64_t> distr,
          size_t threads,                 size_t seed = 0
          ){
            return
              generate_with_distribution_multithreaded(
                &tpp->nodes,         tpp->partitionDispatchStrategy,
                tpp->partitionCount, tpp->tableSize,
                distr,               threads,
                seed);
        }
        
        template< template< typename > class TDistribution >
        static
        std::vector<columnPair_t> *
        generate_with_distribution_multithreaded(
          std::vector<NodeIdentifier*>* nodes,      PartitionDispatchStrategy pds,
          uint64_t partitions,                      uint64_t countValues,
          TDistribution<uint64_t> distr,            uint64_t threads,
          uint64_t seed = 0
          ){
            uint64_t partitionsPerNode   = partitions / nodes->size();
            uint64_t remainingPartitions = partitions % nodes->size();
            
            uint64_t valuesPerPartition = countValues / partitions;
            uint64_t remainingValues    = countValues % partitions;
            
            if(seed == 0)
                seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
            std::default_random_engine generator(seed);
            
            auto result = new std::vector<columnPair_t>();
            
            auto currentNode = nodes->begin();
            /// only used for ::FillNode
            uint64_t nodesVisited = 0;      // number of nodes already filled
            uint64_t iterateCondition = 0;  // placeholder condition variable for node iteration
            
            std::vector<std::thread*> threadDump;
            std::vector<CPUInfo*> * cpus = Platform::getAllThreads();
            
            for(uint64_t partition = 0; partition < partitions; ++partition){
                NodeIdentifier nodeID = **currentNode;
                auto fence = NodeAllocationFence(nodeID);
                
                uint64_t valuesToGenerate = valuesPerPartition + ((remainingValues > 0) ? 1 : 0);
                if(remainingValues > 0) --remainingValues;
                
                /// Generate new column on certain node
                auto col = new column<uncompr_f>(sizeof(uint64_t) * valuesToGenerate);
                uint64_t* colData = col->get_data();
                
                uint64_t stepWidth = std::max(uint64_t(valuesToGenerate / threads) , 100UL);
//                info("Generate " << valuesToGenerate << " values on partition " << partition)
//                size_t stepWidth = valuesToGenerate / threads;
                for(uint64_t t = 0; t < threads; ++t){
                    TDistribution<uint64_t> threadDistr(distr.a(), distr.b());
                    std::default_random_engine threadGenerator(distr(generator));
                    
                    uint64_t threadOffset = t * stepWidth;
                    uint64_t threadValuesToGenerate = std::min(stepWidth, valuesToGenerate - threadOffset);
                    if(threadOffset > valuesToGenerate) {
                        debugh("Break after " << t << " threads.")
                        break;
                    }
//                    info("offset " << threadOffset << " val " << threadValuesToGenerate)
//                    info("Run " << t)
                    uint64_t* threadDataPtr = colData + threadOffset;
                    
                    /// create thread
                    std::thread * th =
                      new std::thread(
                        [threadDistr, threadGenerator, threadValuesToGenerate, threadDataPtr, t] () mutable {
//                            debug("Thread " << t << " start.")
                            /// Generate data
                            for(uint64_t i = 0; i < threadValuesToGenerate; ++i){
                                threadDataPtr[i] = threadDistr(threadGenerator);
                            }
//                            debug("Thread " << t << " done.")
                        }
                      );
                    threadDump.push_back(th);
                    /// set cpu to pin
                    auto cpu = cpus->at(t % cpus->size());
                    ThreadManager::pinThread(th, cpu);
                }
                col->set_meta_data(valuesToGenerate, sizeof(uint64_t) * valuesToGenerate);

//                debug("Generated " << valuesToGenerate << " values on node " << nodeID);
                result->push_back(columnPair_t{nodeID, col});
                
                /// Increment node pointer depending on dispatching strategy
                switch(pds){
                    case PartitionDispatchStrategy::FillNode:
                        /// switch to next node after partitionsPerNode (+1)
                        /**
                         * This is equivalent to:
                         *
                         * // Base threshold depending on node-iterations
                         * cond = (nodeCount+1) * partitionsPerNode;
                         *
                         * // Add additional partitions to threshold, which are remaining, depending on node-iterations
                         * if( remainingPartitions > nodeCount){
                         *   // Add amount of already done node-iterations +1
                         *   cond += nodeCount+1;
                         * } else {
                         *   // Add amount of remaining partitions to threshold (they are already added to previous nodes)
                         *   cond += remainingPartitions;
                         * }
                         */
                        iterateCondition = ((nodesVisited + 1) * partitionsPerNode + ((remainingPartitions > nodesVisited) ? (nodesVisited + 1) : remainingPartitions ));
//                        info("Partition: " << partition << "  Condition: " << cond);
                        if((partition+1) >= iterateCondition){
                            ++currentNode;
                            ++nodesVisited;
                        }
                        break;
                    case PartitionDispatchStrategy::RoundRobin:
                        /// switch to next node after each partition
                        ++currentNode;
                        if(currentNode == nodes->end()){
                            currentNode = nodes->begin();
                        }
                        break;
                } // switch(pds)
                
//                info("Thread Count: " << threadDump.size());
                if(threadDump.size() > std::pow(2, 14)) {
                    for (auto & t : threadDump) {
                        t->join();
                        delete t;
                    }
                    threadDump.clear();
                }
            } // for(partitions)
            
            debug("Wait for threads")
            /// wait for all threads to finish
            for(auto& t : threadDump){
                t->join();
                delete t;
            }
            
            return result;
          
        }
      private:
      
    };
} /// namespace morphstore

#endif //QUEUEBENCHMARK_SRC_UTIL_NUMACOLUMNGENERATOR_H
