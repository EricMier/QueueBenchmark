/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Monitor.h"

Monitor* Monitor::globalMonitor;
Monitor* Monitor::subRunMonitor;
thread_local Monitor* Monitor::threadMonitor;

Monitor::Monitor() {
	tasks[MonitoringType::GlobalRuntime] = MonitorTask();
	tasks[MonitoringType::wDequeuingJob] = MonitorTask();
	tasks[MonitoringType::wExecuteJob] = MonitorTask();
	tasks[MonitoringType::cProcessBuffer] = MonitorTask();
	tasks[MonitoringType::cProcessGlobalBuffer] = MonitorTask();
	tasks[MonitoringType::Test] = MonitorTask();
	tasks[MonitoringType::Test2] = MonitorTask();
}

Monitor::Monitor(const Monitor& orig) {
}

Monitor::~Monitor() = default;

void Monitor::start(MonitoringType type) {
	MonitorTask* task = &tasks[type];
	task->duration = std::chrono::nanoseconds(0);
	task->iterations = 0;
	task->begin = std::chrono::high_resolution_clock::now();
	task->active = true;
}

void Monitor::resume(MonitoringType type) {
	MonitorTask* task = &tasks[type];
	if(task->active)
		return;
	task->begin = std::chrono::high_resolution_clock::now();
	task->active = true;
}

void Monitor::end(MonitoringType type) {
	MonitorTask* task = &tasks[type];
	if(!task->active)
		return;
	auto end = std::chrono::high_resolution_clock::now();
	task->duration = task->duration + std::chrono::duration_cast<std::chrono::nanoseconds>(end - task->begin);
	task->iterations++;
	task->active = false;
}

void Monitor::transmit(Monitor * target, MonitoringType type) {
	if(tasks[type].duration.count() > 0) {
		std::lock_guard<mutex> lock(target->monitorLock);
		target->transmittedTasks[type].push_back(tasks[type]);
	}
}

//template<typename format>
//format Monitor::getDuration(MonitoringType type) {
//	return std::chrono::duration_cast<format>(tasks[type].duration);
//}

unsigned Monitor::getIterations(MonitoringType type) {
	return tasks[type].iterations;
}

chrono::nanoseconds Monitor::averageTransmitted(MonitoringType type) {
	vector<MonitorTask>& tt = transmittedTasks[type];
	std::chrono::nanoseconds avgDuration = std::chrono::nanoseconds(0);
	uint64_t count = 0UL;
	for(auto& t : tt){
		avgDuration += t.duration;
		++count;
	}
	if(count)
		avgDuration = avgDuration / count;
	return avgDuration;
}


//void Monitor::transmit(Monitor * target) {
//	std::lock_guard<std::mutex> lockGuard(target->monitorLock);
//	target->transmittedTasks.emplace_back(tasks.begin(),tasks.end());
//}

//chrono::nanoseconds Monitor::averageTransmitted(MonitoringType type) {
//	chrono::nanoseconds result(0);
//	unsigned cnt = 0;
//	for( auto const& t : processedTasks_Avrg){
//		if(t.at(monitorTask).duration.count() > 0) {
//			result += t.at(monitorTask).duration;
//			cnt++;
//		}
//	}
//
//	if (cnt)
//		return result / cnt;
//	return chrono::nanoseconds();
//}
//
//
//
//chrono::nanoseconds Monitor::bestTransmitted(MonitoringType type) {
//	chrono::nanoseconds result(0);
//	unsigned cnt = 0;
//	for( auto const& t : processedTasks_Best){
//		if(t.at(type).duration.count() > 0) {
//			result += t.at(type).duration;
//			cnt++;
//		}
//	}
//
//	if (cnt)
//		return result / cnt;
//	return chrono::nanoseconds();
//}
//
//
//chrono::nanoseconds Monitor::worstTransmitted(MonitoringType type) {
//	chrono::nanoseconds result(0);
//	unsigned cnt = 0;
//	for( auto const& t : processedTasks_Wrst){
//		if(t.at(type).duration.count() > 0) {
//			result += t.at(type).duration;
//			cnt++;
//		}
//	}
//
//	if (cnt)
//		return result / cnt;
//	return chrono::nanoseconds();
//}


void Monitor::resetThreadMonitor() {
	if(Monitor::threadMonitor != nullptr)
		delete Monitor::threadMonitor;
	Monitor::threadMonitor = new Monitor();
	for(auto t : Monitor::threadMonitor->tasks){
		t.second.duration = chrono::nanoseconds(0);
	}
}

void Monitor::resetGlobalMonitor() {
	if(Monitor::globalMonitor != nullptr)
		delete Monitor::globalMonitor;
	Monitor::globalMonitor = new Monitor();
	for(auto t : Monitor::globalMonitor->tasks){
		t.second.duration = chrono::nanoseconds(0);
	}
}

void Monitor::resetSubRunMonitor() {
	if(Monitor::subRunMonitor != nullptr)
		delete Monitor::subRunMonitor;
	Monitor::subRunMonitor = new Monitor();
	for(auto t : Monitor::subRunMonitor->tasks){
		t.second.duration = chrono::nanoseconds(0);
	}
}
//
//string Monitor::getTimes(MonitoringType monitorTask) {
//	stringstream ss;
//	for( auto const& t : transmittedTasks){
//		ss << chrono::duration_cast<chrono::milliseconds>(t.at(monitorTask).duration).count() << " ";
//	}
//	return ss.str();
//}
//
//void Monitor::processTransmitted() {
//	if(transmittedTasks.size() == 0)
//		return;
//	auto best = transmittedTasks.begin();
//	auto worst = transmittedTasks.begin();
//	//// delete best and worst value
//	for( auto it = transmittedTasks.begin(); it != transmittedTasks.end(); it++ ){
//		if( it->at(MONITOR_GLOBAL_RUNTIME).duration < best->at(MONITOR_GLOBAL_RUNTIME).duration)
//			best = it;
//		if( it->at(MONITOR_GLOBAL_RUNTIME).duration > worst->at(MONITOR_GLOBAL_RUNTIME).duration)
//			worst = it;
//	}
//	transmittedTasks.erase(best);
//	transmittedTasks.erase(worst);
//
//
//	best = transmittedTasks.begin();
//	worst = transmittedTasks.begin();
//	for( auto it = transmittedTasks.begin(); it != transmittedTasks.end(); it++ ){
//		if( it->at(MONITOR_GLOBAL_RUNTIME).duration < best->at(MONITOR_GLOBAL_RUNTIME).duration)
//			best = it;
//		if( it->at(MONITOR_GLOBAL_RUNTIME).duration > worst->at(MONITOR_GLOBAL_RUNTIME).duration)
//			worst = it;
//	}
//
//	//// save best and worst value
//	processedTasks_Best.push_back(*best);
//	processedTasks_Wrst.push_back(*worst);
//
//	//// compute average
//
//	vector<MonitorTask> avrg;
//	for(int i = 0; i < MONITOR_VALS; i++){
//		avrg.emplace_back();
//	}
//
//	unsigned cnt = 0;
//	for( auto& t : transmittedTasks){
//		for(int i = 0; i < MONITOR_VALS; i++){
//			avrg.at(i) += t.at(i);
//		}
//		cnt++;
//	}
//	for(int i = 0; i < MONITOR_VALS; i++){
//		avrg.at(i) /= cnt;
//	}
//
//	//// save average
//	processedTasks_Avrg.push_back(avrg);
//	transmittedTasks.clear();
//
//
//
//}


