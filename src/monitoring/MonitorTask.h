/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_MONITORTASK_H
#define QUEUEBENCHMARK_MONITORTASK_H

#include <stdlibs>

struct MonitorTask {
	std::chrono::nanoseconds duration = std::chrono::nanoseconds(0);
	std::chrono::system_clock::time_point begin;
	std::chrono::system_clock::time_point end;
	unsigned iterations = 0;
	bool active = false;
	void operator+=(MonitorTask& other){
		duration += other.duration;
	}
	
	void operator/=(unsigned divisor){
		duration = duration / divisor;
	}
};

#endif //QUEUEBENCHMARK_MONITORTASK_H
