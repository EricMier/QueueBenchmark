/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef MONITOR_H
#define MONITOR_H



#include <monitoring/MonitorTask.h>
#include <util/BenchmarkResult_old.h>


enum class MonitoringType {
	GlobalRuntime,
	wDequeuingJob,
	wExecuteJob,
	wExecuteGlobalJob,
	cProcessBuffer,
	cProcessGlobalBuffer,
	InitialEnqueuing,
	Test,
	Test2,
	DataGeneration
};


class Monitor {
  public:
	Monitor();
	Monitor(const Monitor & orig);
	virtual ~Monitor();
	
	void start(MonitoringType type);
	void resume(MonitoringType type);
	void end(MonitoringType type);
	
	void transmit(Monitor * target, MonitoringType type);
	
	template<typename format = std::chrono::nanoseconds>
	format getDuration(MonitoringType type){
	    return std::chrono::duration_cast<format>(tasks[type].duration);
    };
	unsigned getIterations(MonitoringType type);
	
	void processTransmitted();
	chrono::nanoseconds averageTransmitted(MonitoringType type);
	chrono::nanoseconds bestTransmitted(MonitoringType type);
	chrono::nanoseconds worstTransmitted(MonitoringType type);
	string getTimes(MonitoringType monitorTask);
	
	unordered_map<MonitoringType, MonitorTask> tasks;
	map<MonitoringType, vector<MonitorTask>> transmittedTasks;
	vector<vector<MonitorTask>> processedTasks_Best;
	vector<vector<MonitorTask>> processedTasks_Avrg;
	vector<vector<MonitorTask>> processedTasks_Wrst;
	
	mutex monitorLock;
	
	static Monitor * globalMonitor;
	static Monitor * subRunMonitor;
	static thread_local Monitor * threadMonitor;
	static void resetThreadMonitor();
	static void resetGlobalMonitor();
	static void resetSubRunMonitor();


private:
};

#endif /* MONITOR_H */

