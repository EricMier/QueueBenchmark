/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef MONITOR_OLD_H
#define MONITOR_OLD_H



#include <monitoring/MonitorTask.h>
#include <util/BenchmarkResult_old.h>


class Monitor_old {
  public:
	Monitor_old();
	Monitor_old(const Monitor_old & orig);
	virtual ~Monitor_old();
	
	void start(unsigned monitorTask);
	void resume(unsigned monitorTask);
	void end(unsigned monitorTask);
	
	void transmit(Monitor_old * target);
	
	chrono::nanoseconds getDuration(unsigned monitorTask);
	unsigned getIterations(unsigned monitorTask);
	
	void processTransmitted();
	chrono::nanoseconds averageTransmitted(unsigned monitorTask);
	chrono::nanoseconds bestTransmitted(unsigned monitorTask);
	chrono::nanoseconds worstTransmitted(unsigned monitorTask);
	string getTimes(unsigned monitorTask);
	
	vector<MonitorTask> tasks;
	vector<vector<MonitorTask>> transmittedTasks;
	vector<vector<MonitorTask>> processedTasks_Best;
	vector<vector<MonitorTask>> processedTasks_Avrg;
	vector<vector<MonitorTask>> processedTasks_Wrst;
	
	mutex monitorLock;
	
	static Monitor_old * globalMonitor;
	static thread_local Monitor_old * threadMonitor;
	static void resetThreadMonitor();
	static void resetGlobalMonitor();


private:
	
};

#endif /* MONITOR_OLD_H */

