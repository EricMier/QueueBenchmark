/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Monitor_old.h"
#include <utils>

Monitor_old* Monitor_old::globalMonitor;
thread_local Monitor_old* Monitor_old::threadMonitor;

Monitor_old::Monitor_old() {
	for(int i = 0; i < MONITOR_VALS; i++){
		tasks.emplace_back();
	}
}

Monitor_old::Monitor_old(const Monitor_old& orig) {
}

Monitor_old::~Monitor_old() = default;

void Monitor_old::start(unsigned monitorTask) {
	MonitorTask* task = &tasks.at(monitorTask);
	task->duration = std::chrono::nanoseconds(0);
	task->iterations = 0;
	task->begin = std::chrono::high_resolution_clock::now();

}

void Monitor_old::resume(unsigned monitorTask) {
	MonitorTask* task = &tasks.at(monitorTask);
	task->begin = std::chrono::high_resolution_clock::now();
}

void Monitor_old::end(unsigned monitorTask) {
	auto end = std::chrono::high_resolution_clock::now();
	MonitorTask* task = &tasks.at(monitorTask);
	task->duration = task->duration + std::chrono::duration_cast<std::chrono::nanoseconds>(end - task->begin);
	task->iterations++;
}

chrono::nanoseconds Monitor_old::getDuration(unsigned monitorTask) {
	return tasks.at(monitorTask).duration;
}

unsigned Monitor_old::getIterations(unsigned monitorTask) {
	return tasks.at(monitorTask).iterations;
}

void Monitor_old::transmit(Monitor_old * target) {
	std::lock_guard<std::mutex> lockGuard(target->monitorLock);
	target->transmittedTasks.emplace_back(tasks.begin(),tasks.end());
}

chrono::nanoseconds Monitor_old::averageTransmitted(unsigned monitorTask) {
	chrono::nanoseconds result(0);
	unsigned cnt = 0;
	for( auto const& t : processedTasks_Avrg){
		if(t.at(monitorTask).duration.count() > 0) {
			result += t.at(monitorTask).duration;
			cnt++;
		}
	}

	if (cnt)
		return result / cnt;
	return chrono::nanoseconds();
}

//chrono::nanoseconds Monitor::averageTransmitted(unsigned monitorTask) {
//	vector<MonitorTask> avrg;
//	for(int i = 0; i < MONITOR_VALS; i++){
//		avrg.emplace_back();
//	}
//
//	unsigned cnt = 0;
//	for( auto& t : processedTasks_Avrg){
//		for(int i = 0; i < MONITOR_VALS; i++){
//			avrg.at(i) += t.at(i);
//		}
//		cnt++;
//	}
//	for(int i = 0; i < MONITOR_VALS; i++){
//		avrg.at(i) /= cnt;
//	}
//
//	return avrg.at(monitorTask).duration;
//}

chrono::nanoseconds Monitor_old::bestTransmitted(unsigned monitorTask) {
	chrono::nanoseconds result(0);
	unsigned cnt = 0;
	for( auto const& t : processedTasks_Best){
		if(t.at(monitorTask).duration.count() > 0) {
			result += t.at(monitorTask).duration;
			cnt++;
		}
	}

	if (cnt)
		return result / cnt;
	return chrono::nanoseconds();
}
//chrono::nanoseconds Monitor::bestTransmitted(unsigned monitorTask) {
//	vector<MonitorTask> best;
//	for(int i = 0; i < MONITOR_VALS; i++){
//		best.emplace_back();
//	}
//
//	unsigned cnt = 0;
//	for( auto& t : processedTasks_Best){
//		for(int i = 0; i < MONITOR_VALS; i++){
//			best.at(i) += t.at(i);
//		}
//		cnt++;
//	}
//	for(int i = 0; i < MONITOR_VALS; i++){
//		best.at(i) /= cnt;
//	}
//
//	return best.at(monitorTask).duration;
//}

chrono::nanoseconds Monitor_old::worstTransmitted(unsigned monitorTask) {
	chrono::nanoseconds result(0);
	unsigned cnt = 0;
	for( auto const& t : processedTasks_Wrst){
		if(t.at(monitorTask).duration.count() > 0) {
			result += t.at(monitorTask).duration;
			cnt++;
		}
	}

	if (cnt)
		return result / cnt;
	return chrono::nanoseconds();
}
//chrono::nanoseconds Monitor::worstTransmitted(unsigned monitorTask) {
//	vector<MonitorTask> wrst;
//	for(int i = 0; i < MONITOR_VALS; i++){
//		wrst.emplace_back();
//	}
//
//	unsigned cnt = 0;
//	for( auto& t : processedTasks_Wrst){
//		for(int i = 0; i < MONITOR_VALS; i++){
//			wrst.at(i) += t.at(i);
//		}
//		cnt++;
//	}
//	for(int i = 0; i < MONITOR_VALS; i++){
//		wrst.at(i) /= cnt;
//	}
//
//	return wrst.at(monitorTask).duration;
//}

void Monitor_old::resetThreadMonitor() {
	if(Monitor_old::threadMonitor != nullptr)
		delete Monitor_old::threadMonitor;
	Monitor_old::threadMonitor = new Monitor_old();
	for(auto t : Monitor_old::threadMonitor->tasks){
		t.duration = chrono::nanoseconds(0);
	}
}

void Monitor_old::resetGlobalMonitor() {
	if(Monitor_old::globalMonitor != nullptr)
		delete Monitor_old::globalMonitor;
	Monitor_old::globalMonitor = new Monitor_old();
	for(auto t : Monitor_old::globalMonitor->tasks){
		t.duration = chrono::nanoseconds(0);
	}
}

string Monitor_old::getTimes(unsigned monitorTask) {
	stringstream ss;
	for( auto const& t : transmittedTasks){
		ss << chrono::duration_cast<chrono::milliseconds>(t.at(monitorTask).duration).count() << " ";
	}
	return ss.str();
}

void Monitor_old::processTransmitted() {
	if(transmittedTasks.size() == 0)
		return;
	auto best = transmittedTasks.begin();
	auto worst = transmittedTasks.begin();
	//// delete best and worst value
	for( auto it = transmittedTasks.begin(); it != transmittedTasks.end(); it++ ){
		if( it->at(MONITOR_GLOBAL_RUNTIME).duration < best->at(MONITOR_GLOBAL_RUNTIME).duration)
			best = it;
		if( it->at(MONITOR_GLOBAL_RUNTIME).duration > worst->at(MONITOR_GLOBAL_RUNTIME).duration)
			worst = it;
	}
	transmittedTasks.erase(best);
	transmittedTasks.erase(worst);
	
	
	best = transmittedTasks.begin();
	worst = transmittedTasks.begin();
	for( auto it = transmittedTasks.begin(); it != transmittedTasks.end(); it++ ){
		if( it->at(MONITOR_GLOBAL_RUNTIME).duration < best->at(MONITOR_GLOBAL_RUNTIME).duration)
			best = it;
		if( it->at(MONITOR_GLOBAL_RUNTIME).duration > worst->at(MONITOR_GLOBAL_RUNTIME).duration)
			worst = it;
	}
	
	//// save best and worst value
	processedTasks_Best.push_back(*best);
	processedTasks_Wrst.push_back(*worst);
	
	//// compute average
	
	vector<MonitorTask> avrg;
	for(int i = 0; i < MONITOR_VALS; i++){
		avrg.emplace_back();
	}
	
	unsigned cnt = 0;
	for( auto& t : transmittedTasks){
		for(int i = 0; i < MONITOR_VALS; i++){
			avrg.at(i) += t.at(i);
		}
		cnt++;
	}
	for(int i = 0; i < MONITOR_VALS; i++){
		avrg.at(i) /= cnt;
	}
	
	//// save average
	processedTasks_Avrg.push_back(avrg);
	transmittedTasks.clear();
	
	
	
}


