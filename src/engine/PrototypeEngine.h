/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class PrototypeEngine;

#ifndef QUEUEBENCHMARK_PROTOTYPEENGINE_H
#define QUEUEBENCHMARK_PROTOTYPEENGINE_H

#include <stdlibs>

class PrototypeEngine {
  public:
	// Destructor
	virtual ~PrototypeEngine();
	
	static PrototypeEngine* getInstance();
	static void start();
	static void pageAllocationTest();
	static void pageAllocationTest2();
	static void pageAllocationTest3();
	
	inline static uint64_t test_partitionCount = 0UL;
	inline static std::string test_version ="v0";
	inline static uint64_t workerCnt;
	
  private:
	// Constructor
	PrototypeEngine();
	// Copy constructor
	PrototypeEngine(const PrototypeEngine & other) = delete;
	// Move constructor
	PrototypeEngine(PrototypeEngine && other) noexcept = delete;
	
	static PrototypeEngine* instance;
	
};


#endif //QUEUEBENCHMARK_PROTOTYPEENGINE_H
