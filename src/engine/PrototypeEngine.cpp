/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "PrototypeEngine.h"

#include <storage/GenericRow.h>
#include <util/CSVReader.h>
#include <platform/Platform.h>
#include <partitioning/TablePartitioningPolicy.h>
#include <storage/Database.h>
#include <storage/Table.h>
#include <thread/ThreadManager.h>
#include <thread/NodeAllocationFence.h>
#include <thread/Coordinator.h>
#include <monitoring/Monitor.h>
#include "platform/Node.h"
#include "storage/Column.h"
//#include "core/operators/interfaces/group.h"
//#include "core/operators/scalar/group_uncompr.h"
#include "thread/MasterCoordinator.h"

PrototypeEngine* PrototypeEngine::instance = nullptr;

/**
 * @brief Constructor of PrototypeEngine
 */
PrototypeEngine::PrototypeEngine() {
	// @todo Implement PrototypeEngine constructor
}


/**
 * @brief Destructor of PrototypeEngine
 */
PrototypeEngine::~PrototypeEngine() {
	// @todo Implement PrototypeEngine destructor
}

PrototypeEngine * PrototypeEngine::getInstance() {
	if(instance == nullptr)
		instance = new PrototypeEngine();
	return instance;
}

int get_node2(void *p){
	#ifndef USE_LIBNUMA
	return -1;
	#endif
	
	int status[1];
	void *pa;
	unsigned long a;
	
	// round p down to the nearest page boundary
	a  = (unsigned long) p;
	a  = a - (a % ((unsigned long) getpagesize()));
	pa = (void *) a;
	
//	if (move_pages(0,1,&pa,NULL,status,0) != 0) {
//	if (numa_move_pages(0,1,&pa,NULL,status,0) != 0) {
	if (__move_pages(0,1,&pa,NULL,status,0) != 0) {
		fprintf(stderr,"Problem in %s line %d calling move_pages()\n",__FILE__,__LINE__);
		abort();
	}
	
	return(status[0]);
}

int get_node3(void* p){
	#ifdef USE_LIBNUMA
	void * ptr_to_check = p;
	/*here you should align ptr_to_check to page boundary */
	int status[1];
	long ret_code;
	status[0]=-1;
	ret_code=move_pages(0 /*self memory */, 1, &ptr_to_check,NULL, status, 0);
//	printf("Memory at %p is at %d node (retcode %d)\n", ptr_to_check, status[0], ret_code);
	return status[0];
	#else
	return -1;
	#endif
}
int get_node4(void *p){
	#ifndef USE_LIBNUMA
	return -1;
	#endif
	
	int status[1];
	
	if (move_pages(0,1,&p,NULL,status,0) != 0) {
		fprintf(stderr,"Problem in %s line %d calling move_pages()\n",__FILE__,__LINE__);
		abort();
	}
	
	return(status[0]);
}

/**
 * @brief This function starts the Engine, i.e. setting up Coordinator and Worker threads, loading data etc.
 */
void PrototypeEngine::start() {
	Database::createDatabase();
	
	CSVReader reader(';');
	if(false) {
		/// setup partitioning policy for table customer
		TablePartitioningPolicy customerPP;
		customerPP << *Platform::getNodeIds();
		customerPP.partitionDispatchStrategy = PartitionDispatchStrategy::RoundRobin;
		customerPP.tableSize = 30000; /// MAX: 30.000
		customerPP.partitionCount = 1;
		customerPP.fillupUnknownParameter();
		
		vector<string> customerColumns{
				"C_CUSTKEY", "C_NAME", "C_ADDRES",
				"C_CITY", "C_NATION", "C_REGION",
				"C_PHONE", "C_MKTSEGMENT"
		};
		vector<BaseType> customerTypes{
				BaseType::uint64, BaseType::uint64, BaseType::uint64,
				BaseType::uint64, BaseType::uint64, BaseType::uint64,
				BaseType::uint64, BaseType::uint64
		};
		
		/// create new table object(s)
		Table * customer = Database::createNewTable("Customer", &customerPP);
		/// add columns to table
		for (unsigned x = 0; x < customerColumns.size(); x++) {
			customer->addColumn(customerColumns.at(x), customerTypes.at(x));
		}
		/// add empty partitions to table
		customer->addPartitions(customerPP.partitionCount);
		/// Load table customer from (pre-encoded) file / tuples are automatically distributed
		/// over the partitions of table customer
		reader.setColumnNames(&customerColumns)
				.setColumnTypes(&customerTypes)
				.fromFile("resources/ssb/encoded/customer.tbl")
				.toTable(customer)
				.setLimit(customerPP.tableSize)
				.go();
		

		/// ungrouped Aggregation operator
		OperatorWrapper c_opAgg(OperatorType::Agg_Sum_Global);
		c_opAgg.targetTable = customer->getId();
		c_opAgg.in_ColumnIdentifier[0] = customer->getColumnID("C_CUSTKEY");
		c_opAgg.execFunction = &OperatorWrapper::AGG_SUM;
	
		/// group by operator
		OperatorWrapper c_opGr(OperatorType::Group_Unary);
		c_opGr.targetTable = customer->getId();
		c_opGr.in_ColumnIdentifier[0] = customer->getColumnID("C_MKTSEGMENT");
		c_opGr.execFunction = &OperatorWrapper::GROUP_BY;
	
		/// grouped aggregation operator
		OperatorWrapper c_opAggGr(OperatorType::Agg_Sum_Grouped);
		c_opAggGr.targetTable = customer->getId();
		c_opAggGr.in_ColumnIdentifier[0] = c_opGr.out_ColumnMeta[0]->cid;
		c_opAggGr.in_ColumnIdentifier[1] = customer->getColumnID("C_REGION");
		c_opAggGr.in_ColumnIdentifier[2] = c_opGr.out_ColumnMeta[1]->cid;
		c_opAggGr.execFunction = &OperatorWrapper::AGG_SUM_GROUPED;
		OperatorPrerequisite c_opAggGr_req;
		c_opAggGr_req += c_opAggGr.in_ColumnIdentifier[0]; /// group ids from preceding group by
		c_opAggGr_req += c_opAggGr.in_ColumnIdentifier[2]; /// extends from preceding group by
	
		/// projection operator
		OperatorWrapper c_opProj(OperatorType::Projection);
		c_opProj.targetTable = customer->getId();
		c_opProj.in_ColumnIdentifier[0] = customer->getColumnID("C_MKTSEGMENT");
		c_opProj.in_ColumnIdentifier[1] = c_opGr.out_ColumnMeta[1]->cid;
		c_opProj.execFunction = &OperatorWrapper::PROJECTION;
		OperatorPrerequisite c_opProj_req;
		c_opProj_req += c_opProj.in_ColumnIdentifier[1]; /// extends from preceding group by
	
		/// global synchronisation of Aggregation results
		OperatorWrapper c_opAggSumMerge(OperatorType::Agg_Sum_Merge);
		c_opAggSumMerge.targetTable = customer->getId();
		c_opAggSumMerge.in_ColumnIdentifier[0] = c_opProj.out_ColumnMeta[0]->cid;
		c_opAggSumMerge.in_ColumnIdentifier[1] = c_opAggGr.out_ColumnMeta[0]->cid;
		c_opAggSumMerge.execFunction = &OperatorWrapper::AGG_SUM_MERGE;
		OperatorPrerequisite c_opAggSumMerge_req;
		c_opAggSumMerge_req += c_opAggSumMerge.in_ColumnIdentifier[0];
		c_opAggSumMerge_req += c_opAggSumMerge.in_ColumnIdentifier[1];
	
		//	/// ungrouped Aggregation operator
		//	Operator l_opAgg(OperatorType::Agg_Sum_Global);
		//	l_opAgg.targetTable = lineorder->getId();
		//	l_opAgg.in_ColumnIdentifier[0] = lineorder->getColumnID("C_CUSTKEY");
		//	l_opAgg.execFunction = Operator::AGG_SUM;
	
	}
	
	/// create table LineOrder
	TablePartitioningPolicy * lineorderPP = new TablePartitioningPolicy();
//	lineorderPP << *Platform::getNodeIds();
	lineorderPP->addNodes(Platform::getNodeIds());
	lineorderPP->partitionDispatchStrategy = PartitionDispatchStrategy::RoundRobin;
//	lineorderPP->tableSize = 6 * 1000 * 1000; /// Max: 6.000.000
	lineorderPP->tableSize = 6001214; /// Max: 6.001.214 for lineorder.tbl
//	lineorderPP->tableSize = 59986052 - 1; /// Max: 59.986.051 for lineorder_scale_10.tbl
//	lineorderPP->tableSize = 119994608 - 1; /// Max: 119.994.607 for lineorder_scale_20.tbl
//	lineorderPP.partitionCount = 112*4;
//	lineorderPP.partitionCount = 27*4;
//	lineorderPP.partitionCount = 4;
//	lineorderPP.partitionCount = 1;

	/// take partition count from runtime parameter
	lineorderPP->partitionCount = test_partitionCount;
	lineorderPP->fillupUnknownParameter();
	
	vector<string> lineorderColumns {
		"L_ORDERKEY",       "L_PARTKEY",        "L_SUPPKEY",
		"L_LINENUMBER",     "L_QUANTITY",       "L_EXTENDEDPRICE",
		"L_DISCOUNT",       "L_TAX",            "L_RETURNFLAG",
		"L_LINESTATUS",     "L_SHIPDATE",       "L_COMMITDATE",
		"L_RECEIPTDATE",    "L_SHIPINSTRUCT",   "L_SHIPMODE",
		"L_COMMENT"
	};
	
	vector<BaseType> lineorderTypes {
		BaseType::uint64,   BaseType::uint64,   BaseType::uint64,
		BaseType::uint64,   BaseType::uint64,   BaseType::uint64,
		BaseType::uint64,   BaseType::uint64,   BaseType::uint64,
		BaseType::uint64,   BaseType::uint64,   BaseType::uint64,
		BaseType::uint64,   BaseType::uint64,   BaseType::uint64,
		BaseType::uint64
	};
	
	/// create table
	Table * lineorder = Database::createNewTable("Lineorder", lineorderPP);
	/// add columns to table
	for(uint64_t x = 0; x < lineorderColumns.size(); ++x){
		lineorder->addColumn(lineorderColumns.at(x), lineorderTypes.at(x));
	}
	/// create partitions in table
//	lineorder->addPartitions(lineorderPP.partitionCount);
	/// load data into table
	info("Load table lineorder... (" << dotNumber(to_string(lineorderPP->tableSize)) << " entries / " << dotNumber(to_string(lineorderPP->partitionCount)) << " partitions)");
	reader.setColumnNames(&lineorderColumns)
		  .setColumnTypes(&lineorderTypes)
		  .fromFile("resources/ssb/encoded/lineorder.tbl")
//		  .fromFile("resources/ssb/encoded/lineorder_scale_20.tbl")
		  .toTable(lineorder)
		  .setLimit(lineorderPP->tableSize)
		  .go();
	
	
	
	#ifdef DEBUG
//	{ /// print all partitions to stdout
//		vector<ColumnIdentifier> customerColumnIDs;
//		for (auto cname : customerColumns) {
//			customerColumnIDs.push_back(customer->getColumnID(cname));
//		}
//		for (int j = 0; j < customerPP.partitionCount; ++j) {
//			customer->printPartition(PartitionIndex(j), &customerColumnIDs);
//		}
//	}
	#endif
//	{ /// print all partitions to stdout
//		debug("Print table lineorder");
//		vector<ColumnIdentifier> lineorderColumnIDs;
//		for (auto lname : lineorderColumns) {
//			lineorderColumnIDs.push_back(lineorder->getColumnID(lname));
//		}
//		for (int j = 0; j < lineorderPP.partitionCount; ++j) {
//			lineorder->printPartition(PartitionIndex(j), &lineorderColumnIDs);
//			break;
//		}
//	}
//	return;
	/// for each table object of customer table, print all partition indices of that table/node
//	customer->printTables();
	lineorder->printTables();
	
	info("The highest PartitionIndex: " << lineorder->partitionIndexProducer->highestId());
	
	uint64_t total_runs = 10;
	for(uint64_t run = 0; run < total_runs; ++run) {
		info(GREEN << str_mfill(" new subrun (" + to_string(run+1) + "/" + to_string(total_runs) + ") ", 35+12+4, "="));



//		std::function<void(Operator*)> * lamb = new std::function<void(Operator*)>([](Operator*) {} );
//		auto func = [] () {
//
//		};
//
//		Operator l_opSel(OperatorType::Select);
//		l_opSel.targetTable = lineorder->getId();
//		l_opSel.in_ColumnIdentifier[0] = lineorder->getColumnID("L_PARTKEY");
//		l_opSel.val[0] = SelectOperator::greater_equal; // Select Operattion
//		l_opSel.val[1] = 42; // Operationswert
//		l_opSel.execFunction = new std::function<void(Operator*)> ( [] (Operator*) { <Operator Code> } );
//
//		Operator l_opProj(OperatorType::Projection);
//		l_opProj.targetTable = lineorder->getId();
//		l_opProj.in_ColumnIdentifier[0] = lineorder->getColumnID("L_QUANTITY");
//		l_opProj.in_ColumnIdentifier[1] = l_opSel.out_ColumnMeta[0]->cid;
//		l_opProj.execFunction = new std::function<void(Operator*)> ( [] (Operator*) { <Operator Code> } );
//
//		OperatorPrerequisite l_opProj_req;
//		l_opProj_req += l_opProj.in_ColumnIdentifier[1];
//
//		l_opProj.execFunction = new std::function<void(Operator*)> ( Operator::agg_sum_merge );
//


		/// group by operator
		OperatorWrapper l_opGr(OperatorType::Group_Unary);
		l_opGr.targetTable = lineorder->getId();
//		l_opGr.in_ColumnIdentifier[0] = lineorder->getColumnID("L_PARTKEY");
		l_opGr.in_ColumnIdentifier[0] = lineorder->getColumnID("L_SUPPKEY");
//		l_opGr.execFunction = &OperatorWrapper::GROUP_BY;

		
		/// grouped aggregation operator
		OperatorWrapper l_opAggGr(OperatorType::Agg_Sum_Grouped);
		l_opAggGr.targetTable = lineorder->getId();
		l_opAggGr.in_ColumnIdentifier[0] = l_opGr.out_ColumnMeta[0]->cid;
		l_opAggGr.in_ColumnIdentifier[1] = lineorder->getColumnID("L_QUANTITY");
		l_opAggGr.in_ColumnIdentifier[2] = l_opGr.out_ColumnMeta[1]->cid;
//		l_opAggGr.execFunction = &OperatorWrapper::AGG_SUM_GROUPED;
		OperatorPrerequisite l_opAggGr_req;
		l_opAggGr_req += l_opAggGr.in_ColumnIdentifier[0]; /// group ids from preceding group by
		l_opAggGr_req += l_opAggGr.in_ColumnIdentifier[2]; /// extends from preceding group by
		
		/// projection operator
		OperatorWrapper l_opProj(OperatorType::Projection);
		l_opProj.targetTable = lineorder->getId();
//		l_opProj.in_ColumnIdentifier[0] = lineorder->getColumnID("L_PARTKEY");
		l_opProj.in_ColumnIdentifier[0] = lineorder->getColumnID("L_SUPPKEY");
		l_opProj.in_ColumnIdentifier[1] = l_opGr.out_ColumnMeta[1]->cid;
//		l_opProj.execFunction = &OperatorWrapper::PROJECTION;
		OperatorPrerequisite l_opProj_req;
		l_opProj_req += l_opProj.in_ColumnIdentifier[1]; /// extends from preceding group by
		
		/// global synchronisation of Aggregation results
		OperatorWrapper l_opAggSumMerge(OperatorType::Agg_Sum_Merge);
		l_opAggSumMerge.targetTable = lineorder->getId();
		l_opAggSumMerge.in_ColumnIdentifier[0] = l_opProj.out_ColumnMeta[0]->cid;
		l_opAggSumMerge.in_ColumnIdentifier[1] = l_opAggGr.out_ColumnMeta[0]->cid;
//		l_opAggSumMerge.execFunction = &OperatorWrapper::AGG_SUM_MERGE;
//		l_opAggSumMerge.execFunction = new std::function<void(Operator*)> (Operator::agg_sum_merge);
		
		OperatorPrerequisite l_opAggSumMerge_req;
		l_opAggSumMerge_req += l_opAggSumMerge.in_ColumnIdentifier[0];
		l_opAggSumMerge_req += l_opAggSumMerge.in_ColumnIdentifier[1];
		
		/// setup master coordinator and runtime (worker, coordinator)
		MasterCoordinator master;
		master.setupRuntime();
		
		Monitor::resetSubRunMonitor();
		Monitor * mon = Monitor::subRunMonitor;
		mon->start(MonitoringType::InitialEnqueuing);
		/// dispatch operators to all slave coordinators
		for (auto & slave : master.slaves) {
//		debug("Next slave dispatch")
			slave->distributeOperator(l_opGr);
			slave->enqueueSucceedingOperator(l_opAggGr, OperatorScope::Partition, l_opAggGr_req);
			slave->enqueueSucceedingOperator(l_opProj, OperatorScope::Partition, l_opProj_req);
			slave->printLinkedOperators();
		}
		
		master.slaves[0]->enqueueGlobalOperator(l_opAggSumMerge, OperatorScope::Global, l_opAggSumMerge_req);
		mon->end(MonitoringType::InitialEnqueuing);
		info("Start query execution");
		/// start processing
		master.start();
		info(str_rfill("[M] Runtime: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->getDuration(MonitoringType::GlobalRuntime)).count()))
				,12)
		        << " us");
		info(str_rfill("[M] Enqueueing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->getDuration(MonitoringType::InitialEnqueuing)).count()))
				,12)
				<< " us");
		info(str_rfill("[W] Avg Dequeuing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::wDequeuingJob)).count()))
				,12)
				<< " us");
		info(str_rfill("[W] Avg Executing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::wExecuteJob)).count()))
				,12)
				<< " us");
		info(str_rfill("[W] Avg Global job Executing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::wExecuteGlobalJob)).count()))
				,12)
				<< " us");
		info(str_rfill("[C] Avg Buffer processing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::cProcessBuffer)).count()))
				,12)
				<< " us");
		info(str_rfill("[C] Avg global Buffer processing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::cProcessGlobalBuffer)).count()))
				,12)
				<< " us");
//		info("Avg Test time: " << dotNumber(to_string(
//				chrono::duration_cast<chrono::microseconds>(mon->averageTransmitted(MonitoringType::Test)).count()))
//		                       << "us");
//		info("Avg Test2 time: " << dotNumber(to_string(
//				chrono::duration_cast<chrono::microseconds>(mon->averageTransmitted(MonitoringType::Test2)).count()))
//		                        << "us");
		
		/// submit measurements to global monitor
		mon->transmit(Monitor::globalMonitor, MonitoringType::GlobalRuntime);
		mon->tasks[MonitoringType::wDequeuingJob].duration = mon->averageTransmitted(MonitoringType::wDequeuingJob);
		mon->transmit(Monitor::globalMonitor, MonitoringType::wDequeuingJob);
		
		mon->tasks[MonitoringType::wExecuteJob].duration = mon->averageTransmitted(MonitoringType::wExecuteJob);
		mon->transmit(Monitor::globalMonitor, MonitoringType::wExecuteJob);
		
		mon->tasks[MonitoringType::wExecuteGlobalJob].duration = mon->averageTransmitted(MonitoringType::wExecuteGlobalJob);
		mon->transmit(Monitor::globalMonitor, MonitoringType::wExecuteGlobalJob);
		
		mon->tasks[MonitoringType::cProcessBuffer].duration = mon->averageTransmitted(MonitoringType::cProcessBuffer);
		mon->transmit(Monitor::globalMonitor, MonitoringType::cProcessBuffer);
		
		mon->tasks[MonitoringType::cProcessGlobalBuffer].duration = mon->averageTransmitted(MonitoringType::cProcessGlobalBuffer);
		mon->transmit(Monitor::globalMonitor, MonitoringType::cProcessGlobalBuffer);
		
		
	} /// end of reruns
}

void PrototypeEngine::pageAllocationTest() {
	
	
	NodeAllocationFence fence;
	cout << "page size: " << getpagesize() << endl;
	
	unsigned long line = 0UL;
	unsigned long lastChange_line = 0UL;
	unsigned long lastChange_assumed = 0UL;
	unsigned long boundary = 1000UL;
	GlobalColumnMetainformation colInfo("col", BaseType::uint64, ColumnIdentifier::createNew());
	int prevNode = -1;
	unsigned n = 0;
	auto print = [&](void* ptr) -> void {
		if(prevNode != get_node2(ptr)){
			unsigned long distance = line - lastChange_line;
			lastChange_line = line;
			info( RED << "#" << str_lfill(to_string(line), 5, "0") << ": Allocated byte is on node "<< get_node2(ptr) << RESET
					<< "  //  distance to last change: " << distance
					<< "  //  distance to assumed change: " << line - lastChange_assumed << " (>0 = bad)");
			prevNode = get_node2(ptr);
		} else {
//			info( str_lfill(to_string(line), 5, "0") << ": at node " << get_node2(ptr) );
		}
		line++;
	};
	
	info(GREEN << "Allocations between node switches: " << boundary)
//	numa_set_strict(1);
	for(unsigned j = 0; j < 10; j++) {
		fence = NodeAllocationFence(*(*Platform::getNodeIds())[n]);
		/// instead of fence, use libnuma calls directly
//		nodemask_t nodemask;
//		nodemask_zero(&nodemask);
//		nodemask_set_compat(&nodemask, n);
		// set memory policy
//		numa_set_membind_compat(&nodemask);
		
		/// increment to next node
		n = (n + 1) % 4;
		info("[Switch to node " << ThreadManager::currentAllocationNode.getPhysicalNode() << "] "
			 << "Assumed change at allocation #" << line)
		lastChange_assumed = line;
		for (unsigned long i = 0UL; i < boundary; i++) {
//			unsigned long * ptr = (unsigned long *) malloc(4096*2);
			void * ptr = (void*) new char();
//			void * ptr = (void*) new(__alloc(char)) char;
//			void * ptr = (void*) new Column(&colInfo, 100);
//			void * ptr = (void*) __new(Column)(&colInfo, 100);
//			void * ptr = (void*) __new(unsigned);
//			*((unsigned*)ptr) = 4UL;
			*((char*)ptr) = 'a';
			print(ptr);
		}
//		boundary -= 1;
//		if(!boundary)
//			break;
	}
	
	
	return;
	
	#ifdef USE_LIBNUMA
	NodeIdentifier* node0 = Platform::getFirstNode();
	NodeIdentifier* node1 = (*Platform::getNodeIds())[1];
	NodeIdentifier* node2 = (*Platform::getNodeIds())[2];
	NodeIdentifier* node3 = (*Platform::getNodeIds())[3];
	
	
	
	fence = NodeAllocationFence(*node2);
	malloc((size_t) getpagesize());
	unsigned long * abc123 = new unsigned long[1024 * 8];
	unsigned long * ttt = new unsigned long[1];
	cout << "test(" << abc123 << ")[1024 * " << 2 << "] is located on node " << get_node2(abc123) << endl;
	
	
	
	fence = NodeAllocationFence(*node3);
	ThreadManager::printAllocationPolicy();
	unsigned long size = 1024UL * 1024 * 128 * 10;
	size = 1024;
//	unsigned long * test = (unsigned long *) numa_alloc_onnode(size * sizeof(unsigned long), 3);
//	unsigned long * test = (unsigned long *) numa_alloc_local(sizeof(unsigned long) * 4);
//	unsigned long * test = (unsigned long *) malloc(size * sizeof(unsigned long));
	unsigned long * test3 = new unsigned long[size];
	cout << "test3(" << test3 << ") is located on node " << get_node2(test3) << endl;
	cout << "test3[256](" << &test3[256] << ") is located on node " << get_node2(&test3[256]) << endl;
	
	
	
	
	ThreadManager::printAllocationPolicy();
//	{
	auto fill = new unsigned long[100];
//		auto fence = ThreadManager::directAllocationToNode(node2);
	fence = NodeAllocationFence(*node1);
	ThreadManager::printAllocationPolicy();
//	unsigned long * test = new unsigned long[100];
	void * tmp = numa::numa_alloc_onnode(sizeof(unsigned long) * 100, ThreadManager::currentAllocationNode.getPhysicalNode());
//	unsigned long * test = new(tmp) unsigned long[100];
	unsigned long size2 = 100;
	void* p1 = numa::numa_alloc_onnode(getpagesize(),ThreadManager::currentAllocationNode.getPhysicalNode());
	void* p2 = numa::numa_alloc_onnode(getpagesize(),ThreadManager::currentAllocationNode.getPhysicalNode());
	(*(unsigned long*) p1)  = 1UL;
	(*(unsigned long*) p2)  = 1UL;
	cout << "p1 is on node " << get_node2(p1) << endl;
	cout << "p2 is on node " << get_node2(p2) << endl;
	for(unsigned long i = 0; i < 10; i++){
		unsigned long * test = new unsigned long[1024 / sizeof(unsigned long)];
		cout << i << " : test(" << test << ") is located on node " << get_node2(test) << endl;

	}
	unsigned long stackVar[4096];
	stackVar[0] = 1;
	cout << "stackVar is on node " << get_node2(stackVar) << endl;
//	cout << "tmp(" << tmp << ")" << endl;
//	cout << "test(" << test << ") is located on node " << get_node2(test) << endl;
//	test[0] = 3UL;
//	cout << "test(" << test << ") is located on node " << get_node2(test) << endl;
//	cout << test[20] << endl;
//	cout << "test(" << test << ") is located on node " << get_node2(test) << endl;
//	cout << "test[20](" << &test[20] << ") is located on node " << get_node2(&test[20]) << endl;
	
	ThreadManager::printAllocationPolicy();
//	NodeAllocationFence fence(*node3);
	fence = NodeAllocationFence(*node2);
	ThreadManager::printAllocationPolicy();
//	unsigned long size = 1024UL * 1024 * 128 * 10;
//	unsigned long * test = (unsigned long *) numa_alloc_onnode(size * sizeof(unsigned long), 3);
//	unsigned long * test = (unsigned long *) numa_alloc_local(sizeof(unsigned long) * 4);
//	unsigned long * test = (unsigned long *) malloc(size * sizeof(unsigned long));
	unsigned long * test2 = new unsigned long[size];
	cout << "test2(" << test2 << ") is located on node " << get_node2(test2) << endl;
	cout << "test2[256](" << &test2[256] << ") is located on node " << get_node2(&test2[256]) << endl;

	
	
	
//	cout << "numa_get_mems_allowed() = " << *numa_get_mems_allowed() << endl;
//	cout << "numa_get_membind() = " << *numa_get_membind() << endl;
//	cout << "numa_get_membind() = " << *numa_get_membind() << endl;
//	cout << "numa_get_run_node_mask() = " << *numa_get_run_node_mask() << endl;
//	cout << "numa_all_nodes_ptr = " << *numa_all_nodes_ptr << endl;
	#endif
	return;
}

void PrototypeEngine::pageAllocationTest2() {

	
	NodeAllocationFence fence;
	cout << "page size: " << getpagesize() << endl;
	
	unsigned long line = 0UL;
	unsigned long lastChange_line = 0UL;
	unsigned long lastChange_assumed = 0UL;
	unsigned long boundary = 128UL;
	GlobalColumnMetainformation colInfo("col", BaseType::uint64, ColumnIdentifier::createNew());
	int prevNode = -1;
	unsigned n = 0;
	auto print = [&](Column * ptr) -> void {
		int colNode = get_node2((void*) ptr);
		int dataNode = get_node2((void*) ptr->getData());
		if(colNode != dataNode){
			info(RED << str_lfill(to_string(line), 5, "0") << ": desired node: " << ThreadManager::currentAllocationNode.getPhysicalNode()
				<< " | column on node: " << colNode << " | data on node: " << dataNode)
		}
		
		
		
//		if(prevNode != get_node2(ptr)){
//			unsigned long distance = line - lastChange_line;
//			lastChange_line = line;
//			info( RED << str_lfill(to_string(line), 5, "0") << ": at node "<< get_node2(ptr)
//					<< "    distance to last change: " << distance
//					<< "    distance to assumed change: " << line - lastChange_assumed);
//			prevNode = get_node2(ptr);
//		} else {
////			info( str_lfill(to_string(line), 5, "0") << ": at node " << get_node2(ptr) );
//		}
		line++;
	};
	
	for(unsigned j = 0; j < 1024; j++) {
		fence = NodeAllocationFence(*(*Platform::getNodeIds())[n]);
		n = (n + 1) % 4;
		info("Assumed change at allocation #" << line << GREEN << " Boundary: " << boundary)
		lastChange_assumed = line;
		for (unsigned long i = 0UL; i < boundary; i++) {
//			unsigned long * ptr = (unsigned long *) malloc(8*4);
//			void * ptr = (void*) new char();
//			Column * ptr = new Column(&colInfo, 100);
			Column * ptr = new(__alloc(Column)) Column(&colInfo, 100);
//			void * ptr = (void*) __new(char);
//			void * ptr = (void*) __new(unsigned);
//			*((unsigned*)ptr) = 4UL;
//			*((char*)ptr) = 'a';
			print(ptr);
		}
		boundary -= 1;
		if(!boundary)
			break;
	}
	
	info("Size of a Column object: " << sizeof(Column) << "B")
}

void PrototypeEngine::pageAllocationTest3() {
	NodeAllocationFence fence;
	cout << "page size: " << getpagesize() << endl;
	ThreadManager::bindToNode(Platform::getNode(3));
	
	unsigned long lastChange_line = 0UL;
	unsigned long lastChange_assumed = 0UL;
	unsigned long boundary = 50000UL;
	void * ptr;
	
	unsigned n = 0;
	int prevNode = -1;
	auto print = [&](void* ptr, unsigned long allocationNr) -> void {
		unsigned long distance = allocationNr - lastChange_line;
		if(ThreadManager::currentAllocationNode.getPhysicalNode() != get_node4(ptr)){
			info( RED << str_lfill(to_string(allocationNr), 5, "0") << ": at node "<< get_node4(ptr)
					<< "    allocations since node change: " << str_lfill(to_string(distance),6)
					<< "    allocations since : " << str_lfill(to_string(allocationNr - lastChange_assumed),6)
			);
//			prevNode = get_node4(ptr);
		} else {
			info( GREEN << str_lfill(to_string(allocationNr), 5, "0") << ": at node "<< get_node4(ptr)
					<< "    distance to last change: " << str_lfill(to_string(distance),6)
					<< "    distance to assumed change: " << str_lfill(to_string(allocationNr - lastChange_assumed),6)
			);
		}
	};
	
	for(unsigned long j = 0; j < 50; j++) {
		fence = NodeAllocationFence(*(*Platform::getNodeIds())[n] addCaller);
//		ThreadManager::bindToNode((*Platform::getNodeIds())[n]);
		n = (n + 1) % 4;
		info("Assumed change at allocation #" << j*boundary << GREEN << " Boundary: " << boundary)
		lastChange_assumed = j*boundary;
		
		vector<unsigned long> * vec = new(__alloc(vector<unsigned long>)) vector<unsigned long>();
		info( "vector vec : at node "<< get_node4((void*) vec));
		for (unsigned long i = 0UL; i < boundary; i++) {
			vec->push_back(i);
			ptr = (void*) &vec->at(i);
			if(prevNode != get_node4(ptr)){
//				if(i>0) {
//					error("print previous " << prevNode << " " << get_node4((void *) &vec->at(i - 1)));
//					print((void *) &vec->at(i - 1), j*boundary + i - 1);
//				}
				print(ptr, j*boundary + i);
				prevNode = get_node4(ptr);
				lastChange_line = j*boundary + i;
			}
		}
//		boundary -= 1;
//		if(!boundary)
//			break;
//		vec->clear();
//		vec->shrink_to_fit();
//		vec->~vector();
//		delete vec;
	}
	
	
}


    

