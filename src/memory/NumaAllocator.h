/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_NUMAALLOCATOR_H
#define QUEUEBENCHMARK_NUMAALLOCATOR_H

#include <libnuma>
#include <threading>
#include <unordered_map>
#include <core/memory/DefaultAllocator.h>

template< typename T >
  class NumaAllocator : public morphstore::DefaultAllocator<T> {
//    std::unordered_map<void*, size_t> allocations;
//    static std::unordered_map<void*, size_t> allocations_;
  public:
    using value_type = T;
    using pointer = value_type*;
    using const_pointer = const pointer;
    
    
    
    
    NumaAllocator() = default;
    
    NumaAllocator(const NumaAllocator&) = default;
    NumaAllocator(NumaAllocator&&) noexcept = default;
    NumaAllocator& operator=(const NumaAllocator&) = default;
    NumaAllocator& operator=(NumaAllocator&&) = default;
    
    //      pointer allocate(size_t size) {
    //          std::cout << "Allocate " << size << " elements of size " << sizeof(value_type) << " using NumaAllocator." << std::endl;
    //          return static_cast<pointer>( malloc(size * sizeof(value_type)) );
    //      }
    
    pointer allocate(size_t num, const void* hint = 0) {
      
      #ifdef USE_LIBNUMA
        debug("Allocate numa: " << num << " elements of size of " << sizeof(value_type) << " bytes on node "
              << ThreadManager::currentAllocationNode.getPhysicalNode() << ".");
        void* ptr = numa::numa_alloc_onnode(num * sizeof(value_type), ThreadManager::currentAllocationNode.getPhysicalNode());
//        allocations[ptr] = num * sizeof(value_type);
        return static_cast<pointer> (ptr);
      #else
        std::cout << "Allocate malloc" << std::endl;
      return static_cast<pointer>( malloc(num * sizeof(value_type)) );
      #endif
    //          std::cout << "Allocate " << num << " elements of size "
    //          << sizeof(value_type) << " using NumaAllocator." << std::endl;
    //          return static_cast<pointer>( malloc(num * sizeof(value_type)) );
    }
    static
    pointer staticAllocate(size_t num, const void* hint = 0) {
      #ifdef USE_LIBNUMA
        debug("Static Allocate numa: " << num << " elements of size of " << sizeof(value_type) << " bytes on node "
              << ThreadManager::currentAllocationNode.getPhysicalNode() << ".");
        void* ptr = numa::numa_alloc_onnode(num * sizeof(value_type), ThreadManager::currentAllocationNode.getPhysicalNode());
        #ifdef DEBUG
        if(ptr == nullptr){
            severe("Could not allocate memory of " << num * sizeof(value_type) << " bytes.")
        }
        #endif
//        allocations_[ptr] = num * sizeof(value_type);
        return static_cast<pointer> (ptr);
      #else
        std::cout << "Allocate malloc" << std::endl;
      return static_cast<pointer>( malloc(num * sizeof(value_type)) );
      #endif
    //          std::cout << "Allocate " << num << " elements of size "
    //          << sizeof(value_type) << " using NumaAllocator." << std::endl;
    //          return static_cast<pointer>( malloc(num * sizeof(value_type)) );
    }
    
    
    void deallocate (pointer p, std::size_t num) {
        #ifdef USE_LIBNUMA
            debug("Deallocate numa_free");
            numa::numa_free(p, num * sizeof(value_type));
        #else
            std::cout << "Deallocate free" << std::endl;
            free(p);
        #endif
    }
    
//    void deallocate(pointer p){
//    #ifdef USE_LIBNUMA
//        std::cout << "Deallocate numa_free" << std::endl;
//      numa_free(p, allocations[p]);
//    #else
//        std::cout << "Deallocate free" << std::endl;
//      free(p);
//    #endif
//    }
    
    static
    void staticDeallocate (pointer p, std::size_t num) {
        #ifdef USE_LIBNUMA
            debug("Deallocate numa_free");
            numa::numa_free(p, num * sizeof(value_type));
        #else
            std::cout << "Deallocate free" << std::endl;
            free(p);
        #endif
    }
    static
    void staticDeallocate (void* p, std::size_t num) {
        #ifdef USE_LIBNUMA
            debug("Deallocate numa_free");
            numa::numa_free(p, num * sizeof(value_type));
        #else
            std::cout << "Deallocate free" << std::endl;
            free(p);
        #endif
    }
    
    //      void construct(pointer p, const value_type& val) {
    //          std::cout << "NumaAllocator.construct with val" << std::endl;
    //          new(static_cast<void*>(p)) value_type(val);
    //      }
    //
    //      void construct(pointer p) {
    //          std::cout << "NumaAllocator.construct" << std::endl;
    //          new(static_cast<void*>(p)) value_type();
    //      }
      
  };

#endif //QUEUEBENCHMARK_NUMAALLOCATOR_H
