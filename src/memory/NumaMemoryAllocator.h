/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_SRC_MEMORY_NUMAMEMORYALLOCATOR_H
#define QUEUEBENCHMARK_SRC_MEMORY_NUMAMEMORYALLOCATOR_H

#include <logging>
#include <libnuma>

class NumaMemoryAllocator : public BaseMemoryAllocator {
    using pointer = void*;
  public:
    virtual pointer allocate(std::size_t size){
        #ifdef USE_LIBNUMA
//          debug("Allocate numa: " << size << " bytes on node "
//                << ThreadManager::currentAllocationNode.getPhysicalNode() << ".");
          void* ptr = numa::numa_alloc_onnode(size, ThreadManager::currentAllocationNode.getPhysicalNode());
//          allocations[ptr] = num * sizeof(value_type);
          return static_cast<pointer> (ptr);
        #else
            std::cout << "Allocate malloc" << std::endl;
            return static_cast<pointer>( malloc(size) );
        #endif
    }
    
    virtual void deallocate (pointer p, std::size_t size){
        #ifdef USE_LIBNUMA
//            debug("Deallocate numa_free");
            numa::numa_free(p, size);
        #else
            std::cout << "Deallocate free" << std::endl;
            free(p);
        #endif
    }
};

#endif //QUEUEBENCHMARK_SRC_MEMORY_NUMAMEMORYALLOCATOR_H
