/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class TableInsertHelper;

#ifndef QUEUEBENCHMARK_TABLEINSERTHELPER_H
#define QUEUEBENCHMARK_TABLEINSERTHELPER_H

#include "util/Identifier.h"
#include <forward>
#include <storage/Partition.h>


class TableInsertHelper {
  public:
	//// Constructor
	TableInsertHelper();
	//// Copy constructor
	TableInsertHelper(const TableInsertHelper & other);
	//// Move constructor
	TableInsertHelper(TableInsertHelper && other) noexcept = default;
	// Destructor
	virtual ~TableInsertHelper();
	
	//// Assignment operator
	TableInsertHelper & operator=(const TableInsertHelper & rhs) = delete;
	//// Move assignment operator
	TableInsertHelper & operator=(TableInsertHelper && rhs) noexcept = delete;
	
	//// todo : locks for thread safety
	TableInsertHelper & operator++();
	TableInsertHelper operator++(int);
	
	
	PartitionIndex currentPartitionIndex;
	Partition * currentPartition = nullptr;
  private:
	
};

//// stream out operator
std::ostream & operator<<(std::ostream & os, const TableInsertHelper & obj);


#endif //QUEUEBENCHMARK_TABLEINSERTHELPER_H
