/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "TablePartitioningPolicy.h"
#include <logging>

#include "platform/Node.h"


TablePartitioningPolicy & TablePartitioningPolicy::operator<<(NodeIdentifier * nodeId) {
	nodes.push_back(nodeId);
	it = nodes.begin();
	return *this;
}

TablePartitioningPolicy & TablePartitioningPolicy::operator<<(Node * node) {
	nodes.push_back(node->getIdentifier());
	it = nodes.begin();
	return *this;
}
TablePartitioningPolicy & TablePartitioningPolicy::operator<<(std::vector<NodeIdentifier *> nodeIds) {
	for(auto n : nodeIds){
		nodes.push_back(n);
	}
	it = nodes.begin();
	return *this;
}

unsigned TablePartitioningPolicy::definedParameterCount() {
	unsigned definedParameters = 0;
	if(partitionSize  > 0) definedParameters++;
	if(partitionCount > 0) definedParameters++;
	if(tableSize      > 0) definedParameters++;
	return definedParameters;
}

void TablePartitioningPolicy::fillupUnknownParameter() {
	if(definedParameterCount() > 1){
		if(partitionSize  == 0)
			partitionSize = tableSize / partitionCount + ((tableSize % partitionCount > 0) ? 1 : 0);
		if(partitionCount == 0)
			partitionCount = tableSize / partitionSize + ((tableSize % partitionSize  > 0) ? 1 : 0);
		if(tableSize      == 0)
			tableSize = partitionCount * partitionSize;
	} else {
		severe("Set parameters are insufficient. Set at least two.")
	}
}

NodeIdentifier TablePartitioningPolicy::nextDispatchNode() {
	if(it == nodes.end())
		it = nodes.begin();
	NodeIdentifier res = **it;
	switch(partitionDispatchStrategy){
		case PartitionDispatchStrategy::FillNode:
			partitionCounter++;
			if(partitionCounter >= (ceil(partitionCount / nodes.size()))){
				partitionCounter = 0;
				it++;
			}
			break;
		case PartitionDispatchStrategy::RoundRobin:
			it++;
			break;
	}
	return res;
}

NodeIdentifier * TablePartitioningPolicy::predictLocationOfPartition(PartitionIndex partitionIndex) {
	unsigned long perNode, node;
	switch (partitionDispatchStrategy){
		case PartitionDispatchStrategy::FillNode:
			perNode = static_cast<unsigned long>(ceil(partitionCount / nodes.size()));
			node = (partitionIndex.getID() - (partitionIndex.getID() % perNode)) / perNode;
			node += ((partitionIndex.getID() % perNode) >= 0) ? 1 : 0;
			return nodes.at((node - 1) % nodes.size());
		case PartitionDispatchStrategy::RoundRobin:
			return nodes.at(partitionIndex.getID() % nodes.size());
	}
	return nullptr;
}

void TablePartitioningPolicy::addNodes(std::vector<NodeIdentifier *> * nodeIds) {
	for(auto n : *nodeIds){
		nodes.push_back(n);
	}
	it = nodes.begin();
}
