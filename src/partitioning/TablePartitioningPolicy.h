/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class TablePartitioningPolicy;

#ifndef QUEUEBENCHMARK_TABLEPARTITIONINGPOLICY_H
#define QUEUEBENCHMARK_TABLEPARTITIONINGPOLICY_H

#include <platform>
#include "util/Identifier.h"

/**
 * @brief Strategies how to dispatch partitions
 */
enum class PartitionDispatchStrategy {
	FillNode,   //// First fill all partition slots of one node, then go to the next
	RoundRobin  //// Fill one partition slot of every node at a time, then repeat
};

/**
 * @brief Strategies, how data is distributed among partitions
 */
enum class DataPartitionStrategy {
	Hash,       //// Partition is choosen by the hash of a tuple
	Range       //// Partition is determined by the ranges of primary key
};

class TablePartitioningPolicy {
  public:
	//// Constructor
	TablePartitioningPolicy() = default;
	//// Copy constructor
	TablePartitioningPolicy(const TablePartitioningPolicy & other) = default;
	//// Move constructor
	TablePartitioningPolicy(TablePartitioningPolicy && other) noexcept = default;
	// Destructor
	virtual ~TablePartitioningPolicy() = default;
	
	//// Assignment operator
	TablePartitioningPolicy & operator=(const TablePartitioningPolicy & rhs) = default;
	//// Move assignment operator
	TablePartitioningPolicy & operator=(TablePartitioningPolicy && rhs) noexcept = default;
	
	
	//// Which partition dispatch strategy shall be followed
	PartitionDispatchStrategy partitionDispatchStrategy = PartitionDispatchStrategy::FillNode;
	//// Strategy, how to distribute data among partitions
	DataPartitionStrategy dataPartitionStrategy = DataPartitionStrategy::Range;
	//// Nodes, where this Table resides on
	std::vector<NodeIdentifier*> nodes = {};
	
	
	//// ! At least two of the three following parameters have to be set.
	//// ! Otherwise the system is not able to support all partition dispatch strategies
	
	//// Quantity of partitions
	uint64_t partitionCount = 0;
	//// Size of a partition (elements in each column == tuple count)
	uint64_t partitionSize  = 0;
	//// Size of the whole table
	uint64_t tableSize      = 0;
	
	
	//// Tolerance of partition size (for later use, where DML are supported, defines how many elements more or less
	//// in a partition are allowed, before a redistribution shall be triggered)
	uint64_t partitionSizeTolerance = 0;
	
	unsigned definedParameterCount();
	void fillupUnknownParameter();
	NodeIdentifier nextDispatchNode();
	NodeIdentifier * predictLocationOfPartition(PartitionIndex partitionIndex);
	void addNodes(std::vector<NodeIdentifier *> * nodeIds);
	
	TablePartitioningPolicy & operator<<(NodeIdentifier * nodeId);
	TablePartitioningPolicy & operator<<(std::vector<NodeIdentifier *> nodeIds);
	TablePartitioningPolicy & operator<<(Node * node);
  private:
	std::vector<NodeIdentifier*>::iterator it = nodes.begin();
	uint64_t partitionCounter = 0;
};


#endif //QUEUEBENCHMARK_TABLEPARTITIONINGPOLICY_H
