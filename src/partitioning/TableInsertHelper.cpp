/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "TableInsertHelper.h"


/**
 * @brief Constructor of TableInsertHelper
 */
TableInsertHelper::TableInsertHelper() : currentPartitionIndex(0UL){
	// @todo Implement TableInsertHelper constructor
}

/**
 * @brief Copy constructor of TableInsertHelper
 * @param other Instance of TableInsertHelper to copy from
 */
TableInsertHelper::TableInsertHelper(const TableInsertHelper & other) : currentPartitionIndex(other.currentPartitionIndex) {
	currentPartition = other.currentPartition;
}


///**
// * @brief Move constructor of TableInsertHelper
// * @param other RValue of type TableInsertHelper to steal from
// */
//TableInsertHelper::TableInsertHelper(TableInsertHelper && other) noexcept {
//	// @todo Implement TableInsertHelper move constructor
//}


/**
 * @brief Destructor of TableInsertHelper
 */
TableInsertHelper::~TableInsertHelper() {
	// @todo Implement TableInsertHelper destructor
}

TableInsertHelper & TableInsertHelper::operator++() {
	++currentPartitionIndex;
	return *this;
}

TableInsertHelper TableInsertHelper::operator++(int) {
	TableInsertHelper tmp = *this;
	++*this;
	return tmp;
}


std::ostream & operator<<(std::ostream & os, const TableInsertHelper & obj) {
	//os << "<TableInsertHelper>[" << "attrib1:" << obj.___ << ", attrib2:" << obj.___ << "]";
	os << "[TODO: Implement outstream operator for <TableInsertHelper>]";
	return os;
}

