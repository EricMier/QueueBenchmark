/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_BENCHMARK_H
#define QUEUEBENCHMARK_BENCHMARK_H


#include <util/BenchmarkResult_old.h>

using namespace std;

class Benchmark {
  public:
    // Constructor
    Benchmark();

    // Copy constructor
    Benchmark(const Benchmark &other);

    // Move constructor
    Benchmark(Benchmark &&other) noexcept;

    // Destructor
    ~Benchmark();

    // Assignment operator
    Benchmark &operator=(const Benchmark &rhs) = delete;

    // Move assignment operator
    Benchmark &operator=(Benchmark &&rhs) noexcept = delete;

    static void QueueAccessBenchmark();
    static void QueueAccessBenchmark2();
    static void NumaBenchmark();

    template<typename ValueT, typename SeriesT, typename ResultT>
    static void writeBenchmarkResults(BenchmarkResult<ValueT, SeriesT, ResultT>* benchmark);
    template<typename ValueT, typename SeriesT, typename ResultT>
    static bool checkOutputFileIsWritable(BenchmarkResult<ValueT, SeriesT, ResultT>* benchmark);
    
    static unsigned long getMinBufferSize();
    
  private:
	static unsigned long minimumBufferSize;
};


#endif //QUEUEBENCHMARK_BENCHMARK_H
