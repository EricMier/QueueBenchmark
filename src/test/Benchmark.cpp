/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Benchmark.h"
#include <stdexcept>


#include <threading>

#include "util/BenchmarkResult_old.h"
#include "util/Randomizer.h"
#include "monitoring/Monitor_old.h"
#include "storage/Database.h"
#include "util/range.h"
#include "platform/Platform.h"

using namespace std;

unsigned long Benchmark::minimumBufferSize = 0UL;

/**
 * @brief Constructor of Benchmark
 */
Benchmark::Benchmark() {
	// @todo Implement Benchmark constructor
}

/**
 * @brief Copy constructor of Benchmark
 * @param other Instance of Benchmark to copy from
 */
Benchmark::Benchmark(const Benchmark & other) {
	// @todo Implement Benchmark copy constructor
}


/**
 * @brief Move constructor of Benchmark
 * @param other RValue of type Benchmark to steal from
 */
Benchmark::Benchmark(Benchmark && other) noexcept {
	// @todo Implement Benchmark move constructor
}


/**
 * @brief Destructor of Benchmark
 */
Benchmark::~Benchmark() {
	// @todo Implement Benchmark destructor
}


/*
void Benchmark::QueueAccessBenchmark() {
	
	Coordinator coordinator;
	// setup platform info
	coordinator.setupCpuOrderForAllNodes();
	
	string testCase = "QueueAccessBenchmark";
	string subTestCase = "worker_" + std::to_string(WORKER_CNT_PER_NODE);
	
	string outputFile = "output/" + testCase + "/" + subTestCase + ".csv";
	string optimalOutputFile = "output/" + testCase + "/" + subTestCase + "_opt.csv";


//	std::vector<unsigned> test_operatorCnt = {1,10,20,50,100,200,500,1000,2000,5000,10000};
	std::vector<unsigned> test_operatorCnt = {1, 10, 20, 50, 100, 200};
//	std::vector<unsigned> test_operatorCnt = {1,10,20};

//	std::vector<unsigned> test_waitTimes = {1,2,4,8,10,16,20,50,100,200,500,1000};
//	std::vector<unsigned> test_waitTimes = {1,2,4,8,10,16,20,50,100,200};
	std::vector<unsigned> test_waitTimes = {1, 2, 4, 8, 10, 16, 20, 50};
//	std::vector<unsigned> test_waitTimes = {1,10,20,50,100};
//	std::vector<unsigned> test_waitTimes = {20,10,8,4,2,1};
//	std::vector<unsigned> test_waitTimes = {1,1,1,2,2,2,4,4,4,8,8,8,10,10,10,20,20,20};




//	map<unsigned, map<unsigned, BenchmarkResult>> benchResults;
	vector<BenchmarkResult_old> benchResults;
	
	// initialize random generator
	Randomizer generator(100, 100);
	
	// create partition list
	Database::createPartitionList(0, new std::vector<Partition *>);
	std::vector<Partition *> * partitions = Database::getPartitionList(0);
	
	// create monitor
	Monitor_old::globalMonitor = new Monitor_old();
	Monitor_old * mon = Monitor_old::globalMonitor;
	
	// create Worker classes
	coordinator.setupWorkerForAllNodes();
	
	
	unsigned testCnt = test_operatorCnt.size() * test_waitTimes.size();
	unsigned testX = 0;
	// outer loop : iterate wait times
	for (unsigned waitTime : test_waitTimes) {
//		benchResults.insert({waitTime, map<unsigned, BenchmarkResult>()});
		// clear partition list
		partitions->clear();
		// fill partition list
		generator.fillPartitionList(partitions, PARTITION_CNT, waitTime);
		
		// summarize waiting time
		unsigned sumWait = 0;
		for (Partition * partition : *partitions) {
			sumWait += partition->getWait();
		}
		// inner loop : iterate operator counts
		for (unsigned operatorCnt : test_operatorCnt) {
			cout << "\n\e[91mNew Test (" << testX + 1 << "/" << testCnt << ")\e[39m\n";
			testX++;
			cout << "  WaitTime:  " << waitTime << "ms\n  Operators: " << operatorCnt << "\n";
			// calculate optimal execution time
			double optimalExecTime = (double) sumWait * operatorCnt / WORKER_CNT_PER_NODE;
			// create operator generator
			Randomizer operatorGenerator(SIMULATED_WAIT_MIN, SIMULATED_WAIT_MAX);
			std::vector<Operator_old> operators;
			operatorGenerator.fillOperators(&operators, operatorCnt);
			
			Coordinator::currentCoordinator->jobCnt = 0;
			int jobCounter = 0;
			for (Operator_old op : operators) {
				for (Partition * partition : *partitions) {
					partition->queue_old.enqueue(Job_old(op, jobCounter++));
					Coordinator::currentCoordinator->jobCnt++;
				}
			}
			
			cout << "  Jobs assigned:" << Coordinator::currentCoordinator->jobCnt << endl;

            // start worker threads
			coordinator.initWorkerThreads();
			
			// start measurement and set start flag for workers
			mon->start(MONITOR_GLOBAL_RUNTIME);
			ThreadManager::unblockWorkerThreads();
			
			// wait for all worker threads to stop
			coordinator.waitForWorkerThreads();
			
			// end measurement
			mon->end(MONITOR_GLOBAL_RUNTIME);

//			double duration = chrono::duration_cast<chrono::milliseconds>(mon->getDuration(MONITOR_GLOBAL_RUNTIME)).count();
			double duration = (double) mon->getDuration(MONITOR_GLOBAL_RUNTIME).count() / 1000000;
			
			cout << "  Result:\n    Jobs left: " << Coordinator::currentCoordinator->jobCnt << "\n";
			cout << "    optimal : " << optimalExecTime << "ms\n    measured: " << duration << "ms\n    overhead: +"
			     << duration - (double) optimalExecTime << "ms" << endl;

//			benchResults.at(waitTime).insert({operatorCnt, BenchmarkResult(optimalExecTime, duration)});
			benchResults.emplace_back(to_string(waitTime), operatorCnt, optimalExecTime, duration);
		} // end inner loop
		
		
	} // end outer loop
	
	
	cout << "Write results to: " << outputFile << endl;
	cout << "Write optimal results to: " << optimalOutputFile << endl;
	
	ofstream file, optFile;
	file.open(outputFile, ios::in | ios::out | ios::trunc);
	optFile.open(optimalOutputFile, ios::in | ios::out | ios::trunc);
	
	if (!file)
		cout << "Could not write to: " << outputFile << "\n";
	if (!optFile)
		cout << "Could not write to: " << optimalOutputFile << "\n";
	if (!file or !optFile)
		return;
	// print results
	stringstream head;
	string separator;
	for (auto const & result : test_operatorCnt) {
		head << separator << result;
		separator = "\t";
	}
	file << head.str() << "\n";
	optFile << head.str() << "\n";
	cout << "Operators: " << head.str() << endl;
	
	unsigned operatorIt = 0;
	for (auto const & result : benchResults) {
		if (operatorIt % test_operatorCnt.size() == 0) {
			file << result.label;
			optFile << result.label;
			operatorIt = 0;
			cout << result.label << " : ";
		}
		file << "\t" << result.measuredValue;
		optFile << "\t" << result.optimalValue;
		cout << "(" << result.optimalValue << ":" << result.measuredValue << "), ";
		operatorIt++;
		if (operatorIt % test_operatorCnt.size() == 0) {
			file << "\n";
			optFile << "\n";
			cout << endl;
		}
	}
	file.close();
	optFile.close();
	
}

void Benchmark::QueueAccessBenchmark2() {
	
	Coordinator coordinator;
	// setup platform info
	coordinator.setupCpuOrderForAllNodes();
	
	string testCase = "QueueAccessBenchmark2";
	string subTestCase = "differentWaitTimesPerJob_WorkpackageSize_" + std::to_string(WORK_PACKAGE_SIZE);
	
	string outputFile = "output/" + testCase + "/" + subTestCase + ".csv";
	string optimalOutputFile = "output/" + testCase + "/" + subTestCase + "_opt.csv";


//	std::vector<unsigned> test_operatorCnt = {1,10,20,50,100,200,500,1000,2000,5000,10000};
	std::vector<unsigned> test_operatorCnt = {1, 10, 20, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500};
//	std::vector<unsigned> test_operatorCnt = {1,10,20};

//	std::vector<unsigned> test_waitTimes = {1,2,4,8,10,16,20,50,100,200,500,1000};
//	std::vector<unsigned> test_waitTimes = {1,2,4,8,10,16,20,50,100,200};
//    std::vector<unsigned> test_waitTimes = {1,2,4,8,10,16,20,50};
//	std::vector<unsigned> test_waitTimes = {1,10,20,50,100};
//	std::vector<string> test_label = {"20", "10", "8", "4", "2", "1"};
	std::vector<string> test_label = {"Seed0", "Seed1", "Seed2", "Seed4", "Seed8", "Seed16"};
	vector<unsigned> test_label_value = {0, 1, 2, 4, 8, 16};
//	std::vector<unsigned> test_waitTimes = {1,1,1,2,2,2,4,4,4,8,8,8,10,10,10,20,20,20};




//	map<unsigned, map<unsigned, BenchmarkResult>> benchResults;
	vector<BenchmarkResult_old> benchResults;
	
	// initialize random generator
	Randomizer generator(100, 100);
	
	// create partition list
	Database::createPartitionList(0, new std::vector<Partition *>);
	std::vector<Partition *> * partitions = Database::getPartitionList(0);
	
	// create monitor
	Monitor_old::globalMonitor = new Monitor_old();
	Monitor_old * mon = Monitor_old::globalMonitor;
	
	// create Worker classes
	coordinator.setupWorkerForAllNodes();
	
	
	unsigned long testCnt = test_operatorCnt.size() * test_label.size();
	unsigned testX = 0;
	if (test_label.size() != test_label_value.size())
		throw runtime_error("Vector test_label and test_label_value have different sizes!");
	// outer loop : iterate wait times
	for (long int idx : range(0, test_label.size())) {
		std::string label = test_label.at(idx);
		unsigned labelValue = test_label_value.at(idx);
//		benchResults.insert({waitTime, map<unsigned, BenchmarkResult>()});
		// clear partition list
		partitions->clear();
		// fill partition list
		Randomizer WaitGenerator(2, 16, labelValue);
		WaitGenerator.fillPartitionList(partitions, PARTITION_CNT, 1);
		
		for (Partition * partition : *partitions) {
//            sumWait += partition->getWait();
//            maxWait = maxWait > partition->getWait() ? maxWait : partition->getWait();
		}
		// inner loop : iterate operator counts
		for (unsigned operatorCnt : test_operatorCnt) {
			cout << "\n\e[91mNew Test (" << testX + 1 << "/" << testCnt << ")\e[39m\n";
			testX++;
			cout << "  Label:  " << label << "\n  Operators: " << operatorCnt << "\n";
			// create operator generator
			Randomizer operatorGenerator(SIMULATED_WAIT_MIN, SIMULATED_WAIT_MAX);
			std::vector<Operator_old> operators;
			operatorGenerator.fillOperators(&operators, operatorCnt);
			
			// summarize waiting time
			unsigned long sumWait = 0;
			unsigned long maxWait = 0;
			for (Partition * partition : *partitions) {
				partition->waitSum = 0;
			}
			
			
			Coordinator::currentCoordinator->jobCnt = 0;
			int jobCounter = 0;
			
			for (Operator_old op : operators) {
				for (Partition * partition : *partitions) {
					// @todo Job wait time mitgeben
					unsigned long wait = WaitGenerator.random();
					sumWait += wait;
					partition->waitSum += wait;
					partition->queue_old.enqueue(Job_old(op, jobCounter++, wait));
					Coordinator::currentCoordinator->jobCnt++;
				}
			}
			for (Partition * partition : *partitions) {
				maxWait = maxWait > partition->waitSum ? maxWait : partition->waitSum;
			}
			
			// calculate optimal execution time
			double optimalExecTime = (double) sumWait / WORKER_CNT_PER_NODE;
			double expectedExecTime = (double) maxWait;
			
			cout << "  Jobs assigned: " << Coordinator::currentCoordinator->jobCnt << endl;

            // start worker threads
			coordinator.initWorkerThreads();
			
			// start measurement and set start flag for workers
			mon->start(MONITOR_GLOBAL_RUNTIME);
			ThreadManager::unblockWorkerThreads();
			
			// wait for all worker threads to stop
			coordinator.waitForWorkerThreads();
			
			// end measurement
			mon->end(MONITOR_GLOBAL_RUNTIME);

//			double duration = chrono::duration_cast<chrono::milliseconds>(mon->getDuration(MONITOR_GLOBAL_RUNTIME)).count();
			double duration = (double) mon->getDuration(MONITOR_GLOBAL_RUNTIME).count() / 1000000;
			
			cout << "  Result:\n";
			cout << "    Jobs left: " << Coordinator::currentCoordinator->jobCnt << "\n";
			cout << "    optimal : " << optimalExecTime << "ms\n    expected: " << expectedExecTime << "ms\n"
			     << "    measured: " << duration << "ms\n"
			     << "    overhead opt: +" << duration - optimalExecTime << "ms\n"
			     << "    overhead exp: +" << duration - expectedExecTime << "ms" << endl;

//			benchResults.at(waitTime).insert({operatorCnt, BenchmarkResult(optimalExecTime, duration)});
//            benchResults.emplace_back(label, operatorCnt, optimalExecTime, duration);
			benchResults.emplace_back(label, operatorCnt, expectedExecTime, duration);
		} // end inner loop
		
		
	} // end outer loop
	
	
	cout << "Write results to: " << outputFile << endl;
	cout << "Write optimal results to: " << optimalOutputFile << endl;
	
	ofstream file, optFile;
	file.open(outputFile, ios::in | ios::out | ios::trunc);
	optFile.open(optimalOutputFile, ios::in | ios::out | ios::trunc);
	
	if (!file)
		cout << "Could not write to: " << outputFile << "\n";
	if (!optFile)
		cout << "Could not write to: " << optimalOutputFile << "\n";
	if (!file or !optFile)
		return;
	// print results
	stringstream head;
	string separator;
	for (auto const & result : test_operatorCnt) {
		head << separator << result;
		separator = "\t";
	}
	file << head.str() << "\n";
	optFile << head.str() << "\n";
	cout << "Operators: " << head.str() << endl;
	
	unsigned operatorIt = 0;
	for (auto const & result : benchResults) {
		if (operatorIt % test_operatorCnt.size() == 0) {
			file << result.label;
			optFile << result.label;
			operatorIt = 0;
			cout << result.label << " : ";
		}
		file << "\t" << result.measuredValue;
		optFile << "\t" << result.optimalValue;
		cout << "(" << result.optimalValue << ":" << result.measuredValue << "), ";
		operatorIt++;
		if (operatorIt % test_operatorCnt.size() == 0) {
			file << "\n";
			optFile << "\n";
			cout << endl;
		}
	}
	file.close();
	optFile.close();
	
}

void Benchmark::NumaBenchmark() {
//	numa_run_on_node(0);
	unsigned columnWidth = 26;
	ThreadManager::bindToNode(0);
	//// === Setup Benchmark === ////
	using ValueT = unsigned;
	using SeriesT = unsigned;
	using ResultT = double;
	
	BenchmarkResult<ValueT, SeriesT, ResultT> benchmark;
	
	
	//// Benchmark Tescase Name
	benchmark.testCase = "NumaBenchmark";
	
	
	//// Benchmark Subtestcase Name
//	benchmark.subTestCase = "Partitioning_Benchmark_physical_2";
//#ifdef CONTROL_NUMA_POLICY
//	benchmark.subTestCase = "Allocation_with_Policy";
//#else
//	benchmark.subTestCase = "Allocation_without_Policy";
////	benchmark.subTestCase = "Allocation_without_Policy_2";
//#endif

//#ifdef USE_PHYSICAL_PARTITIONING
//	benchmark.subTestCase = "Partitioning_Benchmark_physical";
//	benchmark.addInformation("Partitioning=physical");
//#else
//	benchmark.subTestCase = "Partitioning_Benchmark_logical";
//	benchmark.addInformation("Partitioning=logical");
//#endif

#ifdef USE_LIBNUMA
	benchmark.subTestCase = "Benchmark_with_LibNuma";
#else
	benchmark.subTestCase = "Benchmark_without_LibNuma";
#endif
	
	//// Benchmark Version
	benchmark.version = "v20";
	
	
	//// Benchmark additional Information
	benchmark.addComment("lookup optimization");
	benchmark.addComment("atomic job counter per socket");
	benchmark.addComment("more segments (10000) in opertor.run");
	benchmark.addComment("and physical partitioning");
//	benchmark.addInformation("very small computation");
//	benchmark.addComment("copy whole partition");
//	benchmark.addInformation("copy whole partition once with vector extension avx512");
	#ifdef WORKLOAD_RANDOM_MEMORY_ACCESS
	benchmark.addComment("Copy segments of the partition in random order");
	#else
	benchmark.addComment("Copy segments of the partition in sequential order");
	#endif
	
	
	//// Benchmark test scenario
//	benchmark.addTestValues(vector<ValueT>({10,20,50,100,200,500}));
//	benchmark.addTestValues(vector<ValueT>({100,200,300,400,500}));
	benchmark.addTestValue(10);
	benchmark.addTestValue(20);
	benchmark.addTestValue(30);
	benchmark.addTestValue(40);
	benchmark.addTestValue(50);
	benchmark.addTestValue(60);
	benchmark.addTestValue(70);
	benchmark.addTestValue(80);
	benchmark.addTestValue(90);
	benchmark.addTestValue(100);


//	benchmark.addTestSeries(5, "100 prt");
	benchmark.addTestSeries(Platform::numCpusPerNode(), to_string(Platform::numCpusPerNode()) + " prt");
	benchmark.addTestSeries(Platform::numCpusPerNode()*2, to_string(Platform::numCpusPerNode()*2) + " prt");
	benchmark.addTestSeries(Platform::numCpusPerNode()*4, to_string(Platform::numCpusPerNode()*4) + " prt");
//	benchmark.addTestSeries(20, "20 prt");
//	benchmark.addTestSeries(100, "100 prt");
//	benchmark.addTestSeries(200, "200 prt");
//	benchmark.addTestSeries(500, "500 prt");
//	benchmark.addTestSeries(1000, "1.000 prt");
//	benchmark.addTestSeries(2000, "2.000 prt");
//	benchmark.addTestSeries(5000, "5.000 prt");

//	benchmark.addTestSeries(10000, "10.000 prt");
//	benchmark.addTestSeries(20000, "20.000 prt");
//	benchmark.addTestSeries(50000, "50.000 prt");

	
	//// how many tuples are generated (per socket)
//	unsigned long tuples = 20;
	unsigned long tuples = 1000UL * 1000 * 100;
	
	//// add settings
//	benchmark.addSetting("Plot", "XLabel", "Number of Operators");
	benchmark.addSetting("Plot", "XLabel", "Proportion of segments that are copied (in %%)");
	benchmark.addSetting("Benchmark", "Tuples", to_string(tuples));
	benchmark.addSetting("Benchmark", "Worker", to_string(WORKER_CNT_PER_NODE));
	benchmark.addSetting("Benchmark", "WorkPackageSize", to_string(WORK_PACKAGE_SIZE));
	benchmark.addSetting("Benchmark", "Repeat", to_string(REPEAT_BENCHMARK));
	
	#ifdef USE_PHYSICAL_PARTITIONING
	benchmark.addSetting("Benchmark", "Partitioning", "physical");
	#else
	benchmark.addSetting("Benchmark", "Partitioning", "logical");
	#endif
	
	#ifdef USE_LIBNUMA
	benchmark.addSetting("Benchmark", "LibNuma_enabled", "true");
	#else
	benchmark.addSetting("Benchmark", "LibNuma_enabled", "false");
	#endif
	#ifdef CONTROL_NUMA_POLICY
	benchmark.addSetting("Benchmark", "ControlNumaPolicy", "true");
	#else
	benchmark.addSetting("Benchmark", "ControlNumaPolicy", "false");
	#endif
	#ifdef WORKLOAD_RANDOM_MEMORY_ACCESS
	benchmark.addSetting("Benchmark", "RandomAccess", "true");
	#else
	benchmark.addSetting("Benchmark", "RandomAccess", "false");
	#endif
	
	
	benchmark.addSetting("Slots", "MONITOR_GLOBAL_RUNTIME", "0");
	benchmark.addSetting("Slots", "MONITOR_FIND_JOB", "1");
	benchmark.addSetting("Slots", "MONITOR_EXECUTE_JOB", "2");
	
	
	

	
	//// === set minimum buffer size === ////
	SeriesT min = (SeriesT) 0UL - 1;
	for( auto& series : *benchmark.testSeries){
		if(series.testSeriesValue < min)
			min = series.testSeriesValue;
	}
	Benchmark::minimumBufferSize = tuples / min * sizeof(unsigned long);
	
	
	//// === Data Generator === ////
	Randomizer generator(2, 1024, 1337);
	
	
	//// === Copy Benchmark Container === ////
//	BenchmarkResult<ValueT, SeriesT, ResultT> benchmarkOpt(benchmark);
//	benchmarkOpt.suffix = "opt";
	
	
	//// === Check if outpufile is writable beforehand === ////
	if (!checkOutputFileIsWritable(&benchmark))
		return;
//	if (!checkOutputFileIsWritable(&benchmarkOpt))
//		return;
	
	vector<thread> threads;
	
	for (int socket = 0; socket < Platform::numNodes(); ++socket) {
//	for (int socket = 0; socket < 1; socket++) {
		ThreadManager::bindToNode(socket);
		auto fence = ThreadManager::directAllocationToNode(socket);
		//// create partition list
		Database::createPartitionList(socket, new std::vector<Partition *>);
//		threads.push_back(new thread(&Randomizer::generateData, &generator, Database::getData(socket), tuples));
//		cout << "Setup for socket " << socket << endl;
		threads.emplace_back([&, socket, tuples]() {
			//// create data vector
			Database::createDataVector(socket, tuples);
			//// fill data vector
			generator.generateData(Database::getData(socket), tuples);
		});
	}
	ThreadManager::bindToNode(0);
	for(auto& t : threads) {
		t.join();
	}
	threads.clear();
	cout << "After Generation: " << ThreadManager::checkMemoryConfiguration() << endl;
	
//	Database::printData();
	
	//// === Setup Executables === ////
	//// create Coordinator
	Coordinator coordinator;
	//// setup worker
	coordinator.setupCpuOrderForAllNodes();
	coordinator.setupWorkerForAllNodes();
	
	
	//// === Begin Testing === ////
	unsigned long testCnt = benchmark.testValues->size() * benchmark.testSeries->size();
	unsigned long testX = 0;
	unsigned long testSeriesX = 0;
	
	for (BenchmarkSeries<SeriesT, ResultT> & series : *benchmark.testSeries) {
		for (int socket = 0; socket < Platform::numNodes(); ++socket) {
			auto fence = ThreadManager::directAllocationToNode(socket);
			ThreadManager::bindToNode(socket);
			//// split data vector into partitions
			threads.emplace_back(&Randomizer::partitionData,
					&generator,
					Database::getData(socket),
					series.testSeriesValue,
					Database::getPartitionList(socket));
//			threads.emplace_back([&, socket](){
//				generator.partitionData(Database::getData(socket), series.testSeriesValue,
//			                        Database::getPartitionList(socket));
//			});
//			generator.partitionData(Database::getData(socket), series.testSeriesValue,
//			                        Database::getPartitionList(socket));
		}
		ThreadManager::bindToNode(0);
		for(auto& t : threads) {
			t.join();
		}
		threads.clear();
		
//		Database::printPartitions();
//		return;
		
		
		for (ValueT testValue : *benchmark.testValues) {
			cout << "\e[91m|" << str_mfill("[Run " + to_string(testX + 1) + "/" + to_string(testCnt) + "]", columnWidth, "=") << "|\e[39m\n";
			testX++;
			coordinator.jobCnt = 0;
            for (int socket = 0; socket < Platform::numNodes(); ++socket)
                *coordinator.jobCntV->at(socket) = 0;
			
			//// reset monitor
			Monitor_old::resetGlobalMonitor();
			
			//// create operator generator
			Randomizer operatorGenerator(SIMULATED_WAIT_MIN, SIMULATED_WAIT_MAX);
			std::vector<Operator_old> operators;
//			operatorGenerator.fillOperators(&operators, testValue);
			operatorGenerator.fillOperators(&operators, 200);
			for(auto& op : operators){
				op.setProportion(testValue);
			}
			
			cout << "|" << str_lfill(WHITE + " Jobs assigned " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=")
				 << "|" << str_lfill(WHITE + " Test value "    + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=")
				 << "|" << str_lfill(WHITE + " Series value "  + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=")
				 << "|" << endl;
			cout << "|" << str_lfill(to_string(Platform::numNodes() * Database::getPartitionList(0)->size() * operators.size()) + " ", columnWidth)
				 << "|" << str_lfill(to_string(testValue) + " ", columnWidth)
				 << "|" << str_lfill(to_string(series.testSeriesValue) + " ", columnWidth)
				 << "|" << endl;
			
			//// Repeat Benchmark N times
			for(int i = 0; i < REPEAT_BENCHMARK; i++) {
				//// distribute operators over partitions
				for (int socket = 0; socket < 4; ++socket) {
					auto fence = ThreadManager::directAllocationToNode(socket);
					for (Partition * partition : *Database::getPartitionList(socket)) {
						for (Operator_old& op : operators) {
							partition->queue_old.enqueue(Job_old(op));
//						    coordinator.jobCnt++;
							(*coordinator.jobCntV->at(socket))++;
						}
					}
				}
				
//				cout << "JobCount bf: " << coordinator.getJobCnt() << endl;
				
				coordinator.initWorkerThreads();
				
				std::this_thread::sleep_for(std::chrono::milliseconds(50));
				//// start measurement and wake up workers
				Monitor_old::globalMonitor->resume(MONITOR_GLOBAL_RUNTIME);
				ThreadManager::unblockWorkerThreads();
				
				//// wait for all worker threads to stop
				coordinator.waitForWorkerThreads();
				Monitor_old::globalMonitor->end(MONITOR_GLOBAL_RUNTIME);
//				cout << endl;
				Monitor_old::globalMonitor->processTransmitted();
			}
			
			//// save results
			series.newMeasurement();
			//// 1st Global Runtime
			series.addMeasurement(
					(ResultT)
							Monitor_old::globalMonitor->averageTransmitted(MONITOR_GLOBAL_RUNTIME).count()
					/ 1000 / 1000
			);
			//// 2nd Time measured finding jobs
			series.addMeasurement(
					(ResultT)
							Monitor_old::globalMonitor->averageTransmitted(MONITOR_FIND_JOB).count()
					/ 1000 / 1000
			);
			//// 3rd Time measured executing jobs
			series.addMeasurement(
					(ResultT)
							Monitor_old::globalMonitor->averageTransmitted(MONITOR_EXECUTE_JOB).count()
					/ 1000 / 1000
			);
			
			//// Print best results to console
			cout << "|" << str_lfill(WHITE + "" + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Best GlobalRuntime " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Best FindJob Time " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Best ExecuteJob Time " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + "" + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|" << endl;
			
			
			cout << "|" << str_lfill(GREEN + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
						<< str_lfill(GREEN + to_string(
								(double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->bestTransmitted(MONITOR_GLOBAL_RUNTIME)).count() / 1000
								) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
						<< str_lfill(GREEN + to_string(
								(double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->bestTransmitted(MONITOR_FIND_JOB)).count() / 1000
								) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
		                << str_lfill(GREEN + to_string(
				                (double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->bestTransmitted(MONITOR_EXECUTE_JOB)).count() / 1000
		                		) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
		                << str_lfill(GREEN + RESET, columnWidth + GREEN.length() + RESET.length()) << "|" << endl;

			
			//// Print average results to console
			cout << "|" << str_lfill(WHITE + " Runtime " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Avg GlobalRuntime " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Avg FindJob Time " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Avg ExecuteJob Time " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " other overhead " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|" << endl;
			
			
			cout << "|" << str_lfill(GREEN + to_string(
					(double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->getDuration(MONITOR_GLOBAL_RUNTIME)).count() / 1000
								) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
						<< str_lfill(GREEN + to_string(
								(double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->averageTransmitted(MONITOR_GLOBAL_RUNTIME)).count() / 1000
								) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
						<< str_lfill(GREEN + to_string(
								(double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->averageTransmitted(MONITOR_FIND_JOB)).count() / 1000
								) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
		                << str_lfill(GREEN + to_string(
				                (double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->averageTransmitted(MONITOR_EXECUTE_JOB)).count() / 1000
		                		) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
		                << str_lfill(GREEN + to_string(
		                		(double) chrono::duration_cast<chrono::microseconds>(
						                Monitor_old::globalMonitor->averageTransmitted(MONITOR_GLOBAL_RUNTIME)
						                - Monitor_old::globalMonitor->averageTransmitted(MONITOR_FIND_JOB)
						                - Monitor_old::globalMonitor->averageTransmitted(MONITOR_EXECUTE_JOB)
		                				).count() / 1000
		                		) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|" << endl;

			
			//// Print worst results to console
			cout << "|" << str_lfill(WHITE + "" + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Worst GlobalRuntime " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Worst FindJob Time " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + " Worst ExecuteJob Time " + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|"
						<< str_lfill(WHITE + "" + RESET + "=", columnWidth + WHITE.length() + RESET.length(), "=") << "|" << endl;
			
			
			cout << "|" << str_lfill(GREEN + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
						<< str_lfill(GREEN + to_string(
								(double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->worstTransmitted(MONITOR_GLOBAL_RUNTIME)).count() / 1000
								) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
						<< str_lfill(GREEN + to_string(
								(double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->worstTransmitted(MONITOR_FIND_JOB)).count() / 1000
								) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
		                << str_lfill(GREEN + to_string(
				                (double) chrono::duration_cast<chrono::microseconds>(Monitor_old::globalMonitor->worstTransmitted(MONITOR_EXECUTE_JOB)).count() / 1000
		                		) + "ms " + RESET, columnWidth + GREEN.length() + RESET.length()) << "|"
		                << str_lfill(GREEN + RESET, columnWidth + GREEN.length() + RESET.length()) << "|" << endl;

		}
		testSeriesX++;
	}
	
	cout << "\n\e[91m=== Finished ===\e[39m\n";
	//// === Write results to csv files === ////
	writeBenchmarkResults(&benchmark);
//	writeBenchmarkResults(&benchmarkOpt);
	
	
}

template<typename ValueT, typename SeriesT, typename ResultT>
void Benchmark::writeBenchmarkResults(BenchmarkResult<ValueT, SeriesT, ResultT> * benchmark) {
	map<string, string> filesP;
	filesP["results"]  = benchmark->path() + "/" + benchmark->prefix + "results" + benchmark->suffix + ".csv";
	filesP["settings"] = benchmark->path() + "/" + benchmark->prefix + "settings" + benchmark->suffix + ".ini";
	filesP["comments"] = benchmark->path() + "/" + benchmark->prefix + "comments" + benchmark->suffix + ".txt";
	
	cout << "Write results  to: " << filesP["results"] << endl;
	cout << "Write settings to: " << filesP["settings"] << endl;
	cout << "Write comments to: " << filesP["comments"] << endl;
	
	ofstream file;
	
	for(auto const& [key, path] : filesP){
		file.open(path, ios::in | ios::out | ios::trunc);
		
		if (!file) {
			cout << "Could not write to: " << path << "\n";
			return;
		}
		
		if(key == "results")
			file << benchmark->to_csv();
		else if(key == "settings")
			file << benchmark->to_ini();
		else if(key == "comments")
			file << benchmark->to_txt();
		
		file.close();
	}
	
	
//	// print results
//	stringstream head;
//	if (!benchmark->additionalInformation->empty()) {
//		head << "#";
//		for (auto const & info : *benchmark->additionalInformation) {
//			head << info << "\t";
//		}
//		head << "\n";
//	}
//	head << benchmark->testValuesLabel;
//	for (auto const & value : *benchmark->testValues) {
//		head << "\t" << to_string(value);
//	}
//	file << head.str() << "\n";
//	cout << "Operators: " << head.str() << endl;
//
//	unsigned operatorIt = 0;
////	for (BenchmarkSeries<SeriesT, ResultT> const & s : *benchmark->testSeries) {
////		file << s.testSeriesLabel;
////		for (ResultT const & b : s.testResults) {
////			file << "\t" << b;
////		}
////		file << "\n";
////	}
//	file.close();
}

template<typename ValueT, typename SeriesT, typename ResultT>
bool Benchmark::checkOutputFileIsWritable(BenchmarkResult<ValueT, SeriesT, ResultT> * benchmark) {
	//// create output directory
	if(!testAndCreateDirectory(benchmark->path()))
		return false;
	
	//// check if output file is writable
	vector<string> files({
		benchmark->path() + "/" + benchmark->prefix + "results"  + benchmark->suffix + ".csv",
		benchmark->path() + "/" + benchmark->prefix + "settings" + benchmark->suffix + ".ini",
		benchmark->path() + "/" + benchmark->prefix + "comments" + benchmark->suffix + ".txt"
	});
	
	ofstream file;
	for( string f : files ){
		file.open(f, ios::in | ios::app);
		if (!file) {
			cerr << "Can not write to: " << f << "\n";
			return false;
		}
		file.close();
		cout << "output file will be " << f << endl;
		
	}
	
	
	return true;
}

unsigned long Benchmark::getMinBufferSize() {
	return Benchmark::minimumBufferSize;
}

*/

