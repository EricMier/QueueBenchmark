/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_TEST_H
#define QUEUEBENCHMARK_TEST_H




class Test {
  public:
	//// Constructor
	Test();
	//// Copy constructor
	Test(const Test & other);
	//// Move constructor
	Test(Test && other) noexcept;
	// Destructor
	virtual ~Test();
	
	//// Assignment operator
	Test & operator=(const Test & rhs);
	//// Move assignment operator
	Test & operator=(Test && rhs) noexcept;
	
	
	void run();
	
	
	
  private:
	
};

//// stream out operator
std::ostream & operator<<(std::ostream & os, const Test & obj);


#endif //QUEUEBENCHMARK_TEST_H
