/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include <task/OperatorWrapper.h>
#include <thread/Coordinator.h>
#include <logging/Logger.h>
#include "Test.h"
#include "storage/Column.h"
#include "task/Job.h"
#include "memory/NumaAllocator.h"
#include "multInclude.h"
#include "monitoring/Monitor.h"
#include "logging/stringManipulation.h"

/**
 * @brief Constructor of Test
 */
Test::Test() {
	// @todo Implement Test constructor
}

/**
 * @brief Copy constructor of Test
 * @param other Instance of Test to copy from
 */
Test::Test(const Test & other) {
	// @todo Implement Test copy constructor
}


/**
 * @brief Move constructor of Test
 * @param other RValue of type Test to steal from
 */
Test::Test(Test && other) noexcept {
	// @todo Implement Test move constructor
}


/**
 * @brief Destructor of Test
 */
Test::~Test() {
	// @todo Implement Test destructor
}



void test(string s){
	cout << s << endl;
}



//template<typename type>
//class MyAlloc : public allocator<type> {
//  public:
//	type * allocate(size_t size){
//		cout << "Allocate using MyAlloc / size: " << size << endl;
//		return (type*) malloc(sizeof(type) * size);
//	}
//
//	void deallocate(type * ptr, size_t size){
//		cout << "Deallocate using MyAlloc / ptr: " << ptr << " / size: " << size << endl;
//		free(ptr);
//	}
//};
template<class T>
class BaseClass {
  public:
	BaseClass() {};
	
	inline
	void* operator new (size_t size){
		cout << "Called new Operator / sizeof(T): " << sizeof(T) << " / size: " << size << endl;
		return malloc(size);
	};
	inline
	void* operator new[] (size_t size){
		cout << "Called new[] / size: " << size << endl;
		return malloc(size);
	}
	inline
	void operator delete (void* ptr){
		cout << "delte from base class" << endl;
		free(ptr);
	};
};

class SubClass : public BaseClass<SubClass> {
	uint64_t bla;
	uint64_t blub;
};



template <class T>
class MyAlloc {
 public:
   // type definitions
   typedef T        value_type;
   typedef T*       pointer;
   typedef const T* const_pointer;
   typedef T&       reference;
   typedef const T& const_reference;
   typedef std::size_t    size_type;
   typedef std::ptrdiff_t difference_type;

   // rebind allocator to type U
   template <class U>
   struct rebind {
       typedef MyAlloc<U> other;
   };

   // return address of values
   pointer address (reference value) const {
       return &value;
   }
   const_pointer address (const_reference value) const {
       return &value;
   }

   /* constructors and destructor
    * - nothing to do because the allocator has no state
    */
   MyAlloc() throw() {
   }
   MyAlloc(const MyAlloc&) throw() {
   }
   template <class U>
     MyAlloc (const MyAlloc<U>&) throw() {
   }
   ~MyAlloc() throw() {
   }

   // return maximum number of elements that can be allocated
   size_type max_size () const throw() {
       return std::numeric_limits<std::size_t>::max() / sizeof(T);
   }

   // allocate but don't initialize num elements of type T
   pointer allocate (size_type num, const void* = 0) {
       // print message and allocate memory with global new
       std::cerr << "allocate " << num << " element(s)"
                 << " of size " << sizeof(T) << std::endl;
//           pointer ret = (pointer)(::operator new(num*sizeof(T)));
//           pointer ret = (pointer)(malloc(num * sizeof(T)));
//           pointer ret = (pointer)(numa_alloc_onnode(num * sizeof(value_type), ThreadManager::currentAllocationNode.getPhysicalNode()));
       pointer ret = (pointer)(__alloc(value_type));
       std::cerr << " allocated at: " << (void*)ret << std::endl;
       return ret;
   }

   // initialize elements of allocated storage p with value value
   void construct (pointer p, const T& value) {
       // initialize memory with placement new
       std::cerr << "called construct on " << p << std::endl;
       new((void*)p)T(value);
   }

   // destroy elements of initialized storage p
   void destroy (pointer p) {
       std::cerr << "called destroy on " << p << std::endl;
       // destroy objects by calling their destructor
       p->~T();
   }

   // deallocate storage p of deleted elements
   void deallocate (pointer p, size_type num) {
       // print message and deallocate memory with global delete
       std::cerr << "deallocate " << num << " element(s)"
                 << " of size " << sizeof(T)
                 << " at: " << (void*)p << std::endl;
//           ::operator delete((void*)p);
//           numa_free(p, num * sizeof(value_type));
		__destruct(p, value_type);
   }
};

// return that all specializations of this allocator are interchangeable
template <class T1, class T2>
bool operator== (const MyAlloc<T1>&,
                const MyAlloc<T2>&) throw() {
   return true;
}
template <class T1, class T2>
bool operator!= (const MyAlloc<T1>&,
                const MyAlloc<T2>&) throw() {
   return false;
}



class T_Partition {
	
};

template<typename T>
class T_Column {
	T value;
};

/*
   
void Test::run() {
	{
		
		
		return;
	}
	{
		std::unordered_map<uint64_t, uint64_t> map;
		std::unordered_map<uint64_t, uint64_t> map2;
		Monitor mon;
		mon.start(MonitoringType::Test);
		map.reserve(28000);
//		map.rehash(21000);
		mon.end(MonitoringType::Test);
		info("Rehashing time: " <<
			dotNumber(
					to_string(
							std::chrono::duration_cast<chrono::microseconds>(
									mon.getDuration(MonitoringType::Test)
									).count()
					)
			) << " us"
		);
		
		
		mon.start(MonitoringType::Test);
		
		for(uint64_t i = 0; i < 20000; ++i){
			map[i] = 3;
		}
		
		mon.end(MonitoringType::Test);
		info("Map fill time: " <<
			dotNumber(
					to_string(
							std::chrono::duration_cast<chrono::microseconds>(
									mon.getDuration(MonitoringType::Test)
									).count()
					)
			) << " us"
		);
		
		
		mon.start(MonitoringType::Test);
		
		for(uint64_t i = 0; i < 20000; ++i){
			map2[i] = 3;
		}
		
		mon.end(MonitoringType::Test);
		info("Map fill time: " <<
			dotNumber(
					to_string(
							std::chrono::duration_cast<chrono::microseconds>(
									mon.getDuration(MonitoringType::Test)
									).count()
					)
			) << " us"
		);
		
		info("load_factor():" <<map.load_factor());
		info("bucket_count(): " << map.bucket_count());
		info("bucket_count(): " << map2.bucket_count());
		
		return;
	}
	
	
	{
		std::function<void(uint64_t *)> * lamb = new std::function<void(uint64_t *)>(
				[](uint64_t* value) {
					info("Print value from lambda function: " << *value);
				}
				);
		
		
		uint64_t val = 423464545U;
		(*lamb)(&val);
		return;
	}
	
	
	{
		info("Size of operator: " << sizeof(OperatorWrapper));
		info("Size of operator type: " << sizeof(OperatorType));
		info("Size of TableIdentifier: " << sizeof(TableIdentifier));
		return;
	}
	
	{
		uint64_t number = 325082958;
		info("Dot: " << number << " to " << dotNumber(to_string(number)));
		return;
	}
	
	{
		unordered_map<uint64_t, uint64_t> umap;
		umap[3] = 4;
		for (uint64_t j = 0; j < 100; ++j) {
			auto found = umap.find(j);
			if(found != umap.end()){
				info("umap[" << j << "]=" << umap[j]);
				found->second = 7;
				info("umap[" << j << "]=" << umap[j]);
			}
			
		}
		return;
	}
	{
		info("NodeIdentifier: " << sizeof(NodeIdentifier));
		info("CoordinatorIdentifier: " << sizeof(CoordinatorIdentifier));
		info("Size of Identifier: " << sizeof(Identifier))
		info("Size of TableIdentifier: " << sizeof(TableIdentifier))
		info("Size of ColumnIdentifier: " << sizeof(ColumnIdentifier))
		info("Size of PartitionIndex: " << sizeof(PartitionIndex))
		return;
	}
	
	
	{
		WorkerIdentifier wid1(WorkerIdentifier::createNew());
		WorkerIdentifier wid2 = WorkerIdentifier::createNew();
		WorkerIdentifier wid3 = WorkerIdentifier::createNew();
		CoordinatorIdentifier cid1 = CoordinatorIdentifier::createNew();
		CoordinatorIdentifier cid2 = CoordinatorIdentifier::createNew();
		CoordinatorIdentifier cid3 = CoordinatorIdentifier::createNew();
		info("wid1: " << wid1 << " type: " << wid1.type());
		info("wid2: " << wid2 << " type: " << wid2.type());
		info("wid3: " << wid3 << " type: " << wid3.type());
		info("cid1: " << cid1 << " type: " << cid1.type());
		info("cid2: " << cid2 << " type: " << cid2.type());
		info("cid3: " << cid3 << " type: " << cid3.type());
		return;
	}
	
	
	{
		OperatorPrerequisite opr;
		ColumnIdentifier cid = ColumnIdentifier::createNew();
		debugh("Test");
		Column * c = new Column(new GlobalColumnMetainformation("T", BaseType::uint64, cid), 0);
		info("Opr size: " << opr.requisites.size() << " cond: " << opr.isFullfilled());
		info("oprCol&: " << opr[cid]);
		info("Opr size: " << opr.requisites.size() << " cond: " << opr.isFullfilled());
		opr += cid;
		opr[cid] = c;
		
		info("Column&: " << c);
		info("oprCol&: " << opr[cid] << " cond: " << opr.isFullfilled());
		
		OperatorPrerequisite opr2(opr);
		info("opr2Col&: " << opr2[cid] << " cond: " << opr2.isFullfilled());
		
		return;
	}
	
	

	int size = 10;
	NumaAllocator<int> intAlloc;
//	int * is = intAlloc.allocate(5);
	vector<int, MyAlloc<int>> ivec;
	map<int, int, std::less<>, MyAlloc<std::pair<int, int>>> imap;
	for(int i = 0; i < size; i++){
//		is[i] = i;
//		ivec.push_back(i);
		imap.insert({i,i+5});
	}
	for(int i = 0; i < size; i++){
//		cout << "is[" << i << "] = " << i << endl;
//		cout << "ivec[" << i << "] = " << ivec.at(i) << endl;
		cout << "imap[" << i << "] = " << imap[i] << endl;
	}
	
//	Logger::getInstance() << LogLevel::error << "This is a test.";
//	Logger::getInstance().addLocation(__FILE__, __LINE__);
//	Logger::getInstance().flush();
//	Logger::getInstance() << boost::stacktrace::stacktrace();
//	Logger::getInstance().flush();
//	info("INFO TEST");
//	warn("WARNING TEST")
//	error("ERROR TEST")
//	severe("SEVERE TEST")
//	lowwarn("LOWWARN TEST")
//	alloc("ALLOC TEST")
//	debugh("DEBUGH TEST")
//	debug("DEBUG TEST")
//	debugstack("DEBUGSTACK TEST")
//	debugwarn("DEBUGWARN TEST")

	SubClass * ptr = new SubClass[10];
}

*/

ostream & operator<<(ostream & os, const Test & obj) {
	//os << "<Test>[" << "attrib1:" << obj.___ << ", attrib2:" << obj.___ << "]";
	os << "[TODO: Implement outstream operator for <Test>]";
	return os;
}
