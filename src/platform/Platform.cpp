/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Platform.h"
#include "CPUInfo.h"
#include "Node.h"

#include <libnuma>
#include <utils>
#include <logging>

#include "thread/ThreadManager.h"
#include "engine/PrototypeEngine.h"

Platform* Platform::instance;



/**
 * @brief Destructor of Platform
 */
Platform::~Platform() {
    delete allThreads;
    delete mainThreads;
    delete hyperThreads;
}

void Platform::_setup() {
	
    if(__setup)
        return;
    
    char hn[128];
	if (gethostname(hn, 128) < 0) {
		strcpy(hn, "unknown");
	}
    hostname = std::string(hn);

	#ifdef USE_LIBNUMA
    if(numa::numa_available() >= 0){
    	if(hostname == "haecboehmthesecond") {
	        hyperThreadCount = 2;
    	} else
    	if(hostname == "haecBoehm") {
	        hyperThreadCount = 2;
    	} else
    	if(hostname == "taishan-mrc-01") {
	        hyperThreadCount = 1;
    	
    	} else {
    	    severe("Unknown host server");
    	}
//        debug("numa_max_node() " << numa::numa_max_node());
//        debug("numa_num_task_nodes() " << numa::numa_num_task_nodes());
//        debug("numa_num_task_cpus() " << numa::numa_num_task_cpus());
//        for(uint64_t i = 0; i < numa::numa_num_task_cpus(); ++i){
//            debug("Node of cpu " << i << " = " << numa::numa_node_of_cpu(i));
//        }
	    numberOfSockets = (uint64_t) std::min(numa::numa_num_task_nodes(), MAX_NODES_USED);
	    coresPerSocket = (uint64_t) numa::numa_num_task_cpus() / numa::numa_num_task_nodes() / hyperThreadCount;
//	    debug("coresPerSocket " << coresPerSocket)
//	    debug("numberOfSockets " << numberOfSockets)
    } else {
	#endif
    	if(hostname == "haecboehmthesecond") {
			numberOfSockets = 4;
		    coresPerSocket = 14;
		    hyperThreadCount = 2;
	    }
    	else if(hostname == "haecBoehm") {
		    numberOfSockets = 4;
		    coresPerSocket = 16;
		    hyperThreadCount = 2;
	    }
	    else if(hostname == "erisboehm") {
		    numberOfSockets = 4;
		    coresPerSocket = 8;
		    hyperThreadCount = 2;
	    }
	    else if(hostname == "C940") {
		    numberOfSockets = 1;
		    coresPerSocket = 8;
		    hyperThreadCount = 2;
	    }
	    else if(hostname == "odroid") {
		    numberOfSockets = 1;
		    coresPerSocket = 4;
		    hyperThreadCount = 1;
	    } else {
    		severe("Hostname " + hostname + " is unknown and LibNuma is not available to read hardware information.");
    	}
	#ifdef USE_LIBNUMA
    }
    #endif

    // fill sockets
    for(int socketID = 0; socketID < (int) numberOfSockets; socketID++ ){
        Node* node = new Node(NodeIdentifier::newNodeIdentifier(socketID));
    	nodes.push_back(node);
    	nodeIds.push_back(node->getIdentifier());
    }

    // fill cpus
    int logicalId = 0;
    for(int hyperThread = 0; hyperThread < hyperThreadCount; hyperThread++ ){
        int physicalId = 0;
        for(int socket = 0; socket < numberOfSockets; socket++ ) {
            for (int core = 0; core < coresPerSocket; core ++) {
                auto node = nodeIds[numa::numa_node_of_cpu(logicalId)];
                node->getNode()->addCpu(
                  new CPUInfo(physicalId, logicalId, *node, (hyperThread > 0))
                );
                logicalId ++;
                physicalId ++;
            }
        }
    }
    Platform::__setup = true;
}

void Platform::setup() {
    Platform::getInstance()->_setup();
}

Platform* Platform::getInstance() {
    if(Platform::instance == nullptr){
        Platform::instance = new Platform();
    }
    return Platform::instance;
}

std::vector<CPUInfo*>* Platform::getAllThreadsOfSocket(unsigned id) {
    return Platform::getInstance()->_getSocket(id)->getAllThreads();
}

std::vector<CPUInfo*>* Platform::getMainThreadsOfSocket(unsigned id) {
    return Platform::getInstance()->_getSocket(id)->getMainThreads();
}

std::vector<CPUInfo*>* Platform::getHyperThreadsOfSocket(unsigned id) {
    return Platform::getInstance()->_getSocket(id)->getHyperThreads();
}

std::vector<Node *> * Platform::getSockets() {
	return Platform::getInstance()->_getSockets();
}

Node * Platform::getSocket(unsigned id) {
	return Platform::getInstance()->_getSocket(id);
}





std::vector<Node*>* Platform::_getSockets() {
    if(!__setup)
        _setup();
    return &nodes;
}

Node* Platform::_getSocket(unsigned id) {
    if(!__setup)
        _setup();
    return nodes.at(id);
}


std::vector<CPUInfo*>* Platform::getMainThreads() {
    if(Platform::getInstance()->mainThreads == nullptr){
        auto threads = new std::vector<CPUInfo*>;
        auto sockets = Platform::getInstance()->_getSockets();
        for( auto socket : *sockets ){
            threads->insert(threads->end(), socket->getMainThreads()->begin(), socket->getMainThreads()->end());
        }
        Platform::getInstance()->mainThreads = threads;
    }
    return Platform::getInstance()->mainThreads;
}

std::vector<CPUInfo*>* Platform::getHyperThreads() {
    if(Platform::getInstance()->hyperThreads == nullptr){
        auto threads = new std::vector<CPUInfo*>;
        auto sockets = Platform::getInstance()->_getSockets();
        for( auto socket : *sockets ){
            threads->insert(threads->end(), socket->getHyperThreads()->begin(), socket->getHyperThreads()->end());
        }
        Platform::getInstance()->hyperThreads = threads;
    }
    return Platform::getInstance()->hyperThreads;
}

std::vector<CPUInfo*>* Platform::getAllThreads() {
    if(Platform::getInstance()->allThreads == nullptr){
        auto threads = new std::vector<CPUInfo*>;
        threads->insert(threads->end(), Platform::getMainThreads()->begin(), Platform::getMainThreads()->end());
        threads->insert(threads->end(), Platform::getHyperThreads()->begin(), Platform::getHyperThreads()->end());
        Platform::getInstance()->allThreads = threads;
    }
    return Platform::getInstance()->allThreads;
}

std::string Platform::getHostname() {
	return Platform::getInstance()->_getHostname();
}

std::string Platform::_getHostname() {
    if(!__setup)
        _setup();
	return std::string(hostname);
}


void Platform::printConfig() {
	auto pf = Platform::getInstance();
	if(!pf->__setup)
        pf->_setup();
//	cout << "// : Platform configuration:\n"
//	     << "// : NUMA Nodes: " << pf->numberOfSockets << "\n"
//	     << "// : Cores per Node: " << pf->coresPerSocket * pf->hyperThreadCount
//	     << " (total: " << pf->coresPerSocket * pf->numberOfSockets * pf->hyperThreadCount << ")\n"
//	     << "// : NUMA lib available: "
//	     #ifdef USE_LIBNUMA
//	     << "true\n"
//	     #else
//	     << "false\n"
//	     #endif
//	     << "// : Control NUMA memory allocation policy: "
//	     #ifdef CONTROL_NUMA_POLICY
//	     << "true\n"
//	     #else
//	     << "false\n"
//	     #endif
//	     << "// : Partitioning: "
//	     #ifdef USE_PHYSICAL_PARTITIONING
//	     << "physical\n"
//		#else
//		<< "logical\n"
//		#endif
//			;
	info("Platform configuration:")
	info("NUMA Nodes: " << pf->numberOfSockets)
	info("Cores per Node: " << pf->coresPerSocket * pf->hyperThreadCount
	<< " (total: " << pf->coresPerSocket * pf->numberOfSockets * pf->hyperThreadCount << ")")
	info("NUMA lib available: "
	     #ifdef USE_LIBNUMA
	        << "true"
	     #else
	        << "false"
		 #endif
	)
	#ifdef USE_LIBNUMA
	info("NUMA system: " << (numa::numa_available() >= 0 ? "yes" : "no"))
	#endif
	info("Control NUMA memory allocation policy: "
	     #ifdef CONTROL_NUMA_POLICY
	        << "true"
	     #else
	        << "false"
		 #endif
	)
	info("Partitioning: "
	     #ifdef USE_PHYSICAL_PARTITIONING
	        << "physical"
		 #else
		    << "logical"
		 #endif
	)
	info("Worker per Node: " << PrototypeEngine::workerCnt);
}

uint64_t Platform::numCpus() {
	auto pf = Platform::getInstance();
	if(!pf->__setup)
        pf->_setup();
    return pf->numberOfSockets * pf->coresPerSocket * pf->hyperThreadCount;
}

uint64_t Platform::numNodes() {
	auto pf = Platform::getInstance();
	if(!pf->__setup)
        pf->_setup();
    return pf->numberOfSockets;
}

uint64_t Platform::numCpusPerNode() noexcept {
	auto pf = Platform::getInstance();
	if(!pf->__setup)
        pf->_setup();
    return pf->coresPerSocket;
}

std::vector<NodeIdentifier *> * Platform::_getNodeIds() {
	return &nodeIds;
}

std::vector<NodeIdentifier *> * Platform::getNodeIds() {
	return getInstance()->_getNodeIds();
}

NodeIdentifier * Platform::getFirstNode() {
	return (*getInstance()->_getNodeIds())[0];
}

bool Platform::isNuma() {
	#ifdef USE_LIBNUMA
	return numa::numa_available() >= 0;
	#else
	return false;
	#endif
}

NodeIdentifier * Platform::getNode(unsigned long id) {
	if(id > getInstance()->_getNodeIds()->size() - 1){
		error("Requested ID is out of scope!")
	}
	return (*getInstance()->_getNodeIds())[id];
}

NodeIdentifier Platform::getNodeOfPointer(void * p) {
	#ifndef USE_LIBNUMA
	return NodeIdentifier::createInvalidNodeIdentifier();
	#endif
	
	int status[1];
	
	if (move_pages(0,1,&p,NULL,status,0) != 0) {
		fprintf(stderr,"Problem in %s line %d calling move_pages()\n",__FILE__,__LINE__);
		abort();
	}
	
	if(status[0] < 0){
	    warn("The requested pointer is probably not initialized yet. So memory location is not detectable!")
	}
	
	return *Platform::getNode(status[0]);
}


