/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "NodeIdentifier.h"
#include <logging>
#include "Node.h"

//// statics
uint64_t NodeIdentifier::nextUniqueNodeIdentifier = 0UL;
std::mutex NodeIdentifier::nodeIdentifierCreationLock;


/**
 * @brief Constructor of NodeIdentifier
 */
NodeIdentifier::NodeIdentifier(unsigned long uni, int physicalNode)
		: uniqueNodeIdentifier(uni), physicalNode(physicalNode) {}


/**
 * @brief Used to create a new unique node identifier. Threadsafe.
 * @return
 */
NodeIdentifier NodeIdentifier::newNodeIdentifier(int physicalNode) {
//	debug("Is there a fix for lock_guard<mutex>(...) -> mbind: invalid argument ??")
	std::lock_guard<std::mutex> lockGuard(NodeIdentifier::nodeIdentifierCreationLock);
	return NodeIdentifier(nextUniqueNodeIdentifier++, physicalNode);
}

Node* NodeIdentifier::getNode() {
	return linkedNode;
}

bool NodeIdentifier::isValid() const {
	//// check if unique ID does not equals -1
	return (uniqueNodeIdentifier != (unsigned long) -1)
	//// and if this identifier is equal to the identifier, the linked node holds
		&& (*linkedNode->getIdentifier() == *this);
}

NodeIdentifier NodeIdentifier::createInvalidNodeIdentifier() {
	return NodeIdentifier();
}

unsigned long NodeIdentifier::getUNI() const {
	return uniqueNodeIdentifier;
}

bool NodeIdentifier::operator==(const NodeIdentifier & rhs) const {
	return rhs.uniqueNodeIdentifier == uniqueNodeIdentifier;
}

bool NodeIdentifier::operator==(NodeIdentifier & rhs) {
	return rhs.uniqueNodeIdentifier == uniqueNodeIdentifier;
}

bool NodeIdentifier::operator!=(const NodeIdentifier & rhs) const {
	return rhs.uniqueNodeIdentifier != uniqueNodeIdentifier;
}

bool NodeIdentifier::operator!=(NodeIdentifier & rhs) {
	return rhs.uniqueNodeIdentifier != uniqueNodeIdentifier;
}

int NodeIdentifier::getPhysicalNode() const {
	return physicalNode;
}

bool NodeIdentifier::operator>=(const NodeIdentifier & rhs) const {
	return uniqueNodeIdentifier >= rhs.uniqueNodeIdentifier;
}

bool NodeIdentifier::operator>=(NodeIdentifier & rhs) {
	return uniqueNodeIdentifier >= rhs.uniqueNodeIdentifier;
}

bool NodeIdentifier::operator<=(const NodeIdentifier & rhs) const {
	return uniqueNodeIdentifier <= rhs.uniqueNodeIdentifier;
}

bool NodeIdentifier::operator<=(NodeIdentifier & rhs) {
	return uniqueNodeIdentifier <= rhs.uniqueNodeIdentifier;
}

bool NodeIdentifier::operator>(const NodeIdentifier & rhs) const {
	return uniqueNodeIdentifier > rhs.uniqueNodeIdentifier;
}

bool NodeIdentifier::operator>(NodeIdentifier & rhs) {
	return uniqueNodeIdentifier > rhs.uniqueNodeIdentifier;
}

bool NodeIdentifier::operator<(const NodeIdentifier & rhs) const {
	return uniqueNodeIdentifier < rhs.uniqueNodeIdentifier;
}

bool NodeIdentifier::operator<(NodeIdentifier & rhs) {
	return uniqueNodeIdentifier < rhs.uniqueNodeIdentifier;
}

std::string NodeIdentifier::getUNIs() const {
	return (uniqueNodeIdentifier == (unsigned long) -1) ? "invalid" : std::to_string(uniqueNodeIdentifier);
}

std::ostream & operator<<(std::ostream & os, const NodeIdentifier & nodeID) {
	os << "<NodeID>[" << nodeID.getUNI() << "/" << nodeID.getPhysicalNode() << "]";
	return os;
}
