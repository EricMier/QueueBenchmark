/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "CPUInfo.h"
#include "platform/NodeIdentifier.h"

CPUInfo::CPUInfo() : nodeId(NodeIdentifier::createInvalidNodeIdentifier()) {
    //
}

CPUInfo::CPUInfo(int physicalId, int logicalId, NodeIdentifier & nodeId, bool isHyperThread)
: physicalId(physicalId), logicalId(logicalId), nodeId(nodeId), isHyperThread(isHyperThread){
    //
}

std::string CPUInfo::to_string() const {
    return "lc:" + std::to_string(logicalId) + " pc:" + std::to_string(physicalId)
        + " soc:" + std::to_string(nodeId.getPhysicalNode()) + " ht:" + std::to_string(isHyperThread);
}
