/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "Node.h"
#include "CPUInfo.h"
#include <logging>
/**
 * @brief Constructor of SocketInfo
 */
Node::Node(NodeIdentifier &  nodeId) : nodeIdentifier(nodeId) {
	//// link this Node to the identifier
	nodeIdentifier.linkedNode = this;
	debugh("Address of Node: " << nodeIdentifier.linkedNode);
}
Node::Node(NodeIdentifier && nodeId) : nodeIdentifier(nodeId) {
	//// link this Node to the identifier
	nodeIdentifier.linkedNode = this;
}

/**
 * @brief Copy constructor of SocketInfo
 * @param other Instance of SocketInfo to copy from
 */
Node::Node(const Node &other) : nodeIdentifier(other.nodeIdentifier) {}


/**
 * @brief Move constructor of SocketInfo
 * @param other RValue of type SocketInfo to steal from
 */
Node::Node(Node &&other) noexcept : nodeIdentifier(other.nodeIdentifier) {}


void * Node::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(Node));
}

void Node::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Node));
}


///**
// * @brief Assignment operator of SocketInfo
// * @param rhs Instance of SocketInfo to copy from
// * @return Reference of this SocketInfo-object
// */
//Node &Node::operator=(const Node &rhs) {
//    // @todo Implement assignment operator
//    throw std::runtime_error("NodeInfo::AssignmentOperator not implemented!");
//    return *this;
//}
//
//// Move assignment operator
///**
// * @brief Constructor of SocketInfo
// * @param rhs RValue of type SocketInfo to steal from
// * @return Reference of this SocketInfo-object
// */
//Node &Node::operator=(Node &&rhs) noexcept {
//    // @todo Implement move assignment operator
//    throw std::runtime_error("NodeInfo::MoveAssignmentOperator not implemented!");
//    return *this;
//}

void Node::addCpu(CPUInfo &cpu) {
    if(cpu.isHyperThread)
        hyperThreads.push_back(new CPUInfo(cpu));
    else
        mainThreads.push_back(new CPUInfo(cpu));
}

void Node::addCpu(CPUInfo &&cpu) {
    if(cpu.isHyperThread)
        hyperThreads.push_back(new CPUInfo(cpu));
    else
        mainThreads.push_back(new CPUInfo(cpu));
}

void Node::addCpu(CPUInfo *cpu) {
    if(cpu->isHyperThread)
        hyperThreads.push_back(cpu);
    else
        mainThreads.push_back(cpu);
}

std::vector<CPUInfo*>* Node::getAllThreads() {
    if(allThreads.empty()){
        allThreads.insert(allThreads.end(), mainThreads.begin(), mainThreads.end());
        allThreads.insert(allThreads.end(), hyperThreads.begin(), hyperThreads.end());
    }
    return &allThreads;
}

std::vector<CPUInfo*>* Node::getMainThreads() {
    return &mainThreads;
}

std::vector<CPUInfo*>*Node::getHyperThreads() {
    return &hyperThreads;
}

NodeIdentifier* Node::getIdentifier() {
	return &nodeIdentifier;
}

int Node::getPhysicalSocket() {
	return nodeIdentifier.getPhysicalNode();
}

void Node::linkDatabase(Database * database) {
	linkedDatabase = database;
}

Database * Node::getDatabase() {
	return linkedDatabase;
}

void Node::linkCoordinator(Coordinator * coordinator) {
	linkedCoordinator = coordinator;
}

Coordinator * Node::getCoordinator() {
	return linkedCoordinator;
}
