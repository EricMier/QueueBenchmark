/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

//// Don't touch this.
//// this ensures that NodeIdentifier is loaded _before_ Node, because Node hooks into NodeIdentifier

class Node;

//#pragma once
//#if !defined QUEUEBENCHMARK_NODE_H && defined QUEUEBENCHMARK_NODEIDENTIFIER_H
#ifndef QUEUEBENCHMARK_NODE_H
#define QUEUEBENCHMARK_NODE_H

//#ifndef INCLUDES_H
//#   include "includesForHeader.h"
//#endif

//#include "storage"
//#include "threading"
//
//#include "CPUInfo.h"
#include "CPUInfo.h"
#include "NodeIdentifier.h"
//#include "storage/Database.h"
#include "thread/Coordinator.h"

class Node {
public:
    //// Constructor
    explicit Node(NodeIdentifier &  nodeId);
    explicit Node(NodeIdentifier && nodeId);

    //// Copy constructor
    Node(const Node &other);

    //// Move constructor
    Node(Node &&other) noexcept;

    //// Destructor
    virtual ~Node() = default;

    //// Assignment operator
    Node &operator=(const Node &rhs) = delete;

    //// Move assignment operator
    Node &operator=(Node &&rhs) noexcept = delete;
    
	void* operator new(size_t size);
	void operator delete(void* p);
    
    void addCpu(CPUInfo& cpu);
    void addCpu(CPUInfo&& cpu);
    void addCpu(CPUInfo* cpu);

    std::vector<CPUInfo*>* getAllThreads();
    std::vector<CPUInfo*>* getMainThreads();
    std::vector<CPUInfo*>* getHyperThreads();
    
    NodeIdentifier* getIdentifier();
    int getPhysicalSocket();
    
    void linkDatabase(Database * database);
    Database * getDatabase();
    void linkCoordinator(Coordinator * coordinator);
    Coordinator * getCoordinator();

  private:
	std::vector<CPUInfo*> allThreads{};
	std::vector<CPUInfo*> mainThreads{};
	std::vector<CPUInfo*> hyperThreads{};
	
	NodeIdentifier nodeIdentifier;
	Database * linkedDatabase = nullptr;
	Coordinator * linkedCoordinator;
	

};


#endif //QUEUEBENCHMARK_NODE_H
