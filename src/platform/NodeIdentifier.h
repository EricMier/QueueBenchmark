/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class NodeIdentifier;

#ifndef QUEUEBENCHMARK_NODEIDENTIFIER_H
#define QUEUEBENCHMARK_NODEIDENTIFIER_H

//#include "util/forwards.h"
//#include "util/includeLibs.h"
//using namespace std;
//#ifndef INCLUDES_H
//#   include "includesForHeader.h"
//#endif
#include <stdlibs>
#include <forward>

//#include "Node.h"

class [[nodiscard]] NodeIdentifier {
	friend class Node;
	friend class NodeAllocationFence;
  public:
	//// Copy constructor
	NodeIdentifier(const NodeIdentifier & other) = default;
	//// Move constructor
	NodeIdentifier(NodeIdentifier && other) noexcept = default;
	//// Destructor
	~NodeIdentifier() = default;
	
	//// Assignment operator
	NodeIdentifier & operator=(const NodeIdentifier & rhs) = default;
	//// Move assignment operator
	NodeIdentifier & operator=(NodeIdentifier && rhs) noexcept = default;
	
	bool operator==(const NodeIdentifier & rhs) const;
	bool operator==(NodeIdentifier & rhs);
	bool operator!=(const NodeIdentifier & rhs) const;
	bool operator!=(NodeIdentifier & rhs);
	bool operator>=(const NodeIdentifier & rhs) const;
	bool operator>=(NodeIdentifier & rhs);
	bool operator<=(const NodeIdentifier & rhs) const;
	bool operator<=(NodeIdentifier & rhs);
	bool operator>(const NodeIdentifier & rhs) const;
	bool operator>(NodeIdentifier & rhs);
	bool operator<(const NodeIdentifier & rhs) const;
	bool operator<(NodeIdentifier & rhs);
	
	
	[[nodiscard]] static NodeIdentifier newNodeIdentifier(int physicalNode);
	[[nodiscard]] static NodeIdentifier createInvalidNodeIdentifier();
	
	Node* getNode();
	bool isValid() const;
	uint64_t getUNI() const;
	std::string getUNIs() const;
	int getPhysicalNode() const;
    
    Node * linkedNode = nullptr;
	////@todo: implement compare operators
  private:
	//// Constructor
	explicit NodeIdentifier(unsigned long uni, int physicalNode);
	//// Plain, invalid Constructor. Don't use this.
	NodeIdentifier() = default;
	
	uint64_t uniqueNodeIdentifier = (unsigned long) -1;
	int physicalNode = 0;
	
	static uint64_t nextUniqueNodeIdentifier;
	static std::mutex nodeIdentifierCreationLock;
};

//// stream out operator
std::ostream& operator<<(std::ostream& os, const NodeIdentifier& nodeID);

#endif //QUEUEBENCHMARK_NODEIDENTIFIER_H
