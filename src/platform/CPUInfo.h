/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

struct CPUInfo;

#ifndef QUEUEBENCHMARK_CPUINFO_H
#define QUEUEBENCHMARK_CPUINFO_H

#include <forward>
#include "NodeIdentifier.h"

struct CPUInfo {
    int physicalId = -1;
    int logicalId = -1;
//    int socketId = -1;
    NodeIdentifier nodeId;
    bool isHyperThread = false;

//    CPUInfo(int physicalId, int logicalId, int socketId, bool isHyperThread)
//        : physicalId(physicalId), logicalId(logicalId), socketId(socketId), isHyperThread(isHyperThread) {};
//    CPUInfo(const CPUInfo& other) = default;
//        : physicalId(other.physicalId), logicalId(other.logicalId), NodeID(other.NodeID) {};

	CPUInfo();
    CPUInfo(int physicalId, int logicalId, NodeIdentifier & nodeId, bool isHyperThread);
	[[nodiscard]]
	std::string to_string() const;
};

#endif //QUEUEBENCHMARK_CPUINFO_H
