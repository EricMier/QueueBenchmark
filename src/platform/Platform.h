/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class Platform;

#ifndef QUEUEBENCHMARK_PLATFORM_H
#define QUEUEBENCHMARK_PLATFORM_H

#include "Node.h"
#include "CPUInfo.h"
//#include "platform/NodeIdentifier.h"

class Platform {
public:
    static void setup();

    static std::vector<CPUInfo*>* getAllThreads();
    static std::vector<CPUInfo*>* getMainThreads();
    static std::vector<CPUInfo*>* getHyperThreads();

    static std::vector<CPUInfo*>* getAllThreadsOfSocket(unsigned id);
    static std::vector<CPUInfo*>* getMainThreadsOfSocket(unsigned id);
    static std::vector<CPUInfo*>* getHyperThreadsOfSocket(unsigned id);
    
    static std::vector<Node*>* getSockets();
    static Node* getSocket(unsigned id);
    static std::vector<NodeIdentifier*>* getNodeIds();
    static NodeIdentifier* getFirstNode();
    static NodeIdentifier* getNode(unsigned long id);
    
    static NodeIdentifier getNodeOfPointer(void* p);
    
    static std::string getHostname();
    static uint64_t numCpus();
    static uint64_t numCpusPerNode() noexcept;
    static uint64_t numNodes();
    static bool isNuma();
    
    static void printConfig();

protected:
    static Platform* getInstance();
private:
    static Platform* instance;


    // Constructor
    Platform() = default;
    // Destructor
    virtual ~Platform();

    void _setup();
    std::vector<Node*>* _getSockets();
    Node* _getSocket(unsigned id);
    std::vector<NodeIdentifier*>* _getNodeIds();
    std::string _getHostname();
    
    void _printConfig();

    bool __setup = false;
    std::vector<Node*> nodes;
    std::vector<NodeIdentifier*> nodeIds;
    std::vector<CPUInfo*>* allThreads = nullptr;
    std::vector<CPUInfo*>* mainThreads = nullptr;
    std::vector<CPUInfo*>* hyperThreads = nullptr;
    
	std::string hostname;
	uint64_t numberOfSockets;
	uint64_t coresPerSocket;
	uint64_t hyperThreadCount;
	
};


#endif //QUEUEBENCHMARK_PLATFORM_H
