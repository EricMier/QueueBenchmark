
#foreach(target IN ITEMS ${TARGETS})
#    add_executable(${target} "")
#endforeach(target)


add_subdirectory(storage)
add_subdirectory(engine)
add_subdirectory(logging)
add_subdirectory(memory)
add_subdirectory(monitoring)
add_subdirectory(partitioning)
add_subdirectory(platform)
add_subdirectory(task)
add_subdirectory(test)
add_subdirectory(thread)
add_subdirectory(util)

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    SET(build_path  "${CMAKE_BINARY_DIR}/build/release")
else(CMAKE_BUILD_TYPE STREQUAL "Debug")
    SET(build_path  "${CMAKE_BINARY_DIR}/build/debug")
endif()

foreach(target IN ITEMS ${TARGETS})
    ## add main to targets
    target_sources(${target}
        PUBLIC
            ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
    )

    target_include_directories(${target} PUBLIC /usr/include)
    ## Add boost library in debug mode, to enable stacktracing
    if(INCLUDE_BOOST_STACKTRACE STREQUAL "TRUE")
#        set(LINK_LIBS ${Boost_LIBRARIES} dl)
        set(LINK_LIBS dl backtrace)
    endif()
#    target_link_libraries( ${target} LINK_PUBLIC ${Boost_LIBRARIES} )

    ## Link in LibNuma if not Target without libnuma
#    if(NOT ${target} STREQUAL "QueueBenchmark_WO_LIBNUMA" AND NOT ${CMAKE_BUILD_TYPE} STREQUAL "Local:Debug")
    if(NOT target STREQUAL "QueueBenchmark_WO_LIBNUMA" )
        find_package(Numa)
        if(NOT "${NUMA_LIBRARY}" STREQUAL "")
            message( STATUS "NumaLib found in : ${NUMA_LIBRARY}")
        else()
            message( FATAL_ERROR "NumaLib not found. Make sure NumaLib is installed.")
        endif()

#        target_link_libraries(${target} libnuma.a)
        if(NOT ${host_name} STREQUAL "C940")
#            set(LINK_LIBS ${LINK_LIBS} libnuma.a)
            set(LINK_LIBS ${LINK_LIBS} ${NUMA_LIBRARY})
            target_compile_options(${target} PRIVATE -DUSE_LIBNUMA)
        endif()

    endif()

    ## link set libraries in
    target_link_libraries( ${target} LINK_PUBLIC ${LINK_LIBS} )

    #find_package(Numa)

    set_target_properties( ${target}
        PROPERTIES
        ARCHIVE_OUTPUT_DIRECTORY "${build_path}/lib"
        LIBRARY_OUTPUT_DIRECTORY "${build_path}/lib"
        RUNTIME_OUTPUT_DIRECTORY "${build_path}/bin"
    )

    if(target STREQUAL "QueueBenchmark_AVX512")
        target_compile_options(QueueBenchmark_AVX512 PRIVATE -march=native -DAVX512 -mavx512f -mavx512pf -mavx512er -mavx512cd -mavx512vl -mavx512dq)
    endif()
endforeach(target)

if(NOT CMAKE_BUILD_TYPE STREQUAL "Local:Debug")
    target_compile_options(PrototypeEngine PUBLIC -DCONTROL_NUMA_POLICY)
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Release")
    SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -O3")
endif()


if(CMAKE_BUILD_TYPE STREQUAL "Debug" OR CMAKE_BUILD_TYPE STREQUAL "Local:Debug")
    target_compile_options(PrototypeEngine PUBLIC -DDEBUG)
#    SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -g -O0")
endif()
if(CMAKE_BUILD_TYPE STREQUAL "Debug1")
    target_compile_options(PrototypeEngine PUBLIC -DDEBUG -DDEBUG1)
#    SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -g -O0")
endif()
if(CMAKE_BUILD_TYPE STREQUAL "Debug2")
    target_compile_options(PrototypeEngine PUBLIC -DDEBUG -DDEBUG1 -DDEBUG2)
endif()

if(${DEBUG})
    SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -g -O0 -rdynamic -DBOOST_STACKTRACE_USE_BACKTRACE ")
endif()

if("${INCLUDE_BOOST_STACKTRACE}" STREQUAL "TRUE")
    SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -DINCLUDE_STACKTRACE ")
endif()
