/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/

class OperatorDependency;

#ifndef QUEUEBENCHMARK_OPERATORDEPENDENCY_H
#define QUEUEBENCHMARK_OPERATORDEPENDENCY_H

#include "task/OperatorScope.h"
#include <task/OperatorWrapper.h>
#include "task/OperatorPrerequisite.h"


class OperatorDependency {
  public:
	//// Constructor
	OperatorDependency(OperatorWrapper * op, OperatorScope opScope, OperatorPrerequisite * opPrerequisite, Coordinator * targetCoordinator);
	//// Copy constructor
	OperatorDependency(const OperatorDependency & other);
	//// Move constructor
	OperatorDependency(OperatorDependency && other) noexcept;
	/// Destructor
	virtual ~OperatorDependency();
	
	//// Assignment operator
	OperatorDependency & operator=(const OperatorDependency & rhs);
	//// Move assignment operator
	OperatorDependency & operator=(OperatorDependency && rhs) noexcept;
	
	void* operator new(size_t size);
	void operator delete(void* p);
	
	void addPartition(PartitionIndex pid);
	bool isFulfilled();
	
	OperatorWrapper * op;
	OperatorScope opScope;
	OperatorPrerequisite * opPrerequisite;
	identifier_map<PartitionIndex, OperatorPrerequisite*> requiredPartitions;
	Coordinator * targetCoordinator = nullptr;
};

//// stream out operator
std::ostream & operator<<(std::ostream & os, const OperatorDependency & obj);


#endif //QUEUEBENCHMARK_OPERATORDEPENDENCY_H
