/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Job.h
 * Author: EricMier
 *
 * Created on 1. August 2019, 14:55
 */

class Job;

#ifndef JOB_H
#define JOB_H


#include "OperatorWrapper.h"

class Job {
  public:
	Job() : op(nullptr) {};
	Job(OperatorWrapper * op, PartitionIndex partId);
	Job(Job && other) noexcept;
	Job(const Job & other);
	
	Job& operator=(const Job & rhs) = default;
	
	void* operator new(size_t size);
	void operator delete(void* p);
	
	OperatorWrapper * op;
	PartitionIndex partId;
	
	void reset();
};

#endif /* JOB_H */

