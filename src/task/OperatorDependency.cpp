/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "OperatorDependency.h"


/**
 * @brief Constructor of Dependency
 */
OperatorDependency::OperatorDependency(
  OperatorWrapper * op, OperatorScope opScope,
  OperatorPrerequisite * opPrerequisite, Coordinator * targetCoordinator = nullptr)
		: op(op), opScope(opScope), opPrerequisite(opPrerequisite), targetCoordinator(targetCoordinator) {}

/**
 * @brief Copy constructor of Dependency
 * @param other Instance of Dependency to copy from
 */
OperatorDependency::OperatorDependency(const OperatorDependency & other) {
	// @todo Implement Dependency copy constructor
}


/**
 * @brief Move constructor of Dependency
 * @param other RValue of type Dependency to steal from
 */
OperatorDependency::OperatorDependency(OperatorDependency && other) noexcept {
	// @todo Implement Dependency move constructor
}


/**
 * @brief Destructor of Dependency
 */
OperatorDependency::~OperatorDependency() {
	// @todo Implement Dependency destructor
}


/**
 * @brief Assignment operator of Dependency
 * @param rhs Instance of Dependency to copy from
 * @return Reference of this Dependency-object
 */
OperatorDependency & OperatorDependency::operator=(const OperatorDependency & rhs) {
	// @todo Implement assignment operator
	throw std::runtime_error("Dependency::AssignmentOperator not implemented!");
	return *this;
}

/**
 * @brief Move assignment operator of Dependency
 * @param rhs RValue of type Dependency to steal from
 * @return Reference of this Dependency-object
 */
OperatorDependency & OperatorDependency::operator=(OperatorDependency && rhs) noexcept {
	// @todo Implement move assignment operator
	throw std::runtime_error("Dependency::MoveAssignmentOperator not implemented!");
	return *this;
}

void * OperatorDependency::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(OperatorDependency));
}

void OperatorDependency::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(OperatorDependency));
    
}

void OperatorDependency::addPartition(PartitionIndex pid) {
	requiredPartitions[pid] = new OperatorPrerequisite(*opPrerequisite);
}

bool OperatorDependency::isFulfilled() {
	for(auto& part : requiredPartitions){
		if(!part.second->isFullfilled())
			return false;
	}
	return true;
}

std::ostream & operator<<(std::ostream & os, const OperatorDependency & obj) {
	//os << "<Dependency>[" << "attrib1:" << obj.___ << ", attrib2:" << obj.___ << "]";
	os << "[TODO: Implement outstream operator for <Dependency>]";
	return os;
}

