/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_SRC_TASK_OPERATORTYPE_H
#define QUEUEBENCHMARK_SRC_TASK_OPERATORTYPE_H
enum class OperatorType {
	Select,
	Projection,
	Merge_Sorted,
	Intersect_Sorted,
	Nested_Loop_Join,
	Left_Semi_Nto1_Nested_Loop_Join,
	Semi_Join,
	Agg_Sum_Global,
	Agg_Sum_Grouped,
	Calc_Binary,
	Calc_Unary,
	Group_Unary,
	Group_Binary,
	Agg_Sum_Merge
};

static
std::ostream & operator<<(std::ostream & os, const OperatorType & type){
	switch(type){
		case OperatorType::Select:
			os << "Select";
			break;
		case OperatorType::Projection:
			os << "Projection";
			break;
		case OperatorType::Merge_Sorted:
			os << "Merge_Soretd";
			break;
		case OperatorType::Intersect_Sorted:
			os << "Intersect_Sorted";
			break;
		case OperatorType::Nested_Loop_Join:
			os << "Nested_Loop_Join";
			break;
		case OperatorType::Left_Semi_Nto1_Nested_Loop_Join:
			os << "Left_Semi_Nto1_Nested_Loop_Join";
			break;
		case OperatorType::Semi_Join:
			os << "Semi_Join";
			break;
		case OperatorType::Agg_Sum_Global:
			os << "Agg_Sum_Global";
			break;
		case OperatorType::Agg_Sum_Grouped:
			os << "Agg_Sum_Grouped";
			break;
		case OperatorType::Calc_Binary:
			os << "Calc_Binary";
			break;
		case OperatorType::Calc_Unary:
			os << "Calc_Unary";
			break;
		case OperatorType::Group_Unary:
			os << "Group_Unary";
			break;
		case OperatorType::Group_Binary:
			os << "Group_Binary";
			break;
		case OperatorType::Agg_Sum_Merge:
			os << "Agg_Sum_Merge";
			break;
	}
	return os;
};
#endif //QUEUEBENCHMARK_SRC_TASK_OPERATORTYPE_H
