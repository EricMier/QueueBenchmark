/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#include "OperatorWrapper.h"

#include <thread/Coordinator.h>
#include <util/Randomizer.h>
#include "storage/Partition.h"
#include "storage/Column.h"
#include "thread/Worker.h"
#include "monitoring/Monitor.h"

//#include <core/operators/interfaces/group.h>
//#include "core/operators/scalar/group_uncompr.h"
//#include "core/operators/interfaces/agg_sum.h"
//#include "core/operators/scalar/agg_sum_uncompr.h"
//#include "core/operators/interfaces/project.h"
//#include "core/operators/scalar/project_uncompr.h"

using namespace std;

/// statics
//int Operator_old::operatorId = 0;
//
//Operator_old::Operator_old() {
//	id = Operator_old::getNextId();
//	proportion = 0.f;
//}
//
//Operator_old::Operator_old(float proportion) : proportion(proportion) {
//	id = Operator_old::getNextId();
//}
//
//Operator_old::Operator_old(std::chrono::nanoseconds workload): workload(workload) {
//	id = Operator_old::getNextId();
//	proportion = 0.f;
//}
//
//Operator_old::Operator_old(const Operator_old& orig) {
//	workload = orig.workload;
//	id = orig.id;
//	proportion = orig.proportion;
//}
//
//Operator_old::~Operator_old() {
//}
//
//
//int Operator_old::getId() {
//	return id;
//}
//
//std::chrono::nanoseconds Operator_old::getWorkload() {
//	return workload;
//}

/*
void Operator_old::run(Partition* partition) {
	vector<unsigned long>* data = partition->getData();
	unsigned long offset = partition->getOffset();
	unsigned long size = partition->getSize();
	volatile double result = 0.f;
	
//	stringstream s;
//	s << "Partition " << partition->getMeta()->partitionIndex << " size: " << data->size() << "\n";
//	cout << s.str();
//	partition->print();
	
//	auto compute = []( const size_t targetBufferMult ) -> double {
//		std::mt19937::result_type seed = time(0);
//		auto rngsus = std::bind( std::uniform_int_distribution<uint64_t>(2,INT_RND_RNG), std::mt19937(seed));
//		double ret = rngsus();
//		for ( size_t pos = 0; pos < 2*targetBufferMult; ++pos ) {
//			ret += 1;
//			ret = std::sqrt(ret) + 1;
//			ret = std::sin( std::sqrt(rngsus()) + std::pow(ret, 2) );
//		}
//		return ret;
//	};
	
//	auto begin = data->begin() + offset;
//	auto end = data->begin() + offset + size;
//    Randomizer generator(2, *begin, time(nullptr));
//	for(; begin != end; ++begin){
////		std::mt19937::result_type seed = time(nullptr);
////		auto rngsus = std::bind( std::uniform_int_distribution<uint64_t>(2,*begin), std::mt19937(seed));
////		double ret = rngsus();
//
////		auto ret = generator.random();
////		ret = std::sqrt(ret) + 1;
////		ret = std::sin( std::sqrt(generator.random()) + std::pow(ret, 2) );
////		result += ret;
//        result += *begin;
//        result += 1;
////        result = sqrt(result) + 1;
//	}
	
	//// random acces on partitions
//    Randomizer accessGenerator(0, size-1, time(nullptr));
//	for(unsigned long i = 0; i < size; i++){
//		result += *(partition->begin() + accessGenerator.random());
//	}
	
	
	#if defined( AVX512 )
	//// use avx512 vector extenstions to copy partition data
	auto overlap = static_cast<unsigned char>(size % 8);
	auto outputBuffer = (unsigned long*) _mm_malloc(sizeof(unsigned long) * size, 64);
	unsigned long* src = &((*data)[0]);
	unsigned long * tar = &(*outputBuffer);
	
	// set all bits of mask
	__mmask8 mask = ~0;
	__m512i buffer, dump = _mm512_set1_epi64(0);
	
	for (unsigned long i = 0; i < ((size - overlap) / 8); ++i) {
//		buffer = _mm512_mask_loadu_epi64(dump, mask, src);
//		buffer = _mm512_mask_loadu_epi64(dump, mask, src);
		buffer = _mm512_loadu_si512((void const*) src);
//		_mm512_store_epi64(tar, buffer);
		_mm512_store_si512((void*) tar, buffer);
		src += 8;
		tar += 8;
	}
	
	//// copy last tuples, which do not fill the whole 512 bits
	mask = ~(~0 << overlap);
	buffer = _mm512_mask_loadu_epi64(dump, mask, src);
	_mm512_mask_store_epi64(tar, mask, buffer);
	
	// check for failures
//	for (int j = 0; j < size; ++j) {
//		if((*data)[j] != outputBuffer[j]){
//			cout << "copy failure. unmatching value found. " + to_string((*data)[j]) + " != " + to_string(outputBuffer[j]) + "\n";
//		}
//	}
	
	_mm_free(outputBuffer);
	
	// #def AVX512
	#else
	
	
	if(proportion <= 0.f or proportion > 100.f)
		throw runtime_error("Proportion value has to have value in intervall 0.f < proportion <= 100.f. Is: " + to_string(proportion));
	
	//// for 100 segments
	unsigned long segmentSize = 128UL;
    unsigned long segmentCnt = size / segmentSize + (size % segmentSize > 0 ? 1 : 0);
//    unsigned long segmentCnt = 10000;
//    unsigned long segments = 2;
	unsigned long accessedSegmentsCnt = static_cast<unsigned long>(static_cast<float>(segmentCnt) * proportion / 100);
	unsigned long stepSize = size / segmentCnt;
	
	//// #hotfix for small partitions
	if(stepSize == 0)
		stepSize++;
	if(accessedSegmentsCnt == 0)
		accessedSegmentsCnt++;
	
	
	//// allocate array of size equals to data vector size
	unsigned long * buffer = (unsigned long *) malloc(accessedSegmentsCnt * stepSize * sizeof(unsigned long));
//	unsigned long * buffer = Worker::currentWorker->buffer;
	unsigned long * ptr;
	
    //// generate random position to access in target buffer
    Randomizer randomBufferPosition(0, accessedSegmentsCnt * stepSize - 1, time(nullptr));
    #ifdef WORKLOAD_RANDOM_MEMORY_ACCESS
	//// generate random segment index
    Randomizer randomSegment(0, segmentCnt - 1, time(nullptr));
    //// generate random position in current segment
    Randomizer randomSegmentPosition(0, stepSize - 1, time(nullptr));
    //// save, access and delete segment indices
    unsigned long segmentPosition;
    vector<unsigned long> segmentPositions;
    for(unsigned long i = 0; i < segmentCnt; i++){
    	segmentPositions.push_back(i);
    }
	#endif
 
	//// copy data vector to buffer
	for(unsigned long i = 0; i < accessedSegmentsCnt; i++){
	    #ifdef WORKLOAD_RANDOM_MEMORY_ACCESS
//		ptr = &((*data)[0]) + position;
//		ptr = &((*data)[randomSegment.random() * stepSize]);
//		ptr = data->data() + position;
		//// select a random segement index through the segementPositions vector and delete the index from the vector
		segmentPosition = randomSegment.random() % segmentPositions.size();
		ptr = &((*data)[segmentPositions[segmentPosition] * stepSize]);
		segmentPositions.erase(segmentPositions.begin() + segmentPosition);
	    #else
		ptr = &(*data)[0] + i * stepSize;
		#endif
		//// copy the current segment
		memcpy(buffer + i * stepSize, ptr, stepSize * sizeof(unsigned long));
		for(unsigned j = 0; j < 100; j++) {
			result += buffer[i * stepSize + randomSegmentPosition.random()];
//			result = sqrt(result) + 1;
//			result = sqrt(randomSegmentPosition.random()) + pow(result, 2);
		}
	}
	
	result += buffer[randomBufferPosition.random()];
	Worker::currentWorker->result += result;
	free((void *) buffer);
	#endif // #def AVX512

	//// decrease job counter by 1
//	Coordinator::currentCoordinator->jobCnt--;
    (*Worker::currentWorker->jobCnt)--;
}
*/

//int Operator_old::getNextId() {
//	return Operator_old::operatorId++;
//}
//
//void Operator_old::setProportion(float proportion) {
//	this->proportion = proportion;
//}


/// =================================================== new operator =============================================== ///
OperatorWrapper::OperatorWrapper(OperatorType type) {
	/// create new unique operator id
	id = OperatorIdentifier::createNew();
	string outColumnNames[2];
	this->type = type;
	
	switch (type){
		case OperatorType::Select:
			inSize  = 1; /// inDataCol
			outSize = 1; /// outPosCol
			valSize = 3; /// operationType, val, outPosCountEstimate
			outColumnNames[0] = "ResCol_Select_OID_" + id.to_string();
			break;
		case OperatorType::Projection:
			inSize  = 2; /// inDataCol, inPosCol
			outSize = 1; /// outDataCol
			valSize = 0;
			outColumnNames[0] = "ResCol_Projection_OID_" + id.to_string();
			break;
		case OperatorType::Merge_Sorted:
			inSize  = 2; /// inPosLCol, inPosRCol
			outSize = 1; /// outPosCol
			valSize = 0;
			outColumnNames[0] = "ResCol_MergeSorted_OID_" + id.to_string();
			break;
		case OperatorType::Intersect_Sorted:
			inSize  = 2; /// inPosLCol, inPosRCol
			outSize = 1; /// outPosCol
			valSize = 0;
			outColumnNames[0] = "ResCol_IntersectSorted_OID_" + id.to_string();
			break;
		case OperatorType::Nested_Loop_Join:
			inSize  = 2; /// inDataLCol, inDataRCol
			outSize = 2; /// outPosLCol, outPosRCol
			valSize = 0;
			outColumnNames[0] = "ResCol_NestedLoopJoin_PosR_OID_" + id.to_string();
			outColumnNames[1] = "ResCol_NestedLoopJoin_PosL_OID_" + id.to_string();
			break;
		case OperatorType::Left_Semi_Nto1_Nested_Loop_Join:
			inSize  = 2; /// inDataLCol, inDataRCol
			outSize = 1; /// outPosLCol
			valSize = 0;
			outColumnNames[0] = "ResCol_LeftSemiNto1NestedLoopJoin_OID_" + id.to_string();
			break;
		case OperatorType::Semi_Join:
			/// @todo operator noch unterstüzt?
			inSize  = 0;
			outSize = 0;
			valSize = 0;
			break;
		case OperatorType::Agg_Sum_Global:
			inSize  = 1; /// inDataCol
			outSize = 1; /// outDataCol
			valSize = 0;
			outColumnNames[0] = "ResCol_AggSum_Ungrouped_OID_" + id.to_string();
			break;
		case OperatorType::Agg_Sum_Grouped:
			inSize  = 3; /// inGrCol, inDataCol, inExtCol
			outSize = 1; /// outDataCol
			valSize = 0;
			outColumnNames[0] = "ResCol_AggSum_Grouped_OID_" + id.to_string();
			break;
		case OperatorType::Calc_Binary:
			inSize  = 2; /// inDataLCol, inDataRCol
			outSize = 1; /// outDataCol
			valSize = 1; /// operationType
			outColumnNames[0] = "ResCol_CalcBinary_OID_" + id.to_string();
			break;
		case OperatorType::Calc_Unary:
			inSize  = 1; /// inDataCol
			outSize = 1; /// outDataCol
			valSize = 2; /// operationType, operationValue
			outColumnNames[0] = "ResCol_CalcUnary_OID_" + id.to_string();
			break;
		case OperatorType::Group_Unary:
			inSize  = 1; /// inDataCol
			outSize = 2; /// outGrCol, outExtCol
			valSize = 0;
			outColumnNames[0] = "ResCol_GroupUnary_GrCol_OID_" + id.to_string();
			outColumnNames[1] = "ResCol_GroupUnary_ExtCol_OID_" + id.to_string();
			break;
		case OperatorType::Group_Binary:
			inSize  = 2; /// inGrCol, inDataCol
			outSize = 2; /// outGrCol, outExtCol
			valSize = 0;
			outColumnNames[0] = "ResCol_GroupBinary_GrCol_OID_" + id.to_string();
			outColumnNames[1] = "ResCol_GroupBinary_ExtCol_OID_" + id.to_string();
			break;
		case OperatorType::Agg_Sum_Merge:
			inSize  = 2; /// inCharCol, inAggSumCol
			outSize = 2; /// outCharCol, outAggSumCol
			valSize = 1;
			outColumnNames[0] = "ResCol_AggSumMerge_Cha_OID_" + id.to_string();
			outColumnNames[1] = "ResCol_AggSumMerge_AggSum_OID_" + id.to_string();
			break;
	}
	
	
	
	
//	in                   = new(__allocArray(Column*,                      inSize))                        Column*[inSize];
//	in_ColumnIdentifier  = new(__allocArray(ColumnIdentifier,             inSize))               ColumnIdentifier[inSize];
////	in_ColumnSizes       = new(__allocArray(int64_t,                      inSize))                        int64_t[inSize];
//	out                  = new(__allocArray(Column*,                      outSize))                      Column*[outSize];
//	out_ColumnMeta       = new(__allocArray(GlobalColumnMetainformation*, outSize)) GlobalColumnMetainformation*[outSize];
//	val                  = new(__allocArray(int64_t,                      valSize))                      int64_t[valSize];
//
//
//	debug("reqSize: " << requiredSize());
//	void* ptr = __allocSize(requiredSize());
//	in = new ((Column**) ptr) Column*[inSize];
//	in_ColumnIdentifier = new (((ColumnIdentifier*) ptr) + inSize) ColumnIdentifier[inSize];
//	out = new (((Column**) ptr) + inSize * 2) Column*[outSize];
//	out_ColumnMeta = new (((GlobalColumnMetainformation**) ptr) + inSize * 2 + outSize) GlobalColumnMetainformation*[outSize];
//	val = new (((int64_t*) ptr) + inSize * 2 + outSize * 2) int64_t[valSize];

	allocateArrays();


	for(uint32_t i = 0U; i < inSize; ++i){
		in[i] = nullptr;
	}
	
	for(uint32_t i = 0U; i < outSize; ++i){
		out[i] = nullptr;
//		out_ColumnIdentifier[i] = ColumnIdentifier::createNew();
		out_ColumnMeta[i] = new GlobalColumnMetainformation(outColumnNames[i], BaseType::uint64, ColumnIdentifier::createNew());
	}
	
}

OperatorWrapper::OperatorWrapper(
  OperatorType type,
  uint8_t numberOfInputs,
  uint8_t numberOfOutputs,
  uint32_t numberOfInputValues,
  std::vector<std::string> namesOfOutputColumns
) : inSize(numberOfInputs),
    outSize(numberOfOutputs),
    valSize(numberOfInputValues),
    type(type)
{
	id = OperatorIdentifier::createNew();
    std::string outColumnNames[2];
    
    allocateArrays();
    
	for(uint32_t i = 0U; i < inSize; ++i){
		in[i] = nullptr;
	}
	
	for(uint32_t i = 0U; i < outSize; ++i){
		out[i] = nullptr;
//		out_ColumnIdentifier[i] = ColumnIdentifier::createNew();
		out_ColumnMeta[i] = new GlobalColumnMetainformation(outColumnNames[i], BaseType::uint64, ColumnIdentifier::createNew());
	}
}


void OperatorWrapper::run(Partition * partition) {
    static uint64_t partitionCount = 0UL;
//    if(partition) {
//        debug("Run operator " << id << " on partition " << partition->getPartitionIndex() << " count: "
//                              << partitionCount ++);
//    }
    for (uint32_t i = 0U; i < inSize; ++i) {
        /// test if input is already set, if not, get columns from partition
        if (!in[i]) {
            in[i] = partition->getColumn(in_ColumnIdentifier[i]);
        }
    }
    
	if(execFunction) {
		(*execFunction)(this);
	} else {
		severe("There is no execution function defined for operator " << id);
	}
}

//Operator::Operator() {
//}

OperatorWrapper::OperatorWrapper(const OperatorWrapper & other) {
//	debug("Copy operator from reference.");
	/// delete own arrays
//	__deleteArray(in, inSize)
//	__deleteArray(in_ColumnIdentifier, inSize);
////	__deleteArray(in_ColumnSizes, inSize);
//	__deleteArray(out, outSize);
////	__deleteArray(out_ColumnIdentifier, outSize);
//	__deleteArray(out_ColumnMeta, outSize);
//	__deleteArray(val, valSize);
//	Monitor::threadMonitor->resume(MonitoringType::Test2);

//	deallocateArrays();
	
	/// copy values from rhs
	inSize  = other.inSize;
	outSize = other.outSize;
	valSize = other.valSize;
	type    = other.type;
	
	/// reallocate arrays
//	in                   = new(__allocArray(Column*,                      inSize))                        Column*[inSize];
//	in_ColumnIdentifier  = new(__allocArray(ColumnIdentifier,             inSize))               ColumnIdentifier[inSize];
////	in_ColumnSizes       = new(__allocArray(int64_t,                      inSize))                        int64_t[inSize];
//	out                  = new(__allocArray(Column*,                      outSize))                      Column*[outSize];
////	out_ColumnIdentifier = new(__allocArray(ColumnIdentifier,             outSize))             ColumnIdentifier[outSize];
//	out_ColumnMeta       = new(__allocArray(GlobalColumnMetainformation*, outSize)) GlobalColumnMetainformation*[outSize];
//	val                  = new(__allocArray(int64_t,                      valSize))                      int64_t[valSize];
	
	allocateArrays();

	for(uint32_t i = 0; i < inSize; i++){
		in                  [i] = other.in                  [i];
		in_ColumnIdentifier [i] = other.in_ColumnIdentifier [i];
//		in_ColumnSizes      [i] = other.in_ColumnSizes      [i];
	}
	for(uint32_t i = 0; i < outSize; i++){
		out                 [i] = other.out                 [i];
//		out_ColumnIdentifier[i] = other.out_ColumnIdentifier[i];
		out_ColumnMeta      [i] = other.out_ColumnMeta      [i];
	}
	for(uint32_t i = 0; i < valSize; i++){
		val[i] = other.val[i];
	}
	
	id           = other.id;
	execFunction = other.execFunction;
	targetTable  = other.targetTable;
//	Monitor::threadMonitor->end(MonitoringType::Test2);
}

//OperatorWrapper::OperatorWrapper(OperatorWrapper && rhs) {
//	debug("Copy from rvalue assignment.");
//	/// delete own arrays
////	__deleteArray(in, inSize)
////	__deleteArray(in_ColumnIdentifier, inSize);
//////	__deleteArray(in_ColumnSizes, inSize);
////	__deleteArray(out, outSize);
//////	__deleteArray(out_ColumnIdentifier, outSize);
////	__deleteArray(out_ColumnMeta, outSize);
////	__deleteArray(val, valSize);
//	deallocateArrays();
//
//	/// steal values from rhs
//	inSize      = rhs.inSize;
//	rhs.inSize  = 0;
//	outSize     = rhs.outSize;
//	rhs.outSize = 0;
//	valSize     = rhs.valSize;
//	rhs.valSize = 0;
//	type        = rhs.type;
//
//	// todo: copy arrays on local node?
//	in                       = rhs.in;
//	rhs.in                   = nullptr;
//	out                      = rhs.out;
//	rhs.out                  = nullptr;
//	val                      = rhs.val;
//	rhs.val                  = nullptr;
////	in_ColumnSizes           = rhs.in_ColumnSizes;
////	rhs.in_ColumnSizes       = nullptr;
//	in_ColumnIdentifier      = rhs.in_ColumnIdentifier;
//	rhs.in_ColumnIdentifier  = nullptr;
////	out_ColumnIdentifier     = rhs.out_ColumnIdentifier;
////	rhs.out_ColumnIdentifier = nullptr;
//	out_ColumnMeta           = rhs.out_ColumnMeta;
//	rhs.out_ColumnMeta       = nullptr;
//
//	id           = std::move(rhs.id);
//	execFunction = std::move(rhs.execFunction);
//	targetTable  = std::move(rhs.targetTable);
//
//	/// invalidate rhs' identifier
//	rhs.id.reset();
//	rhs.targetTable.reset();
//	rhs.execFunction = nullptr;
//}


bool OperatorWrapper::isValid() {
	if(!id.isValid())
		return false;
	if(!targetTable.isValid())
		return false;
//	if(!in_ColumnIdentifier)
//		return false;
//	for(uint32_t i = 0; i < inSize; i++){
//		if(!in_ColumnIdentifier[i].isValid())
//			return false;
//	}
	return true;
}

OperatorWrapper & OperatorWrapper::operator=(const OperatorWrapper & rhs) {
	debug("Copy from lvalue assignment");
	/// delete own arrays
//	__deleteArray(in, inSize)
//	__deleteArray(in_ColumnIdentifier, inSize);
////	__deleteArray(in_ColumnSizes, inSize);
//	__deleteArray(out, outSize);
////	__deleteArray(out_ColumnIdentifier, outSize);
//	__deleteArray(out_ColumnMeta, outSize);
//	__deleteArray(val, valSize);
	deallocateArrays();
	
	
	/// copy values from rhs
	inSize  = rhs.inSize;
	outSize = rhs.outSize;
	valSize = rhs.valSize;
	type    = rhs.type;
	
//	delete in;
//	delete in_ColumnIdentifier;
//	delete in_ColumnSizes;
//	delete out;
//	delete out_ColumnIdentifier;
//	delete val;
	
	/// reallocate arrays
//	in                   = new(__allocArray(Column*,                      inSize))                        Column*[inSize];
//	in_ColumnIdentifier  = new(__allocArray(ColumnIdentifier,             inSize))               ColumnIdentifier[inSize];
////	in_ColumnSizes       = new(__allocArray(int64_t,                      inSize))                        int64_t[inSize];
//	out                  = new(__allocArray(Column*,                      outSize))                      Column*[outSize];
////	out_ColumnIdentifier = new(__allocArray(ColumnIdentifier,             outSize))             ColumnIdentifier[outSize];
//	out_ColumnMeta       = new(__allocArray(GlobalColumnMetainformation*, outSize)) GlobalColumnMetainformation*[outSize];
//	val                  = new(__allocArray(int64_t,                      valSize))                      int64_t[valSize];
	
	allocateArrays();

	for(uint32_t i = 0; i < inSize; i++){
		in                  [i] = rhs.in                  [i];
		in_ColumnIdentifier [i] = rhs.in_ColumnIdentifier [i];
//		in_ColumnSizes      [i] = rhs.in_ColumnSizes      [i];
	}
	for(uint32_t i = 0; i < outSize; i++){
		out                 [i] = rhs.out                 [i];
//		out_ColumnIdentifier[i] = rhs.out_ColumnIdentifier[i];
		out_ColumnMeta      [i] = rhs.out_ColumnMeta      [i];
	}
	for(uint32_t i = 0; i < valSize; i++){
		val[i] = rhs.val[i];
	}
	
	id           = rhs.id;
	execFunction = rhs.execFunction;
	targetTable  = rhs.targetTable;
	
	return *this;
}

OperatorWrapper & OperatorWrapper::operator=(OperatorWrapper && rhs) noexcept {
	debug("Copy from rvalue assignment.");
	/// delete own arrays
//	__deleteArray(in, inSize)
//	__deleteArray(in_ColumnIdentifier, inSize);
////	__deleteArray(in_ColumnSizes, inSize);
//	__deleteArray(out, outSize);
////	__deleteArray(out_ColumnIdentifier, outSize);
//	__deleteArray(out_ColumnMeta, outSize);
//	__deleteArray(val, valSize);
	deallocateArrays();
	
	/// steal values from rhs
	inSize      = rhs.inSize;
	rhs.inSize  = 0;
	outSize     = rhs.outSize;
	rhs.outSize = 0;
	valSize     = rhs.valSize;
	rhs.valSize = 0;
	type        = rhs.type;
	
	// todo: copy arrays on local node?
	in                       = rhs.in;
	rhs.in                   = nullptr;
	out                      = rhs.out;
	rhs.out                  = nullptr;
	val                      = rhs.val;
	rhs.val                  = nullptr;
//	in_ColumnSizes           = rhs.in_ColumnSizes;
//	rhs.in_ColumnSizes       = nullptr;
	in_ColumnIdentifier      = rhs.in_ColumnIdentifier;
	rhs.in_ColumnIdentifier  = nullptr;
//	out_ColumnIdentifier     = rhs.out_ColumnIdentifier;
//	rhs.out_ColumnIdentifier = nullptr;
	out_ColumnMeta           = rhs.out_ColumnMeta;
	rhs.out_ColumnMeta       = nullptr;
	
	id           = std::move(rhs.id);
	execFunction = std::move(rhs.execFunction);
	targetTable  = std::move(rhs.targetTable);
	
	/// invalidate rhs' identifier
	rhs.id.reset();
	rhs.targetTable.reset();
	rhs.execFunction = nullptr;
	
	return *this;
}

void * OperatorWrapper::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(OperatorWrapper));
}

void OperatorWrapper::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(OperatorWrapper));
}

void OperatorWrapper::reset() {
	/// delete own arrays
//	__deleteArray(in, inSize)
////	__deleteArray(in_ColumnSizes, inSize);
//	__deleteArray(in_ColumnIdentifier, inSize);
//	__deleteArray(out, outSize);
////	__deleteArray(out_ColumnIdentifier, outSize);
//	__deleteArray(out_ColumnMeta, outSize);
//	__deleteArray(val, valSize);
	deallocateArrays();
	
	in                   = nullptr;
	in_ColumnIdentifier  = nullptr;
//	in_ColumnSizes       = nullptr;
	out                  = nullptr;
//	out_ColumnIdentifier = nullptr;
	out_ColumnMeta       = nullptr;
	val                  = nullptr;
	
	inSize  = 0;
	outSize = 0;
	valSize = 0;
	
	execFunction = nullptr;
	id.reset();
	targetTable.reset();
}

ostream & operator<<(ostream & os, const OperatorWrapper & op) {
    std::stringstream s;
	s << "ID: " << op.id << endl;
	s << "TargetTable: " << op.targetTable << endl;
	s << "inSize: " << op.inSize << endl;
	s << "InPtr: " << op.in << endl;
	for(uint32_t i = 0; i < op.inSize; i++){
		s << "InColumn["<< i <<"]: " << op.in_ColumnIdentifier[i] << endl;
	}
	for(uint32_t i = 0; i < op.inSize; i++){
		s << "InPtr["<< i <<"]: " << op.in[i] << endl;
	}
//	for(uint32_t i = 0; i < op.inSize; i++){
//		os << "InColSize["<< i <<"]: " << op.in_ColumnSizes[i] << endl;
//	}
	s << "outSize: " << op.outSize << endl;
	s << "OutPtr: " << op.out << endl;
	for(uint32_t i = 0; i < op.outSize; i++){
		s << "OutPtr["<< i <<"]: " << op.out[i] << endl;
	}
	s << "valSize: " << op.valSize << endl;
	for(uint32_t i = 0; i < op.valSize; i++){
		s << "Value["<< i <<"]: " << op.val[i] << endl;
	}

	os << s.str();
    os << op.id;
	return os;
}




/*
std::function<void(OperatorWrapper*)> OperatorWrapper::GROUP_BY = [](OperatorWrapper * op){
//		info("The output columns name: " << op->out_ColumnMeta[0]->name);
	const std::tuple<Column*, Column*> result =
			morphstore::group(
				op->in[0],
				op->out_ColumnMeta[0],
				op->out_ColumnMeta[1]
			);
	
	op->out[0] = get<0>(result);
	op->out[1] = get<1>(result);
	
	#ifdef PRINT_OPERATOR_RESULTS
	Column * cols[3] = {op->in[0], op->out[0], op->out[1]};
	info("Printing result:" << "\n" << printColumnsAsTable(cols,3));
	#endif
};

std::function<void(OperatorWrapper*)> OperatorWrapper::AGG_SUM = [](OperatorWrapper * op){
	Column * result = morphstore::agg_sum(op->in[0], op->out_ColumnMeta[0]);
	op->out[0] = result;
	
	#ifdef PRINT_OPERATOR_RESULTS
	info("Aggregation Sum result: " << ((uint64_t*) result->getData())[0]);
	#endif
};

std::function<void(OperatorWrapper*)> OperatorWrapper::AGG_SUM_GROUPED = [] (OperatorWrapper * op) {
	Column * result = morphstore::agg_sum(
			op->in[0], /// column with group ids
			op->in[1], /// column with data
			op->out_ColumnMeta[0], /// column information of output column
			op->in[2]->getElementCount() /// size of extends
			);
	op->out[0] = result;
	
//	stringstream ss;
//	ss << "|";
//	ss << str_lfill(" " + op->out[0]->getName() +" =", 40, "=") << "|";
//	ss << "\n";
//
//	for (uint64_t j = 0; j < op->out[0]->getElementCount(); ++j) {
//		ss << "|";
//		ss << str_lfill( to_string(((uint64_t *) op->out[0]->getData())[j]) , 40 ) << "|";
//		ss << "\n";
//	}
//	info("Printing result:" << "\n" << ss.str());
	#ifdef PRINT_OPERATOR_RESULTS
	Column * cols[4] = {op->in[0],op->in[1],op->in[2], op->out[0]};
	info("Printing result:" << "\n" << printColumnsAsTable(cols,4));
	#endif
};

std::function<void(OperatorWrapper*)> OperatorWrapper::PROJECTION = [] (OperatorWrapper * op) {
	Column * result = morphstore::project(
			op->in[0],
			op->in[1],
			op->out_ColumnMeta[0]
			);
	op->out[0] = result;
	#ifdef PRINT_OPERATOR_RESULTS
	Column * cols[3] = {op->in[0],op->in[1], op->out[0]};
	info("Printing result:" << "\n" << printColumnsAsTable(cols,3));
	#endif
};

std::function<void(OperatorWrapper*)> OperatorWrapper::AGG_SUM_MERGE = [] (OperatorWrapper * op) {
	std::tuple<Column*, Column*> result = morphstore::agg_sum_merge(
			(const Column** const) op->in[0],
			(const Column** const) op->in[1],
			op->out_ColumnMeta[0],
			op->out_ColumnMeta[1],
			(uint64_t) op->val[0]
	);
	op->out[0] = get<0>(result);
	op->out[1] = get<1>(result);
	#ifdef PRINT_OPERATOR_RESULTS
	Column * cols[2] = {op->out[0],op->out[1]};
	info("Printing result:" << "\n" << printColumnsAsTable(cols,2));
	#endif
};
void OperatorWrapper::agg_sum_merge(OperatorWrapper * op) {
	std::tuple<Column*, Column*> result = morphstore::agg_sum_merge(
			(const Column** const) op->in[0],
			(const Column** const) op->in[1],
			op->out_ColumnMeta[0],
			op->out_ColumnMeta[1],
			(uint64_t) op->val[0]
	);
	op->out[0] = get<0>(result);
	op->out[1] = get<1>(result);
	#ifdef PRINT_OPERATOR_RESULTS
	Column * cols[2] = {op->out[0],op->out[1]};
	info("Printing result:" << "\n" << printColumnsAsTable(cols,2));
	#endif
}

*/

uint32_t OperatorWrapper::requiredSize() {
	return sizeof(void*) * (inSize * 2 + outSize * 2 + valSize);
}

void OperatorWrapper::allocateArrays() {
//	debugh("inSize before alloc: " << (uint64_t)inSize);
//	in                   = new(__allocArray(Column*,                      inSize))                        Column*[inSize];
//	in_ColumnIdentifier  = new(__allocArray(ColumnIdentifier,             inSize))               ColumnIdentifier[inSize];
//	out                  = new(__allocArray(Column*,                      outSize))                      Column*[outSize];
//	out_ColumnMeta       = new(__allocArray(GlobalColumnMetainformation*, outSize)) GlobalColumnMetainformation*[outSize];
//	val                  = new(__allocArray(int64_t,                      valSize))                      int64_t[valSize];


//	debug("reqSize: " << requiredSize());

	uint64_t* ptr = (uint64_t*) morphstore::MemoryManager::staticAllocate(requiredSize());
	
//	in = new (ptr) Column*[inSize];
//	in_ColumnIdentifier = new (ptr + inSize) ColumnIdentifier[inSize];
//	out = new (ptr + inSize * 2) Column*[outSize];
//	out_ColumnMeta = new (ptr + inSize * 2 + outSize) GlobalColumnMetainformation*[outSize];
//	val = new (ptr + inSize * 2 + outSize * 2) int64_t[valSize];
	
	in = (column_t**) ptr;
	in_ColumnIdentifier = (ColumnIdentifier*) (ptr + inSize);
	out = (column_t**) (ptr + inSize * 2);
	out_ColumnMeta = (GlobalColumnMetainformation**) (ptr + inSize * 2 + outSize);
	val = (int64_t*) (ptr + inSize * 2 + outSize * 2);

//	debugh("inSize after alloc: " << (uint64_t)inSize);

}

void OperatorWrapper::deallocateArrays() {
//	__deleteArray(in, inSize)
//	__deleteArray(in_ColumnIdentifier, inSize);
//	__deleteArray(out, outSize);
//	__deleteArray(out_ColumnMeta, outSize);
//	__deleteArray(val, valSize);

	morphstore::MemoryManager::staticDeallocate((uint64_t*)in, requiredSize());
	
//	in                   = nullptr;
//	in_ColumnIdentifier  = nullptr;
////	in_ColumnSizes       = nullptr;
//	out                  = nullptr;
////	out_ColumnIdentifier = nullptr;
//	out_ColumnMeta       = nullptr;
//	val                  = nullptr;
	
}
