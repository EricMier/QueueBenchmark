/**********************************************************************************************
 * Copyright (C) 2019 by MorphStore-Team                                                      *
 *                                                                                            *
 * This file is part of MorphStore - a compression aware vectorized column store.             *
 *                                                                                            *
 * This program is free software: you can redistribute it and/or modify it under the          *
 * terms of the GNU General Public License as published by the Free Software Foundation,      *
 * either version 3 of the License, or (at your option) any later version.                    *
 *                                                                                            *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;  *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  *
 * See the GNU General Public License for more details.                                       *
 *                                                                                            *
 * You should have received a copy of the GNU General Public License along with this program. *
 * If not, see <http://www.gnu.org/licenses/>.                                                *
 **********************************************************************************************/


#ifndef QUEUEBENCHMARK_OPERATORPREREQUISITE_H
#define QUEUEBENCHMARK_OPERATORPREREQUISITE_H

#include "util/Identifier.h"
#include <forward>
#include <core/memory/MemoryManager.h>



class OperatorPrerequisite {
    using column_t = const morphstore::column<morphstore::uncompr_f>;
  public:
	//// Constructor
	OperatorPrerequisite() = default;
	//// Copy constructor
	OperatorPrerequisite(const OperatorPrerequisite & other) = default;
	//// Move constructor
	OperatorPrerequisite(OperatorPrerequisite && other) noexcept = default;
	// Destructor
	virtual ~OperatorPrerequisite() = default;
	
	//// Assignment operator
	OperatorPrerequisite & operator=(const OperatorPrerequisite & rhs) = default;
	//// Move assignment operator
	OperatorPrerequisite & operator=(OperatorPrerequisite && rhs) noexcept = default;
	
	void* operator new(size_t size){
        return morphstore::MemoryManager::staticAllocate(sizeof(OperatorPrerequisite));
	}
	void operator delete(void* p){
        morphstore::MemoryManager::staticDeallocate(p, sizeof(OperatorPrerequisite));
	}
	
	identifier_map<ColumnIdentifier, column_t*> requisites;
	
	bool isFullfilled();
	
	OperatorPrerequisite & operator += (ColumnIdentifier & cid);
	column_t *& operator [](ColumnIdentifier cid);
	
  private:
	
};

inline
bool OperatorPrerequisite::isFullfilled() {
	for(auto& req : requisites){
		if(req.second == nullptr)
			return false;
	}
	return true;
}

inline
OperatorPrerequisite & OperatorPrerequisite::operator += (ColumnIdentifier & cid){
	requisites.insert({cid, nullptr});
	return *this;
}

inline
OperatorPrerequisite::column_t *& OperatorPrerequisite::operator [](ColumnIdentifier cid){
	return requisites[cid];
}


#endif //QUEUEBENCHMARK_OPERATORPREREQUISITE_H
