/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Job.cpp
 * Author: EricMier
 * 
 * Created on 1. August 2019, 14:55
 */

#include "Job.h"

#include <utility>
#include "OperatorWrapper.h"
#include <logging>

//Job_old::Job_old(): id(-1) {
//	this->op = Operator_old();
//}
//
//Job_old::Job_old(Operator_old& op): id(-1) {
//	this->op = Operator_old(op);
//}
//
//Job_old::Job_old(Operator_old op, int id): op(op), id(id) {
//}
//
//Job_old::Job_old(Operator_old op, int id, unsigned long wait) : op(op), id(id), wait(wait) {
//
//}
//
//// @todo overload move operator
//Job_old::Job_old(const Job_old& orig) {
//	id = orig.id;
//	op = Operator_old(orig.op);
//	wait = orig.wait;
//}
//
//Job_old::~Job_old() {
//}
//
//
//int Job_old::getId(){
//	return this->id;
//}
//
//Operator_old* Job_old::getOperator() {
//	return &op;
//}
//
//unsigned long Job_old::getWait() {
//    return wait;
//}


Job::Job(OperatorWrapper * op, PartitionIndex partId) : op(op), partId(std::move(partId)) {
    //
}

Job::Job(Job && other) noexcept {
	op = other.op;
	partId = std::move(other.partId);
}

Job::Job(const Job & other) {
	debugstack(YELLOW << "Copy job");
	op = other.op;
	partId = other.partId;
}

void * Job::operator new(size_t size) {
    return morphstore::MemoryManager::staticAllocate(sizeof(Job));
}

void Job::operator delete(void * p) {
    morphstore::MemoryManager::staticDeallocate(p, sizeof(Job));
}

void Job::reset() {
	op->reset();
	partId.reset();
}
