/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Operator.h
 * Author: EricMier
 *
 * Created on 7. August 2019, 10:52
 */

class OperatorWrapper;

#ifndef OPERATOR_H
#define OPERATOR_H

//#include <storage/Column.h>
#include <forward>

#include <stdlibs>
#include "util/Identifier.h"
#include "OperatorScope.h"
#include "OperatorType.h"
#include "core/storage/column.h"
//#include <storage>



//enum class SelectOperationType {
//	less,
//	lessEquals,
//	greater,
//	greaterEquals,
//	equals,
//	notEquals
//};


class [[nodiscard]] OperatorWrapper {
    using column_t = const morphstore::column<morphstore::uncompr_f>;
  public:
	OperatorWrapper() = default;
	explicit OperatorWrapper(OperatorType type);
	OperatorWrapper(OperatorType type, uint8_t numberOfInputs, uint8_t numberOfOutputs, uint32_t numberOfInputValues, std::vector<std::string> namesOfOutputColumns);
	OperatorWrapper(const OperatorWrapper & other);
//	OperatorWrapper(OperatorWrapper && other);
//	OperatorWrapper(OperatorWrapper && other) = delete;
	
	OperatorWrapper & operator=(const OperatorWrapper & rhs);
	OperatorWrapper & operator=(OperatorWrapper && rhs) noexcept ;
	
	void* operator new(size_t size);
	void operator delete(void* p);
	
	void run(Partition * partition);
//	void run(Partition * partition);
	bool isValid();
	void reset();
	
	uint32_t requiredSize();
	void allocateArrays();
	void deallocateArrays();
	
	typedef std::function<void(OperatorWrapper*)> Executable;
	
	Executable * execFunction = nullptr; /// size: 8
	
	/// input columns
	column_t ** in = nullptr; /// size: 8
	/// output columns
	column_t ** out = nullptr; /// size: 8
	/// input values
	int64_t * val = nullptr; /// size: 8
	
	/// input column sizes
//	int64_t * in_ColumnSizes = nullptr; /// size: 8
	
	/// number of inputs
	uint8_t inSize {0}; /// size: 1
	/// number of outputs
	uint8_t outSize {0}; /// size: 1
	/// number of input values
	uint32_t valSize {0}; /// size: 4
	
	OperatorIdentifier id; /// size: 4/8
	OperatorType type; /// size: 4
	
	/// Identifier of target table
	TableIdentifier targetTable; /// size: 8
	/// Identifier list of incoming columns
	ColumnIdentifier * in_ColumnIdentifier = nullptr; /// size: 8
	///
	GlobalColumnMetainformation ** out_ColumnMeta = nullptr; /// size: 8
	
	static std::function<void(OperatorWrapper*)> GROUP_BY;
	static std::function<void(OperatorWrapper*)> AGG_SUM;
	static std::function<void(OperatorWrapper*)> AGG_SUM_GROUPED;
	static std::function<void(OperatorWrapper*)> PROJECTION;
	static std::function<void(OperatorWrapper*)> AGG_SUM_MERGE;
	
	static void agg_sum_merge(OperatorWrapper* op);
	
};

//// stream out operator
std::ostream & operator<<(std::ostream & os, const OperatorWrapper & op);

#endif /* OPERATOR_H */

