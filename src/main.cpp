/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: EricMier
 *
 * Created on 1. August 2019, 14:04
 */

#include "includesForHeader.h"
#include "includesForCpp.h"

#include <concurrentqueue/concurrentqueue.h>
#include <thread/NodeAllocationFence.h>

#include "storage/Database.h"
#include "storage/Table.h"
#include "monitoring/Monitor_old.h"
#include "platform/Platform.h"
#include "test/Benchmark.h"
#include "thread/Coordinator.h"
#include "thread/ThreadManager.h"
#include "util/Randomizer.h"
#include "engine/PrototypeEngine.h"

#include <bitset>
#include <test/Test.h>
#include <test/multInclude.h>
#include <monitoring/Monitor.h>
//#include <immintrin.h>

using namespace std;

std::mutex iomutex;

/*
 *
 *
 */
int main(int argc, char ** argv) {
	unsigned partitionCnt = 0;
	unsigned operatorCnt = 0;
    ThreadManager::threadName = " MT ";
    PrototypeEngine::workerCnt = WORKER_CNT_PER_NODE;
    
//    cout << "numa_available(): " << numa_available() << endl;
//    if(!(numa_available() >= 0)){
//		severe("NUMA not available.");
//
//    }
//    cout << numa_num_task_nodes() << endl;
//    return 0;

	//// === parse runtime parameters === ////
	std::vector<std::string> args(argv + 1, argv + argc);
	for (auto it = args.begin(); it != args.end(); it++) {
		if (*it == "-p" && (it + 1) != args.end()) {
			PrototypeEngine::test_partitionCount = (uint64_t) std::stoi(*(it + 1));
			it++;
		}
		if (*it == "-v" && (it + 1) != args.end()) {
			PrototypeEngine::test_version = *(it + 1);
			it++;
		}
		if (*it == "-w" && (it + 1) != args.end()) {
			PrototypeEngine::workerCnt = stoul(*(it + 1));
			it++;
		}
		if (*it == "-o" && (it + 1) != args.end()) {
			operatorCnt = (unsigned) std::stoi(*(it + 1));
			it++;
		}
	}
	
	//// === Read platform information === ////
	Platform::setup();
	
	//// === Welcome message === ////
	cout << GREEN
		 << " //" << str_mfill("", 100, "=") << "\\\\\n"
		 << "//" << str_mfill(str_mfill(" Welcome to MorphStore Prototype ", 50, " "), 102, "=") << "\\\\\n"
		 << "\\\\" << str_mfill(
		 		str_mfill(" running on " + RED + Platform::getHostname() + GREEN + " ", 50 + GREEN.length() + RED.length(), " "),
		 		102 + GREEN.length() + RED.length(), "="
		 	) << "//\n"
		 << " \\\\" << str_mfill("", 100, "=") << "//\n"
		 << RESET;
	Platform::printConfig();
	cout << endl;
	
	//// === Bind main thread to first node === ////
//	ThreadManager::bindToNode(Platform::getFirstNode());
	ThreadManager::bindToNode(Platform::getNode(0));
	
	//// === color output === ////
//	for(int i = 0; i < 16; i++){
//		for(int j = 0; j < 16; j++){
//			int code = i * 16 + j;
//			cout << COLOR(code) << str_lfill(to_string(code),4);
//		}
//		cout << endl;
//	}
//	return 0;

    //// === start benchmark === ////
//	Benchmark::QueueAccessBenchmark();
//	Benchmark::QueueAccessBenchmark2();
//	Benchmark::NumaBenchmark();

//	multInclude mi;
//	mi.someFunction();
//	Test().run();

//    PrototypeEngine::pageAllocationTest();
//    PrototypeEngine::pageAllocationTest2();
//    PrototypeEngine::pageAllocationTest3();


//	return 0;
	
	//// === start prototype === ////
	try {
		string version = PrototypeEngine::test_version;
//		ofstream runtime, partCount;
		map<string, ofstream> files;
		files["partCount"];
		files["workerCount"];
		files["runtime"];
		files["wDequeuingJob"];
		files["wExecuteJob"];
		files["wExecuteGlobalJob"];
		files["cProcessBuffer"];
		files["cProcessGlobalBuffer"];
		
		/// object vs pointer enqueuing
		#ifdef ENQUEUE_JOB_POINTER
		string path = "./output/" + Platform::getHostname() + "/queue_with_pointer/" + version;
		#else
		string path = "./output/" + Platform::getHostname() + "/queue_with_objects/" + version;
		#endif
		
		/// check if output path exists, if not, create it
		if(!testAndCreateDirectory(path)){
			error("Can not create directory.");
		}
		
		
		/// open output files
		for(auto& [name, file] : files){
			file.open(path + "/" + name + ".csv", ios::in | ios::out | ios::app);
			if(!file){
				severe("Can not write to file: " << path << "/" << name << ".csv");
			}
		}
		
		
//		runtime.open(path + "/runtime.csv", ios::in | ios::out | ios::app);
//		partCount.open(path + "/partCount.csv", ios::in | ios::out | ios::app);
//		if(!runtime) {
//			severe("Can not write to file: " << path << "/runtime.csv");
//		}
//		if(!partCount) {
//			severe("Can not write to file: " << path << "/partCount.csv");
//		}


		//// === Run Benchmark === ////
		info(GREEN << "======= New Run (" << PrototypeEngine::test_partitionCount << " partitions) =======");
		Monitor::resetGlobalMonitor();
		Monitor::threadMonitor = Monitor::globalMonitor;
		PrototypeEngine::start(); /// <-- the interesting part @dirk & @alex
		Monitor * mon = Monitor::globalMonitor;
		files["partCount"]              << PrototypeEngine::test_partitionCount << ";";
		files["workerCount"]            << PrototypeEngine::workerCnt << ";";
//		files["runtime"]                << mon->getDuration(MonitoringType::GlobalRuntime).count() << ";";
		files["runtime"]                << mon->averageTransmitted(MonitoringType::GlobalRuntime).count() << ";";
		files["wDequeuingJob"]          << mon->averageTransmitted(MonitoringType::wDequeuingJob).count() << ";";
		files["wExecuteJob"]            << mon->averageTransmitted(MonitoringType::wExecuteJob).count() << ";";
		files["wExecuteGlobalJob"]      << mon->averageTransmitted(MonitoringType::wExecuteGlobalJob).count() << ";";
		files["cProcessBuffer"]         << mon->averageTransmitted(MonitoringType::cProcessBuffer).count() << ";";
		files["cProcessGlobalBuffer"]     << mon->averageTransmitted(MonitoringType::cProcessGlobalBuffer).count() << ";";
		
		info(GREEN << str_mfill(" Print average results of all sub runs ",35+12+4, "="));
		info(str_rfill("[M] Runtime: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::GlobalRuntime)).count()))
				,12)
		        << " us");
		info(str_rfill("[W] Avg Dequeuing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::wDequeuingJob)).count()))
				,12)
				<< " us");
		info(str_rfill("[W] Avg Executing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::wExecuteJob)).count()))
				,12)
				<< " us");
		info(str_rfill("[W] Avg Global job Executing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::wExecuteGlobalJob)).count()))
				,12)
				<< " us");
		info(str_rfill("[C] Avg Buffer processing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::cProcessBuffer)).count()))
				,12)
				<< " us");
		info(str_rfill("[C] Avg global Buffer processing: ", 35) << YELLOW << str_lfill(
				dotNumber(to_string(chrono::duration_cast<chrono::microseconds>(
				mon->averageTransmitted(MonitoringType::cProcessGlobalBuffer)).count()))
				,12)
				<< " us");
		
		Database::destroyDatabase();
		Identifier::resetAllIds();
		
		

		for(auto& [name, file] : files){
			file.close();
		}
//		runtime.close();
//		partCount.close();
    } catch (exception& e) {
    	cout << endl << RED << str_mfill(" MorphStore Prototype crashed ", LINE_LENGTH + 10, "=") << RESET << endl;
    	cout << "MorphStore Prototype has stopped unexpected. Reason: \n" << e.what() << endl;
    	return 101;
    }
    return 0;

	
	//// === just testing === ////
	return 0;
}

