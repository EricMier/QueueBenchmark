macro(set_debug_mode target)
    message( "Set debug mode for target ${target}" )
    target_compile_options(${target} PUBLIC -g -O0)
    target_compile_definitions(${target} PUBLIC DEBUG)
endmacro()

macro(set_release_mode target)
    message( "Set release mode for target ${target}" )
    target_compile_options(${target} PUBLIC -O3)
endmacro()
