function(print_sources_of_target target)
    message("This is the source file list of target ${target}")
    get_target_property(MY_PROJECT_SOURCES ${target} SOURCES)
    message(STATUS "MY_PROJECT_SOURCES: ${MY_PROJECT_SOURCES}")

endfunction()
