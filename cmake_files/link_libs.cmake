macro(link_libnuma_to_target target)
    message( "Link LibNuma to target ${target}" )
    ##### find LibNuma
    find_package(Numa)
    if(NOT "${NUMA_LIBRARY}" STREQUAL "")
        message( STATUS "NumaLib found in : ${NUMA_LIBRARY}")
    else()
        message( FATAL_ERROR "NumaLib not found. Make sure NumaLib is installed.")
    endif()

    ##### Link LibNuma to target, except on my local machine (which isn't a Numa system)
    if(NOT ${host_name} STREQUAL "C940")
        target_compile_definitions(${target} PUBLIC USE_LIBNUMA)
#        target_compile_options(${target} PRIVATE -DUSE_LIBNUMA)
        target_link_libraries( ${target} LINK_PUBLIC ${NUMA_LIBRARY} )
    endif()
endmacro()



macro(link_boost_stacktrace_to_target target)
    ####### find boost library
    message( "Link Boost::stacktrace to target ${target}")
    set(_Boost_STACKTRACE_BASIC_HEADERS     "boost/stacktrace.hpp")
    set(_Boost_STACKTRACE_BACKTRACE_HEADERS "boost/stacktrace.hpp")
    set(_Boost_STACKTRACE_ADDR2LINE_HEADERS "boost/stacktrace.hpp")
    set(_Boost_STACKTRACE_NOOP_HEADERS      "boost/stacktrace.hpp")

    find_package(Boost COMPONENTS stacktrace_basic stacktrace_backtrace stacktrace_addr2line stacktrace_noop)
    include_directories( ${Boost_INCLUDE_DIR} )

    target_compile_definitions(${target} PUBLIC BOOST_STACKTRACE_USE_BACKTRACE)
    target_compile_options(${target} PUBLIC -rdynamic)
    target_link_libraries( ${target} LINK_PUBLIC dl backtrace )
endmacro()

