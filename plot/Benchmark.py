import string
import configparser
from typing import NamedTuple, List
import os
import matplotlib.pyplot as plt
import numpy as np

class Benchmark:
    xValues: list
    curveLabel: list
    yValues: list
    xAxis: string

    def __init__(self, xV: list, cL: list, yV: list, xA: string):
        self.xValues = xV
        self.curveLabel = cL
        self.yValues = yV
        self.xAxis = xA


class MeasurementTuple:
    value: float
    optimum: float = 0.0

    def __init__(self, input):
        if len(input.split(",")) == 2:
            self.value, self.optimum = [float(i) for i in input.split(",")]
        else:
            self.value = float(input)


class Measurement:
    tuples: List[MeasurementTuple] = []

    def __init__(self, input):
        self.tuples = []
        for n in input.split(";"):
            self.tuples.append(MeasurementTuple(n))

    def __getitem__(self, item):
        if self.tuples[item]:
            return self.tuples[item]
        return None


class TestSeries:
    label: string
    value: float
    measurements: List[Measurement] = []

    def __init__(self, input):
        self.measurements = []
        label, m = input.split("$")
        self.label, tmp = label.split("###", 1)
        self.value = float(tmp)
        for mm in m.split("###"):
            self.measurements.append(Measurement(mm))

    def __getitem__(self, item):
        if self.measurements[item]:
            return self.measurements[item]
        return None

    def getSlot(self, slot):
        items = []
        for m in self.measurements:
            items.append(m.tuples[int(slot)].value)
        return items





class Benchmark2:
    testCase: string
    subTestCase: string
    version: string
    basePath: string
    hostname: string
    config = configparser.ConfigParser()

    results: string
    settings: string
    comments: string

    xLabel: string
    xValues: List[float] = []  # integer
    yValues: List[TestSeries] = []  # TestSeries

    defaultFigSize = (10, 5)


    def __init__(self, path: string):
        self.xValues = []
        self.yValues = []
        pathSplit = path.split("/")
        while pathSplit[len(pathSplit) - 1] == "":
            del pathSplit[len(pathSplit) - 1]
        self.version = pathSplit[len(pathSplit) - 1]
        self.subTestCase = pathSplit[len(pathSplit) - 2]
        self.testCase = pathSplit[len(pathSplit) - 3]
        self.hostname = pathSplit[len(pathSplit) - 4]
        self.basePath = path

        self.results = path + "/results.csv"
        self.settings = path + "/settings.ini"
        self.comments = path + "/comments.ini"

        if not os.path.isfile(self.results):
            print("Error: For " + self.testCase + "/" + self.subTestCase + "/" + self.version + " no results.csv found!")
            raise ValueError("Error: For " + self.testCase + "/" + self.subTestCase + "/" + self.version + " no results.csv found!")
        if not os.path.isfile(self.settings):
            print("Error: For " + self.testCase + "/" + self.subTestCase + "/" + self.version + " no settings.ini found!")
            raise ValueError("Error: For " + self.testCase + "/" + self.subTestCase + "/" + self.version + " no settings.ini found!")

        self.config.read(self.settings)

        self.xValues = [float(i) for i in self.config["Plot"]["XValues"].split(",")]
        self.xLabel = self.config["Plot"]["XLabel"]

        lines = []
        file = open(self.results, "r")
        for line in file:
            lines.append(line.replace("\n", ""))
        file.close()

        if not lines:
            raise ValueError("Error while reading " + self.results)

        for line in lines:
            self.yValues.append(TestSeries(line))

        # print(self.yValues[0].getSlot(0))
        # print(self.yValues[0].getSlot(1))
        # print(self.yValues[0].getSlot(2))




    def buildBasePath(self):
        return "../output/" + self.hostname + "/" + self.testCase + "/" + self.subTestCase + "/" + self.version



    def plotOverallRuntime(self, show=False, override=False):
        outputFile = self.buildBasePath() + "/overallRuntime.png"
        if not override and os.path.isfile(outputFile):
            return self

        fig, ax = plt.subplots(ncols=1, figsize=self.defaultFigSize)

        for series in self.yValues:
            ax.plot(
                np.array(self.xValues),
                np.array(series.getSlot(self.config["Slots"]["MONITOR_GLOBAL_RUNTIME"])),
                # np.array(self.yValues[i].getSlot(self.config["Slots"]["MONITOR_FIND_JOB"])),
                label=series.label,
                marker="^",
                markersize=6)

        ax.set_title(self.testCase + "/" + self.version + "/" + self.subTestCase + "\n" + "Overall runtime", fontsize=18)
        ax.set_xlabel(self.xLabel)
        ax.set_ylabel("Runtime in ms")

        ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10, which="both")

        # ax.legend(loc="upper right")
        ax.legend(loc="best")

        print("Plot " + self.buildBasePath() + ": OverallRuntime saved to: " + outputFile)
        plt.savefig(outputFile)
        if show:
            plt.show()
        plt.close()
        return self



    def plotOverhead(self, show=False, override=False):
        outputFile = self.buildBasePath() + "/overhead.png"
        if not override and os.path.isfile(outputFile):
            return self

        fig, ax = plt.subplots(ncols=1, figsize=self.defaultFigSize)

        for series in self.yValues:
            overhead: List[float] = []
            for m in series.measurements:
                overhead.append(
                    m.tuples[int(self.config["Slots"]["MONITOR_GLOBAL_RUNTIME"])].value
                    - m.tuples[int(self.config["Slots"]["MONITOR_EXECUTE_JOB"])].value
                )
            ax.plot(
                np.array(self.xValues),
                np.array(overhead),
                label=series.label,
                marker="^",
                markersize=6)

        ax.set_title(self.testCase + "/" + self.version + "/" + self.subTestCase + "\n" + "Overhead", fontsize=18)
        ax.set_xlabel(self.xLabel)
        ax.set_ylabel("Overhead in ms")

        ax.set_xlim(self.xValues[0] - 10)
        ax.set_ylim(0)

        ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10, which="both")

        # ax.legend(loc="upper right")
        ax.legend(loc="best")

        print("Plot " + self.buildBasePath() + ": Overhead saved to: " + outputFile)
        plt.savefig(outputFile)
        if show:
            plt.show()
        plt.close()
        return self



    def plotOverheadPercentage(self, show=False, override=False):
        outputFile = self.buildBasePath() + "/overheadPercentage.png"
        if not override and os.path.isfile(outputFile):
            return self

        fig, ax = plt.subplots(ncols=1, figsize=self.defaultFigSize)

        for series in self.yValues:
            overhead: List[float] = []
            for m in series.measurements:
                overhead.append(
                    (m.tuples[int(self.config["Slots"]["MONITOR_GLOBAL_RUNTIME"])].value
                     / m.tuples[int(self.config["Slots"]["MONITOR_EXECUTE_JOB"])].value
                     - 1) * 100
                )

            ax.plot(
                np.array(self.xValues),
                np.array(overhead),
                label=series.label,
                marker="^",
                markersize=6)

        ax.set_title(self.testCase + "/" + self.version + "/" + self.subTestCase + "\n" + "Overhead", fontsize=18)
        ax.set_xlabel(self.xLabel)
        ax.set_ylabel("Overhead in %\n(Runtime / Worktime - 1) * 100")

        ax.set_xlim(self.xValues[0] - 10)
        ax.set_ylim(0)

        ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10, which="both")

        # ax.legend(loc="upper right")
        ax.legend(loc="best")

        print("Plot " + self.buildBasePath() + ": OverheadPercentage saved to: " + outputFile)
        plt.savefig(outputFile)
        if show:
            plt.show()
        plt.close()
        return self



    def plotAll(self, override=False):
        self.plotOverallRuntime(override=override)
        self.plotOverhead(override=override)
        self.plotOverheadPercentage(override=override)
        return self



    def plotDifferenceRuntime(self, other, show=False, override=False):
        outputFile = self.buildBasePath() + "/difference.png"
        if not override and os.path.isfile(outputFile):
            return self

        fig, ax = plt.subplots(ncols=1, figsize=self.defaultFigSize)
        if len(self.yValues) != len(other.yValues):
            raise ValueError("plotDifferenceRuntime: length of y values of the benchmarks does not match")
        for i in range(0, len(self.yValues)):
            overhead: List[float] = []
            for j in range(0, len(self.yValues[i].measurements)):
                m = self.yValues[i].measurements[j]
                n = other.yValues[i].measurements[j]
                overhead.append(
                    n.tuples[int(self.config["Slots"]["MONITOR_GLOBAL_RUNTIME"])].value
                    - m.tuples[int(other.config["Slots"]["MONITOR_GLOBAL_RUNTIME"])].value
                )

            ax.plot(
                np.array(self.xValues),
                np.array(overhead),
                label=self.yValues[i].label,
                marker="^",
                markersize=6)

        ax.set_title(self.testCase + "/" + self.version + "/" + self.subTestCase + "\n" + "vs "
                     + other.testCase + "/" + other.version + "/" + other.subTestCase, fontsize=18)
        ax.set_xlabel(self.xLabel)
        ax.set_ylabel("Difference in ms\n" + other.subTestCase + " - " + self.subTestCase)

        # ax.set_xlim(self.xValues[0] - 10)
        # ax.set_ylim(0)

        ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10, which="both")

        # ax.legend(loc="upper right")
        ax.legend(loc="best")

        print("Plot " + self.buildBasePath() + ": Differnce saved to: " + outputFile)
        plt.savefig(outputFile)
        if show:
            plt.show()
        plt.close()
        return self
