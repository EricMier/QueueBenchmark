from re import sub

import matplotlib.pyplot as plt
import csv
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FuncFormatter, LogLocator
import numpy as np
from Benchmark import Benchmark
import matplotlib
import string


class BenchmarkMeta:
    host: string
    name: string

    def __init__(self, path: string):
        pathSplit = path.split("/")
        self.name = pathSplit[len(pathSplit) - 1]
        self.host = pathSplit[len(pathSplit) - 2]




def buildBasePath(testCase, version, subTestCase):
    return "../output/" + testCase + "/" + subTestCase + "/" + version + "/" + subTestCase + "." + version


def parseBenchmark(testCase, version, subTestCase, suffix=""):
    csvLines = []

    if suffix:
        suffix = "." + suffix

    file = open(buildBasePath(testCase, version, subTestCase) + suffix + '.csv', 'r')
    for line in file:
        csvLines.append(line.replace("\n", ""))
    file.close()

    if not csvLines:
        return None

    result = Benchmark([], [], [], "")
    if csvLines[0].startswith("#"):
        del csvLines[0]
    # header with x values
    head = csvLines[0].split("\t")
    result.xAxis = head[0]
    del head[0]
    for x in head:
        result.xValues.append(float(x))

    # parse curves
    for x in range(1, len(csvLines)):
        arry = []
        line = csvLines[x].split("\t")
        # first value of line is the curve label
        result.curveLabel.append(line[0])
        del line[0]

        for y in line:
            arry.append(float(y))
        # remaining values are the y values
        result.yValues.append(arry)

    return result


def plotOverallRuntime(testCase, version, subTestCase, show=False):
    fig, ax = plt.subplots(ncols=1, figsize=(10, 5))

    bench = parseBenchmark(testCase, version, subTestCase)
    if bench is None:
        return

    # print(bench.operatorCntL)
    for x in range(0, len(bench.yValues)):
        ax.plot(np.array(bench.xValues), np.array(bench.yValues[x]), label=bench.curveLabel[x], marker="^", markersize=6)
        # print(bench.values[x])

    # ax.yaxis.set_major_locator(MultipleLocator(10000))
    # ax.yaxis.set_minor_locator(MultipleLocator(1000))
    # ax.xaxis.set_major_locator(MultipleLocator(10))
    # ax.xaxis.set_minor_locator(MultipleLocator(10))

    ax.set_xlim(1)
    ax.set_ylim(0)

    ax.set_title(testCase + "/" + version + "/" + subTestCase + "\n" + "Overall runtime", fontsize=18)
    # ax.set_xlabel("Number of Operators")
    ax.set_xlabel(bench.xAxis)
    ax.set_ylabel("Runtime in ms")

    ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10, which="both")

    # plt.xscale('logit')
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    # ax.legend(loc="upper right")
    ax.legend(loc="best")

    print("Plot " + buildBasePath(testCase, version, subTestCase) + ": OverallRuntime saved to: "
          + buildBasePath(testCase, version, subTestCase) + "_overallRuntime.png")
    plt.savefig(buildBasePath(testCase, version, subTestCase) + "_overallRuntime.png")
    if show:
        plt.show()
    plt.close()



def plotOverhead(testCase, version, subTestCase, show=False):
    bench = parseBenchmark(testCase, version, subTestCase)
    bench_opt = parseBenchmark(testCase, version, subTestCase, "opt")
    if bench is None:
        return
    if bench_opt is None:
        return

    overheadL = []
    for i in range(0, len(bench.yValues)):
        arry = []
        x = bench.yValues[i]
        y = bench_opt.yValues[i]
        for j in range(0, len(x)):
            # arry.append((x[j] / y[j] - 1) * 100)
            arry.append(x[j] - y[j])
        # print(arry)
        overheadL.append(arry)

    fig, ax = plt.subplots(figsize=(10, 5))

    for x in range(0, len(overheadL)):
        ax.plot(bench_opt.xValues, overheadL[x], label=bench.curveLabel[x], marker="^")
    ax.set_xlim((1))
    ax.set_ylim((0))

    ax.legend(loc="best")
    ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10)

    ax.set_title(testCase + "/" + version + "/" + subTestCase + "\n" + "Overhead of a Worker dequeuing jobs", fontsize=18)
    # ax.set_xlabel("Number of Operators")
    ax.set_xlabel(bench.xAxis)
    ax.set_ylabel("Overhead in ms")

    print("Plot " + buildBasePath(testCase, version, subTestCase) + ": Overhead saved to: "
          + buildBasePath(testCase, version, subTestCase) + "_overhead.png")
    plt.savefig(buildBasePath(testCase, version, subTestCase) + "_overhead.png")
    if show:
        plt.show()
    plt.close()



def plotOverheadPercentage(testCase, version, subTestCase, show=False):
    bench = parseBenchmark(testCase, version, subTestCase)
    bench_opt = parseBenchmark(testCase, version, subTestCase, "opt")
    if bench is None:
        return
    if bench_opt is None:
        return

    overheadL = []
    for i in range(0, len(bench.yValues)):
        arry = []
        x = bench.yValues[i]
        y = bench_opt.yValues[i]
        for j in range(0, len(x)):
            arry.append((x[j] / y[j] - 1) * 100)
            # arry.append(x[j] - y[j])
        # print(arry)
        overheadL.append(arry)

    fig, ax = plt.subplots(figsize=(10, 5))

    for x in range(0, len(overheadL)):
        ax.plot(bench_opt.xValues, overheadL[x], label=bench.curveLabel[x], marker="^")
    ax.set_xlim((1))
    ax.set_ylim((0))

    ax.legend(loc="best")
    ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10)

    ax.set_title(testCase + "/" + version + "/" + subTestCase + "\n" + "Overhead of a Worker dequeuing jobs (%)", fontsize=18)
    # ax.set_xlabel("Number of Operators")
    ax.set_xlabel(bench.xAxis)
    ax.set_ylabel("Overhead in % \n((Time / Optimum - 1) * 100)")

    print("Plot " + buildBasePath(testCase, version, subTestCase) + ": Overhead (percentage) saved to: " + buildBasePath(testCase, version, subTestCase) + "_overhead_percent.png")
    plt.savefig(buildBasePath(testCase, version, subTestCase) + "_overhead_percent.png")
    if show:
        plt.show()
    plt.close()


def plotNumaBench(path):
    meta = BenchmarkMeta(path)
    xFile = open(path + "/copy_sizes.csv", "r")
    xFileLines = []
    for line in xFile:
        xFileLines.append(line.replace("\n", ""))
    xFile.close()

    xValues = []
    for x in xFileLines[0].split("###"):
        if x == "":
            break
        xValues.append(int(x))

    yFile = open(path + "/results.csv", "r")
    yFileLines = []
    for line in yFile:
        if len(line) > 0:
            yFileLines.append(line.replace("\n", ""))
    yFile.close()

    yValues = []
    yLabels = []
    for idx, line in enumerate(yFileLines):
        if line == "":
            break
        tmp = line.split("###")
        yLabels.append(tmp[0])
        yValues.append([])
        del tmp[0]
        for testRun in tmp:
            if testRun == "":
                continue
            testRunValue = 0
            testRunValueCnt = 0
            for val in testRun.split(";"):
                if val != "":
                    testRunValue += int(val)
                    testRunValueCnt += 1
            testRunValue = testRunValue / testRunValueCnt
            yValues[idx].append(testRunValue)
        # //// del if no Values found
        if len(yValues[idx]) == 0:
            del yValues[idx]

    # print("xValues:")
    # print(len(xValues))
    # print("yValues:")
    # for y in yValues:
    #     print(len(y))

    ratioValues = []
    for idx in range(0, len(yValues[0])):
        local = yValues[0][idx]
        remote = 0
        for yWidth in range(1, len(yValues)):
            remote += yValues[yWidth][idx]
        remote = remote / (len(yValues) - 1)

        ratioValues.append(remote / local)


    pathSplit = path.split("/")

    fig, ax = plt.subplots(ncols=1, figsize=(14, 8))

    plt.figtext(0.002, 0.02, meta.host, rotation=90)

    for idx, vec in enumerate(yValues):
        ax.plot(np.array(xValues), np.array(vec), label=yLabels[idx], marker=".", linewidth=0, markersize=2)

    ax.set_title(meta.name + "\nCopying 64MB from node to node")
    ax.set_xlabel("CopySize [kB]")
    ax.set_ylabel("Runtime [us]", color="tab:blue")
    ax.set_xlim(0, xValues[len(xValues) - 1] +256)
    plt.xticks(rotation=45)


    # ax.yaxis.set_major_locator(MultipleLocator(2000))
    # ax.yaxis.set_minor_locator(MultipleLocator(1000))
    ax.xaxis.set_major_locator(MultipleLocator(2048))
    ax.xaxis.set_minor_locator(MultipleLocator(512))
    ax.grid(linestyle=":", linewidth=0.5, color='.25', zorder=-10, which="both")

    ax.legend(loc="upper left")

    # //// log scale for x axis
    # ax.set_xscale("log", basex=2)
    # ax.xaxis.set_major_locator(LogLocator(base=2, numticks=20))
    # ax.xaxis.set_minor_locator(LogLocator(base=2, numticks=40))

    ax2 = ax.twinx()
    ax2.set_ylabel("Local:Remote ratio", color="tab:red")
    ax2.plot(np.array(xValues), np.array(ratioValues), label="local:remote-ratio", marker=",", linewidth=0.6, markersize=0, color="tab:red")

    ax2.legend(loc="upper right")
    ax2.set_ylim(bottom=0, top=2)

    # plt.show()
    plt.savefig(path + "/" + meta.name + ".png")
    plt.close()

