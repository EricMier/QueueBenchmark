from Benchmark import Benchmark2
from linePlot import LinePlot
from plotFunctions import *
from plotFunctionsV2 import *
import os
from typing import NamedTuple, List

testCase = "NumaBenchmark"
# versionL = ["v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10"]
versionL = []
for i in range(1, 20):
    versionL.append("v" + str(i))

# subTestCase = "Partitioning_Benchmark_logical"
subTestCaseL = []
subTestCaseL.append("Partitioning_Benchmark_physical")
subTestCaseL.append("Partitioning_Benchmark_physical_2")
subTestCaseL.append("Partitioning_Benchmark_physical_3")
subTestCaseL.append("Partitioning_Benchmark_logical")
subTestCaseL.append("Allocation_with_Policy")
subTestCaseL.append("Allocation_with_Policy_2")
subTestCaseL.append("Allocation_without_Policy")
subTestCaseL.append("IOBenchmark")
# subTestCaseL = ["Partitioning_Benchmark_physical", "Partitioning_Benchmark_physical_2",
#                 "Partitioning_Benchmark_physical_3", "Partitioning_Benchmark_logical",
#                 "Allocation_with_Policy","Allocation_with_Policy_2", "Allocation_without_Policy", "IOBenchmark"]

override = False
# subTestCase = "worker_28"
# subTestCase = "worker_28_2"

for subTestCase in subTestCaseL:
    for version in versionL:
        inputFile = buildBasePath(testCase, version, subTestCase) + ".csv"
        # print(inputFile)
        outputFile = buildBasePath(testCase, version, subTestCase) + "_overallRuntime.png"
        if os.path.isfile(inputFile):
            if override or not os.path.isfile(outputFile):
                plotOverallRuntime(testCase, version, subTestCase)
                plotOverhead(testCase, version, subTestCase)
                plotOverheadPercentage(testCase, version, subTestCase)
                print()
            # else:
            #     print(outputFile + " already exist.")
        # else:
            # print(inputFile + " not found.")

#
# testCase = "mainAndHyperThreads_extended"
#
# plotOverallRuntime(testCase)
# plotOverhead(testCase)
# plotOverheadPercentage(testCase)

# Benchmark2("../output/NumaBenchmark/Benchmark_with_LibNuma/v1").plotAll()
# Benchmark2("../output/NumaBenchmark/Benchmark_with_LibNuma/v1").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/NumaBenchmark/Benchmark_without_LibNuma/v1").plotAll()
# )

# Benchmark2("../output/NumaBenchmark/Benchmark_with_LibNuma/v2").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/NumaBenchmark/Benchmark_without_LibNuma/v2").plotAll()
# )
# Benchmark2("../output/NumaBenchmark/Benchmark_with_LibNuma/v3").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/NumaBenchmark/Benchmark_without_LibNuma/v3").plotAll()
# )
# Benchmark2("../output/NumaBenchmark/Benchmark_with_LibNuma/v4").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/NumaBenchmark/Benchmark_without_LibNuma/v4").plotAll()
# )
# # Benchmark2("../output/NumaBenchmark/Benchmark_with_LibNuma/v6").plotAll().plotDifferenceRuntime(
# # )
# Benchmark2("../output/NumaBenchmark/Benchmark_without_LibNuma/v6").plotAll()
#
# Benchmark2("../output/NumaBenchmark/Benchmark_with_LibNuma/v9").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/NumaBenchmark/Benchmark_without_LibNuma/v9").plotAll()
# )


# plotNumaBench("../../numaBench/output/haecboehm/multiple_concat")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/multiple_concat")
# # plotNumaBench("../../numaBench/output/haecboehm/multiple_1")
# # plotNumaBench("../../numaBench/output/haecboehmthesecond/multiple_1")
#
# plotNumaBench("../../numaBench/output/haecboehm/fastCopy_streamLoad_streamStore")
# plotNumaBench("../../numaBench/output/haecboehm/fastCopy_casualLoad_streamStore")
# plotNumaBench("../../numaBench/output/haecboehm/fastCopy_streamLoad_casualStore")
# plotNumaBench("../../numaBench/output/haecboehm/fastCopy_casualLoad_casualStore")
#
# plotNumaBench("../../numaBench/output/haecboehmthesecond/fastCopy_streamLoad_streamStore")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/fastCopy_casualLoad_streamStore")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/fastCopy_streamLoad_casualStore")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/fastCopy_casualLoad_casualStore")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/pufferTest")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/pufferTestv2")

# plotNumaBench("../../numaBench/output/haecboehmthesecond/memcpy_w_init")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/memcpy_wo_init")
# plotNumaBench("../../numaBench/output/haecboehm/memcpy_w_init")
# plotNumaBench("../../numaBench/output/haecboehm/memcpy_wo_init")

# // override title when ploting
# plotNumaBench("../../numaBench/output/haecboehmthesecond/memcpy_w_init_2048")
# plotNumaBench("../../numaBench/output/haecboehmthesecond/memcpy_wo_init_2048")



# exit()


# Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_with_LibNuma/v1").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_without_LibNuma/v1").plotAll()
# )
# Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_with_LibNuma/v5").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_without_LibNuma/v5").plotAll()
# )
# Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_with_LibNuma/v6").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_without_LibNuma/v6").plotAll()
# )
# Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_with_LibNuma/v9").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_without_LibNuma/v9").plotAll()
# )
# Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_with_LibNuma/v10").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_without_LibNuma/v10").plotAll()
# )
# Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_with_LibNuma/v20").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_without_LibNuma/v20").plotAll()
# )
# Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_with_LibNuma/v11").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehmthesecond/NumaBenchmark/Benchmark_without_LibNuma/v11").plotAll()
# )
# exit()



# Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_with_LibNuma/v2").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_without_LibNuma/v2").plotAll()
# )
# Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_with_LibNuma/v3").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_without_LibNuma/v3").plotAll()
# )
# Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_with_LibNuma/v4").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_without_LibNuma/v4").plotAll()
# )
# Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_with_LibNuma/v5").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_without_LibNuma/v5").plotAll()
# )
# Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_with_LibNuma/v6").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_without_LibNuma/v6").plotAll()
# )
# Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_with_LibNuma/v7").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/haecboehm/NumaBenchmark/Benchmark_without_LibNuma/v7").plotAll()
# )
# Benchmark2("../output/NumaBenchmark/Allocation_with_Policy/v11").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/NumaBenchmark/Allocation_without_Policy/v11").plotAll()
# )





# Benchmark2("../output/erisboehm/NumaBenchmark/Benchmark_with_LibNuma/v7").plotAll().plotDifferenceRuntime(
#     Benchmark2("../output/erisboehm/NumaBenchmark/Benchmark_without_LibNuma/v7").plotAll()
# )
def plot1():
    line: LinePlot = LinePlot()
    line.setTitle("Runtime of different queueing methods")
    line.setXLabel("Number of Partitions")
    line.setYLabel("Runtime in ms")

    partCountF = open("../output/haecboehmthesecond/queue_with_pointer/partCount.csv")
    runtimeF = open("../output/haecboehmthesecond/queue_with_pointer/runtime.csv")

    for p, r in zip(partCountF, runtimeF):
        xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]]   [:112]
        yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]]   [:112]
        line.addLine(xValues, yValues, label="Pointers", marker=".")


    partCountF = open("../output/haecboehmthesecond/queue_with_objects/partCount.csv")
    runtimeF = open("../output/haecboehmthesecond/queue_with_objects/runtime.csv")

    for p, r in zip(partCountF, runtimeF):
        xValues = [float(i) for i in [s for s in p.replace("\n", "").split(";") if s is not ""]][:112]
        yValues = [float(i)/ 1000 / 1000 for i in [s for s in r.replace("\n", "").split(";") if s is not ""]][:112]
        line.addLine(xValues, yValues, label="Objects", marker=".")


    partCountF = open("../output/haecboehmthesecond/queue_with_pointer/v2/partCount.csv")
    runtimeF = open("../output/haecboehmthesecond/queue_with_pointer/v2/runtime.csv")

    serialTime=0

    for p, r in zip(partCountF, runtimeF):
        xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]][:112]
        yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]][:112]
        line.addLine(xValues, yValues, label="Pointers (optimized)", marker=".")
        serialTime=yValues[0]

    xValues = [float(f) for f in list(range(1,113))]
    yValues = [float(serialTime) / i for i in list(range(1,113))]
    line.addLine(xValues, yValues, label="Optimal Scaling", linewidth=1, marker="", color="r")

    line.plot("../output/haecboehmthesecond/queue_with_plots/plot3.png")
    line.show()


# plot1()


def plot2():
    line: LinePlot = LinePlot()
    line.setTitle("Hyperthreading vs MainThreads only")
    line.setXLabel("Number of Partitions")
    line.setYLabel("Runtime in ms")


    partCountF = open("../output/haecboehmthesecond/queue_with_pointer/v3/partCount.csv")
    runtimeF = open("../output/haecboehmthesecond/queue_with_pointer/v3/runtime.csv")

    for p, r in zip(partCountF, runtimeF):
        xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]]   [:112]
        yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]]   [:112]
        line.addLine(xValues, yValues, label="MainThreads only", marker=".")

    partCountF = open("../output/haecboehmthesecond/queue_with_pointer/v2/partCount.csv")
    runtimeF = open("../output/haecboehmthesecond/queue_with_pointer/v2/runtime.csv")

    serialTime=0

    for p, r in zip(partCountF, runtimeF):
        xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]][:112]
        yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]][:112]
        line.addLine(xValues, yValues, label="Hyperthreads", marker=".")
        serialTime=yValues[0]

    xValues = [float(f) for f in list(range(1,113))]
    yValues = [float(serialTime) / i for i in list(range(1,113))]
    line.addLine(xValues, yValues, label="Optimal Scaling", linewidth=1, marker="", color="r")

    line.plot("../output/haecboehmthesecond/queue_with_plots/hyper_vs_main_threading.png")
    line.show()


# plot2()

def plotSingleLine(servername, benchmark, version, title, curvename="Pointer-Queuing", plotname="plot"):
    # serverName = "haecboehmthesecond"
    # serverName = "haecboehm"
    # benchmark = "queue_with_pointer"
    # benchmark = "queue_with_objects"
    # legend_curveName = "Pointers"
    # version = "v9_120kk"
    # plotName = "object_vs_pointer_enqueuing_" + serverName + "_" + "HT_" + version
    # plotName = "scaling_grouped_aggregation_" + serverName + "_" + "HT_" + version


    line: LinePlot = LinePlot()
    line.setTitle(title)
    line.setXLabel("Number of Partitions")
    line.setYLabel("Runtime in ms")

    print("Plot '" + plotname + "' from ../output/" + servername + "/" + benchmark + "/" + version + "")



    partCountF = open("../output/" + servername + "/" + benchmark + "/" + version + "/partCount.csv")
    runtimeF = open("../output/" + servername + "/" + benchmark + "/" + version + "/runtime.csv")

    # Ausgangswert für Optimalkurve
    referenceTime=0

    # für rede testreihe im file
    for p, r in zip(partCountF, runtimeF):
        xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]]
        yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]]
        line.addLine(xValues, yValues, label=curvename, marker=".")
        referenceTime=yValues[0]
        referenceX=xValues[0]
        refyValues = [float(referenceTime) * referenceX / i for i in xValues]
        line.addLine(xValues, refyValues, label="Optimal Scaling for " + curvename, linewidth=1, marker="", color="r")


    # partCountF = open("../output/" + serverName + "/queue_with_objects/" + version + "/partCount.csv")
    # runtimeF = open("../output/" + serverName + "/queue_with_objects/" + version + "/runtime.csv")
    #
    # for p, r in zip(partCountF, runtimeF):
    #     xValues = [float(i) for i in [s for s in p.replace("\n", "").split(";") if s is not ""]][:112]
    #     yValues = [float(i)/ 1000 / 1000 for i in [s for s in r.replace("\n", "").split(";") if s is not ""]][:112]
    #     line.addLine(xValues, yValues, label="Objects", marker=".")

    # xValues = [float(f) for f in list(range(1,113))]
    # yValues = [float(serialTime) / i for i in list(range(1,113))]
    # line.addLine(xValues, yValues, label="Optimal Scaling", linewidth=1, marker="", color="r")

    line.plot("../output/" + servername + "/" + benchmark + "/" + version + "/" + plotname + ".png")
    # line.show()


def plotDoubleLine(servername, benchmark, benchmark2, version, version2, title, curvename="Pointer-Queuing", curvename2="Object-Queuing", plotname="plot"):
    # serverName = "haecboehmthesecond"
    # serverName = "haecboehm"
    # benchmark = "queue_with_pointer"
    # benchmark = "queue_with_objects"
    # legend_curveName = "Pointers"
    # version = "v9_120kk"
    # plotName = "object_vs_pointer_enqueuing_" + serverName + "_" + "HT_" + version
    # plotName = "scaling_grouped_aggregation_" + serverName + "_" + "HT_" + version


    line: LinePlot = LinePlot()
    line.setTitle(title)
    line.setXLabel("Number of Partitions")
    line.setYLabel("Runtime in ms")

    print("Plot '" + plotname + "' from ../output/" + servername + "/" + benchmark + "/" + version + "")



    partCountF = open("../output/" + servername + "/" + benchmark2 + "/" + version2 + "/partCount.csv")
    runtimeF = open("../output/" + servername + "/" + benchmark2 + "/" + version2 + "/runtime.csv")

    for p, r in zip(partCountF, runtimeF):
        xValues = [float(i) for i in [s for s in p.replace("\n", "").split(";") if s is not ""]][:112]
        yValues = [float(i)/ 1000 / 1000 for i in [s for s in r.replace("\n", "").split(";") if s is not ""]][:112]
        line.addLine(xValues, yValues, label=curvename2, marker=".", color="C1")

    partCountF = open("../output/" + servername + "/" + benchmark + "/" + version + "/partCount.csv")
    runtimeF = open("../output/" + servername + "/" + benchmark + "/" + version + "/runtime.csv")

    # Ausgangswert für Optimalkurve
    referenceTime=0

    # für rede testreihe im file
    for p, r in zip(partCountF, runtimeF):
        xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]]
        yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]]
        line.addLine(xValues, yValues, label=curvename, marker=".", color="C0")
        referenceTime=yValues[0]
        referenceX=xValues[0]
        refyValues = [float(referenceTime) * referenceX / i for i in xValues]
        line.addLine(xValues, refyValues, label="Optimal Scaling for " + curvename, linewidth=1, marker="", color="r")


    line.plot("../output/" + servername + "/" + benchmark + "/" + version + "/" + plotname + ".png")



def plotLineExtra(servername, benchmark, version, title, curvename="Pointer-Queuing", plotname="plot"):
    line: LinePlot = LinePlot()
    line.setTitle(title)
    line.setXLabel("Number of Partitions")
    line.setYLabel("Runtime in ms")

    print("Plot '" + plotname + "' from ../output/" + servername + "/" + benchmark + "/" + version + "")


    partCountF = open("../output/" + servername + "/" + benchmark + "/" + version + "/partCount.csv")
    runtimeF = open("../output/" + servername + "/" + benchmark + "/" + version + "/runtime.csv")

    # Ausgangswert für Optimalkurve
    referenceTime=0

    # für rede testreihe im file
    # for p, r in zip(partCountF, runtimeF):
    #     xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]]
    #     yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]]
    #     line.addLine(xValues, yValues, label=curvename, marker=".", color="C0")
        # referenceTime=yValues[0]
        # referenceX=xValues[0]
        # refyValues = [float(referenceTime) * referenceX / i for i in xValues]
        # line.addLine(xValues, refyValues, label="Optimal Scaling for " + curvename, linewidth=1, marker="", color="r")

    wExecuteGlobalJob = open("../output/" + servername + "/" + benchmark + "/" + version + "/wExecuteGlobalJob.csv")
    wExecuteJob = open("../output/" + servername + "/" + benchmark + "/" + version + "/wExecuteJob.csv")
    wDequeuingJob = open("../output/" + servername + "/" + benchmark + "/" + version + "/wDequeuingJob.csv")
    cProcessGlobalBuffer = open("../output/" + servername + "/" + benchmark + "/" + version + "/cProcessGlobalBuffer.csv")
    cProcessBuffer = open("../output/" + servername + "/" + benchmark + "/" + version + "/cProcessBuffer.csv")

    for part, wExeGlo, wExe, cProGloBuf, cProBuf, wDeq in zip(partCountF, wExecuteGlobalJob, wExecuteJob, cProcessGlobalBuffer, cProcessBuffer, wDequeuingJob):
        xValues = [float(f) for f in [s for s in part.replace("\n", "").split(";") if s is not ""]]
        y_wExeGlo = [float(f)/ 1000 / 1000 for f in [s for s in wExeGlo.replace("\n", "").split(";") if s is not ""]]
        y_wExe = [float(f)/ 1000 / 1000 for f in [s for s in wExe.replace("\n", "").split(";") if s is not ""]]
        y_wDeq = [float(f)/ 1000 / 1000 for f in [s for s in wDeq.replace("\n", "").split(";") if s is not ""]]
        y_cProGloBuf = [float(f)/ 1000 / 1000 for f in [s for s in cProGloBuf.replace("\n", "").split(";") if s is not ""]]
        y_cProBuf = [float(f)/ 1000 / 1000 for f in [s for s in cProBuf.replace("\n", "").split(";") if s is not ""]]
        line.addStack(xValues, y_cProBuf,  y_cProGloBuf,y_wExe, y_wExeGlo, y_wDeq,
                      labels=["Coord. Lokal Buffer Processing Time",
                              "Coord. Global Buffer Processing Time",
                              "Worker Execution Time of Lokal Operators",
                              "Worker Execution Time of Global Operators",
                              "Worker Idle/Dequeuing Jobs"])
    # line.ax.set_ylim(top=300)
    line.legend_loc = "upper left"

    # for part, wExeGlo, wExe in zip(partCountF, wExecuteGlobalJob, wExecuteJob):
    #     xValues = [float(f) for f in [s for s in part.replace("\n", "").split(";") if s is not ""]]
    #     y_wExeGlo = [float(f)/ 1000 / 1000 for f in [s for s in wExeGlo.replace("\n", "").split(";") if s is not ""]]
    #     y_wExe = [float(f)/ 1000 / 1000 for f in [s for s in wExe.replace("\n", "").split(";") if s is not ""]]
    #     line.addStack(xValues, y_wExeGlo, y_wExe, labels=["wGlobal", "wLokal"])
    #
    # for part, cProGloBuf, cProBuf in zip(partCountF, cProcessGlobalBuffer, cProcessBuffer):
    #     xValues = [float(f) for f in [s for s in part.replace("\n", "").split(";") if s is not ""]]
    #     y_wExeGlo = [float(f)/ 1000 / 1000 for f in [s for s in cProGloBuf.replace("\n", "").split(";") if s is not ""]]
    #     y_wExe = [float(f)/ 1000 / 1000 for f in [s for s in cProBuf.replace("\n", "").split(";") if s is not ""]]
    #     line.addStack(xValues, y_wExeGlo, y_wExe, labels=["cGlobal", "cLokal"])



    # for p, r in zip(partCountF, runtimeF):
    #     xValues = [float(i) for i in [s for s in p.replace("\n", "").split(";") if s is not ""]][:112]
    #     yValues = [float(i)/ 1000 / 1000 for i in [s for s in r.replace("\n", "").split(";") if s is not ""]][:112]
    #     line.addLine(xValues, yValues, label="GlobalExecutionTime", marker=".", color="C2")

    line.plot("../output/" + servername + "/" + benchmark + "/" + version + "/" + plotname + ".png")



def plotMultipleLine(servername: List[str], benchmark: List[str], version: List[str], title, curvename: List[str], plotname="plot"):
    line: LinePlot = LinePlot()
    line.setTitle(title)
    line.setXLabel("Number of Partitions")
    line.setYLabel("Runtime in ms")

    print("Plot '" + plotname + "' from ../output/" + servername[0] + "/" + benchmark[0] + "/" + version[0] + "")


    for server, bench, ver, curve in zip(servername, benchmark, version, curvename):
        partCountF = open("../output/" + server + "/" + bench + "/" + ver + "/partCount.csv")
        runtimeF = open("../output/" + server + "/" + bench + "/" + ver + "/runtime.csv")

        # Ausgangswert für Optimalkurve
        referenceTime=0

        # für rede testreihe im file
        for p, r in zip(partCountF, runtimeF):
            xValues = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]]
            yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]]
            line.addLine(xValues, yValues, label=curve, marker=".")

    line.plot("../output/" + servername[0] + "/" + benchmark[0] + "/" + version[0] + "/" + plotname + ".png")



def plotSingleLineWorkerBench(servername, benchmark, version, title, curvename=" Partitions", plotname="plot"):
    # serverName = "haecboehmthesecond"
    # serverName = "haecboehm"
    # benchmark = "queue_with_pointer"
    # benchmark = "queue_with_objects"
    # legend_curveName = "Pointers"
    # version = "v9_120kk"
    # plotName = "object_vs_pointer_enqueuing_" + serverName + "_" + "HT_" + version
    # plotName = "scaling_grouped_aggregation_" + serverName + "_" + "HT_" + version


    line: LinePlot = LinePlot()
    line.setTitle(title)
    line.setXLabel("Number of Workers")
    line.setYLabel("Runtime in ms")

    print("Plot '" + plotname + "' from ../output/" + servername + "/" + benchmark + "/" + version + "")



    partCountF = open("../output/" + servername + "/" + benchmark + "/" + version + "/partCount.csv")
    workerCountF = open("../output/" + servername + "/" + benchmark + "/" + version + "/workerCount.csv")
    runtimeF = open("../output/" + servername + "/" + benchmark + "/" + version + "/runtime.csv")

    # Ausgangswert für Optimalkurve
    referenceTime=0

    # für rede testreihe im file
    for p, r, w in zip(partCountF, runtimeF, workerCountF):
        parts = [float(f) for f in [s for s in p.replace("\n", "").split(";") if s is not ""]]
        if len(parts) == 0:
            continue
        xValues = [float(f) for f in [s for s in w.replace("\n", "").split(";") if s is not ""]]
        yValues = [float(f)/ 1000 / 1000 for f in [s for s in r.replace("\n", "").split(";") if s is not ""]]
        line.addLine(xValues, yValues, label=str(parts[0]) + curvename, marker=".")


    # partCountF = open("../output/" + serverName + "/queue_with_objects/" + version + "/partCount.csv")
    # runtimeF = open("../output/" + serverName + "/queue_with_objects/" + version + "/runtime.csv")
    #
    # for p, r in zip(partCountF, runtimeF):
    #     xValues = [float(i) for i in [s for s in p.replace("\n", "").split(";") if s is not ""]][:112]
    #     yValues = [float(i)/ 1000 / 1000 for i in [s for s in r.replace("\n", "").split(";") if s is not ""]][:112]
    #     line.addLine(xValues, yValues, label="Objects", marker=".")

    # xValues = [float(f) for f in list(range(1,113))]
    # yValues = [float(serialTime) / i for i in list(range(1,113))]
    # line.addLine(xValues, yValues, label="Optimal Scaling", linewidth=1, marker="", color="r")

    line.plot("../output/" + servername + "/" + benchmark + "/" + version + "/" + plotname + ".png")
    # line.show()



















# plotDoubleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", benchmark2="queue_with_objects", version="v6",
#                title="Scaling of Grouped Aggregation (6M tuples, MT only)", plotname="scaling_grouped_aggregation_haecboehmthesecond_PointerVsObject_MTonly_v6_6kk")



# Local Benchmark
# plotSingleLine(servername="C940", benchmark="queue_with_pointer", version="v10_groupBySuppkey",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_C940_HT_v10_groupBySuppkey")
# plotLineExtra(servername="C940", benchmark="queue_with_pointer", version="v10_groupBySuppkey",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v10_groupBySuppkey_avg_execution_time")

# # v7
# plotDoubleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", benchmark2="queue_with_objects", version="v7",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_PointerVsObject_HT_v7_6kk")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v7",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v7_avg_execution_time")
#
# # v8
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v8",
#                title="Scaling of Grouped Aggregation (60M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v8_60kk")
plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v8",
               title="Time segmentation of Query Execution (6M tuples)", plotname="v8_avg_execution_time")
#
# # v9_120kk
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v9_120kk",
#                title="Scaling of Grouped Aggregation (120M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v9_120kk")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v9_120kk",
#                title="Time segmentation of Query Execution (120M tuples)", plotname="v9_120kk_avg_execution_time")
#
# # v10_groupBySuppkey
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v10_groupBySuppkey",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v10_groupBySuppkey")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v10_groupBySuppkey",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v10_groupBySuppkey_avg_execution_time")
#
# # v11_fourTimesPartitions
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v11_fourTimesPartitions",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v11_fourTimesPartitions")

# v12_groupBySuppkey
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v12_groupBySuppkey",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v12_groupBySuppkey")


# v10 vs v13
# plotDoubleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", benchmark2="queue_with_pointer",
#                version="v10_groupBySuppkey",version2="v13_groupBySuppkey_MTonly",
#                curvename="Pointer-Queuing w/ HyperThreads",curvename2="Pointer-Queuing w/ MainThreads only",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_v10HT_vs_v13MT_groupBySuppkey")


# v13_groupBySuppkey_MTonly
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v13_groupBySuppkey_MTonly",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_MT_v13_groupBySuppkey")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v13_groupBySuppkey_MTonly",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v13_groupBySuppkey_MTonly_avg_execution_time")

# v14_groupBySuppkey_hashGroupBy
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v14_groupBySuppkey_hashGroupBy",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v14_groupBySuppkey")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v14_groupBySuppkey_hashGroupBy",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v14_groupBySuppkey_HT_avg_execution_time")

# v15_groupBySuppkey_hashmap_initSize_10percent
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v15_groupBySuppkey_hashmap_initSize_10percent",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v15_groupBySuppkey")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v15_groupBySuppkey_hashmap_initSize_10percent",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v15_groupBySuppkey_HT_avg_execution_time")


# # v16_groupBySuppkey_lockPart
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v16_groupBySuppkey_lockPart",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v16_groupBySuppkey_lockPart")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v16_groupBySuppkey_lockPart",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v16_groupBySuppkey_lockPart_HT_avg_execution_time")
#
# # v17_groupByPartkey_lockPart
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v17_groupByPartkey_lockPart",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v17_groupByPartkey_lockPart")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v17_groupByPartkey_lockPart",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v17_groupByPartkey_lockPart_HT_avg_execution_time")
#
# # v18_groupBySuppkey_onlyOneWorkerPerNode
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v18_groupBySuppkey_onlyOneWorkerPerNode",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v18_groupBySuppkey_onlyOneWorkerPerNode")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v18_groupBySuppkey_onlyOneWorkerPerNode",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v18_groupBySuppkey_onlyOneWorkerPerNode_HT_avg_execution_time")

# # v19_groupBySuppkey_sevenWorkerPerNode
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v19_groupBySuppkey_sevenWorkerPerNode",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_MR_v19_groupBySuppkey_sevenWorkerPerNode")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v19_groupBySuppkey_sevenWorkerPerNode",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v19_groupBySuppkey_sevenWorkerPerNode_MT_avg_execution_time")


# plotMultipleLine(servername=["haecboehmthesecond","haecboehmthesecond","haecboehmthesecond","haecboehmthesecond"],
#                  benchmark=["queue_with_pointer","queue_with_pointer","queue_with_pointer","queue_with_pointer"],
#                  version=["v10_groupBySuppkey","v13_groupBySuppkey_MTonly","v18_groupBySuppkey_onlyOneWorkerPerNode","v19_groupBySuppkey_sevenWorkerPerNode"],
#                  curvename=["Pointer-Queuing w/ HyperThreads", "Pointer-Queuing w/ MainThreads only", "Pointer-Queuing w/ 1 Worker per Node", "Pointer-Queuing w/ 7 Worker per Node"],
#                  title="Scaling of Grouped Aggregation (6M tuples)",
#                  plotname="scaling_grouped_aggregation_haecboehmthesecond_v10HT_vs_v13MT_vs_v18ST__vs_v197T_groupBySuppkey")


# v20_groupBySuppkey_variableWorkerCnt
# plotSingleLineWorkerBench(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v20_groupBySuppkey_variableWorkerCnt",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_MR_v20_groupBySuppkey_variableWorkerCnt")


# # v23_groupBySuppkey_fixedCpuFrequency_fixedMeasurement
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v23_groupBySuppkey_fixedCpuFrequency_fixedMeasurement",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v23_groupBySuppkey_fixedCpuFrequency_fixedMeasurement")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v23_groupBySuppkey_fixedCpuFrequency_fixedMeasurement",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v23_groupBySuppkey_fixedCpuFrequency_fixedMeasurement_HT_avg_execution_time")


# # v24_groupBySuppkey_fixedCpuFrequency_fixedMeasurement_unorderedMapInAggMerge
# plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v24_groupBySuppkey_fixedCpuFrequency_fixedMeasurement_unorderedMapInAggMerge",
#                title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v24_groupBySuppkey_fixedCpuFrequency_fixedMeasurement_unorderedMapInAggMerge")
# plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v24_groupBySuppkey_fixedCpuFrequency_fixedMeasurement_unorderedMapInAggMerge",
#                title="Time segmentation of Query Execution (6M tuples)", plotname="v24_groupBySuppkey_fixedCpuFrequency_fixedMeasurement_unorderedMapInAggMerge_HT_avg_execution_time")


# # v25_groupBySuppkey_fixedMeasurement_unorderedMapInAggMerge
plotSingleLine(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v25_groupBySuppkey_fixedMeasurement_unorderedMapInAggMerge",
               title="Scaling of Grouped Aggregation (6M tuples)", plotname="scaling_grouped_aggregation_haecboehmthesecond_HT_v25_groupBySuppkey_fixedMeasurement_unorderedMapInAggMerge")
plotLineExtra(servername="haecboehmthesecond", benchmark="queue_with_pointer", version="v25_groupBySuppkey_fixedMeasurement_unorderedMapInAggMerge",
               title="Time segmentation of Query Execution (6M tuples)", plotname="v25_groupBySuppkey_fixedMeasurement_unorderedMapInAggMerge_HT_avg_execution_time")

