import os
from typing import List

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np


def barPlot(folder, file, name, outputFileName):
    file = open(folder + "/" + file)
    xValues = []
    yValues = []
    errorMin = []
    errorMax = []
    scaleFactor = 1
    for line in file:
        rawValues = line.replace("\n", "").split(";")
        intValues: List[int] = []
        xValues.append(rawValues[0])
        average = 0
        cnt = 0
        # delete empty values
        while rawValues[-1] == '':
            rawValues = rawValues[0:-1]
        # calc average
        for v in rawValues[1:]:
            intValues.append(int(v))
        for val in intValues:
            average += int(val)
            cnt += 1
        average /= cnt
        # append value
        yValues.append(float(average) / 1000 / 1000)  # convert from ns into ms
        # fetch error
        ymin = min(intValues)
        ymax = max(intValues)
        errorMin.append((float(average) - float(ymin)) / 1000 / 1000)
        errorMax.append((float(ymax) - float(average)) / 1000 / 1000)

    fig, ax = plt.subplots(figsize=(10 * scaleFactor, 5 * scaleFactor))
    ax.set_xlabel("Vector type<length>", fontsize=14 * scaleFactor)
    ax.set_ylabel("Runtime in ms", fontsize=14 * scaleFactor)
    ax.grid(linestyle="--", linewidth=0.5 * scaleFactor, color='.25', zorder=-10, which="both", axis="y")

    plt.xticks(fontsize=9 * scaleFactor)
    plt.yticks(fontsize=9 * scaleFactor)
    # ax.xaxis.set_major_locator(MultipleLocator(14 * 2))
    # ax.xaxis.set_minor_locator(MultipleLocator(14))
    ax.set_title("VirtualVector Benchmark / compound query (" + name + ")")
    # ax.bar(np.array(xValues), np.array(yValues), yerr=np.array([errorMin, errorMax]), ecolor='tab:red')
    ax.bar(np.array(xValues), np.array(yValues))
    ax.errorbar(np.array(xValues), np.array(yValues), yerr=np.array([errorMin, errorMax]), ecolor=(0, 0, 0, 0.7), fmt='None', elinewidth=10)
    # print("xValues: " + str(xValues))
    # print("yValues: " + str(yValues))
    # print("ErrorMin: " + str(errorMin))
    # print("ErrorMax: " + str(errorMax))
    plt.savefig(folder + "/" + outputFileName + ".png")
    plt.show()
    # ### end of barPlot() ### #


folderPath = "D:/root/workspace/clion/MorphStore/Engine/output/compound_query"

barPlot(folderPath, "scalar.csv", "Scalar<64>", "Scalar_64")
barPlot(folderPath, "sse_128.csv", "sse<128>", "SSE_128")
barPlot(folderPath, "avx2.csv", "avx2<256>", "AVX2_256")
barPlot(folderPath, "avx512.csv", "avx512<512>", "AVX512_512")
