import configparser
from re import sub

import matplotlib.pyplot as plt
import csv
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FuncFormatter
import numpy as np
from Benchmark import Benchmark


def buildBasePath2(testCase, version, subTestCase):
    return "../output/" + testCase + "/" + subTestCase + "/" + version + "/" + subTestCase + "." + version


def parseBenchmark2(testCase, version, subTestCase, suffix=""):
    csvLines = []

    if suffix:
        suffix = "." + suffix

    file = open(buildBasePath2(testCase, version, subTestCase) + suffix + '.csv', 'r')
    for line in file:
        csvLines.append(line.replace("\n", ""))
    file.close()

    if not csvLines:
        return None

    result = Benchmark([], [], [], "")
    if csvLines[0].startswith("#"):
        del csvLines[0]
    # header with x values
    head = csvLines[0].split("\t")
    result.xAxis = head[0]
    del head[0]
    for x in head:
        result.xValues.append(float(x))

    # parse curves
    for x in range(1, len(csvLines)):
        arry = []
        line = csvLines[x].split("\t")
        # first value of line is the curve label
        result.curveLabel.append(line[0])
        del line[0]

        for y in line:
            arry.append(float(y))
        # remaining values are the y values
        result.yValues.append(arry)

    return result


def plotOverallRuntime2(testCase, version, subTestCase, show=False):
    fig, ax = plt.subplots(ncols=1, figsize=(10, 5))

    bench = parseBenchmark2(testCase, version, subTestCase)
    if bench is None:
        return

    # print(bench.operatorCntL)
    for x in range(0, len(bench.yValues)):
        ax.plot(np.array(bench.xValues), np.array(bench.yValues[x]), label=bench.curveLabel[x], marker="^", markersize=6)
        # print(bench.values[x])

    # ax.yaxis.set_major_locator(MultipleLocator(10000))
    # ax.yaxis.set_minor_locator(MultipleLocator(1000))
    # ax.xaxis.set_major_locator(MultipleLocator(10))
    # ax.xaxis.set_minor_locator(MultipleLocator(10))

    ax.set_xlim(1)
    ax.set_ylim(0)

    ax.set_title(testCase + "/" + version + "/" + subTestCase + "\n" + "Overall runtime", fontsize=18)
    # ax.set_xlabel("Number of Operators")
    ax.set_xlabel(bench.xAxis)
    ax.set_ylabel("Runtime in ms")

    ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10, which="both")

    # plt.xscale('logit')
    # ax.set_xscale('log')
    # ax.set_yscale('log')

    # ax.legend(loc="upper right")
    ax.legend(loc="best")

    print("Plot " + buildBasePath2(testCase, version, subTestCase) + ": OverallRuntime saved to: "
          + buildBasePath2(testCase, version, subTestCase) + "_overallRuntime.png")
    plt.savefig(buildBasePath2(testCase, version, subTestCase) + "_overallRuntime.png")
    if show:
        plt.show()
    plt.close()



def plotOverhead2(testCase, version, subTestCase, show=False):
    bench = parseBenchmark2(testCase, version, subTestCase)
    bench_opt = parseBenchmark2(testCase, version, subTestCase, "opt")
    if bench is None:
        return
    if bench_opt is None:
        return

    overheadL = []
    for i in range(0, len(bench.yValues)):
        arry = []
        x = bench.yValues[i]
        y = bench_opt.yValues[i]
        for j in range(0, len(x)):
            # arry.append((x[j] / y[j] - 1) * 100)
            arry.append(x[j] - y[j])
        # print(arry)
        overheadL.append(arry)

    fig, ax = plt.subplots(figsize=(10, 5))

    for x in range(0, len(overheadL)):
        ax.plot(bench_opt.xValues, overheadL[x], label=bench.curveLabel[x], marker="^")
    ax.set_xlim((1))
    ax.set_ylim((0))

    ax.legend(loc="best")
    ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10)

    ax.set_title(testCase + "/" + version + "/" + subTestCase + "\n" + "Overhead of a Worker dequeuing jobs", fontsize=18)
    # ax.set_xlabel("Number of Operators")
    ax.set_xlabel(bench.xAxis)
    ax.set_ylabel("Overhead in ms")

    print("Plot " + buildBasePath2(testCase, version, subTestCase) + ": Overhead saved to: "
          + buildBasePath2(testCase, version, subTestCase) + "_overhead.png")
    plt.savefig(buildBasePath2(testCase, version, subTestCase) + "_overhead.png")
    if show:
        plt.show()
    plt.close()



def plotOverheadPercentage2(testCase, version, subTestCase, show=False):
    bench = parseBenchmark2(testCase, version, subTestCase)
    bench_opt = parseBenchmark2(testCase, version, subTestCase, "opt")
    if bench is None:
        return
    if bench_opt is None:
        return

    overheadL = []
    for i in range(0, len(bench.yValues)):
        arry = []
        x = bench.yValues[i]
        y = bench_opt.yValues[i]
        for j in range(0, len(x)):
            arry.append((x[j] / y[j] - 1) * 100)
            # arry.append(x[j] - y[j])
        # print(arry)
        overheadL.append(arry)

    fig, ax = plt.subplots(figsize=(10, 5))

    for x in range(0, len(overheadL)):
        ax.plot(bench_opt.xValues, overheadL[x], label=bench.curveLabel[x], marker="^")
    ax.set_xlim((1))
    ax.set_ylim((0))

    ax.legend(loc="best")
    ax.grid(linestyle="--", linewidth=0.5, color='.25', zorder=-10)

    ax.set_title(testCase + "/" + version + "/" + subTestCase + "\n" + "Overhead of a Worker dequeuing jobs (%)", fontsize=18)
    # ax.set_xlabel("Number of Operators")
    ax.set_xlabel(bench.xAxis)
    ax.set_ylabel("Overhead in % \n((Time / Optimum - 1) * 100)")

    print("Plot " + buildBasePath2(testCase, version, subTestCase) + ": Overhead (percentage) saved to: " + buildBasePath(testCase, version, subTestCase) + "_overhead_percent.png")
    plt.savefig(buildBasePath2(testCase, version, subTestCase) + "_overhead_percent.png")
    if show:
        plt.show()
    plt.close()
