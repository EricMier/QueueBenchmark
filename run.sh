#!/bin/bash
host=$(hostname)


#// path to executable
#// release build
exe="build/release/bin/PrototypeEngine"
#// debug build
#exe="build/debug/bin/PrototypeEngine"


#// path to output files (will be generated automatically)
version="v25_groupBySuppkey_fixedMeasurement_unorderedMapInAggMerge"
testcase="queue_with_pointer"
#testcase="queue_with_objects"
path="output/$host/$testcase/$version"

rm -f "$path/runtime.csv"
rm -f "$path/partCount.csv"
rm -f "$path/workerCount.csv"
rm -f "$path/wDequeuingJob.csv"
rm -f "$path/wExecuteJob.csv"
rm -f "$path/wExecuteGlobalJob.csv"
rm -f "$path/cProcessBuffer.csv"
rm -f "$path/cProcessGlobalBuffer.csv"
rm -f "$path/additionalInfo.txt"
rm -f "$path/output.log"

mkdir -p $path
#// run benchmark for range of 1 to 112 partitions
for parts in {1..112}; do
#for parts in {1..448}; do
  echo ./$exe -p $parts -v $version
  ./$exe -p $parts -v $version |
  while read line; do
    echo -E "$line" | sed 's/\x1b\[[0-9;]*m//g' >> "$path/output.log"
    echo "$line"
  done

done


##// run benchmark
#for parts in {1,14,28,42,56,70,84,98,112}; do
##for parts in {1..448}; do
#  for worker in {1..27};do
#    echo ./$exe -p $parts -v $version -w $worker
#    ./$exe -p $parts -v $version -w $worker |
#    while read line; do
#      echo -E "$line" | sed 's/\x1b\[[0-9;]*m//g' >> "$path/output.log"
#      echo "$line"
#    done
#  done
#
#  echo -en "\n" >> "$path/runtime.csv"
#  echo -en "\n" >> "$path/partCount.csv"
#  echo -en "\n" >> "$path/workerCount.csv"
#  echo -en "\n" >> "$path/wDequeuingJob.csv"
#  echo -en "\n" >> "$path/wExecuteJob.csv"
#  echo -en "\n" >> "$path/wExecuteGlobalJob.csv"
#  echo -en "\n" >> "$path/cProcessBuffer.csv"
#  echo -en "\n" >> "$path/cProcessGlobalBuffer.csv"
#done



echo -en "\n" >> "$path/runtime.csv"
echo -en "\n" >> "$path/partCount.csv"
echo -en "\n" >> "$path/workerCount.csv"
echo -en "\n" >> "$path/wDequeuingJob.csv"
echo -en "\n" >> "$path/wExecuteJob.csv"
echo -en "\n" >> "$path/wExecuteGlobalJob.csv"
echo -en "\n" >> "$path/cProcessBuffer.csv"
echo -en "\n" >> "$path/cProcessGlobalBuffer.csv"

#// place for additional information about this benchmark run here:
#additionalInfo="all threads; grouped aggr on lineorder; 6kk tuple; fixed allocation of Jobs to corresponding node; lock partition"
additionalInfo="1 thread per node; grouped aggr on lineorder; 6kk tuple; fixed allocation of Jobs to corresponding node; lock partition"
#additionalInfo="main threads only; grouped aggr on lineorder; 6kk tuple; fixed allocation of Jobs to corresponding node"

echo -e "$additionalInfo" >> "$path/additionalInfo.txt"

echo "Run successfully finished."
